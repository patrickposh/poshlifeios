import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable()
export class ApiConfigService {
    config: any;

    constructor() {
        this.config = environment.server_url;
    }
}