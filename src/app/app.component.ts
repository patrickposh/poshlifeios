import { Component } from '@angular/core';
import { Platform, MenuController, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateProvider } from './providers/translate/translate.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { Pages } from './interfaces/pages';
import { ApiConfigService } from './api.service';
import { EditProfileService } from './pages/edit-profile/edit-profile.service';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['./app.component.scss']
})

export class AppComponent {

	public appPages: Array<Pages>;
	loginFlag: boolean = true;
	userName: any;
	image: any = null;

	constructor(private platform: Platform, private menu: MenuController,
		private splashScreen: SplashScreen, private statusBar: StatusBar, 
		private translateService: TranslateService, public navCtrl: NavController, 
		public router: Router, private translate: TranslateProvider,
		private apiService: ApiConfigService, private toastCtrl: ToastController,
		 private profileService: EditProfileService) {

		var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		if (loginDetails != undefined) {
			this.loginFlag = false;
			this.profileService.getUserDetails(loginDetails.data.userVM.id).subscribe(res => {
				this.userName = res.data.firstname + ' ' + res.data.lastname;
				if (res.data.imageName != null && res.data.imageName != '') {
					this.image = this.apiService.config + 'user/download-image/' + res.data.imageName;
				}
			}, err => {
				if (err.status == 401) {
					this.presentToast("Session Timeout");
					localStorage.removeItem('userDetails');
					localStorage.removeItem('modalShown');
					this.navCtrl.navigateRoot('/login');
					setTimeout(() => {
						window.location.reload();
					}, 300);
				} else {
					this.presentToast(err);
				}
			});
		}

		this.initializeApp();
	}

	login() {
		localStorage.removeItem('userDetails');
		localStorage.removeItem('modalShown');
		this.navCtrl.navigateRoot('/login');
		setTimeout(() => {
			window.location.reload();
		}, 300);
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	initializeApp() {
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			setTimeout(() => {
				this.splashScreen.hide();
			}, 1000);
			// this.splashScreen.hide();
			if (localStorage.getItem('modalShown')) {
				this.router.navigateByUrl('/tabs/(home:home)');
			} else {
				this.router.navigateByUrl('/');
				localStorage.setItem('modalShown', "true");
			}
			// Set language of the app.
			var appLanguage = localStorage.getItem('language');
			if (appLanguage != null) {
				if (appLanguage == 'ar' || appLanguage == 'per') {
					document.documentElement.dir = "rtl";
				}
				this.translateService.use(appLanguage);
				this.translateService.getTranslation(appLanguage).subscribe(translations => {
					this.translate.setTranslations(translations);
				});
			} else {
				this.translateService.setDefaultLang(environment.language);
				this.translateService.use(environment.language);
				this.translateService.getTranslation(environment.language).subscribe(translations => {
					this.translate.setTranslations(translations);
				});
			}

			this.getSideMenuList();
		}).catch(() => {
			// Set language of the app.
			this.translateService.setDefaultLang(environment.language);
			this.translateService.use(environment.language);
			this.translateService.getTranslation(environment.language).subscribe(translations => {
				this.translate.setTranslations(translations);
			});
		});
	}

	getSideMenuList() {
		this.translateService.get(['app.homeScreen', 'app.map', 'app.watched', 'app.getInTouch', 'app.resetPassword', 'app.blogs', 'app.about', 'app.tour']).subscribe(value => {
			this.appPages = [
				{
					title: value['app.homeScreen'],
					url: '/home-results',
					direct: 'root',
					icon: 'home'
				},
				{
					title: value['app.map'],
					url: '/map-view',
					direct: 'forward',
					icon: 'map'
				},
				{
					title: value['app.watched'],
					url: '/favorites',
					direct: 'forward',
					icon: 'star'
				},
				{
					title: value['app.getInTouch'],
					url: '/lead-generation',
					direct: 'forward',
					icon: 'checkbox'
				},
				{
					title: value['app.resetPassword'],
					url: '/resetpwd-view',
					direct: 'forward',
					icon: 'key'
				},
				{
					title: value['app.blogs'],
					url: '/blog',
					direct: 'forward',
					icon: 'list-box'
				},
				{
					title: value['app.about'],
					url: '/about',
					direct: 'forward',
					icon: 'information-circle-outline'
				},
				{
					title: value['app.tour'],
					url: '/',
					direct: 'root',
					icon: 'walk'
				}
			];
		})
	}

	closeMenu() {
		this.menu.close();
	}
}
