import { Component, ViewChild, OnInit } from '@angular/core';
import { NavController, IonItemSliding, LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';
import { FavoritesService } from './favorites.service';

@Component({
	selector: 'app-favorites',
	templateUrl: './favorites.page.html',
	styleUrls: ['./favorites.page.scss'],
	animations: [
		trigger('staggerIn', [
			transition('* => *', [
				query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
				query(':enter', stagger('300ms', [animate('600ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
			])
		])
	]
})

export class FavoritesPage implements OnInit {
	favorites: Array<any>;
	userId: any = null;
	loginFlag: boolean = false;
	isArabic: boolean = false;
	@ViewChild('slidingList', { static: false }) slidingList: IonItemSliding;

	constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,
		private toastCtrl: ToastController, private favoriteSerivce: FavoritesService,
		private translate: TranslateService) {

		var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		var language = localStorage.getItem('language');
		if (language != null && language != undefined) {
			if (language == 'ar') {
				this.isArabic = !this.isArabic;
			}
		}
		if (loginDetails != undefined) {
			this.userId = loginDetails.data.userVM.id;
		} else {
			this.loginFlag = true;
		}
	}

	ngOnInit() {
		if (this.userId != null) {
			this.getFavoriteProperties(this.userId)
		}
	}

	async getFavoriteProperties(userId) {
		var loading: any;
		this.translate.get('app.pleaseWait').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		});
		this.favoriteSerivce.getAllFavoriteProperties(userId).subscribe(async res => {
			await loading.dismiss();
			this.favorites = res.data;
		}, err => {
			loading.dismiss();
			if (err.status == 401) {
				this.presentToast("Session Timeout");
				localStorage.removeItem('userDetails');
				localStorage.removeItem('modalShown');
				this.navCtrl.navigateRoot('/login');
				setTimeout(() => {
					window.location.reload();
				}, 300);
			}
		});
	}

	itemTapped(favorite) {
		this.navCtrl.navigateForward('property-detail/' + favorite.id);
	}

	async removeFavorite(favoriteId) {
		var loading: any;
		this.translate.get('app.pleaseWait').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		});
		this.favoriteSerivce.removeFavoruteProperties(this.userId, favoriteId).subscribe(async res => {
			if (res.code == 200) {
				await loading.dismiss();
				this.presentToast('Property Removed from Favourites')
				this.getFavoriteProperties(this.userId);
			}
		}, err => {
			loading.dismiss();
			console.log(err);
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}
}
