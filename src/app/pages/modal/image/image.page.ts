import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
	selector: 'app-image',
	templateUrl: './image.page.html',
	styleUrls: ['./image.page.scss'],
})

export class ImagePage implements OnInit {
	@Input() data: any;
	virtualTour: any = null;
	images = [];

	constructor(private modalCtrl: ModalController) {}

	ngOnInit() {
		if (this.data.virtualTour != null && this.data.virtualTour != undefined) {
			this.virtualTour = this.data.virtualTour;
		}
		this.images = this.data.images;
	}

	closeModal() {
		this.modalCtrl.dismiss();
	}

}
