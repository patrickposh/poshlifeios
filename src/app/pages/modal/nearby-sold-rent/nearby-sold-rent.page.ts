import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
    selector: 'nearby-sold-rent',
    templateUrl: './nearby-sold-rent.page.html',
    styleUrls: ['./nearby-sold-rent.page.scss'],
})

export class NearbySoldRentPage implements OnInit {
    @Input() data: any;
    @Input() type: any;
    property: any;
    soldProperties: any[] = [];
    leaseProperties: any[] = [];

    constructor(private modalCtrl: ModalController) { }

    ngOnInit() {
        this.property = this.data;
        if (this.type == 'sold') {
            this.soldProperties = this.property.nearby.soldProperties;
        } else {
            this.leaseProperties = this.property.nearby.leaseProperties;
        }
    }

    async openPropertyDetails(data) {
        const onClosedData: string = data;
        await this.modalCtrl.dismiss(onClosedData);
	}

    closeModal() {
        this.modalCtrl.dismiss();
    }
}