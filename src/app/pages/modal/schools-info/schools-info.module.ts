import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { SchoolsInfoPage } from './schools-info.page';
import { StarRatingModule } from 'ionic4-star-rating';

const routes: Routes = [
    {
        path: '',
        component: SchoolsInfoPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        StarRatingModule,
        TranslateModule.forChild(),
        RouterModule.forChild(routes),
    ],
    declarations: [SchoolsInfoPage]
})
export class SchoolsInfoPageModule { }
