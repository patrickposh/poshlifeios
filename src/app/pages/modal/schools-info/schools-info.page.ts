import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as L from "leaflet";

@Component({
    selector: 'app-schools-info',
    templateUrl: './schools-info.page.html',
    styleUrls: ['./schools-info.page.scss'],
})

export class SchoolsInfoPage implements OnInit {
    @Input() data: any;
    school: any;
    map: L.Map;
	marker: any;
	latLong: [];

    constructor(private modalCtrl: ModalController) { }

    ngOnInit() {
        this.school = this.data;
    }

    ionViewDidEnter() {
		this.showMap();
    }
    
    showMap() {
        if(this.school) {
            var lat = this.school.geometry.location.lat;
            var lng = this.school.geometry.location.lng;
            this.map = new L.Map('schoolMap').setView([lat,lng],13);
            this.map.setMinZoom(2);
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            accessToken: 'pk.eyJ1IjoicG9zaGxpZmUiLCJhIjoiY2tkczdwNThsMDV0NzJzcGNmZmtkcGNqdyJ9.YnjXTw7u8tB3TVz-OiWniA'
            }).addTo(this.map);
    
            var greenIcon = L.icon({
                iconUrl: '../../assets/icon/marker-icon.png',
                iconSize: [23, 40],
                iconAnchor: [9, 21],
                popupAnchor: [0, -14]
            });

            var popup = this.school.name +
				'<br/>' + this.school.business_status;
    
            this.marker = L.marker([lat,lng], {icon : greenIcon}).bindPopup(popup, { closeButton : false });
            this.marker.addTo(this.map).openPopup();

        }
	}

    closeModal() {
        this.modalCtrl.dismiss();
    }
}