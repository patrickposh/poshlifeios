import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DataService } from '../../../data.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-search-filter',
	templateUrl: './search-filter.page.html',
	styleUrls: ['./search-filter.page.scss'],
})

export class SearchFilterPage implements OnInit {
	public filterForm: FormGroup;
	currentLat: any = "";
	currentLng: any = "";
	public propertySaleArray: any[] = [];
	public propertySoldArray: any[] = [];
	public minPriceList = [];
	public maxPriceList = [];
	public minSquareFootageList = [];
	public maxSquareFootageList = [];
	public basementArray: any[] = [];
	public propertyTypesArray: any[] = [];
	bedRooms: any[] = [];
	bathRoom: any = '';
	garageParking: any = '';
	openHouse: any = "-1";
	saleOrLease: any = 'Sale';
	propertyType: any = 'residential';
	bedroomAny: boolean = true;
	bedroom1: boolean = false;
	bedroom2: boolean = false;
	bedroom3: boolean = false;
	bedroom4: boolean = false;
	bedroom5: boolean = false;
	bathroomAny: boolean = true;
	bathroom1: boolean = false;
	bathroom2: boolean = false;
	bathroom3: boolean = false;
	bathroom4: boolean = false;
	bathroom5: boolean = false;
	gpAny: boolean = true;
	gp1: boolean = false;
	gp2: boolean = false;
	gp3: boolean = false;
	gp4: boolean = false;
	gp5: boolean = false;
	unspecified: boolean = true;
	today: boolean = false;
	tomorrow: boolean = false;
	sevenDays: boolean = false;
	saleFlag: boolean = true;
	residentialFlag: boolean = true;
	@Input() setFilterObject;
	filterObject: any;

	constructor(private modalCtrl: ModalController, public fb: FormBuilder,
		private data: DataService, private geolocation: Geolocation) {

		this.geolocation.getCurrentPosition().then((resp) => {
			this.currentLat = (resp.coords.latitude).toString();
			this.currentLng = (resp.coords.longitude).toString();
		}, err => {
			this.currentLat = "43.63943345";
			this.currentLng = "-79.39972759226308";
		});

		this.filterObject = {
			address: "",
			approxSquareFootageMax: null,
			approxSquareFootageMin: null,
			basement1: "",
			bedRooms: [],
			currentPage: 1,
			daysOnMarket: "",
			latitude: this.currentLat,
			longitude: this.currentLng,
			listPriceMax: null,
			listPriceMin: null,
			openHouse: this.openHouse,
			parkingSpaces: "",
			propertyOption: "",
			soldDays: "",
			propertyType: this.propertyType,
			saleLease: this.saleOrLease,
			washrooms: "",
			withSold: false
		}
	}

	ngOnInit() {
		if (this.setFilterObject != undefined) {
			this.filterObject = this.setFilterObject;
			this.setFilterObj(this.setFilterObject);
		}
		this.filterForm = this.fb.group({
			propertyOptions: '',
			propertyActiveTime: '',
			soldDays: '',
			basementType: '',
			listPriceMin: null,
			listPriceMax: null,
			approxSquareFootageMin: null,
			approxSquareFootageMax: null

		});

		this.getBasements();
		this.getMinPrices();
		this.getMaxPrices();
		this.getMinSquareFootage();
		this.getMaxSquareFootage();
		this.getPropertyTypes('res');
		this.getSalePropertiesList();
		this.getSoldPropertiesList();
	}

	setFilterObj(obj) {
		this.filterObject.currentPage = obj.currentPage;
		if (obj.saleLease == 'Lease') {
			this.saleFlag = false;
			this.saleOrLease = 'Lease';
		}
		if (obj.propertyType == 'commercial') {
			this.residentialFlag = false;
			this.propertyType = 'commercial';
		}
		if (obj.bedRooms.length != 0) {
			this.bedroomAny = false;
			obj.bedRooms.forEach(element => {
				if (element == '1') {
					this.bedroom1 = true;
				}
				if (element == '2') {
					this.bedroom2 = true;
				}
				if (element == '3') {
					this.bedroom3 = true;
				}
				if (element == '4') {
					this.bedroom4 = true;
				}
				if (element == '5+') {
					this.bedroom5 = true;
				}
			});
		}
		if (obj.parkingSpaces != "") {
			this.gpAny = false;
			if (obj.parkingSpaces == '1') {
				this.gp1 = true;
			}
			if (obj.parkingSpaces == '2') {
				this.gp2 = true;
			}
			if (obj.parkingSpaces == '3') {
				this.gp3 = true;
			}
			if (obj.parkingSpaces == '4') {
				this.gp4 = true;
			}
			if (obj.parkingSpaces == '5') {
				this.gp5 = true;
			}
		}
		if (obj.openHouse != "") {
			this.unspecified = false;
			if (obj.openHouse == "-1") {
				this.unspecified = true;
			}
			if (obj.openHouse == "0") {
				this.today = true;
			}
			if (obj.openHouse == "1") {
				this.tomorrow = true;
			}
			if (obj.openHouse == "7") {
				this.sevenDays = true;
			}
		}
		if (obj.washrooms != "") {
			this.bathroomAny = false;
			if (obj.washrooms == '1') {
				this.bathroom1 = true;
			}
			if (obj.washrooms == '2') {
				this.bathroom2 = true;
			}
			if (obj.washrooms == '3') {
				this.bathroom3 = true;
			}
			if (obj.washrooms == '4') {
				this.bathroom4 = true;
			}
			if (obj.washrooms == '5') {
				this.bathroom5 = true;
			}
		}

		this.filterObject.bedRooms = obj.bedRooms;
		this.bedRooms = obj.bedRooms;
		this.filterObject.washrooms = obj.washrooms;
		this.bathRoom = obj.washrooms;
		this.filterObject.parkingSpaces = obj.parkingSpaces;
		this.garageParking = obj.parkingSpaces;
		this.openHouse = obj.openHouse;

		let self = this;
		setTimeout(() => {
			if (obj.basement1 != '') {
				self.filterForm.controls['basementType'].setValue(obj.basement1);
			}
			if (obj.propertyOption != '') {
				self.filterForm.controls['propertyOptions'].setValue(obj.propertyOption);
			}
			if (obj.daysOnMarket != '') {
				self.filterForm.controls['propertyActiveTime'].setValue(obj.daysOnMarket);
			}
			if (obj.listPriceMin != undefined) {
				self.filterForm.controls['listPriceMin'].setValue(obj.listPriceMin);
			}
			if (obj.listPriceMin != undefined) {
				self.filterForm.controls['listPriceMax'].setValue(obj.listPriceMax);
			}
			if (obj.approxSquareFootageMin != undefined) {
				self.filterForm.controls['approxSquareFootageMin'].setValue(obj.approxSquareFootageMin);
			}
			if (obj.approxSquareFootageMax != undefined) {
				self.filterForm.controls['approxSquareFootageMax'].setValue(obj.approxSquareFootageMax);
			}
			if (obj.soldDays != '') {
				self.filterForm.controls['soldDays'].setValue(obj.soldDays);
			}
		}, 500);
	}

	onChange() {
		this.applyFilter(this.filterForm.value, false);
	}

	bedroom(value) {
		this.bedroomAny = false;
		if (value === 'any') {
			this.bedroom1 = false;
			this.bedroom2 = false;
			this.bedroom3 = false;
			this.bedroom4 = false;
			this.bedroom5 = false;
			this.bedroomAny = true;
			this.bedRooms = [];
		} else if (value === '1') {
			if (this.bedroom1) {
				if (this.bedRooms.includes("1")) {
					const index = this.bedRooms.indexOf('1');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("1");
			}
			if (this.bedroom5 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom1) {
					if (!this.bedroom2 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom1 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom1 = false;
					}
				} else {
					this.bedroom1 = true;
				}
			}
		} else if (value === '2') {
			if (this.bedroom2) {
				if (this.bedRooms.includes("2")) {
					const index = this.bedRooms.indexOf('2');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("2");
			}
			if (this.bedroom1 && this.bedroom5 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom2) {
					if (!this.bedroom1 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom2 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom2 = false;
					}
				} else {
					this.bedroom2 = true;
				}
			}
		} else if (value === '3') {
			if (this.bedroom3) {
				if (this.bedRooms.includes("3")) {
					const index = this.bedRooms.indexOf('3');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("3");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom5 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom3) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom4 && !this.bedroom5) {
						this.bedroom3 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom3 = false;
					}
				} else {
					this.bedroom3 = true;
				}
			}
		} else if (value === '4') {
			if (this.bedroom4) {
				if (this.bedRooms.includes("4")) {
					const index = this.bedRooms.indexOf('4');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("4");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom5) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom4) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom5) {
						this.bedroom4 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom4 = false;
					}
				} else {
					this.bedroom4 = true;
				}
			}
		} else if (value === '5') {
			if (this.bedroom5) {
				if (this.bedRooms.includes("5+")) {
					const index = this.bedRooms.indexOf('5+');
					if (index > -1) {
						this.bedRooms.splice(index, 1);
					}
				}
			} else {
				this.bedRooms.push("5+");
			}
			if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
				this.bedroom1 = false;
				this.bedroom2 = false;
				this.bedroom3 = false;
				this.bedroom4 = false;
				this.bedroom5 = false;
				this.bedroomAny = true;
				this.bedRooms = [];
			} else {
				if (this.bedroom5) {
					if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom4) {
						this.bedroom5 = false;
						this.bedroomAny = true;
					} else {
						this.bedroom5 = false;
					}
				} else {
					this.bedroom5 = true;
				}
			}
		}
		this.applyFilter(this.filterForm.value, false);
	}

	bathroom(value) {
		this.bathroom1 = false;
		this.bathroom2 = false;
		this.bathroom3 = false;
		this.bathroom4 = false;
		this.bathroom5 = false;
		this.bathroomAny = false;
		if (value === 'any') {
			this.bathroomAny = true;
			this.bathRoom = '';
		} else if (value === '1') {
			this.bathroom1 = true;
			this.bathRoom = '1';
		} else if (value === '2') {
			this.bathroom2 = true;
			this.bathRoom = '2';
		} else if (value === '3') {
			this.bathroom3 = true;
			this.bathRoom = '3';
		} else if (value === '4') {
			this.bathroom4 = true;
			this.bathRoom = '4';
		} else if (value === '5') {
			this.bathroom5 = true;
			this.bathRoom = '5';
		}
		this.applyFilter(this.filterForm.value, false);
	}

	gp(value) {
		this.gp1 = false;
		this.gp2 = false;
		this.gp3 = false;
		this.gp4 = false;
		this.gp5 = false;
		this.gpAny = false;
		if (value === 'any') {
			this.gpAny = true;
			this.garageParking = '';
		} else if (value === '1') {
			this.gp1 = true;
			this.garageParking = '1';
		} else if (value === '2') {
			this.gp2 = true;
			this.garageParking = '2';
		} else if (value === '3') {
			this.gp3 = true;
			this.garageParking = '3';
		} else if (value === '4') {
			this.gp4 = true;
			this.garageParking = '4';
		} else if (value === '5') {
			this.gp5 = true;
			this.garageParking = '5';
		}
		this.applyFilter(this.filterForm.value, false)
	}

	setOpenHouse(value) {
		this.unspecified = false;
		this.today = false;
		this.tomorrow = false;
		this.sevenDays = false;
		if (value === "-1") {
			this.unspecified = true;
			this.openHouse = "-1";
		} else if (value === "0") {
			this.today = true;
			this.openHouse = "0";
		} else if (value === "1") {
			this.tomorrow = true;
			this.openHouse = "1";
		} else if (value === "7") {
			this.sevenDays = true;
			this.openHouse = "7";
		}
		this.applyFilter(this.filterForm.value, false)
	}

	saleLease(value) {
		if (value == 'lease') {
			this.saleFlag = false;
			this.saleOrLease = 'Lease';
			this.filterForm.controls['soldDays'].setValue("");
		} else {
			this.saleFlag = true;
			this.saleOrLease = 'Sale';
		}
		this.applyFilter(this.filterForm.value, false);
	}

	propertyTypeChange(value) {
		if (value == 'commercial') {
			this.getPropertyTypes('commer');
			this.residentialFlag = false;
			this.propertyType = 'commercial';
			this.bedRooms = [];
			this.bathRoom = "";
			this.garageParking = "";
			this.openHouse = "";
		} else {
			this.getPropertyTypes('res');
			this.residentialFlag = true;
			this.propertyType = 'residential';
		}
		this.applyFilter(this.filterForm.value, false);
	}

	applyFilter(value, type) {
		var filterObj = {
			address: this.filterObject.address,
			approxSquareFootageMax: value.approxSquareFootageMax,
			approxSquareFootageMin: value.approxSquareFootageMin,
			basement1: value.basementType,
			bedRooms: this.bedRooms,
			currentPage: 1,
			daysOnMarket: value.propertyActiveTime,
			latitude: this.currentLat,
			longitude: this.currentLng,
			listPriceMax: value.listPriceMax,
			listPriceMin: value.listPriceMin,
			openHouse: this.openHouse,
			parkingSpaces: this.garageParking,
			propertyOption: value.propertyOptions,
			soldDays: value.soldDays,
			propertyType: this.propertyType,
			saleLease: this.saleOrLease,
			washrooms: this.bathRoom,
			withSold: false
		}
		if (filterObj.soldDays != "" && filterObj.soldDays != null) {
			filterObj.withSold = true;
		}
		this.filterObject = filterObj;
		if (type) {
			this.closeModal(filterObj)
		}
	}

	async closeModal(data) {
		const onClosedData: string = data;
		await this.modalCtrl.dismiss(onClosedData);
	}

	async dismissModal() {
		await this.modalCtrl.dismiss();
	}

	clearFilter() {
		this.filterObject.withSold = false;
		this.filterObject.address = "";
		setTimeout(() => {
			this.bedRooms = [];
			this.bathRoom = ""
			this.garageParking = "";
			this.openHouse = "-1";
			this.saleOrLease = "Sale";
			this.propertyType = "residential";

			this.filterForm.controls['propertyOptions'].setValue('');
			this.filterForm.controls['propertyActiveTime'].setValue('');
			this.filterForm.controls['soldDays'].setValue('');
			this.filterForm.controls['listPriceMin'].setValue(null);
			this.filterForm.controls['listPriceMax'].setValue(null);
			this.filterForm.controls['approxSquareFootageMin'].setValue(null);
			this.filterForm.controls['approxSquareFootageMax'].setValue(null);
			this.filterForm.controls['basementType'].setValue('');

			this.resetFlags();
		}, 500);
	}

	resetFlags() {
		this.saleFlag = true;
		this.residentialFlag = true;
		this.bedroomAny = true;
		this.bedroom1 = false;
		this.bedroom2 = false;
		this.bedroom3 = false;
		this.bedroom4 = false;
		this.bedroom5 = false;
		this.bathroomAny = true;
		this.bathroom1 = false;
		this.bathroom2 = false;
		this.bathroom3 = false;
		this.bathroom4 = false;
		this.bathroom5 = false;
		this.gpAny = true;
		this.gp1 = false;
		this.gp2 = false;
		this.gp3 = false;
		this.gp4 = false;
		this.gp5 = false;
		this.unspecified = true;
		this.today = false;
		this.tomorrow = false;
		this.sevenDays = false;
	}

	getPropertyTypes(type) {
		this.propertyTypesArray = [];
		if (type == 'res') {
			this.propertyTypesArray = [
				{ id: 1, name: 'Condo Apt' },
				{ id: 2, name: 'Condo Townhouse' },
				{ id: 3, name: 'Detached' },
				{ id: 4, name: 'Semi-Detached' },
				{ id: 5, name: 'Vacant Land' }
			]
		} else {
			this.propertyTypesArray = [
				{ id: 1, name: 'Business' },
				{ id: 2, name: 'Office' },
				{ id: 3, name: 'All' }
			]
		}
	}

	getSalePropertiesList() {
		this.propertySaleArray = this.data.getSalePropertiesList();
	}

	getSoldPropertiesList() {
		this.propertySoldArray = this.data.getSoldPropertiesList();
	}

	getBasements() {
		this.basementArray = this.data.getBasements();
	}

	getMinPrices() {
		this.minPriceList = this.data.getMinPrices(); 
	}

	getMaxPrices() {
		this.maxPriceList = this.data.getMaxPrices();
	}

	getMinSquareFootage() {
		this.minSquareFootageList = this.data.getMinSquareFootage(); 
	}

	getMaxSquareFootage() {
		this.maxSquareFootageList = this.data.getMaxSquareFootage();
	}
}
