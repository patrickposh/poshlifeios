import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core'
import { SearchFilterPage } from './search-filter.page';
import { MatFormFieldModule, MatIconModule, MatAutocompleteModule, MatInputModule, MatExpansionModule, MatSelectModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: SearchFilterPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    IonicModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatInputModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes)
  ],
  declarations: [SearchFilterPage]
})
export class SearchFilterPageModule {}
