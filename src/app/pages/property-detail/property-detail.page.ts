import { Component, Inject, OnInit } from '@angular/core';
import { NavController, ActionSheetController, ModalController, AlertController, LoadingController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ImagePage } from './../modal/image/image.page';
import { trigger, style, animate, transition, query, stagger } from '@angular/animations';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef, MatBottomSheet } from '@angular/material';
import { SchoolsInfoPage } from '../modal/schools-info/schools-info.page';
import { TranslateService } from '@ngx-translate/core';
import { NearbySoldRentPage } from '../modal/nearby-sold-rent/nearby-sold-rent.page';
import { PropertyDetailService } from './property-detail.service';
import * as moment from 'moment';
import * as L from "leaflet";

@Component({
	selector: 'app-property-detail',
	templateUrl: './property-detail.page.html',
	styleUrls: ['./property-detail.page.scss'],
	animations: [
		trigger('staggerIn', [
			transition('* => *', [
				query(':enter', style({ opacity: 0, transform: `translate3d(0,10px,0)` }), { optional: true }),
				query(':enter', stagger('300ms', [animate('600ms', style({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
			])
		])
	]
})

export class PropertyDetailPage implements OnInit {
	propertyID: any;
	userId: any = null;
	property: any;
	propertyopts: String = 'keyFacts';
	public mortgageForm: FormGroup;
	public monthlyPayment: any;
	map: L.Map;
	marker: any;
	latLong: [];
	loginFlag: boolean;
	terminationDate: any;
	loginDetails: any;

	constructor(public asCtrl: ActionSheetController, public navCtrl: NavController,
		public toastCtrl: ToastController, private loadingCtrl: LoadingController,
		public modalCtrl: ModalController, public route: ActivatedRoute,
		public fb: FormBuilder, private detailService: PropertyDetailService,
		public alertCtrl: AlertController, private bottomSheet: MatBottomSheet,
		public router: Router, private translateService: TranslateService) {

		this.propertyID = this.route.snapshot.paramMap.get('id');
		this.getPropertyDetails(this.propertyID);

		this.loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		if (this.loginDetails != undefined) {
			this.loginFlag = false;
			this.userId = this.loginDetails.data.userVM.id;
		}
	}

	ngOnInit() {
		this.mortgageForm = this.fb.group({
			principalAmount: ['', Validators.required],
			downPayment: ['', Validators.required],
			interestRate: ['', Validators.required],
			period: ['', Validators.required]
		});

		this.getRateOfInterest();
	}

	getRateOfInterest() {
		this.detailService.getRateOfInterest().subscribe(res => {
			if (res.code == 200) {
				this.mortgageForm.controls['interestRate'].setValue(res.data.taxValue);
			}
		});
	}

	async getPropertyDetails(propertyId) {
		var loading: any;
		this.translateService.get('app.pleaseWait').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		})
		this.detailService.getPropertyDetails(propertyId).subscribe(async data => {
			await loading.dismiss();
			this.property = data.data[0];
			if (this.property) {
				var roomDetails: any[] = [];
				var roomArr = this.property.roomDetails;
				roomArr.forEach(element => {
					if (element.room != null) {
						roomDetails.push(element);
					}
				});
				this.property.roomDetails = roomDetails;
				if (this.property.saleLease == 'Lease') {
					this.loginFlag = false;
				} else {
					if (this.loginDetails != undefined && this.loginDetails != null) {
						this.loginFlag = false;
					} else {
						this.loginFlag = true;
						this.loginAlert();
					}
				}
				this.mortgageForm.controls['principalAmount'].setValue(this.property.listPrice);
				if(this.property.terminatedDate != null) {
					var tDate = moment(this.property.terminatedDate).format("DD/MM/YYYY");
					var startDate = moment(tDate, "DD/MM/YYYY");
					var currenDate = moment(new Date()).format("DD/MM/YYYY");
					var endDate = moment(currenDate, "DD/MM/YYYY");
					this.terminationDate = endDate.diff(startDate, 'days');
				}
			}
			setTimeout(() => {
				this.showMap();
			}, 300);
			console.log(this.property);
		}, err => {
			loading.dismiss();
		});
	}

	showMap() {
		this.map = new L.Map('propertyMap');
		this.map.setView([this.property.latitude, this.property.longitude], 16);
		this.map.setMinZoom(2);
		L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
			maxZoom: 18,
			id: 'mapbox/streets-v11',
			tileSize: 512,
			zoomOffset: -1,
			accessToken: 'pk.eyJ1IjoicG9zaGxpZmUiLCJhIjoiY2tkczdwNThsMDV0NzJzcGNmZmtkcGNqdyJ9.YnjXTw7u8tB3TVz-OiWniA'
		}).addTo(this.map);

		var propertyMarker: any = L.icon({
			iconUrl: '../../assets/icon/circle.png',
			iconSize: [100, 100],
			popupAnchor: [0, -14]
		});

		var popup = L.popup({ closeOnClick: false }).setContent('Unit '+ this.property.aptUnit + ' - ' + this.property.address);

		this.marker = L.marker([this.property.latitude, this.property.longitude], { icon: propertyMarker }).bindPopup(popup, { closeButton: false });
		this.marker.addTo(this.map).openPopup();
	}

	async loginAlert() {
		const alert = await this.alertCtrl.create({
			cssClass: 'my-custom-class',
			header: 'Alert',
			message: 'Please Sign-in to view this listing',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					cssClass: 'secondary',
					handler: (blah) => {
						this.router.navigateByUrl('/home-results');
					}
				}, {
					text: 'Sign-in',
					handler: () => {
						this.router.navigateByUrl('/login');
					}
				}
			],
			backdropDismiss: false
		});

		await alert.present();
	}

	async presentImage(image: any) {
		const modal = await this.modalCtrl.create({
			component: ImagePage,
			componentProps: {
				data: {
					virtualTour: this.property.virtualTourURL,
					images: image
				}
			}
		});
		return await modal.present();
	}

	async addTofavorite() {
		if (this.userId != null) {
			const loading = await this.loadingCtrl.create({
				message: "Please wait...",
			});
			await loading.present();
			var data = {
				userId: this.userId,
				favoriteId: parseInt(this.propertyID)
			}
			this.detailService.addToFavorite(data).subscribe(async res => {
				await loading.dismiss();
				this.presentToast('Property added to your favorite');
			}, err => {
				loading.dismiss();
				this.presentToast('Something went wrong');
			});
		} else {
			this.presentToast('Please login to add Favorite Property');
		}
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	scheduleVisit(): void {
		this.bottomSheet.open(ScheduleVisitBottom, { data: this.propertyID });
	}

	openPreApprovedMe(): void {
		this.bottomSheet.open(PreApprovedMe, { data: this.propertyID });
	}

	async schoolsInfo(schoolDetails) {
		const modal = await this.modalCtrl.create({
			component: SchoolsInfoPage,
			componentProps: { data: schoolDetails }
		});
		return await modal.present();
	}

	async nearbySoldAndLease(type) {
		const modal = await this.modalCtrl.create({
			component: NearbySoldRentPage,
			componentProps: { data: this.property, type: type }
		});

		modal.onDidDismiss().then((dataReturned) => {
			if(dataReturned != null) {
				if (dataReturned.data != null && dataReturned.data != undefined) {
					this.map.off();
					this.map.remove();
					setTimeout(() => {
						this.router.navigateByUrl('property-detail/' + dataReturned.data);
					}, 500);
				}
			}
		});
		return await modal.present();
	}

	public onMortgageFormSubmit(values: Object) {
		if (this.mortgageForm.valid) {
			var principalAmount = values['principalAmount']
			var down = values['downPayment']
			var interest = values['interestRate']
			var term = values['period']
			this.monthlyPayment = this.calculateMortgage(principalAmount, down, interest / 100 / 12, term * 12).toFixed(2);
		}
	}

	public calculateMortgage(principalAmount: any, downPayment: any, interestRate: any, period: any) {
		return ((principalAmount - downPayment) * interestRate) / (1 - Math.pow(1 + interestRate, -period));
	}

	viewHistoryProperty(id) {
		console.log(id);
		this.map.off();
		// this.map.remove();
		setTimeout(() => {
			this.router.navigate(['property-detail/' + id]);
		}, 500);
	}
}

@Component({
	selector: 'pre-approved-me',
	styleUrls: ['./pre-approved-me.scss'],
	templateUrl: './pre-approved-me.html',
})

export class PreApprovedMe implements OnInit {
	preApprovedMeForm: FormGroup;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
	propertyId: any;

	constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
		private _bottomSheetRef: MatBottomSheetRef<ScheduleVisitBottom>,
		private formBuilder: FormBuilder, private toastCtrl: ToastController,
		private loadingCtrl: LoadingController, private detailService: PropertyDetailService) {

		this._bottomSheetRef.disableClose = false;

		this.propertyId = data;
	}

	ngOnInit() {
		this.preApprovedMeDetailForm();
	}

	preApprovedMeDetailForm() {
		this.preApprovedMeForm = this.formBuilder.group({
			name: '',
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
			contactNumber: '',
			message: '',
			propertyId: this.propertyId
		});
	}

	openLink() {
		this._bottomSheetRef.dismiss();
	}

	async preApprovedMe(value) {
		const loading = await this.loadingCtrl.create({
			message: "Please wait...",
		});
		await loading.present();
		this.detailService.preApproved(value).subscribe(async res => {
			await loading.dismiss();
			if (res.code == 200) {
				this.presentToast('Request Sent Successfully');
			} else {
				this.presentToast('Something went wrong');
			}
			this._bottomSheetRef.dismiss();
		}, err => {
			loading.dismiss();
			console.log(err);
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}
}

@Component({
	selector: 'schedule-visit-bottom',
	styleUrls: ['./schedule-visit-bottom.scss'],
	templateUrl: './schedule-visit-bottom.html',
})

export class ScheduleVisitBottom implements OnInit {
	scheduleVisitForm: FormGroup;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
	propertyId: any;

	constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
		private _bottomSheetRef: MatBottomSheetRef<ScheduleVisitBottom>,
		private formBuilder: FormBuilder, private toastCtrl: ToastController,
		private loadingCtrl: LoadingController, private detailService: PropertyDetailService) {

		this._bottomSheetRef.disableClose = false;

		this.propertyId = data;
	}

	ngOnInit() {
		this.scheduleVisitDetailForm();
	}

	scheduleVisitDetailForm() {
		this.scheduleVisitForm = this.formBuilder.group({
			name: '',
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
			contactNumber: ['', [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
			message: '',
			propertyId: this.propertyId
		});
	}

	openLink() {
		this._bottomSheetRef.dismiss();
	}

	async scheduleVisit(value) {
		const loading = await this.loadingCtrl.create({
			message: "Please wait...",
		});
		await loading.present();
		this.detailService.scheduleVisit(value).subscribe(async res => {
			await loading.dismiss();
			this.presentToast('Request Sent Successfully');
			this._bottomSheetRef.dismiss();
		}, err => {
			loading.dismiss();
			console.log(err);
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}
}
