import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NavController, MenuController, ToastController, LoadingController } from '@ionic/angular';
import { ForgotpwdService } from './forgotpwd.service';

@Component({
	selector: 'app-forgotpwd',
	templateUrl: './forgotpwd.page.html',
	styleUrls: ['./forgotpwd.page.scss'],
})

export class ForgotpwdPage implements OnInit {

	public forgotPwdForm: FormGroup;
	emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';

	constructor(public navCtrl: NavController, public menuCtrl: MenuController,
		public toastCtrl: ToastController, private service: ForgotpwdService,
		public loadingCtrl: LoadingController, private formBuilder: FormBuilder) { }

	ngOnInit() {
		this.forgotFormDetails();
	}

	ionViewWillEnter() {
		this.menuCtrl.enable(false);
		this.menuCtrl.swipeEnable(false);
	}

	forgotFormDetails() {
		this.forgotPwdForm = this.formBuilder.group({
			email: new FormControl('', [Validators.required, Validators.pattern(this.emailReg)]),
		});
	}

	async forgotPassword(value) {
		const loading = await this.loadingCtrl.create({
			message: "Loading...",
		});
		await loading.present();
		this.service.forgotPassword(value).subscribe(async res => {
			await loading.dismiss();
			if (res.code == 200) {
				this.presentToast(res.data);
				this.navCtrl.navigateRoot('/login');
			} else {
				this.presentToast(res.data);
			}
		}, err => {
			loading.dismiss();
		})
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});

		toast.present();
	}

	login() {
		this.navCtrl.navigateRoot('/login');
	}
}
