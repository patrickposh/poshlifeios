import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NavController, LoadingController, ToastController, ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { EditProfileService } from './edit-profile.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import { ApiConfigService } from '../../api.service';

@Component({
	selector: 'app-edit-profile',
	templateUrl: './edit-profile.page.html',
	styleUrls: ['./edit-profile.page.scss'],
})

export class EditProfilePage implements OnInit, AfterViewInit {

	profileForm: FormGroup;
	userId: any = null;
	image: any = null;
	profileChange: boolean = false;

	constructor(public navCtrl: NavController, public loadingCtrl: LoadingController,
		public toastCtrl: ToastController, public profileService: EditProfileService,
		private formBuilder: FormBuilder, private translate: TranslateService,
		private camera: Camera, public actionSheetCtrl: ActionSheetController,
		private apiService: ApiConfigService, private router: Router) {

		var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
		if (loginDetails != undefined) {
			this.userId = loginDetails.data.userVM.id;
		}
	}

	ngOnInit() {
		this.profileDetailForm();
	}

	ngAfterViewInit() {
		setTimeout(() => {
			if (this.userId != null) {
				this.getUserDetails(this.userId);
			}
		}, 500);
	}

	profileDetailForm() {
		this.profileForm = this.formBuilder.group({
			id: this.userId,
			firstname: '',
			lastname: '',
			email: new FormControl({ value: '', disabled: true }),
			phoneNumber: '',
			createdDate: "",
			modifiedDate: new Date(),
			imageName: null,
			imageUrl: null
		});
	}

	async presentActionSheet() {
		this.translate.get(['profile.selectImageSource', 'profile.loadFromLibraty', 'profile.useCamera', 'app.cancel']).subscribe(async value => {
			let actionSheet = await this.actionSheetCtrl.create({
				header: value['profile.selectImageSource'],
				buttons: [
					{
						text: value['profile.loadFromLibraty'],
						handler: () => {
							this.imageUpload(this.camera.PictureSourceType.PHOTOLIBRARY);
						}
					},
					{
						text: value['profile.useCamera'],
						handler: () => {
							this.imageUpload(this.camera.PictureSourceType.CAMERA);
						}
					},
					{
						text: value['app.cancel'],
						role: 'cancel'
					}
				]
			});
			await actionSheet.present();
		});
	}

	async imageUpload(sourceType) {
		const loading = await this.loadingCtrl.create({
			message: "Loading...",
		});
		await loading.present();

		const options: CameraOptions = {
			quality: 50,
			destinationType: this.camera.DestinationType.DATA_URL,
			saveToPhotoAlbum: false,
			correctOrientation: true,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			sourceType: sourceType,
			// destinationType: this.camera.DestinationType.FILE_URI,
		}

		this.profileChange = true;

		this.camera.getPicture(options).then((imageData) => {
			loading.dismiss();
			this.image = imageData;
			console.log("sd", this.image);
			this.image = 'data:image/jpeg;base64,' + imageData;
		}, (err) => {
			loading.dismiss();
			this.presentToast(err);
		});
	}

	async getUserDetails(userId) {
		var loading: any;
		this.translate.get('loading').subscribe(async value => {
			loading = await this.loadingCtrl.create({
				message: value,
			});
			await loading.present();
		});
		this.profileService.getUserDetails(userId).subscribe(async res => {
			await loading.dismiss();
			if (res.code == 200) {
				this.profileForm.controls['firstname'].setValue(res.data.firstname);
				this.profileForm.controls['lastname'].setValue(res.data.lastname);
				this.profileForm.controls['email'].setValue(res.data.email);
				this.profileForm.controls['phoneNumber'].setValue(res.data.phoneNumber);
				this.profileForm.controls['imageName'].setValue(res.data.imageName);
				this.profileForm.controls['imageUrl'].setValue(res.data.imageUrl);
				if (res.data.imageName != null && res.data.imageName != '') {
					this.image = this.apiService.config + 'user/download-image/' + res.data.imageName;
				}
			} else {
				this.presentToast("Something Went Wrong");
			}
		}, err => {
			loading.dismiss();
			if (err.status == 401) {
				this.presentToast("Session Timeout");
				localStorage.removeItem('userDetails');
				localStorage.removeItem('modalShown');
				this.navCtrl.navigateRoot('/login');
				setTimeout(() => {
					window.location.reload();
				}, 300);
			} else {
				this.presentToast(err);
			}
		});
	}

	async blobToFile(url,value) {
		console.log('url',url);
		fetch(url)
			.then(res => res.blob())
			.then(async blob => {
				const file = new File([blob], 'File name');
				console.log("file", file);
				const loading = await this.loadingCtrl.create({
					message: this.translate.instant("Uploading..."),
				});
				await loading.present();
				this.profileService.uploadUserPicturemobile(file).subscribe(async data => {
					console.log('data...!!', data);
					await loading.dismiss();
					if (data.fileName && data.fileDownloadUri) {
						value.imageName = data.fileName;
						value.imageUrl = data.fileDownloadUri;
					}
					this.updateProfileData(value);
				}, err => {
					console.log('error', err);
					this.presentToast(err.error.message);
				})
			})
			
	}

	async updateProfile(value) {
		this.blobToFile(this.image,value);
	}

	async updateProfileData(data) {
		const loading = await this.loadingCtrl.create({
			message: "Loading...",
		});
		await loading.present();
		this.profileService.updateUserDetails(data).subscribe(async res => {
			await loading.dismiss();
			if (res.code == 200) {
				this.presentToast('Profile Updated Successfully');
			} else {
				this.presentToast('Something went wrong');
			}
			setTimeout(() => {
				this.router.navigateByUrl('/home-results');
			}, 150);
		}, err => {
			this.presentToast('Something went wrong');
			loading.dismiss();
		});
	}

	async presentToast(message) {
		const toast = await this.toastCtrl.create({
			showCloseButton: true,
			message: message,
			duration: 2000,
			position: 'bottom'
		});
		toast.present();
	}
}