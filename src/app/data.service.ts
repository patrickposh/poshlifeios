import { Injectable, OnInit } from '@angular/core';

@Injectable()
export class DataService implements OnInit {

    public propertySaleArray: any[] = [];
    public propertySoldArray: any[] = [];
    public basementArray: any[] = [];
    public minPriceList: any[] = [];
    public maxPriceList: any[] = [];
    public minSquareFootageList: any[] = [];
    public maxSquareFootageList: any[] = [];

    constructor() { }

    ngOnInit() {
        this.getSalePropertiesList();
        this.getSoldPropertiesList();
        this.getBasements();
        this.getMinPrices();
        this.getMaxPrices();
        this.getMinSquareFootage();
        this.getMaxSquareFootage();
    }

    getSalePropertiesList() {
        return this.propertySaleArray = [
            { id: 1, name: '1 days', value: '1' },
            { id: 2, name: '7 days', value: '7' },
            { id: 3, name: '30 days', value: '30' },
            { id: 4, name: '60 days', value: '60' },
            { id: 5, name: '90 days', value: '90' },
            { id: 5, name: 'Any', value: '-1' }
        ];
    }

    getSoldPropertiesList() {
        return this.propertySoldArray = [
            { id: 1, name: '1 days', value: '1' },
            { id: 2, name: '7 days', value: '7' },
            { id: 3, name: '30 days', value: '30' },
            { id: 4, name: '60 days', value: '60' },
            { id: 5, name: '90 days', value: '90' },
            { id: 5, name: 'Any', value: '-1' }
        ];
    }

    getBasements() {
        return this.basementArray = [
            { id: 1, name: 'None' },
            { id: 2, name: 'Other' },
            { id: 3, name: 'Unfinished' },
            { id: 4, name: 'W/O' },
            { id: 5, name: 'Fin W/O' },
            { id: 6, name: 'Finished' },
            { id: 7, name: 'Apartment' },
            { id: 8, name: 'Part Fin' },
            { id: 9, name: 'Full' },
            { id: 10, name: 'Crawl Space' },
            { id: 11, name: 'Half' },
            { id: 12, name: 'Part Bsmt' },
            { id: 13, name: 'Walk-Up' },
            { id: 14, name: 'Sep Entrance' }
        ];
    }

    getMinPrices() {
        return this.minPriceList = [
            { price: '$ 0', value: "0" },
            { price: '$ 50,000', value: "50000" },
            { price: '$ 100,000', value: "100000" },
            { price: '$ 150,000', value: "150000" },
            { price: '$ 200,000', value: "200000" },
            { price: '$ 250,000', value: "250000" },
            { price: '$ 300,000', value: "300000" },
            { price: '$ 350,000', value: "350000" },
            { price: '$ 400,000', value: "400000" },
            { price: '$ 500,000', value: "500000" },
            { price: '$ 600,000', value: "600000" },
            { price: '$ 700,000', value: "700000" },
            { price: '$ 800,000', value: "800000" },
            { price: '$ 900,000', value: "900000" },
            { price: '$ 1,000,000', value: "1000000" },
            { price: '$ 5,000,000', value: "5000000" },
            { price: '$ 10,000,000', value: "10000000" }
        ];
    }

    getMaxPrices() {
        return this.maxPriceList = [
            { price: '$ 50,000', value: "50000" },
            { price: '$ 100,000', value: "100000" },
            { price: '$ 150,000', value: "150000" },
            { price: '$ 200,000', value: "200000" },
            { price: '$ 250,000', value: "250000" },
            { price: '$ 300,000', value: "300000" },
            { price: '$ 350,000', value: "350000" },
            { price: '$ 400,000', value: "400000" },
            { price: '$ 500,000', value: "500000" },
            { price: '$ 600,000', value: "600000" },
            { price: '$ 700,000', value: "700000" },
            { price: '$ 800,000', value: "800000" },
            { price: '$ 900,000', value: "900000" },
            { price: '$ 1,000,000', value: "1000000" },
            { price: '$ 5,000,000', value: "5000000" },
            { price: '$ 10,000,000', value: "10000000" },
            { price: 'Unlimited', value: "" }
        ];
    }

    getMinSquareFootage() {
        return this.minSquareFootageList = [
            { footage: '0 feet²', value: "0" },
            { footage: '500 feet²', value: "500" },
            { footage: '1,000 feet²', value: "1000" },
            { footage: '1,500 feet²', value: "1500" },
            { footage: '2,000 feet²', value: "2000" },
            { footage: '2,500 feet²', value: "2500" },
            { footage: '3,000 feet²', value: "3000" },
            { footage: '3,500 feet²', value: "3500" },
            { footage: '4,000 feet²', value: "4000" },
            { footage: '4,500 feet²', value: "4500" },
            { footage: '5,000 feet²', value: "5000" }
        ];
    }

    getMaxSquareFootage() {
        return this.maxSquareFootageList = [
            { footage: '500 feet²', value: "500" },
            { footage: '1,000 feet²', value: "1000" },
            { footage: '1,500 feet²', value: "1500" },
            { footage: '2,000 feet²', value: "2000" },
            { footage: '2,500 feet²', value: "2500" },
            { footage: '3,000 feet²', value: "3000" },
            { footage: '3,500 feet²', value: "3500" },
            { footage: '4,000 feet²', value: "4000" },
            { footage: '4,500 feet²', value: "4500" },
            { footage: '5,000 feet²', value: "5000" },
            { footage: 'Unlimited', value: "" }
        ];
    }
}