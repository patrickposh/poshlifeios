import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
	{ path: '', loadChildren: () => import('./pages/walkthrough/walkthrough.module').then(m => m.WalkthroughPageModule) },
	{ path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule) },
	{ path: 'register', loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule) },
	{ path: 'about', loadChildren: () => import('./pages/about/about.module').then(m => m.AboutPageModule) },
	{ path: 'support', loadChildren: () => import('./pages/support/support.module').then(m => m.SupportPageModule) },
	{ path: 'settings', loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule) },
	{ path: 'edit-profile', loadChildren: () => import('./pages/edit-profile/edit-profile.module').then(m => m.EditProfilePageModule) },
	{ path: 'home-results', loadChildren: () => import('./pages/home-results/home-results.module').then(m => m.HomeResultsPageModule) },
	{ path: 'search-filter', loadChildren: () => import('./pages/modal/search-filter/search-filter.module').then(m => m.SearchFilterPageModule) },
	{ path: 'property-detail/:id', loadChildren: () => import('./pages/property-detail/property-detail.module').then(m => m.PropertyDetailPageModule) },
	{ path: 'lead-generation', loadChildren: () => import('./pages/lead-generation/lead-generation.module').then(m => m.LeadGenerationPageModule) },
	{ path: 'blog', loadChildren: () => import('./pages/blog/blog.module').then(m => m.BlogPageModule) },
	{ path: 'blog-info', loadChildren: () => import('./pages/modal/blog-info/blog-info.module').then(m => m.BlogInfoPageModule) },
	{ path: 'nearby-sold', loadChildren: () => import('./pages/modal/nearby-sold-rent/nearby-sold-rent.module').then(m => m.NearbySoldRentModule) },
	{ path: 'schools-info', loadChildren: () => import('./pages/modal/schools-info/schools-info.module').then(m => m.SchoolsInfoPageModule) },
	{ path: 'favorites', loadChildren: () => import('./pages/favorites/favorites.module').then(m => m.FavoritesPageModule) },
	{ path: 'map-view', loadChildren: () => import('./pages/map-view/map-view.module').then(m => m.MapViewPageModule) },
	{ path: 'resetpwd-view', loadChildren: () => import('./pages/resetpwd/resetpwd.module').then(m => m.ResetpwdPageModule) },
	{ path: 'forgotpwd-view', loadChildren: () => import('./pages/forgotpwd/forgotpwd.module').then(m => m.ForgotpwdPageModule) },
	{ path: 'notification', loadChildren: () => import('./pages/notification/notification.module').then(m => m.NotificationPageModule) },
	{ path: '**', redirectTo: '/home-results' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
