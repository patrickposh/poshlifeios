function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-register-register-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesRegisterRegisterPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content class=\"ion-padding animated fadeIn login\">\n\n\t<div class=\"animated fadeInDown ion-text-center ion-padding-horizontal\">\n\t\t<div class=\"login-logo\"></div>\n\t</div>\n\n\t<!-- Register form -->\n\t<div class=\"title\">{{ 'app.createAccount' | translate }}</div>\n\n\t<form id=\"register\" [formGroup]=\"registerForm\" (ngSubmit)=\"register(registerForm.value)\">\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.firstName' | translate }}</mat-label>\n\t\t\t<input matInput autocomplete=\"off\" required formControlName=\"firstname\">\n\t\t\t<mat-icon matSuffix>account_circle</mat-icon>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.lastName' | translate }}</mat-label>\n\t\t\t<input matInput autocomplete=\"off\" required formControlName=\"lastname\">\n\t\t\t<mat-icon matSuffix>account_circle</mat-icon>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.email' | translate }}</mat-label>\n\t\t\t<input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n\t\t\t<mat-icon matSuffix>mail_outline</mat-icon>\n\t\t\t<mat-error *ngIf=\"registerForm.get('email').hasError('required')\">\n\t\t\t\t{{ 'register.emailRequired' | translate }}\n\t\t\t</mat-error>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.password' | translate }}</mat-label>\n\t\t\t<input matInput type=\"password\" autocomplete=\"off\" required formControlName=\"password\">\n\t\t\t<mat-icon matSuffix>vpn_key</mat-icon>\n\t\t\t<mat-error>{{ 'register.passwordRequired' | translate }}</mat-error>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.confirmPassword' | translate }}</mat-label>\n\t\t\t<input matInput type=\"password\" autocomplete=\"off\" required formControlName=\"confirmPassword\">\n\t\t\t<mat-icon matSuffix>vpn_key</mat-icon>\n\t\t\t<mat-error *ngIf=\"registerForm.get('confirmPassword').hasError('required')\">\n\t\t\t\t{{ 'register.confirmPasswordRequired' | translate }}\n\t\t\t</mat-error>\n\t\t\t<mat-error *ngIf=\"!registerForm.get('confirmPassword').hasError('required') &&\n\t\t\t\t\t\t\t   registerForm.get('confirmPassword').hasError('passwordsNotMatching')\">\n\t\t\t\t{{ 'register.passwordMatch' | translate }}\n\t\t\t</mat-error>\n\t\t</mat-form-field>\n\n\t\t<ion-button type=\"submit\" class=\"submit-btn\" color=\"danger\" [disabled]=\"registerForm.invalid\">\n\t\t\t{{ 'app.createAccount' | translate }}\n\t\t</ion-button>\n\n\t\t<div class=\"or-format\">-- {{ 'register.or' | translate }} --</div>\n\n\t\t<ion-button type=\"button\" class=\"google-btn\">\n\t\t\t{{ 'register.signUpGoogle' | translate }}\n\t\t</ion-button>\n\n\t\t<ion-button type=\"button\" class=\"fb-btn\" (click)=\"signUpWithFacebok()\">\n\t\t\t{{ 'register.signUpFacebook' | translate }}\n\t\t</ion-button>\n\n\t\t<div class=\"already-account\">\n\t\t\t<span>{{ 'register.alreadyAccount' | translate }}</span>\n\t\t\t<a class=\"loginLink\" (click)=\"login()\">{{ 'register.login' | translate }}</a>\n\t\t</div>\n\t</form>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/register/register.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/register/register.module.ts ***!
    \***************************************************/

  /*! exports provided: RegisterPageModule */

  /***/
  function srcAppPagesRegisterRegisterModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function () {
      return RegisterPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _register_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./register.page */
    "./src/app/pages/register/register.page.ts");
    /* harmony import */


    var _register_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./register.service */
    "./src/app/pages/register/register.service.ts");
    /* harmony import */


    var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! @ionic-native/facebook/ngx */
    "./node_modules/@ionic-native/facebook/ngx/index.js");

    var routes = [{
      path: '',
      component: _register_page__WEBPACK_IMPORTED_MODULE_10__["RegisterPage"]
    }];

    var RegisterPageModule = function RegisterPageModule() {
      _classCallCheck(this, RegisterPageModule);
    };

    RegisterPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateModule"].forChild(), _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes)],
      declarations: [_register_page__WEBPACK_IMPORTED_MODULE_10__["RegisterPage"]],
      providers: [_register_service__WEBPACK_IMPORTED_MODULE_11__["RegisterService"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_12__["Facebook"]]
    })], RegisterPageModule);
    /***/
  },

  /***/
  "./src/app/pages/register/register.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/pages/register/register.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesRegisterRegisterPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  text-align: center !important;\n  padding: 24px !important;\n}\n:host form {\n  width: 100%;\n  padding-top: 32px;\n  font-size: 14px;\n}\n.login-logo {\n  height: 120px;\n  width: 128px;\n  margin: 32px auto;\n  background: url(\"/assets/img/logo.png\") no-repeat;\n  background-size: 100%;\n}\n.title {\n  font-size: 21px;\n  text-align: center;\n  font-weight: 400 !important;\n}\n.fieldFormat {\n  width: 90%;\n}\n.loginLink {\n  font-size: 13px;\n  font-weight: 500;\n  margin-bottom: 5px;\n  color: #039be5;\n}\n.submit-btn {\n  width: 80%;\n}\n.fb-btn {\n  text-transform: inherit;\n  --background: #3f5c9a;\n  font-size: 13px;\n  margin: auto;\n  margin-bottom: 15px;\n  display: flex;\n  width: 70%;\n  letter-spacing: 0px;\n}\n.google-btn {\n  text-transform: inherit;\n  --background: #d73d32;\n  font-size: 13px;\n  margin: auto;\n  margin-bottom: 7px;\n  display: flex;\n  width: 70%;\n  letter-spacing: 0px;\n}\n.or-format {\n  color: #888;\n  font-weight: 500;\n  margin: 10px;\n}\n.already-account {\n  flex-direction: column;\n  box-sizing: border-box;\n  display: flex;\n  place-content: center;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvcmVnaXN0ZXIvcmVnaXN0ZXIucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9yZWdpc3Rlci9yZWdpc3Rlci5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSw2QkFBQTtFQUNBLHdCQUFBO0FDQVI7QURHSTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNEUjtBREtBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGlEQUFBO0VBQ0EscUJBQUE7QUNGSjtBREtBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7QUNGSjtBREtBO0VBQ0ksVUFBQTtBQ0ZKO0FES0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNGSjtBREtBO0VBQ0ksVUFBQTtBQ0ZKO0FES0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUNGSjtBREtBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FDRko7QURLQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNGSjtBREtBO0VBQ0ksc0JBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0FDRkoiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9yZWdpc3Rlci9yZWdpc3Rlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgICAgICAgcGFkZGluZzogMjRweCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIGZvcm0ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgcGFkZGluZy10b3A6IDMycHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG59XG5cbi5sb2dpbi1sb2dvIHtcbiAgICBoZWlnaHQ6IDEyMHB4O1xuICAgIHdpZHRoOiAxMjhweDtcbiAgICBtYXJnaW46IDMycHggYXV0bztcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9sb2dvLnBuZ1wiKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xufVxuXG4udGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMjFweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbn1cblxuLmZpZWxkRm9ybWF0IHtcbiAgICB3aWR0aDogOTAlO1xufVxuXG4ubG9naW5MaW5rIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgY29sb3I6ICMwMzliZTU7XG59XG5cbi5zdWJtaXQtYnRuIHtcbiAgICB3aWR0aDogODAlO1xufVxuXG4uZmItYnRuIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogaW5oZXJpdDtcbiAgICAtLWJhY2tncm91bmQ6ICMzZjVjOWE7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xufVxuXG4uZ29vZ2xlLWJ0biB7XG4gICAgdGV4dC10cmFuc2Zvcm06IGluaGVyaXQ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZDczZDMyO1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBtYXJnaW46IGF1dG87XG4gICAgbWFyZ2luLWJvdHRvbTogN3B4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBsZXR0ZXItc3BhY2luZzogMHB4O1xufVxuXG4ub3ItZm9ybWF0IHtcbiAgICBjb2xvcjogIzg4ODtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIG1hcmdpbjogMTBweDtcbn1cblxuLmFscmVhZHktYWNjb3VudCB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcGxhY2UtY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMjRweCAhaW1wb3J0YW50O1xufVxuOmhvc3QgZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogMzJweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4ubG9naW4tbG9nbyB7XG4gIGhlaWdodDogMTIwcHg7XG4gIHdpZHRoOiAxMjhweDtcbiAgbWFyZ2luOiAzMnB4IGF1dG87XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2xvZ28ucG5nXCIpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xufVxuXG4udGl0bGUge1xuICBmb250LXNpemU6IDIxcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xufVxuXG4uZmllbGRGb3JtYXQge1xuICB3aWR0aDogOTAlO1xufVxuXG4ubG9naW5MaW5rIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIGNvbG9yOiAjMDM5YmU1O1xufVxuXG4uc3VibWl0LWJ0biB7XG4gIHdpZHRoOiA4MCU7XG59XG5cbi5mYi1idG4ge1xuICB0ZXh0LXRyYW5zZm9ybTogaW5oZXJpdDtcbiAgLS1iYWNrZ3JvdW5kOiAjM2Y1YzlhO1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcbiAgZGlzcGxheTogZmxleDtcbiAgd2lkdGg6IDcwJTtcbiAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbn1cblxuLmdvb2dsZS1idG4ge1xuICB0ZXh0LXRyYW5zZm9ybTogaW5oZXJpdDtcbiAgLS1iYWNrZ3JvdW5kOiAjZDczZDMyO1xuICBmb250LXNpemU6IDEzcHg7XG4gIG1hcmdpbjogYXV0bztcbiAgbWFyZ2luLWJvdHRvbTogN3B4O1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogNzAlO1xuICBsZXR0ZXItc3BhY2luZzogMHB4O1xufVxuXG4ub3ItZm9ybWF0IHtcbiAgY29sb3I6ICM4ODg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbjogMTBweDtcbn1cblxuLmFscmVhZHktYWNjb3VudCB7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBsYWNlLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/register/register.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/register/register.page.ts ***!
    \*************************************************/

  /*! exports provided: RegisterPage, confirmPasswordValidator */

  /***/
  function srcAppPagesRegisterRegisterPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterPage", function () {
      return RegisterPage;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "confirmPasswordValidator", function () {
      return confirmPasswordValidator;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _register_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./register.service */
    "./src/app/pages/register/register.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ionic-native/facebook/ngx */
    "./node_modules/@ionic-native/facebook/ngx/index.js");

    var RegisterPage = /*#__PURE__*/function () {
      function RegisterPage(navCtrl, menuCtrl, loadingCtrl, toastCtrl, registerService, formBuilder, facebook) {
        _classCallCheck(this, RegisterPage);

        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.registerService = registerService;
        this.formBuilder = formBuilder;
        this.facebook = facebook;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        this._unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_5__["Subject"]();
      }

      _createClass(RegisterPage, [{
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.menuCtrl.enable(false);
          this.menuCtrl.swipeEnable(false);
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {
          this.registerFormDetails();
        }
      }, {
        key: "registerFormDetails",
        value: function registerFormDetails() {
          var _this = this;

          this.registerForm = this.formBuilder.group({
            firstname: '',
            lastname: '',
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.emailReg)]),
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            confirmPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, confirmPasswordValidator]]
          });
          this.registerForm.get('password').valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["takeUntil"])(this._unsubscribeAll)).subscribe(function () {
            _this.registerForm.get('confirmPassword').updateValueAndValidity();
          });
        }
      }, {
        key: "register",
        value: function register(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingCtrl.create({
                      message: "Loading..."
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                    this.registerService.register(value).subscribe(function (data) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                _context.next = 2;
                                return loading.dismiss();

                              case 2:
                                if (data.code == 200) {
                                  this.presentToast(data.message);
                                  this.navCtrl.navigateRoot('/login');
                                } else {
                                  this.presentToast(data.message);
                                }

                              case 3:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee, this);
                      }));
                    }, function (err) {
                      loading.dismiss();

                      _this2.presentToast(err);
                    });

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "signUpWithFacebok",
        value: function signUpWithFacebok() {
          var _this3 = this;

          var userData = null;
          ;
          this.facebook.login(['email', 'public_profile']).then(function (response) {
            _this3.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(function (profile) {
              userData = {
                email: profile['email'],
                first_name: profile['first_name'],
                picture: profile['picture_large']['data']['url'],
                username: profile['name']
              };
              console.log("d", userData);
            });
          });
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "login",
        value: function login() {
          this.navCtrl.navigateRoot('/login');
        }
      }]);

      return RegisterPage;
    }();

    RegisterPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _register_service__WEBPACK_IMPORTED_MODULE_4__["RegisterService"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_7__["Facebook"]
      }];
    };

    RegisterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-register',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./register.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/register/register.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./register.page.scss */
      "./src/app/pages/register/register.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _register_service__WEBPACK_IMPORTED_MODULE_4__["RegisterService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_7__["Facebook"]])], RegisterPage);

    var confirmPasswordValidator = function confirmPasswordValidator(control) {
      if (!control.parent || !control) {
        return null;
      }

      var password = control.parent.get('password');
      var confirmPassword = control.parent.get('confirmPassword');

      if (!password || !confirmPassword) {
        return null;
      }

      if (confirmPassword.value === '') {
        return null;
      }

      if (password.value === confirmPassword.value) {
        return null;
      }

      return {
        passwordsNotMatching: true
      };
    };
    /***/

  },

  /***/
  "./src/app/pages/register/register.service.ts":
  /*!****************************************************!*\
    !*** ./src/app/pages/register/register.service.ts ***!
    \****************************************************/

  /*! exports provided: RegisterService */

  /***/
  function srcAppPagesRegisterRegisterServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterService", function () {
      return RegisterService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../api.service */
    "./src/app/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var RegisterService = /*#__PURE__*/function () {
      function RegisterService(http, apiService) {
        _classCallCheck(this, RegisterService);

        this.http = http;
        this.apiService = apiService;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
          headers: headers
        });
      }

      _createClass(RegisterService, [{
        key: "register",
        value: function register(data) {
          return this.http.post(this.apiService.config + 'create', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }]);

      return RegisterService;
    }();

    RegisterService.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }, {
        type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]
      }];
    };

    RegisterService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])], RegisterService);
    /***/
  }
}]);
//# sourceMappingURL=pages-register-register-module-es5.js.map