function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-property-detail-property-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/pre-approved-me.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/pre-approved-me.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesPropertyDetailPreApprovedMeHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<form [formGroup]=\"preApprovedMeForm\" (ngSubmit)=\"preApprovedMe(preApprovedMeForm.value)\">\n    <mat-accordion multi=\"true\">\n        <mat-expansion-panel disabled=\"true\" expanded>\n            <mat-expansion-panel-header class=\"expansion-header\">\n                <mat-panel-title color=\"dark\" class=\"panelTitle\">\n                    Pre Approved Me\n                </mat-panel-title>\n                <ion-buttons slot=\"end\">\n                    <ion-button size=\"small\" shape=\"round\" color=\"dark\" (click)=\"openLink()\">\n                        <ion-icon name=\"close\"></ion-icon>\n                    </ion-button>\n                </ion-buttons>\n            </mat-expansion-panel-header>\n\n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>Your Name</mat-label>\n                <input matInput autocomplete=\"off\" required formControlName=\"name\">\n                <mat-icon matSuffix>account_circle</mat-icon>\n            </mat-form-field>\n\n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>Contact Number</mat-label>\n                <input matInput autocomplete=\"off\" required formControlName=\"contactNumber\">\n                <mat-icon matSuffix>phone</mat-icon>\n            </mat-form-field>\n            \n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>Email</mat-label>\n                <input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n                <mat-icon matSuffix>mail_outline</mat-icon>\n            </mat-form-field>\n\n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>Your Message</mat-label>\n                <textarea matInput formControlName=\"message\"></textarea>\n            </mat-form-field>\n\n            <ion-button type=\"submit\" class=\"submit-btn\" color=\"danger\" [disabled]=\"preApprovedMeForm.invalid\">\n                SUBMIT\n            </ion-button>\n\n        </mat-expansion-panel>\n    </mat-accordion>\n</form>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/property-detail.page.html":
  /*!*******************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/property-detail.page.html ***!
    \*******************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesPropertyDetailPropertyDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header [ngClass]=\"{'login-required': loginFlag === true}\">\n\t<ion-toolbar class=\"toolbar-format\">\n\t\t<ion-buttons slot=\"start\" style=\"margin-top: 5px;\">\n\t\t\t<ion-back-button text=\"\" style=\"color: #fff;\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<div style=\"padding: 0px 10px;\">\n\t\t\t<img class=\"logo-image\" src=\"assets/img/poshlife.png\">\n\t\t</div>\n\t\t<!-- <ion-buttons slot=\"end\">\n\t\t\t<ion-button size=\"small\" shape=\"round\" (click)=\"share()\">\n\t\t\t\t<ion-icon name=\"share\" style=\"color: #fff;\"></ion-icon>\n\t\t\t</ion-button>\n\t\t</ion-buttons> -->\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content [ngClass]=\"{'login-required': loginFlag === true}\">\n\t<div style=\"background: #efefef;\" *ngIf=\"property\">\n\t\t<div class=\"photo\" [style.background-image]=\"'url('+property.propertyImage[0]+')'\">\n\t\t\t<div class=\"photo-button\" (click)=\"presentImage(property.propertyImage)\">\n\t\t\t\t<p class=\"view-image\">{{ 'property.details.viewImages' | translate }} ({{ property.propertyImage.length }})</p>\n\t\t\t</div>\n\t\t\t<div class=\"meta\">\n\t\t\t\t<p class=\"property-title\">\n\t\t\t\t\t<span *ngIf=\"property.aptUnit\">Unit {{ property.aptUnit }} - </span>\n\t\t\t\t\t{{ property.address }}\n\t\t\t\t</p>\n\t\t\t\t<p class=\"property-title\">{{ property.community }} - {{ property.municipality }}</p>\n\t\t\t</div>\n\t\t</div>\n\n\t\t<ion-card class=\"bg-white\">\n\t\t\t<div class=\"listed-format\">\n\t\t\t\t<p class=\"p-listing\">\n\t\t\t\t\t<span style=\"margin-right: 5PX;\">{{ 'property.listed' | translate }}:</span>\n\t\t\t\t\t<!-- CHECK PROPERTY TERMINATED OR NOT-->\n\t\t\t\t\t<span *ngIf=\"property.event !== 'Terminated'\">\n\t\t\t\t\t\t<span *ngIf=\"property.saleLease == 'Sale'\"\n\t\t\t\t\t\t\t[ngStyle]=\"property.soldPrice ? {'text-decoration': 'line-through'} : {'text-decoration': 'none'}\"\n\t\t\t\t\t\t\tstyle=\"color: #ff5b29;\">\n\t\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US'}}\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span *ngIf=\"property.saleLease == 'Lease'\" style=\"color: #558b2f;\">\n\t\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US'}} /month\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</span>\n\t\t\t\t\t<!-- CHECK PROPERTY TERMINATED OR NOT-->\n\t\t\t\t\t<span *ngIf=\"property.event === 'Terminated'\">\n\t\t\t\t\t\t<span *ngIf=\"property.saleLease == 'Sale'\"\n\t\t\t\t\t\t\t[ngStyle]=\"property.event ? {'text-decoration': 'line-through'} : {'text-decoration': 'none'}\">\n\t\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US'}}\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span *ngIf=\"property.saleLease == 'Lease'\"\n\t\t\t\t\t\t[ngStyle]=\"property.event ? {'text-decoration': 'line-through'} : {'text-decoration': 'none'}\">\n\t\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US'}} /month\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<p class=\"p-termination\">Terminated {{ terminationDate }} days ago</p>\n\t\t\t\t\t</span>\n\t\t\t\t</p>\n\t\t\t\t<span *ngIf=\"property.event !== 'Terminated'\">\n\t\t\t\t\t<p class=\"saleClass\"\n\t\t\t\t\t\t*ngIf=\"property.soldPrice === 0 && property.saleLease === 'Sale'\">\n\t\t\t\t\t\t{{ property.saleLease }}\n\t\t\t\t\t</p>\n\t\t\t\t\t<p class=\"leaseClass\"\n\t\t\t\t\t\t*ngIf=\"property.soldPrice === 0 && property.saleLease === 'Lease'\">\n\t\t\t\t\t\t{{ property.saleLease }}\n\t\t\t\t\t</p>\n\t\t\t\t\t<p class=\"soldClass\"\n\t\t\t\t\t\t*ngIf=\"property.soldPrice !== 0\">\n\t\t\t\t\t\t{{ 'property.sold' | translate }}\n\t\t\t\t\t</p>\n\t\t\t\t</span>\n\t\t\t</div>\n\t\t\t<div class=\"listed-format\">\n\t\t\t\t<p class=\"p-type\">{{ property.type }}</p>\n\t\t\t\t<p class=\"p-listing-date\">\n\t\t\t\t\t{{ 'property.listedIn' | translate }} {{ property.listingEntryDate | date}}\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div *ngIf=\"property.soldPrice\" class=\"listed-format\">\n\t\t\t\t<p class=\"p-estimate\">\n\t\t\t\t\t<span style=\"margin-right: 5PX;\">{{ 'property.soldFor' | translate }}:</span>\n\t\t\t\t\t<span style=\"color: #ff5b29;\">${{ property.soldPrice | number:'1.0':'en-US'}}</span>\n\t\t\t\t</p>\n\t\t\t\t<p class=\"p-listing-date\">\n\t\t\t\t\t{{ 'property.soldDate' | translate }} {{ property.soldDate | date }}\n\t\t\t\t</p>\n\t\t\t</div>\n\n\t\t\t<hr style=\"background: #eee;\">\n\n\t\t\t<div class=\"ion-margin-top\">\n\t\t\t\t<div *ngIf=\"property.propertyType === 'residential'\" style=\"display: flex;\">\n\t\t\t\t\t<div class=\"dd\">\n\t\t\t\t\t\t<img style=\"width: 36px;\" src=\"assets/img/bedrooms.svg\">\n\t\t\t\t\t\t<p *ngIf=\"!property.bedroomsPlus\">\n\t\t\t\t\t\t\t{{ property.bedrooms }} {{ 'property.bedrooms' | translate }}\n\t\t\t\t\t\t</p>\n\t\t\t\t\t\t<p *ngIf=\"property.bedroomsPlus\">\n\t\t\t\t\t\t\t{{ property.bedrooms }} + {{ property.bedroomsPlus }} {{ 'property.bedrooms' | translate }}\n\t\t\t\t\t\t</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"dd\">\n\t\t\t\t\t\t<img style=\"width: 36px;\" src=\"assets/img/bathrooms.svg\">\n\t\t\t\t\t\t<p>{{ property.washrooms }} {{ 'property.bathroom' | translate }}</p>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"dd\">\n\t\t\t\t\t\t<img style=\"width: 36px;\" src=\"assets/img/parking.svg\">\n\t\t\t\t\t\t<p>{{ property.garageSpaces }} {{ 'property.parking' | translate }}</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<hr style=\"background: #eee;\">\n\t\t\t<div class=\"listed-format\">\n\t\t\t\t<p class=\"p-listing\">{{ 'property.history' | translate }}</p>\n\t\t\t\t<p>{{'property.buyOrSaleHistory' | translate }} \n\t\t\t\t\t<span *ngIf=\"property.aptUnit\">Unit {{ property.aptUnit }} - </span>\n\t\t\t\t\t{{ property.address }}, {{ property.municipality }} ({{property.type}})</p>\n\t\t\t</div>\n\t\t\t<div *ngFor=\"let history of property.listingHistory\">\n\t\t\t\t<div class=\"property-history\">\n\t\t\t\t\t<p class=\"history-price\">\n\t\t\t\t\t\t<span *ngIf=\"property.soldPrice === 0\" class=\"leasePrice\">\n\t\t\t\t\t\t\t${{ history.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span *ngIf=\"property.soldPrice !== 0\" class=\"leasePrice\">\n\t\t\t\t\t\t\t${{ history.soldPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span class=\"saleLeaseBadge\">{{ history.event }}</span>\n\t\t\t\t\t</p>\n\t\t\t\t\t<ion-button *ngIf=\"history.mls !== property.mls\"\n\t\t\t\t\t\ttype=\"button\" size=\"small\"\n\t\t\t\t\t\tcolor=\"danger\" class=\"btn-view\" \n\t\t\t\t\t\t(click)=\"viewHistoryProperty(history.id)\">\n\t\t\t\t\t\tView\n\t\t\t\t\t</ion-button>\n\t\t\t\t\t<p *ngIf=\"history.event !== 'Terminated'\" class=\"p-list-date\">\n\t\t\t\t\t\tList: {{ history.listingEntryDate | date }} | End: {{ history.expiryDate | date }}\n\t\t\t\t\t</p>\n\t\t\t\t\t<p *ngIf=\"history.event === 'Terminated'\" class=\"p-list-date\">\n\t\t\t\t\t\tList: {{ history.listingEntryDate | date:'yyyy-MM-dd' }} | End: {{ history.terminatedDate | date:'yyyy-MM-dd' }}\n\t\t\t\t\t</p>\n\t\t\t\t\t<p class=\"p-mls\">{{ history.mls }}</p>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"bg-white\">\n\t\t\t<div class=\"section-nearby\">\n\t\t\t\t<ion-button class=\"nearby-btn\" type=\"button\" \n\t\t\t\t\tsize=\"medium\" \n\t\t\t\t\tcolor=\"danger\"\n\t\t\t\t\t(click)=\"nearbySoldAndLease('sold')\">\n\t\t\t\t\t{{ 'property.nearbySold' | translate }}\n\t\t\t\t</ion-button>\n\t\t\t\t<ion-button class=\"nearby-btn\" type=\"button\" \n\t\t\t\t\tsize=\"medium\" \n\t\t\t\t\tcolor=\"danger\"\n\t\t\t\t\t(click)=\"nearbySoldAndLease('lease')\">\n\t\t\t\t\t{{ 'property.nearbyLease' | translate }}\n\t\t\t\t</ion-button>\n\t\t\t</div>\n\t\t\t<div id=\"propertyMap\" style=\"width: 100%; height: 260px;\"></div>\n\t\t\t\n\t\t\t<ion-card-content>\n\t\t\t\t<h3 class=\"ion-text-center\">\n\t\t\t\t\t<ion-text color=\"dark\">\n\t\t\t\t\t\t{{property.address}} • {{property.streetName}} - {{property.municipality}}\n\t\t\t\t\t</ion-text>\n\t\t\t\t</h3>\n\t\t\t</ion-card-content>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"bg-white\">\n\t\t\t<ion-segment color=\"danger\" [(ngModel)]=\"propertyopts\">\n\t\t\t\t<ion-segment-button value=\"keyFacts\">\n\t\t\t\t\t{{ 'property.keyFacts' | translate }}\n\t\t\t\t</ion-segment-button>\n\t\t\t\t<ion-segment-button value=\"details\">\n\t\t\t\t\t{{ 'property.details' | translate }}\n\t\t\t\t</ion-segment-button>\n\t\t\t\t<ion-segment-button value=\"room\">\n\t\t\t\t\t{{ 'property.rooms' | translate }}\n\t\t\t\t</ion-segment-button>\n\t\t\t</ion-segment>\n\n\t\t\t<div [ngSwitch]=\"propertyopts\">\n\t\t\t\t<div *ngSwitchCase=\"'keyFacts'\">\n\t\t\t\t\t<ion-grid style=\"padding: 10px; color: #333;\">\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.tax' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.taxes\" size=\"6\" align=\"right\">${{ property.taxes | number:'1.0':'en-US' }} /-</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.taxes\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.type' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.type\" size=\"6\" align=\"right\">{{ property.type }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.type\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.propertyStatus' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.lastStatus\" size=\"6\" align=\"right\">{{ property.lastStatus }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.lastStatus\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.listing' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.mls\" size=\"6\" align=\"right\">{{ property.mls }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.mls\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.dateListed' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.listingEntryDate\" size=\"6\" align=\"right\">{{ property.listingEntryDate | date}}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.listingEntryDate\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.daysOnMarket' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.daysOnMarket\" size=\"6\" align=\"right\">{{ property.daysOnMarket }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.daysOnMarket\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.buildingAge' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.approxAge\" size=\"6\" align=\"right\">{{ property.approxAge }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.approxAge\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.yearBuilt' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.contractDate\" size=\"6\" align=\"right\">{{ property.contractDate | date }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.contractDate\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.basement' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.basement1\" size=\"6\" align=\"right\">{{ property.basement1 }}, {{property.basement2 }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.basement1\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.size' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.approxSquareFootage\" size=\"6\" align=\"right\">{{ property.approxSquareFootage }} feet²</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.approxSquareFootage\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.updatedOn' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.updatedtimestamp\" size=\"6\" align=\"right\">{{ property.updatedtimestamp | date }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.updatedtimestamp\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t</ion-grid>\n\t\t\t\t</div>\n\n\t\t\t\t<div *ngSwitchCase=\"'details'\">\n\t\t\t\t\t<ion-grid style=\"padding: 10px; color: #333;\">\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.address' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.address\" size=\"6\" align=\"right\">{{ property.address}}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.address\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"5\">{{ 'property.details.intersection' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.directionsCrossStreets\" size=\"7\" align=\"right\">{{ property.directionsCrossStreets}}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.directionsCrossStreets\" size=\"7\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.balcony' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.balcony\" size=\"6\" align=\"right\">{{ property.balcony }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.balcony\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.garageType' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.garageType\" size=\"6\" align=\"right\">{{ property.garageType }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.garageType\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.waterSupply' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.laundryAccess' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.laundryAccess\" size=\"6\" align=\"right\">{{ property.laundryAccess }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.laundryAccess\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.heatSource' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.heatSource\" size=\"6\" align=\"right\">{{ property.heatSource }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.heatSource\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.firePlace' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.fireplaceStove\" size=\"6\" align=\"right\">{{ property.fireplaceStove }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.fireplaceStove\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.sewer' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.sewer\" size=\"6\" align=\"right\">{{ property.sewer }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.sewer\" size=\"6\" align=\"right\">{{ property.sewer }}</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.zoning' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.zoning\" size=\"6\" align=\"right\">{{ property.zoning }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.zoning\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.construction' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.construction\" size=\"6\" align=\"right\">{{ property.construction}}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.construction\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.municipality' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.municipality\" size=\"6\" align=\"right\">{{ property.municipality }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.municipality\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.postalCode' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.postalCode\" size=\"6\" align=\"right\">{{ property.postalCode }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.postalCode\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.province' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.province\" size=\"6\" align=\"right\">{{ property.province }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.province\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"6\">{{ 'property.details.community' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.community\" size=\"6\" align=\"right\">{{ property.community }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.community\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"12\">{{ 'property.details.description' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.remarksForClients\" size=\"12\" style=\"line-height: 22px;\">{{ property.remarksForClients }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.remarksForClients\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t<hr style=\"background: #ccc;\">\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col size=\"12\">{{ 'property.details.extras' | translate }}:</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"property.extras\" size=\"12\" style=\"line-height: 22px;\">{{ property.extras }}</ion-col>\n\t\t\t\t\t\t\t<ion-col *ngIf=\"!property.extras\" size=\"6\" align=\"right\">--</ion-col>\n\t\t\t\t\t\t</ion-row>\n\n\t\t\t\t\t</ion-grid>\n\t\t\t\t</div>\n\n\t\t\t\t<div *ngSwitchCase=\"'room'\" class=\"bg-white\">\n\t\t\t\t\t<div *ngFor=\"let room of property?.roomDetails\" class=\"row-room\">\n\t\t\t\t\t\t<span style=\"font-size: 15px;\">{{ room.room }}</span>\n\t\t\t\t\t\t<span style=\"flex: 1; margin-left: 4px; font-size: 15px;\"> \n\t\t\t\t\t\t\t({{ room.roomLength }} x {{ room.roomWidth }} m)\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span style=\"font-size: 15px; color: #333;\">\n\t\t\t\t\t\t\t{{ 'property.rooms.level' | translate }}: {{ room.level.value }}\n\t\t\t\t\t\t</span>\n\t\t\t\t\t\t<span *ngIf=\"room.roomDesc1\" class=\"room-desc\">\n\t\t\t\t\t\t\t{{ room.roomDesc1 }}, \n\t\t\t\t\t\t\t<span *ngIf=\"room.roomDesc2\">\n\t\t\t\t\t\t\t\t{{ room.roomDesc2 }},\n\t\t\t\t\t\t\t</span> \n\t\t\t\t\t\t\t{{ room.roomDesc3 }}\n\t\t\t\t\t\t</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"bg-white\">\n\t\t\t<mat-accordion multi=\"true\">\n                <mat-expansion-panel>\n                    <mat-expansion-panel-header class=\"expansion-header\">\n                        <mat-panel-title color=\"dark\">\n                            {{ 'property.features' | translate }}\n                        </mat-panel-title>\n\t\t\t\t\t</mat-expansion-panel-header>\n\t\t\t\t\t<ion-item *ngFor=\"let feature of property?.features\">\n\t\t\t\t\t\t<ion-label style=\"font-size: 14px;\">{{ feature }}</ion-label>\n\t\t\t\t\t\t<ion-checkbox slot=\"end\" checked=\"true\"></ion-checkbox>\n\t\t\t\t\t</ion-item>\n\t\t\t\t</mat-expansion-panel>\n\t\t\t</mat-accordion>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"bg-white\">\n\t\t\t<mat-accordion multi=\"true\">\n                <mat-expansion-panel>\n                    <mat-expansion-panel-header class=\"expansion-header\">\n                        <mat-panel-title color=\"dark\">\n                            {{ 'property.schoolsInfo' | translate }}\n                        </mat-panel-title>\n\t\t\t\t\t</mat-expansion-panel-header>\n\n\t\t\t\t\t<div *ngFor=\"let school of property?.schools.results\">\n\t\t\t\t\t\t<div class=\"section-format\">\n\t\t\t\t\t\t\t<div class=\"section\">\n\t\t\t\t\t\t\t\t<p class=\"school-name\">{{ school.name }}</p>\n\t\t\t\t\t\t\t\t<p class=\"rating\">\n\t\t\t\t\t\t\t\t\t<span class=\"actual-rating\">{{ school.rating }}</span> / 5\n\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div>\n\t\t\t\t\t\t\t\t<div *ngFor=\"let type of school.types\" class=\"chip-span\">{{ type }}</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<p (click)=\"schoolsInfo(school)\" class=\"p-mr\">\n\t\t\t\t\t\t\t\t<span class=\"viewDetail\">{{ 'property.moreDetails' | translate }}</span>\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div align=\"center\" *ngIf=\"property?.schools.results.length == 0\">\n\t\t\t\t\t\t<h5 class=\"fw400\">{{ 'property.noNearbySchools' | translate }}</h5>\n\t\t\t\t\t</div>\n\t\t\t\t</mat-expansion-panel>\n\t\t\t</mat-accordion>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"bg-white\" *ngIf=\"property.saleLease === 'Sale'\">\n\t\t\t<form [formGroup]=\"mortgageForm\" (ngSubmit)=\"onMortgageFormSubmit(mortgageForm.value)\">\n\t\t\t\t<mat-accordion multi=\"true\">\n\t\t\t\t\t<mat-expansion-panel>\n\t\t\t\t\t\t<mat-expansion-panel-header class=\"expansion-header\">\n\t\t\t\t\t\t\t<mat-panel-title color=\"dark\">\n\t\t\t\t\t\t\t\t{{ 'property.mortgageCalculator' | translate }}\n\t\t\t\t\t\t\t</mat-panel-title>\n\t\t\t\t\t\t</mat-expansion-panel-header>\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"w100\">\n\t\t\t\t\t\t\t<mat-label>\n\t\t\t\t\t\t\t\t{{ 'mortgage.principalAmount' | translate }}\n\t\t\t\t\t\t\t</mat-label>\n\t\t\t\t\t\t\t<input matInput placeholder=\"$\" autocomplete=\"off\" formControlName=\"principalAmount\" type=\"number\">\n\t\t\t\t\t\t</mat-form-field>\n\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"w100\">\n\t\t\t\t\t\t\t<mat-label>\n\t\t\t\t\t\t\t\t{{ 'mortgage.downPayment' | translate }}\n\t\t\t\t\t\t\t</mat-label>\n\t\t\t\t\t\t\t<input matInput placeholder=\"$\" autocomplete=\"off\" formControlName=\"downPayment\" type=\"number\">\n\t\t\t\t\t\t</mat-form-field>\n\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"w100\">\n\t\t\t\t\t\t\t<mat-label>\n\t\t\t\t\t\t\t\t{{ 'mortgage.interestRate' | translate }}\n\t\t\t\t\t\t\t</mat-label>\n\t\t\t\t\t\t\t<input matInput autocomplete=\"off\" formControlName=\"interestRate\" type=\"number\">\n\t\t\t\t\t\t\t<span matSuffix>%</span>\n\t\t\t\t\t\t</mat-form-field>\n\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"w100\">\n\t\t\t\t\t\t\t<mat-label>\n\t\t\t\t\t\t\t\t{{ 'mortgage.period' | translate }}\n\t\t\t\t\t\t\t</mat-label>\n\t\t\t\t\t\t\t<input matInput placeholder=\"Number of Years\" autocomplete=\"off\" formControlName=\"period\" type=\"number\">                  \n\t\t\t\t\t\t</mat-form-field>\n\n\t\t\t\t\t\t<p *ngIf=\"monthlyPayment\">{{ 'mortgage.monthlyPayment' | translate }}: <span style=\"margin-left: 10px;\">${{monthlyPayment}}</span></p>\n\t\t\t\t\t\t<div align=\"center\">\n\t\t\t\t\t\t\t<ion-button class=\"mort-btn\" size=\"mediun\" type=\"button\" color=\"secondary\" (click)=\"openPreApprovedMe()\">\n\t\t\t\t\t\t\t\t{{ 'mortgage.preApprovedMe' | translate }}\n\t\t\t\t\t\t\t</ion-button>\n\t\t\t\t\t\t\t<ion-button class=\"mort-btn\" size=\"mediun\" type=\"submit\" color=\"danger\">\n\t\t\t\t\t\t\t\t{{ 'mortgage.calculateMortgate' | translate }}\n\t\t\t\t\t\t\t</ion-button>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</mat-expansion-panel>\n\t\t\t\t</mat-accordion>\n\t\t\t</form>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"bg-white\">\n\t\t\t<subscribe-newsletter></subscribe-newsletter>\n\t\t</ion-card>\n\t</div>\n</ion-content>\n\n<ion-footer class=\"animated fadeIn\" [ngClass]=\"{'login-required': loginFlag === true}\">\n\t<ion-row>\n\t\t<ion-col size=\"6\" align=\"center\" class=\"pd-0\" (click)=\"addTofavorite()\">\n\t\t\t<ion-button expand=\"full\" color=\"danger\" class=\"add-fav-margin\">\n\t\t\t\t<mat-icon style=\"margin-right: 5px;\">star_border</mat-icon>\n\t\t\t\t{{ 'property.addWatched' | translate }}\n\t\t\t</ion-button>\n\t\t</ion-col>\n\t\t<ion-col size=\"6\" align=\"center\" class=\"pd-0\">\n\t\t\t<ion-button expand=\"full\" color=\"dark\" class=\"schedule-margin\" (click)=\"scheduleVisit()\">\n\t\t\t\t<ion-icon name=\"today\" style=\"margin-right: 5px;\"></ion-icon>\n\t\t\t\t{{ 'property.scheduleVisit' | translate }}\n\t\t\t</ion-button>\n\t\t</ion-col>\n\t</ion-row>\n</ion-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/schedule-visit-bottom.html":
  /*!********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/schedule-visit-bottom.html ***!
    \********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesPropertyDetailScheduleVisitBottomHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<form [formGroup]=\"scheduleVisitForm\" (ngSubmit)=\"scheduleVisit(scheduleVisitForm.value)\">\n    <mat-accordion multi=\"true\">\n        <mat-expansion-panel disabled=\"true\" expanded>\n            <mat-expansion-panel-header class=\"expansion-header\">\n                <mat-panel-title color=\"dark\" class=\"panelTitle\">\n                    {{ 'property.scheduleVisit' | translate }}\n                </mat-panel-title>\n                <ion-buttons slot=\"end\">\n                    <ion-button size=\"small\" shape=\"round\" color=\"dark\" (click)=\"openLink()\">\n                        <ion-icon name=\"close\"></ion-icon>\n                    </ion-button>\n                </ion-buttons>\n            </mat-expansion-panel-header>\n\n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>{{ 'scheduleVisit.yourName' | translate }}</mat-label>\n                <input matInput autocomplete=\"off\" required formControlName=\"name\">\n                <mat-icon matSuffix>account_circle</mat-icon>\n            </mat-form-field>\n\n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>{{ 'scheduleVisit.contactNumber' | translate }}</mat-label>\n                <input matInput autocomplete=\"off\" required formControlName=\"contactNumber\">\n                <mat-icon matSuffix>phone</mat-icon>\n            </mat-form-field>\n            \n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>{{ 'register.email' | translate }}</mat-label>\n                <input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n                <mat-icon matSuffix>mail_outline</mat-icon>\n            </mat-form-field>\n\n            <mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n                <mat-label>{{ 'scheduleVisit.yourMessage' | translate }}</mat-label>\n                <textarea matInput formControlName=\"message\"></textarea>\n            </mat-form-field>\n\n            <ion-button type=\"submit\" class=\"submit-btn\" color=\"danger\" [disabled]=\"scheduleVisitForm.invalid\">\n                {{ 'scheduleVisit.submit' | translate }}\n            </ion-button>\n\n        </mat-expansion-panel>\n    </mat-accordion>\n</form>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.html":
  /*!*********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.html ***!
    \*********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesPropertyDetailSubscribeNewsletterSubscribeNewsletterPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-accordion multi=\"true\">\n    <mat-expansion-panel>\n        <mat-expansion-panel-header class=\"expansion-header\">\n            <mat-panel-title color=\"dark\" class=\"panelTitle\">\n                {{ 'property.dailyProperties' | translate }}\n            </mat-panel-title>\n        </mat-expansion-panel-header>\n            \n        <form [formGroup]=\"leadGenerationForm\">\n\n            <div class=\"row-div\" style=\"margin-bottom: 5px;\">\n                <ion-row class=\"room-row\">\n                    <ion-col size=\"3\" [ngClass]=\"{'activeFilter' : residentialFlag, 'room-col' : !residentialFlag}\" (click)=\"propertyTypeChange('residential')\">\n                        {{ 'property.residential' | translate }}\n                    </ion-col>\n                    <ion-col size=\"3\" [ngClass]=\"{'activeFilter' : !residentialFlag, 'room-col' : residentialFlag}\" (click)=\"propertyTypeChange('commercial')\">\n                        {{ 'property.commercial' | translate }}\n                    </ion-col>\n                    <ion-col size=\"3\" [ngClass]=\"{'activeFilter' : saleFlag, 'room-col' : !saleFlag}\" (click)=\"saleLease('sale')\">\n                        {{ 'property.sale' | translate }}\n                    </ion-col>\n                    <ion-col size=\"3\" [ngClass]=\"{'activeFilter' : !saleFlag, 'room-col' : saleFlag}\" (click)=\"saleLease('lease')\">\n                        {{ 'property.lease' | translate }}\n                    </ion-col>\n                </ion-row>\n            </div>\n\n            <mat-form-field appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'subscribe.name' | translate }}</mat-label>\n                <input matInput autocomplete=\"off\" required formControlName=\"name\">\n                <mat-icon matSuffix>account_circle</mat-icon>\n            </mat-form-field>\n            \n            <mat-form-field appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'register.email' | translate }}</mat-label>\n                <input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n                <mat-icon matSuffix>mail_outline</mat-icon>\n                <mat-error *ngIf=\"leadGenerationForm.get('email').hasError('required')\">\n                    {{ 'register.emailRequired' | translate }}\n                </mat-error>\n            </mat-form-field>\n\n            <mat-form-field appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'subscribe.addressPostalMLS' | translate }}</mat-label>\n                <input matInput autocomplete=\"off\" formControlName=\"address\">\n                <mat-icon matSuffix>edit</mat-icon>\n            </mat-form-field>\n\n            <mat-form-field appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'subscribe.areaRadius' | translate }}</mat-label>\n                <input matInput type=\"number\" autocomplete=\"off\" formControlName=\"areaRadius\">\n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'subscribe.phone' | translate }}</mat-label>\n                <input matInput autocomplete=\"off\" formControlName=\"phone\">\n                <mat-icon matSuffix>phone</mat-icon>\n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'property.propertyTypes' | translate }}</mat-label> \n                <mat-select placeholder=\"Select Property Type\" formControlName=\"propertyOptions\">\n                    <mat-option *ngFor=\"let propertyType of propertyTypesArray\" \n                        [value]=\"propertyType.name\">\n                        {{ propertyType.name }}\n                    </mat-option>\n                </mat-select>\n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" style=\"padding-right: 5px;\" class=\"fieldformat\">\n                <mat-label>{{ 'property.minPrice' | translate }}</mat-label>\n                <mat-select placeholder=\"$\" formControlName=\"listPriceMin\" >\n                    <mat-option *ngFor=\"let price of minPriceList\" \n                        [value]=\"price.value\">\n                        {{ price.price }}\n                    </mat-option>\n                </mat-select> \n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" class=\"fieldformat\">\n                <mat-label>{{ 'property.maxPrice' | translate }}</mat-label>\n                <mat-select placeholder=\"$\" formControlName=\"listPriceMax\" >\n                    <mat-option *ngFor=\"let price of maxPriceList\" \n                        [value]=\"price.value\">\n                        {{ price.price }}\n                    </mat-option>\n                </mat-select> \n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" style=\"padding-right: 5px;\" class=\"fieldformat\">\n                <mat-label>{{ 'property.minFootage' | translate }}</mat-label>\n                <mat-select formControlName=\"approxSquareFootageMin\" >\n                    <mat-option *ngFor=\"let footage of minSquareFootageList\" \n                        [value]=\"footage.value\">\n                        {{ footage.footage }}\n                    </mat-option>\n                </mat-select> \n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" class=\"fieldformat\">\n                <mat-label>{{ 'property.maxFootage' | translate }}</mat-label>\n                <mat-select formControlName=\"approxSquareFootageMax\" >\n                    <mat-option *ngFor=\"let footage of maxSquareFootageList\" \n                        [value]=\"footage.value\">\n                        {{ footage.footage }}\n                    </mat-option>\n                </mat-select> \n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag && residentialFlag\" appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'property.details.basement' | translate }}</mat-label>\n                <mat-select placeholder=\"Select ...\" formControlName=\"basementStyle\" >\n                    <mat-option *ngFor=\"let basement of basementArray\" \n                        [value]=\"basement.name\">\n                        {{ basement.name }}\n                    </mat-option>\n                </mat-select> \n            </mat-form-field>\n\n            <mat-form-field *ngIf=\"!detailFormFlag\" appearance=\"outline\" class=\"w100\">\n                <mat-label>{{ 'subscribe.message' | translate }}</mat-label>\n                <textarea matInput autocomplete=\"off\" formControlName=\"message\"></textarea>\n            </mat-form-field>\n    \n        </form>\n\n        <div *ngIf=\"!detailFormFlag\">\n            <div *ngIf=\"residentialFlag\">\n                <div class=\"row-div\">\n                    <span style=\"font-size: 14px;\">{{ 'property.bedrooms' | translate}}</span>\n                    <ion-row class=\"room-row\">\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bedroomAny, 'room-col' : !bedroomAny}\" (click)=\"bedroom('any')\">\n                            {{ 'property.any' | translate }}\n                        </ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bedroom1, 'room-col' : !bedroom1}\" (click)=\"bedroom('1')\">1</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bedroom2, 'room-col' : !bedroom2}\" (click)=\"bedroom('2')\">2</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bedroom3, 'room-col' : !bedroom3}\" (click)=\"bedroom('3')\">3</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bedroom4, 'room-col' : !bedroom4}\" (click)=\"bedroom('4')\">4</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bedroom5, 'room-col' : !bedroom5}\" (click)=\"bedroom('5')\">5+</ion-col>\n                    </ion-row>\n                </div>\n                \n                <div class=\"row-div\">\n                    <span style=\"font-size: 14px;\">{{ 'property.bathroom' | translate }}</span>\n                    <ion-row class=\"room-row\">\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bathroomAny, 'room-col' : !bathroomAny}\" (click)=\"bathroom('any')\">\n                            {{ 'property.any' | translate }}\n                        </ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bathroom1, 'room-col' : !bathroom1}\" (click)=\"bathroom('1')\">1+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bathroom2, 'room-col' : !bathroom2}\" (click)=\"bathroom('2')\">2+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bathroom3, 'room-col' : !bathroom3}\" (click)=\"bathroom('3')\">3+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bathroom4, 'room-col' : !bathroom4}\" (click)=\"bathroom('4')\">4+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : bathroom5, 'room-col' : !bathroom5}\" (click)=\"bathroom('5')\">5+</ion-col>\n                    </ion-row>\n                </div>\n                \n                <div class=\"row-div\">\n                    <span style=\"font-size: 14px;\">{{ 'property.garage/parking' | translate }}</span>\n                    <ion-row class=\"room-row\">\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : gpAny, 'room-col' : !gpAny}\" (click)=\"gp('any')\">\n                            {{ 'property.any' | translate }}\n                        </ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : gp1, 'room-col' : !gp1}\" (click)=\"gp('1')\">1+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : gp2, 'room-col' : !gp2}\" (click)=\"gp('2')\">2+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : gp3, 'room-col' : !gp3}\" (click)=\"gp('3')\">3+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : gp4, 'room-col' : !gp4}\" (click)=\"gp('4')\">4+</ion-col>\n                        <ion-col size=\"2\" [ngClass]=\"{'activeFilter' : gp5, 'room-col' : !gp5}\" (click)=\"gp('5')\">5+</ion-col>\n                    </ion-row>\n                </div>\n            </div>\n        </div>\n        \n        \n        <div class=\"btn-margin\">\n            <ion-button class=\"button-format\" color=\"danger\" [disabled]=\"leadGenerationForm.invalid\" (click)=\"subscribeToDailyNews(leadGenerationForm.value)\">\n                {{ 'subscribe.submit' | translate }}\n            </ion-button>\n            <ion-button *ngIf=\"detailFormFlag\" class=\"button-format\" color=\"dark\" (click)=\"showMore()\">\n                {{ 'subscribe.more' | translate }}\n            </ion-button>\n            <ion-button *ngIf=\"!detailFormFlag\" class=\"button-format\" color=\"dark\" (click)=\"showMore()\">\n                {{ 'subscribe.hide' | translate }}\n            </ion-button>\n        </div>\n\n    </mat-expansion-panel>\n</mat-accordion>";
    /***/
  },

  /***/
  "./src/app/pages/property-detail/pre-approved-me.scss":
  /*!************************************************************!*\
    !*** ./src/app/pages/property-detail/pre-approved-me.scss ***!
    \************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesPropertyDetailPreApprovedMeScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "form {\n  width: 100%;\n  text-align: center;\n  font-size: 14px;\n}\n\n.mat-expansion-panel-header-title {\n  color: #101010 !important;\n}\n\n.expansion-header {\n  height: 50px !important;\n  padding: 0px 10px !important;\n}\n\n.panelTitle {\n  margin-top: 6px;\n  font-size: 16px;\n}\n\n.fieldFormat {\n  width: 90%;\n}\n\n.submit-btn {\n  width: 80%;\n  margin: 16px auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3ByZS1hcHByb3ZlZC1tZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9wcm9wZXJ0eS1kZXRhaWwvcHJlLWFwcHJvdmVkLW1lLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSx5QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSw0QkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3ByZS1hcHByb3ZlZC1tZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZm9ybSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlIHtcbiAgICBjb2xvcjogIzEwMTAxMCAhaW1wb3J0YW50O1xufVxuXG4uZXhwYW5zaW9uLWhlYWRlciB7XG4gICAgaGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZzogMHB4IDEwcHggIWltcG9ydGFudDtcbn1cblxuLnBhbmVsVGl0bGUge1xuICAgIG1hcmdpbi10b3A6IDZweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5maWVsZEZvcm1hdCB7XG4gICAgd2lkdGg6IDkwJTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luOiAxNnB4IGF1dG87XG59IiwiZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlIHtcbiAgY29sb3I6ICMxMDEwMTAgIWltcG9ydGFudDtcbn1cblxuLmV4cGFuc2lvbi1oZWFkZXIge1xuICBoZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZzogMHB4IDEwcHggIWltcG9ydGFudDtcbn1cblxuLnBhbmVsVGl0bGUge1xuICBtYXJnaW4tdG9wOiA2cHg7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLmZpZWxkRm9ybWF0IHtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICB3aWR0aDogODAlO1xuICBtYXJnaW46IDE2cHggYXV0bztcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/property-detail/property-detail.module.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/property-detail/property-detail.module.ts ***!
    \*****************************************************************/

  /*! exports provided: PropertyDetailPageModule */

  /***/
  function srcAppPagesPropertyDetailPropertyDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PropertyDetailPageModule", function () {
      return PropertyDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _property_detail_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./property-detail.page */
    "./src/app/pages/property-detail/property-detail.page.ts");
    /* harmony import */


    var _property_detail_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./property-detail.service */
    "./src/app/pages/property-detail/property-detail.service.ts");
    /* harmony import */


    var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ionic4-star-rating */
    "./node_modules/ionic4-star-rating/dist/index.js");
    /* harmony import */


    var _subscribe_newsletter_subscribe_newsletter_page__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./subscribe-newsletter/subscribe-newsletter.page */
    "./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.ts");
    /* harmony import */


    var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/material/bottom-sheet */
    "./node_modules/@angular/material/esm2015/bottom-sheet.js");

    var routes = [{
      path: '',
      component: _property_detail_page__WEBPACK_IMPORTED_MODULE_10__["PropertyDetailPage"]
    }];

    var PropertyDetailPageModule = function PropertyDetailPageModule() {
      _classCallCheck(this, PropertyDetailPageModule);
    };

    PropertyDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatListModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"], ionic4_star_rating__WEBPACK_IMPORTED_MODULE_12__["StarRatingModule"], _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_14__["MatBottomSheetModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild()],
      exports: [_subscribe_newsletter_subscribe_newsletter_page__WEBPACK_IMPORTED_MODULE_13__["SubscribeNewsletterPage"]],
      declarations: [_property_detail_page__WEBPACK_IMPORTED_MODULE_10__["PropertyDetailPage"], _subscribe_newsletter_subscribe_newsletter_page__WEBPACK_IMPORTED_MODULE_13__["SubscribeNewsletterPage"], _property_detail_page__WEBPACK_IMPORTED_MODULE_10__["ScheduleVisitBottom"], _property_detail_page__WEBPACK_IMPORTED_MODULE_10__["PreApprovedMe"]],
      providers: [_property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"]],
      entryComponents: [_property_detail_page__WEBPACK_IMPORTED_MODULE_10__["ScheduleVisitBottom"], _property_detail_page__WEBPACK_IMPORTED_MODULE_10__["PreApprovedMe"]]
    })], PropertyDetailPageModule);
    /***/
  },

  /***/
  "./src/app/pages/property-detail/property-detail.page.scss":
  /*!*****************************************************************!*\
    !*** ./src/app/pages/property-detail/property-detail.page.scss ***!
    \*****************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesPropertyDetailPropertyDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host .toolbar-format {\n  --background: #e01a27 !important;\n  zoom: 0.9 !important;\n  border: none;\n}\n:host ion-content {\n  --background: linear-gradient(-135deg, #f6f6f6, var(--ion-color-pmary));\n}\n:host ion-slides {\n  box-shadow: none;\n}\n:host .photo {\n  position: relative;\n  width: 100%;\n  max-height: 40vh;\n  min-height: 35vh;\n  background-repeat: no-repeat;\n  background-size: cover;\n  background-position: center center;\n}\n:host .photo-button {\n  position: absolute;\n  right: 10px;\n  top: 10px;\n}\n:host .view-image {\n  border: 1px solid #fff;\n  border-radius: 2px;\n  font-size: 14px;\n  color: #fff;\n  background-color: rgba(0, 0, 0, 0.5);\n  padding: 5px;\n  margin: 0px;\n  height: 34px;\n  display: flex;\n  align-items: center;\n}\n:host .meta {\n  position: absolute;\n  z-index: 10;\n  left: 0;\n  bottom: 0;\n  width: 100%;\n  padding: 20px 52px 5px 10px;\n  background: linear-gradient(180deg, transparent, rgba(0, 0, 0, 0.7), rgba(0, 0, 0, 0.8));\n}\n:host .property-title {\n  font-size: 17px;\n  color: #fff;\n  margin: 2px 0px;\n}\n:host .listed-format {\n  flex-wrap: wrap;\n  justify-content: space-between;\n  margin: 10px 15px 0;\n  display: flex;\n  align-items: center;\n}\n:host .p-listing-date {\n  margin: 0px;\n  font-size: 15px;\n}\n:host .p-listing {\n  font-size: 18px;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  color: #000;\n}\n:host .p-termination {\n  font-size: 15px;\n  margin-top: 5px;\n  margin-bottom: 0px;\n  color: #444;\n}\n:host .p-estimate {\n  font-size: 16px;\n  color: #000;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n:host .p-type {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #009cff;\n  padding: 1px 4px;\n  font-size: 15px;\n  background-color: #e1f3ff;\n  color: #009cff;\n  font-weight: 400;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n:host .saleClass {\n  background: #1e88e5;\n  color: #fff;\n  font-weight: 500;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding: 1px 4px;\n  font-size: 15px;\n  margin-top: 0px;\n  border-radius: 5px;\n  margin-bottom: 0px;\n}\n:host .soldClass {\n  background: #ff9e26;\n  color: #fff;\n  font-weight: 500;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding: 1px 4px;\n  font-size: 15px;\n  margin-top: 0px;\n  border-radius: 5px;\n  margin-bottom: 0px;\n}\n:host .leaseClass {\n  background: #558b2f;\n  color: #fff;\n  font-weight: 500;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  padding: 1px 4px;\n  font-size: 15px;\n  margin-top: 0px;\n  border-radius: 5px;\n  margin-bottom: 0px;\n}\n:host .dd {\n  position: relative;\n  flex-direction: column;\n  justify-content: flex-start;\n  align-items: center;\n  display: flex;\n  margin: auto;\n}\n:host .property-history {\n  display: flex;\n  flex-wrap: wrap;\n  align-items: flex-start;\n  border-top: 1px solid #eee;\n  padding: 10px 10px;\n  overflow: hidden;\n}\n:host .history-price {\n  align-self: center;\n  display: flex;\n  align-items: center;\n  margin-right: 4px;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  width: calc(100% - 70px);\n}\n:host .btn-view {\n  align-self: center;\n  text-transform: capitalize;\n  --border-radius: 0px;\n  --box-shadow: none;\n}\n:host .p-list-date {\n  margin-top: 10px;\n  color: #333;\n  margin-bottom: 0px;\n  margin-right: 4px;\n  flex-basis: 72px;\n  flex-grow: 1;\n}\n:host .p-mls {\n  min-width: 72px;\n  text-align: right;\n  white-space: nowrap;\n  margin-top: 2px;\n  font-size: 14px;\n  color: #333;\n  margin-bottom: 0px;\n}\n:host .leasePrice {\n  font-size: 16px;\n  font-weight: 500;\n  color: #333;\n}\n:host .saleLeaseBadge {\n  display: inline-flex;\n  justify-content: center;\n  align-items: center;\n  height: 20px;\n  border: 1px solid #e1f3d8;\n  border-radius: 4px;\n  padding: 0 5px;\n  font-size: 12px;\n  white-space: nowrap;\n  word-break: keep-all;\n  background-color: #f0f9eb;\n  color: #67c23a;\n  margin-left: 4px;\n}\n:host .section-nearby {\n  display: flex;\n  justify-content: space-around;\n  align-items: center;\n  height: 80px;\n}\n:host .nearby-btn {\n  text-transform: capitalize;\n  --box-shadow: none;\n  --border-radius: 0px;\n}\n:host ion-segment-button {\n  text-transform: capitalize !important;\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-light);\n}\n:host ion-card {\n  margin: 5px 1px !important;\n}\n:host ion-chip {\n  font-size: 12px;\n}\n:host .btn-more {\n  color: #fff;\n}\n:host .p-mr {\n  margin: 8px 0px !important;\n  font-size: 12px !important;\n}\n:host .school-title {\n  margin-left: 5px;\n}\n:host .mp-title {\n  font-size: 16px;\n}\n:host .mortgage-price {\n  color: #e88506;\n  font-weight: 500;\n  font-size: 16px;\n}\n:host .mat-expansion-panel-header-title {\n  color: #101010 !important;\n}\n:host .number-format {\n  color: #e88506;\n  font-weight: 500;\n}\n:host .chip-span {\n  border: 1px solid #009cff;\n  padding: 1px 3px;\n  text-transform: capitalize;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  font-size: 12px;\n  margin: 4px 4px 1px 0;\n  border-radius: 2px;\n  background-color: #e1f3ff;\n  color: #009cff;\n  display: inline-flex;\n}\n:host .expansion-header {\n  height: 50px !important;\n  padding: 0px 10px !important;\n}\n:host .head4 {\n  color: #101010;\n  font-weight: 100 !important;\n  font-size: 14px !important;\n  margin-top: 0px !important;\n  margin-bottom: 5px !important;\n}\n:host .head6 {\n  color: #888 !important;\n  font-weight: 100 !important;\n  font-size: 12px !important;\n  margin: 0px !important;\n}\n:host .row-room {\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n  border-bottom: 1px solid #eee;\n  padding: 12px 0;\n  color: #333;\n  margin: 0 15px;\n}\n:host .section-format {\n  margin: 0px 10px;\n  display: block;\n  border-bottom: 1px solid #eee;\n  padding: 8px 0;\n  overflow: hidden;\n}\n:host .section {\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .school-name {\n  font-size: 15px;\n  color: #000;\n  margin: 0px;\n  margin-right: 10px;\n}\n:host .rating {\n  white-space: nowrap;\n  font-size: 15px;\n  color: #333;\n  margin: 0px;\n}\n:host .actual-rating {\n  color: #ff9d00;\n  font-weight: 500;\n}\n:host .room-desc {\n  margin-top: 7px;\n  width: 100%;\n  color: #666;\n  font-size: 14px;\n}\n:host .mort-btn {\n  text-transform: capitalize;\n  font-size: 13px;\n}\n:host .row-bottom {\n  margin-bottom: 5px;\n  border-bottom: 1px solid #dedede;\n}\n:host h6 {\n  font-size: 12px;\n  color: #888;\n}\n:host .fieldformat {\n  width: 70%;\n  padding-right: 5px;\n}\n:host .fieldPercent {\n  width: 30%;\n}\n:host .add-fav-margin {\n  margin: 0px 0px 2px 1px !important;\n  font-size: 12px;\n  height: 40px;\n}\n:host .schedule-margin {\n  margin: 0px 1px 2px 1px !important;\n  height: 40px;\n  font-size: 12px;\n}\n:host .viewDetail {\n  color: #0062ca;\n  text-decoration: underline;\n}\n.row-div {\n  padding: 10px;\n  border-bottom: 1px solid #ccc;\n}\nion-slides {\n  box-shadow: 0 4px 16px rgba(var(--ion-color-dark-rgb), 0.4);\n}\nion-slides ion-slide .shadow {\n  position: absolute;\n  top: 0;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  z-index: 1;\n  box-shadow: inset 0 0 15rem rgba(var(--ion-color-primary-rgb), 0.95);\n}\n.card-img-status {\n  width: 120px;\n  position: absolute;\n  transform: rotate(45deg);\n  padding-top: 0.5rem;\n  padding-bottom: 0.5rem;\n  top: 10px;\n  right: -30px;\n  z-index: 99;\n  text-align: center;\n}\n.card-img-status.closed {\n  background-color: rgba(var(--ion-color-danger-rgb), 0.8);\n}\n.card-img-status.open {\n  background-color: rgba(var(--ion-color-danger-rgb), 0.8);\n}\n.login-required {\n  -webkit-filter: blur(5px);\n          filter: blur(5px);\n  pointer-events: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3Byb3BlcnR5LWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Byb3BlcnR5LWRldGFpbC9wcm9wZXJ0eS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0ksZ0NBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7QUNEUjtBRElJO0VBQ0ksdUVBQUE7QUNGUjtBREtJO0VBQ0ksZ0JBQUE7QUNIUjtBRE1JO0VBQ0ksa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQ0FBQTtBQ0pSO0FET0k7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FDTFI7QURRSTtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLG9DQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDTlI7QURTSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLDJCQUFBO0VBQ0Esd0ZBQUE7QUNQUjtBRFVJO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FDUlI7QURXSTtFQUNJLGVBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDVFI7QURZSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FDVlI7QURhSTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDWFI7QURjSTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FDWlI7QURlSTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDYlI7QURnQkk7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNkUjtBRGlCSTtFQUNJLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ2ZSO0FEa0JJO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDaEJSO0FEbUJJO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0FDakJSO0FEb0JJO0VBQ0ksa0JBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtBQ2xCUjtBRHFCSTtFQUNJLGFBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUNuQlI7QURzQkk7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7QUNwQlI7QUR1Qkk7RUFDSSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQ3JCUjtBRHdCSTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUN0QlI7QUR5Qkk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDdkJSO0FEMEJJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ3hCUjtBRDJCSTtFQUNJLG9CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUN6QlI7QUQ0Qkk7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUMxQlI7QUQ2Qkk7RUFDSSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7QUMzQlI7QUQ4Qkk7RUFDSSxxQ0FBQTtBQzVCUjtBRCtCSTtFQUNJLGdCQUFBO0VBQ0EsZ0RBQUE7QUM3QlI7QURnQ0k7RUFDSSwwQkFBQTtBQzlCUjtBRGlDSTtFQUNJLGVBQUE7QUMvQlI7QURrQ0k7RUFDSSxXQUFBO0FDaENSO0FEbUNJO0VBQ0ksMEJBQUE7RUFDQSwwQkFBQTtBQ2pDUjtBRG9DSTtFQUNJLGdCQUFBO0FDbENSO0FEcUNJO0VBQ0ksZUFBQTtBQ25DUjtBRHNDSTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUNwQ1I7QUR1Q0k7RUFDSSx5QkFBQTtBQ3JDUjtBRHdDSTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBQ3RDUjtBRHlDSTtFQUNJLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtFQUNBLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUN2Q1I7QUQwQ0k7RUFDSSx1QkFBQTtFQUNBLDRCQUFBO0FDeENSO0FEMkNJO0VBQ0ksY0FBQTtFQUNBLDJCQUFBO0VBQ0EsMEJBQUE7RUFDQSwwQkFBQTtFQUNBLDZCQUFBO0FDekNSO0FENENJO0VBQ0ksc0JBQUE7RUFDQSwyQkFBQTtFQUNBLDBCQUFBO0VBQ0Esc0JBQUE7QUMxQ1I7QUQ2Q0k7RUFDSSxhQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7QUMzQ1I7QUQ4Q0k7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw2QkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQzVDUjtBRCtDSTtFQUNJLGFBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FDN0NSO0FEZ0RJO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUM5Q1I7QURpREk7RUFDSSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQy9DUjtBRGtESTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtBQ2hEUjtBRG1ESTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUNqRFI7QURvREk7RUFDSSwwQkFBQTtFQUNBLGVBQUE7QUNsRFI7QURxREk7RUFDSSxrQkFBQTtFQUNBLGdDQUFBO0FDbkRSO0FEc0RJO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNwRFI7QUR1REk7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7QUNyRFI7QUR3REk7RUFDSSxVQUFBO0FDdERSO0FEeURJO0VBQ0ksa0NBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtBQ3ZEUjtBRDBESTtFQUNJLGtDQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUN4RFI7QUQyREk7RUFDSSxjQUFBO0VBQ0EsMEJBQUE7QUN6RFI7QUQ2REE7RUFDSSxhQUFBO0VBQ0EsNkJBQUE7QUMxREo7QUQ2REE7RUFDSSwyREFBQTtBQzFESjtBRDREUTtFQUNBLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxvRUFBQTtBQzFEUjtBRCtEQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDNURKO0FENkRJO0VBQ0ksd0RBQUE7QUMzRFI7QUQ2REk7RUFDSSx3REFBQTtBQzNEUjtBRCtEQTtFQUNJLHlCQUFBO1VBQUEsaUJBQUE7RUFDQSxvQkFBQTtBQzVESiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb3BlcnR5LWRldGFpbC9wcm9wZXJ0eS1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXG4gICAgLnRvb2xiYXItZm9ybWF0IHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZTAxYTI3ICFpbXBvcnRhbnQ7XG4gICAgICAgIHpvb206IDAuOSAhaW1wb3J0YW50O1xuICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgfVxuXG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCAjZjZmNmY2LCB2YXIoLS1pb24tY29sb3ItcG1hcnkpKTtcbiAgICB9XG5cbiAgICBpb24tc2xpZGVzIHtcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICB9XG5cbiAgICAucGhvdG8ge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBtYXgtaGVpZ2h0OiA0MHZoO1xuICAgICAgICBtaW4taGVpZ2h0OiAzNXZoO1xuICAgICAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgY2VudGVyO1xuICAgIH1cblxuICAgIC5waG90by1idXR0b24ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHJpZ2h0OiAxMHB4O1xuICAgICAgICB0b3A6IDEwcHg7XG4gICAgfVxuXG4gICAgLnZpZXctaW1hZ2Uge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsLjUpO1xuICAgICAgICBwYWRkaW5nOiA1cHg7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBoZWlnaHQ6IDM0cHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLm1ldGEge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIHotaW5kZXg6IDEwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBib3R0b206IDA7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nOiAyMHB4IDUycHggNXB4IDEwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsdHJhbnNwYXJlbnQscmdiYSgwLDAsMCwuNykscmdiYSgwLDAsMCwuOCkpO1xuICAgIH1cblxuICAgIC5wcm9wZXJ0eS10aXRsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIG1hcmdpbjogMnB4IDBweDtcbiAgICB9XG5cbiAgICAubGlzdGVkLWZvcm1hdCB7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBtYXJnaW46IDEwcHggMTVweCAwO1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cblxuICAgIC5wLWxpc3RpbmctZGF0ZSB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgfVxuXG4gICAgLnAtbGlzdGluZyB7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgIH1cblxuICAgIC5wLXRlcm1pbmF0aW9uIHtcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICAgICAgY29sb3I6ICM0NDQ7XG4gICAgfVxuXG4gICAgLnAtZXN0aW1hdGUge1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGNvbG9yOiAjMDAwO1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICB9XG5cbiAgICAucC10eXBlIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDljZmY7XG4gICAgICAgIHBhZGRpbmc6IDFweCA0cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2UxZjNmZjtcbiAgICAgICAgY29sb3I6ICMwMDljZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIH1cblxuICAgIC5zYWxlQ2xhc3Mge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjMWU4OGU1O1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDFweCA0cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICB9XG5cbiAgICAuc29sZENsYXNzIHtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmOWUyNjtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAxcHggNHB4O1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gICAgfVxuXG4gICAgLmxlYXNlQ2xhc3Mge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjNTU4YjJmO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDFweCA0cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDBweDtcbiAgICB9XG5cbiAgICAuZGQge1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xuICAgIH1cblxuICAgIC5wcm9wZXJ0eS1oaXN0b3J5IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC13cmFwOiB3cmFwO1xuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICAgICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlZWU7XG4gICAgICAgIHBhZGRpbmc6IDEwcHggMTBweDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICB9XG5cbiAgICAuaGlzdG9yeS1wcmljZSB7XG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA0cHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gNzBweCk7XG4gICAgfVxuXG4gICAgLmJ0bi12aWV3IHtcbiAgICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgICAgIC0tYm94LXNoYWRvdzogbm9uZTtcbiAgICB9XG5cbiAgICAucC1saXN0LWRhdGUge1xuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICAgICBjb2xvcjogIzMzMztcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDRweDtcbiAgICAgICAgZmxleC1iYXNpczogNzJweDtcbiAgICAgICAgZmxleC1ncm93OiAxO1xuICAgIH1cblxuICAgIC5wLW1scyB7XG4gICAgICAgIG1pbi13aWR0aDogNzJweDtcbiAgICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICBjb2xvcjogIzMzMztcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIH1cblxuICAgIC5sZWFzZVByaWNlIHtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICBjb2xvcjogIzMzMztcbiAgICB9XG5cbiAgICAuc2FsZUxlYXNlQmFkZ2Uge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGhlaWdodDogMjBweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2UxZjNkODtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgICAgICBwYWRkaW5nOiAwIDVweDtcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICB3b3JkLWJyZWFrOiBrZWVwLWFsbDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2YwZjllYjtcbiAgICAgICAgY29sb3I6ICM2N2MyM2E7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA0cHg7XG4gICAgfVxuXG4gICAgLnNlY3Rpb24tbmVhcmJ5IHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGhlaWdodDogODBweDtcbiAgICB9XG5cbiAgICAubmVhcmJ5LWJ0biB7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgICAgICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gICAgICAgIC0tYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIH1cblxuICAgIGlvbi1zZWdtZW50LWJ1dHRvbiB7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgfVxuXG4gICAgaW9uLWNhcmQge1xuICAgICAgICBtYXJnaW46IDVweCAxcHggIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICBpb24tY2hpcCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB9XG5cbiAgICAuYnRuLW1vcmUge1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICB9XG5cbiAgICAucC1tciB7XG4gICAgICAgIG1hcmdpbjogOHB4IDBweCAhaW1wb3J0YW50O1xuICAgICAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICAuc2Nob29sLXRpdGxlIHtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICB9XG5cbiAgICAubXAtdGl0bGUge1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgfVxuXG4gICAgLm1vcnRnYWdlLXByaWNlIHtcbiAgICAgICAgY29sb3I6ICNlODg1MDY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICB9XG5cbiAgICAubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItdGl0bGUge1xuICAgICAgICBjb2xvcjogIzEwMTAxMCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5udW1iZXItZm9ybWF0IHtcbiAgICAgICAgY29sb3I6ICNlODg1MDY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgLmNoaXAtc3BhbiB7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDljZmY7XG4gICAgICAgIHBhZGRpbmc6IDFweCAzcHg7XG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgbWFyZ2luOiA0cHggNHB4IDFweCAwO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNlMWYzZmY7XG4gICAgICAgIGNvbG9yOiAjMDA5Y2ZmO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICB9XG5cbiAgICAuZXhwYW5zaW9uLWhlYWRlciB7XG4gICAgICAgIGhlaWdodDogNTBweCAhaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nOiAwcHggMTBweCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIC5oZWFkNCB7XG4gICAgICAgIGNvbG9yOiAjMTAxMDEwO1xuICAgICAgICBmb250LXdlaWdodDogMTAwICFpbXBvcnRhbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHggIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgLmhlYWQ2IHtcbiAgICAgICAgY29sb3I6ICM4ODggIWltcG9ydGFudDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDEwMCAhaW1wb3J0YW50O1xuICAgICAgICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgICAgICAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICAucm93LXJvb20ge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LXdyYXA6IHdyYXA7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xuICAgICAgICBwYWRkaW5nOiAxMnB4IDA7XG4gICAgICAgIGNvbG9yOiAjMzMzO1xuICAgICAgICBtYXJnaW46IDAgMTVweDtcbiAgICB9XG5cbiAgICAuc2VjdGlvbi1mb3JtYXQge1xuICAgICAgICBtYXJnaW46IDBweCAxMHB4O1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZWU7XG4gICAgICAgIHBhZGRpbmc6IDhweCAwO1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIH1cblxuICAgIC5zZWN0aW9uIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIH1cblxuICAgIC5zY2hvb2wtbmFtZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG4gICAgfVxuXG4gICAgLnJhdGluZyB7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgY29sb3I6ICMzMzM7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgIH1cblxuICAgIC5hY3R1YWwtcmF0aW5nIHtcbiAgICAgICAgY29sb3I6ICNmZjlkMDA7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgLnJvb20tZGVzYyB7XG4gICAgICAgIG1hcmdpbi10b3A6IDdweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGNvbG9yOiAjNjY2O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxuXG4gICAgLm1vcnQtYnRuIHtcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICB9XG5cbiAgICAucm93LWJvdHRvbSB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZWRlZGU7XG4gICAgfVxuXG4gICAgaDYge1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGNvbG9yOiAjODg4O1xuICAgIH1cblxuICAgIC5maWVsZGZvcm1hdCB7XG4gICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDVweDtcbiAgICB9XG5cbiAgICAuZmllbGRQZXJjZW50IHtcbiAgICAgICAgd2lkdGg6IDMwJTtcbiAgICB9XG5cbiAgICAuYWRkLWZhdi1tYXJnaW4ge1xuICAgICAgICBtYXJnaW46IDBweCAwcHggMnB4IDFweCAhaW1wb3J0YW50O1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgIGhlaWdodDogNDBweDtcbiAgICB9XG5cbiAgICAuc2NoZWR1bGUtbWFyZ2luIHtcbiAgICAgICAgbWFyZ2luOiAwcHggMXB4IDJweCAxcHggIWltcG9ydGFudDtcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgfVxuXG4gICAgLnZpZXdEZXRhaWwge1xuICAgICAgICBjb2xvcjogIzAwNjJjYTtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG4gICAgfVxufVxuXG4ucm93LWRpdiB7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcbn1cbiAgXG5pb24tc2xpZGVzIHtcbiAgICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwuNCk7XG4gICAgaW9uLXNsaWRlIHtcbiAgICAgICAgLnNoYWRvdyB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICBib3R0b206IDA7XG4gICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICBib3gtc2hhZG93OiBpbnNldCAwIDAgMTVyZW0gcmdiYSh2YXIoLS1pb24tY29sb3ItcHJpbWFyeS1yZ2IpLCAuOTUpXG4gICAgICAgIH1cbiAgICB9XG59XG5cbi5jYXJkLWltZy1zdGF0dXMge1xuICAgIHdpZHRoOiAxMjBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAgIHBhZGRpbmctdG9wOiAuNXJlbTtcbiAgICBwYWRkaW5nLWJvdHRvbTogLjVyZW07XG4gICAgdG9wOiAxMHB4O1xuICAgIHJpZ2h0OiAtMzBweDtcbiAgICB6LWluZGV4OiA5OTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgJi5jbG9zZWQge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYW5nZXItcmdiKSwgLjgpO1xuICAgIH1cbiAgICAmLm9wZW4ge1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYW5nZXItcmdiKSwgLjgpO1xuICAgIH1cbn1cblxuLmxvZ2luLXJlcXVpcmVkIHtcbiAgICBmaWx0ZXI6IGJsdXIoNXB4KTtcbiAgICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn0iLCI6aG9zdCAudG9vbGJhci1mb3JtYXQge1xuICAtLWJhY2tncm91bmQ6ICNlMDFhMjcgIWltcG9ydGFudDtcbiAgem9vbTogMC45ICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogbm9uZTtcbn1cbjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgI2Y2ZjZmNiwgdmFyKC0taW9uLWNvbG9yLXBtYXJ5KSk7XG59XG46aG9zdCBpb24tc2xpZGVzIHtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cbjpob3N0IC5waG90byB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC1oZWlnaHQ6IDQwdmg7XG4gIG1pbi1oZWlnaHQ6IDM1dmg7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XG59XG46aG9zdCAucGhvdG8tYnV0dG9uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTBweDtcbiAgdG9wOiAxMHB4O1xufVxuOmhvc3QgLnZpZXctaW1hZ2Uge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcbiAgcGFkZGluZzogNXB4O1xuICBtYXJnaW46IDBweDtcbiAgaGVpZ2h0OiAzNHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuOmhvc3QgLm1ldGEge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDEwO1xuICBsZWZ0OiAwO1xuICBib3R0b206IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiAyMHB4IDUycHggNXB4IDEwcHg7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxODBkZWcsIHRyYW5zcGFyZW50LCByZ2JhKDAsIDAsIDAsIDAuNyksIHJnYmEoMCwgMCwgMCwgMC44KSk7XG59XG46aG9zdCAucHJvcGVydHktdGl0bGUge1xuICBmb250LXNpemU6IDE3cHg7XG4gIGNvbG9yOiAjZmZmO1xuICBtYXJnaW46IDJweCAwcHg7XG59XG46aG9zdCAubGlzdGVkLWZvcm1hdCB7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBtYXJnaW46IDEwcHggMTVweCAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuOmhvc3QgLnAtbGlzdGluZy1kYXRlIHtcbiAgbWFyZ2luOiAwcHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cbjpob3N0IC5wLWxpc3Rpbmcge1xuICBmb250LXNpemU6IDE4cHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBjb2xvcjogIzAwMDtcbn1cbjpob3N0IC5wLXRlcm1pbmF0aW9uIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgY29sb3I6ICM0NDQ7XG59XG46aG9zdCAucC1lc3RpbWF0ZSB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgY29sb3I6ICMwMDA7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuOmhvc3QgLnAtdHlwZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDA5Y2ZmO1xuICBwYWRkaW5nOiAxcHggNHB4O1xuICBmb250LXNpemU6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlMWYzZmY7XG4gIGNvbG9yOiAjMDA5Y2ZmO1xuICBmb250LXdlaWdodDogNDAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbjpob3N0IC5zYWxlQ2xhc3Mge1xuICBiYWNrZ3JvdW5kOiAjMWU4OGU1O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDFweCA0cHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbjpob3N0IC5zb2xkQ2xhc3Mge1xuICBiYWNrZ3JvdW5kOiAjZmY5ZTI2O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHBhZGRpbmc6IDFweCA0cHg7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbjpob3N0IC5sZWFzZUNsYXNzIHtcbiAgYmFja2dyb3VuZDogIzU1OGIyZjtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBwYWRkaW5nOiAxcHggNHB4O1xuICBmb250LXNpemU6IDE1cHg7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG46aG9zdCAuZGQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuOmhvc3QgLnByb3BlcnR5LWhpc3Rvcnkge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LXdyYXA6IHdyYXA7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2VlZTtcbiAgcGFkZGluZzogMTBweCAxMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuOmhvc3QgLmhpc3RvcnktcHJpY2Uge1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi1yaWdodDogNHB4O1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDcwcHgpO1xufVxuOmhvc3QgLmJ0bi12aWV3IHtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XG4gIC0tYm94LXNoYWRvdzogbm9uZTtcbn1cbjpob3N0IC5wLWxpc3QtZGF0ZSB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIGNvbG9yOiAjMzMzO1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIG1hcmdpbi1yaWdodDogNHB4O1xuICBmbGV4LWJhc2lzOiA3MnB4O1xuICBmbGV4LWdyb3c6IDE7XG59XG46aG9zdCAucC1tbHMge1xuICBtaW4td2lkdGg6IDcycHg7XG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICMzMzM7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbjpob3N0IC5sZWFzZVByaWNlIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBjb2xvcjogIzMzMztcbn1cbjpob3N0IC5zYWxlTGVhc2VCYWRnZSB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgaGVpZ2h0OiAyMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZTFmM2Q4O1xuICBib3JkZXItcmFkaXVzOiA0cHg7XG4gIHBhZGRpbmc6IDAgNXB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHdvcmQtYnJlYWs6IGtlZXAtYWxsO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjBmOWViO1xuICBjb2xvcjogIzY3YzIzYTtcbiAgbWFyZ2luLWxlZnQ6IDRweDtcbn1cbjpob3N0IC5zZWN0aW9uLW5lYXJieSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDgwcHg7XG59XG46aG9zdCAubmVhcmJ5LWJ0biB7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG4gIC0tYm9yZGVyLXJhZGl1czogMHB4O1xufVxuOmhvc3QgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemUgIWltcG9ydGFudDtcbn1cbjpob3N0IGlvbi1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xufVxuOmhvc3QgaW9uLWNhcmQge1xuICBtYXJnaW46IDVweCAxcHggIWltcG9ydGFudDtcbn1cbjpob3N0IGlvbi1jaGlwIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuOmhvc3QgLmJ0bi1tb3JlIHtcbiAgY29sb3I6ICNmZmY7XG59XG46aG9zdCAucC1tciB7XG4gIG1hcmdpbjogOHB4IDBweCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbn1cbjpob3N0IC5zY2hvb2wtdGl0bGUge1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuOmhvc3QgLm1wLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuOmhvc3QgLm1vcnRnYWdlLXByaWNlIHtcbiAgY29sb3I6ICNlODg1MDY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cbjpob3N0IC5tYXQtZXhwYW5zaW9uLXBhbmVsLWhlYWRlci10aXRsZSB7XG4gIGNvbG9yOiAjMTAxMDEwICFpbXBvcnRhbnQ7XG59XG46aG9zdCAubnVtYmVyLWZvcm1hdCB7XG4gIGNvbG9yOiAjZTg4NTA2O1xuICBmb250LXdlaWdodDogNTAwO1xufVxuOmhvc3QgLmNoaXAtc3BhbiB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMDljZmY7XG4gIHBhZGRpbmc6IDFweCAzcHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICB3aWR0aDogZml0LWNvbnRlbnQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luOiA0cHggNHB4IDFweCAwO1xuICBib3JkZXItcmFkaXVzOiAycHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlMWYzZmY7XG4gIGNvbG9yOiAjMDA5Y2ZmO1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbn1cbjpob3N0IC5leHBhbnNpb24taGVhZGVyIHtcbiAgaGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDBweCAxMHB4ICFpbXBvcnRhbnQ7XG59XG46aG9zdCAuaGVhZDQge1xuICBjb2xvcjogIzEwMTAxMDtcbiAgZm9udC13ZWlnaHQ6IDEwMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDVweCAhaW1wb3J0YW50O1xufVxuOmhvc3QgLmhlYWQ2IHtcbiAgY29sb3I6ICM4ODggIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDEwMCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDEycHggIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwcHggIWltcG9ydGFudDtcbn1cbjpob3N0IC5yb3ctcm9vbSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZWU7XG4gIHBhZGRpbmc6IDEycHggMDtcbiAgY29sb3I6ICMzMzM7XG4gIG1hcmdpbjogMCAxNXB4O1xufVxuOmhvc3QgLnNlY3Rpb24tZm9ybWF0IHtcbiAgbWFyZ2luOiAwcHggMTBweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xuICBwYWRkaW5nOiA4cHggMDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbjpob3N0IC5zZWN0aW9uIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuOmhvc3QgLnNjaG9vbC1uYW1lIHtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBjb2xvcjogIzAwMDtcbiAgbWFyZ2luOiAwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cbjpob3N0IC5yYXRpbmcge1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGNvbG9yOiAjMzMzO1xuICBtYXJnaW46IDBweDtcbn1cbjpob3N0IC5hY3R1YWwtcmF0aW5nIHtcbiAgY29sb3I6ICNmZjlkMDA7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG46aG9zdCAucm9vbS1kZXNjIHtcbiAgbWFyZ2luLXRvcDogN3B4O1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6ICM2NjY7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbjpob3N0IC5tb3J0LWJ0biB7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXNpemU6IDEzcHg7XG59XG46aG9zdCAucm93LWJvdHRvbSB7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNkZWRlZGU7XG59XG46aG9zdCBoNiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM4ODg7XG59XG46aG9zdCAuZmllbGRmb3JtYXQge1xuICB3aWR0aDogNzAlO1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG46aG9zdCAuZmllbGRQZXJjZW50IHtcbiAgd2lkdGg6IDMwJTtcbn1cbjpob3N0IC5hZGQtZmF2LW1hcmdpbiB7XG4gIG1hcmdpbjogMHB4IDBweCAycHggMXB4ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuOmhvc3QgLnNjaGVkdWxlLW1hcmdpbiB7XG4gIG1hcmdpbjogMHB4IDFweCAycHggMXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNDBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuOmhvc3QgLnZpZXdEZXRhaWwge1xuICBjb2xvcjogIzAwNjJjYTtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi5yb3ctZGl2IHtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XG59XG5cbmlvbi1zbGlkZXMge1xuICBib3gtc2hhZG93OiAwIDRweCAxNnB4IHJnYmEodmFyKC0taW9uLWNvbG9yLWRhcmstcmdiKSwgMC40KTtcbn1cbmlvbi1zbGlkZXMgaW9uLXNsaWRlIC5zaGFkb3cge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgYm90dG9tOiAwO1xuICByaWdodDogMDtcbiAgei1pbmRleDogMTtcbiAgYm94LXNoYWRvdzogaW5zZXQgMCAwIDE1cmVtIHJnYmEodmFyKC0taW9uLWNvbG9yLXByaW1hcnktcmdiKSwgMC45NSk7XG59XG5cbi5jYXJkLWltZy1zdGF0dXMge1xuICB3aWR0aDogMTIwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICBwYWRkaW5nLXRvcDogMC41cmVtO1xuICBwYWRkaW5nLWJvdHRvbTogMC41cmVtO1xuICB0b3A6IDEwcHg7XG4gIHJpZ2h0OiAtMzBweDtcbiAgei1pbmRleDogOTk7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jYXJkLWltZy1zdGF0dXMuY2xvc2VkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSh2YXIoLS1pb24tY29sb3ItZGFuZ2VyLXJnYiksIDAuOCk7XG59XG4uY2FyZC1pbWctc3RhdHVzLm9wZW4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKHZhcigtLWlvbi1jb2xvci1kYW5nZXItcmdiKSwgMC44KTtcbn1cblxuLmxvZ2luLXJlcXVpcmVkIHtcbiAgZmlsdGVyOiBibHVyKDVweCk7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/property-detail/property-detail.page.ts":
  /*!***************************************************************!*\
    !*** ./src/app/pages/property-detail/property-detail.page.ts ***!
    \***************************************************************/

  /*! exports provided: PropertyDetailPage, PreApprovedMe, ScheduleVisitBottom */

  /***/
  function srcAppPagesPropertyDetailPropertyDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PropertyDetailPage", function () {
      return PropertyDetailPage;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PreApprovedMe", function () {
      return PreApprovedMe;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ScheduleVisitBottom", function () {
      return ScheduleVisitBottom;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _modal_image_image_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./../modal/image/image.page */
    "./src/app/pages/modal/image/image.page.ts");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _modal_schools_info_schools_info_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../modal/schools-info/schools-info.page */
    "./src/app/pages/modal/schools-info/schools-info.page.ts");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _modal_nearby_sold_rent_nearby_sold_rent_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../modal/nearby-sold-rent/nearby-sold-rent.page */
    "./src/app/pages/modal/nearby-sold-rent/nearby-sold-rent.page.ts");
    /* harmony import */


    var _property_detail_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./property-detail.service */
    "./src/app/pages/property-detail/property-detail.service.ts");
    /* harmony import */


    var moment__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! moment */
    "./node_modules/moment/moment.js");
    /* harmony import */


    var moment__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_12__);
    /* harmony import */


    var leaflet__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! leaflet */
    "./node_modules/leaflet/dist/leaflet-src.js");
    /* harmony import */


    var leaflet__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_13__);

    var PropertyDetailPage = /*#__PURE__*/function () {
      function PropertyDetailPage(asCtrl, navCtrl, toastCtrl, loadingCtrl, modalCtrl, route, fb, detailService, alertCtrl, bottomSheet, router, translateService) {
        _classCallCheck(this, PropertyDetailPage);

        this.asCtrl = asCtrl;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.route = route;
        this.fb = fb;
        this.detailService = detailService;
        this.alertCtrl = alertCtrl;
        this.bottomSheet = bottomSheet;
        this.router = router;
        this.translateService = translateService;
        this.userId = null;
        this.propertyopts = 'keyFacts';
        this.propertyID = this.route.snapshot.paramMap.get('id');
        this.getPropertyDetails(this.propertyID);
        this.loginDetails = JSON.parse(localStorage.getItem('userDetails'));

        if (this.loginDetails != undefined) {
          this.loginFlag = false;
          this.userId = this.loginDetails.data.userVM.id;
        }
      }

      _createClass(PropertyDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.mortgageForm = this.fb.group({
            principalAmount: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            downPayment: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            interestRate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            period: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
          });
          this.getRateOfInterest();
        }
      }, {
        key: "getRateOfInterest",
        value: function getRateOfInterest() {
          var _this = this;

          this.detailService.getRateOfInterest().subscribe(function (res) {
            if (res.code == 200) {
              _this.mortgageForm.controls['interestRate'].setValue(res.data.taxValue);
            }
          });
        }
      }, {
        key: "getPropertyDetails",
        value: function getPropertyDetails(propertyId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this.translateService.get('app.pleaseWait').subscribe(function (value) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                _context.next = 2;
                                return this.loadingCtrl.create({
                                  message: value
                                });

                              case 2:
                                loading = _context.sent;
                                _context.next = 5;
                                return loading.present();

                              case 5:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee, this);
                      }));
                    });
                    this.detailService.getPropertyDetails(propertyId).subscribe(function (data) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        var _this3 = this;

                        var roomDetails, roomArr, tDate, startDate, currenDate, endDate;
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                          while (1) {
                            switch (_context2.prev = _context2.next) {
                              case 0:
                                _context2.next = 2;
                                return loading.dismiss();

                              case 2:
                                this.property = data.data[0];

                                if (this.property) {
                                  roomDetails = [];
                                  roomArr = this.property.roomDetails;
                                  roomArr.forEach(function (element) {
                                    if (element.room != null) {
                                      roomDetails.push(element);
                                    }
                                  });
                                  this.property.roomDetails = roomDetails;

                                  if (this.property.saleLease == 'Lease') {
                                    this.loginFlag = false;
                                  } else {
                                    if (this.loginDetails != undefined && this.loginDetails != null) {
                                      this.loginFlag = false;
                                    } else {
                                      this.loginFlag = true;
                                      this.loginAlert();
                                    }
                                  }

                                  this.mortgageForm.controls['principalAmount'].setValue(this.property.listPrice);

                                  if (this.property.terminatedDate != null) {
                                    tDate = moment__WEBPACK_IMPORTED_MODULE_12__(this.property.terminatedDate).format("DD/MM/YYYY");
                                    startDate = moment__WEBPACK_IMPORTED_MODULE_12__(tDate, "DD/MM/YYYY");
                                    currenDate = moment__WEBPACK_IMPORTED_MODULE_12__(new Date()).format("DD/MM/YYYY");
                                    endDate = moment__WEBPACK_IMPORTED_MODULE_12__(currenDate, "DD/MM/YYYY");
                                    this.terminationDate = endDate.diff(startDate, 'days');
                                  }
                                }

                                setTimeout(function () {
                                  _this3.showMap();
                                }, 300);
                                console.log(this.property);

                              case 6:
                              case "end":
                                return _context2.stop();
                            }
                          }
                        }, _callee2, this);
                      }));
                    }, function (err) {
                      loading.dismiss();
                    });

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "showMap",
        value: function showMap() {
          this.map = new leaflet__WEBPACK_IMPORTED_MODULE_13__["Map"]('propertyMap');
          this.map.setView([this.property.latitude, this.property.longitude], 16);
          this.map.setMinZoom(2);
          leaflet__WEBPACK_IMPORTED_MODULE_13__["tileLayer"]('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            maxZoom: 18,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1IjoicG9zaGxpZmUiLCJhIjoiY2tkczdwNThsMDV0NzJzcGNmZmtkcGNqdyJ9.YnjXTw7u8tB3TVz-OiWniA'
          }).addTo(this.map);
          var propertyMarker = leaflet__WEBPACK_IMPORTED_MODULE_13__["icon"]({
            iconUrl: '../../assets/icon/circle.png',
            iconSize: [100, 100],
            popupAnchor: [0, -14]
          });
          var popup = leaflet__WEBPACK_IMPORTED_MODULE_13__["popup"]({
            closeOnClick: false
          }).setContent('Unit ' + this.property.aptUnit + ' - ' + this.property.address);
          this.marker = leaflet__WEBPACK_IMPORTED_MODULE_13__["marker"]([this.property.latitude, this.property.longitude], {
            icon: propertyMarker
          }).bindPopup(popup, {
            closeButton: false
          });
          this.marker.addTo(this.map).openPopup();
        }
      }, {
        key: "loginAlert",
        value: function loginAlert() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var _this4 = this;

            var alert;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.alertCtrl.create({
                      cssClass: 'my-custom-class',
                      header: 'Alert',
                      message: 'Please Sign-in to view this listing',
                      buttons: [{
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: function handler(blah) {
                          _this4.router.navigateByUrl('/home-results');
                        }
                      }, {
                        text: 'Sign-in',
                        handler: function handler() {
                          _this4.router.navigateByUrl('/login');
                        }
                      }],
                      backdropDismiss: false
                    });

                  case 2:
                    alert = _context4.sent;
                    _context4.next = 5;
                    return alert.present();

                  case 5:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "presentImage",
        value: function presentImage(image) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var modal;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.modalCtrl.create({
                      component: _modal_image_image_page__WEBPACK_IMPORTED_MODULE_5__["ImagePage"],
                      componentProps: {
                        data: {
                          virtualTour: this.property.virtualTourURL,
                          images: image
                        }
                      }
                    });

                  case 2:
                    modal = _context5.sent;
                    _context5.next = 5;
                    return modal.present();

                  case 5:
                    return _context5.abrupt("return", _context5.sent);

                  case 6:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }, {
        key: "addTofavorite",
        value: function addTofavorite() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var _this5 = this;

            var loading, data;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    if (!(this.userId != null)) {
                      _context7.next = 10;
                      break;
                    }

                    _context7.next = 3;
                    return this.loadingCtrl.create({
                      message: "Please wait..."
                    });

                  case 3:
                    loading = _context7.sent;
                    _context7.next = 6;
                    return loading.present();

                  case 6:
                    data = {
                      userId: this.userId,
                      favoriteId: parseInt(this.propertyID)
                    };
                    this.detailService.addToFavorite(data).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this5, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                        return regeneratorRuntime.wrap(function _callee6$(_context6) {
                          while (1) {
                            switch (_context6.prev = _context6.next) {
                              case 0:
                                _context6.next = 2;
                                return loading.dismiss();

                              case 2:
                                this.presentToast('Property added to your favorite');

                              case 3:
                              case "end":
                                return _context6.stop();
                            }
                          }
                        }, _callee6, this);
                      }));
                    }, function (err) {
                      loading.dismiss();

                      _this5.presentToast('Something went wrong');
                    });
                    _context7.next = 11;
                    break;

                  case 10:
                    this.presentToast('Please login to add Favorite Property');

                  case 11:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
            var toast;
            return regeneratorRuntime.wrap(function _callee8$(_context8) {
              while (1) {
                switch (_context8.prev = _context8.next) {
                  case 0:
                    _context8.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context8.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context8.stop();
                }
              }
            }, _callee8, this);
          }));
        }
      }, {
        key: "scheduleVisit",
        value: function scheduleVisit() {
          this.bottomSheet.open(ScheduleVisitBottom, {
            data: this.propertyID
          });
        }
      }, {
        key: "openPreApprovedMe",
        value: function openPreApprovedMe() {
          this.bottomSheet.open(PreApprovedMe, {
            data: this.propertyID
          });
        }
      }, {
        key: "schoolsInfo",
        value: function schoolsInfo(schoolDetails) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
            var modal;
            return regeneratorRuntime.wrap(function _callee9$(_context9) {
              while (1) {
                switch (_context9.prev = _context9.next) {
                  case 0:
                    _context9.next = 2;
                    return this.modalCtrl.create({
                      component: _modal_schools_info_schools_info_page__WEBPACK_IMPORTED_MODULE_8__["SchoolsInfoPage"],
                      componentProps: {
                        data: schoolDetails
                      }
                    });

                  case 2:
                    modal = _context9.sent;
                    _context9.next = 5;
                    return modal.present();

                  case 5:
                    return _context9.abrupt("return", _context9.sent);

                  case 6:
                  case "end":
                    return _context9.stop();
                }
              }
            }, _callee9, this);
          }));
        }
      }, {
        key: "nearbySoldAndLease",
        value: function nearbySoldAndLease(type) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
            var _this6 = this;

            var modal;
            return regeneratorRuntime.wrap(function _callee10$(_context10) {
              while (1) {
                switch (_context10.prev = _context10.next) {
                  case 0:
                    _context10.next = 2;
                    return this.modalCtrl.create({
                      component: _modal_nearby_sold_rent_nearby_sold_rent_page__WEBPACK_IMPORTED_MODULE_10__["NearbySoldRentPage"],
                      componentProps: {
                        data: this.property,
                        type: type
                      }
                    });

                  case 2:
                    modal = _context10.sent;
                    modal.onDidDismiss().then(function (dataReturned) {
                      if (dataReturned != null) {
                        if (dataReturned.data != null && dataReturned.data != undefined) {
                          _this6.map.off();

                          _this6.map.remove();

                          setTimeout(function () {
                            _this6.router.navigateByUrl('property-detail/' + dataReturned.data);
                          }, 500);
                        }
                      }
                    });
                    _context10.next = 6;
                    return modal.present();

                  case 6:
                    return _context10.abrupt("return", _context10.sent);

                  case 7:
                  case "end":
                    return _context10.stop();
                }
              }
            }, _callee10, this);
          }));
        }
      }, {
        key: "onMortgageFormSubmit",
        value: function onMortgageFormSubmit(values) {
          if (this.mortgageForm.valid) {
            var principalAmount = values['principalAmount'];
            var down = values['downPayment'];
            var interest = values['interestRate'];
            var term = values['period'];
            this.monthlyPayment = this.calculateMortgage(principalAmount, down, interest / 100 / 12, term * 12).toFixed(2);
          }
        }
      }, {
        key: "calculateMortgage",
        value: function calculateMortgage(principalAmount, downPayment, interestRate, period) {
          return (principalAmount - downPayment) * interestRate / (1 - Math.pow(1 + interestRate, -period));
        }
      }, {
        key: "viewHistoryProperty",
        value: function viewHistoryProperty(id) {
          var _this7 = this;

          console.log(id);
          this.map.off(); // this.map.remove();

          setTimeout(function () {
            _this7.router.navigate(['property-detail/' + id]);
          }, 500);
        }
      }]);

      return PropertyDetailPage;
    }();

    PropertyDetailPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
      }, {
        type: _property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatBottomSheet"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"]
      }];
    };

    PropertyDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-property-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./property-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/property-detail.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({
        opacity: 0,
        transform: "translate3d(0,10px,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])('600ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./property-detail.page.scss */
      "./src/app/pages/property-detail/property-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatBottomSheet"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateService"]])], PropertyDetailPage);

    var PreApprovedMe = /*#__PURE__*/function () {
      function PreApprovedMe(data, _bottomSheetRef, formBuilder, toastCtrl, loadingCtrl, detailService) {
        _classCallCheck(this, PreApprovedMe);

        this.data = data;
        this._bottomSheetRef = _bottomSheetRef;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.detailService = detailService;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        this._bottomSheetRef.disableClose = false;
        this.propertyId = data;
      }

      _createClass(PreApprovedMe, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.preApprovedMeDetailForm();
        }
      }, {
        key: "preApprovedMeDetailForm",
        value: function preApprovedMeDetailForm() {
          this.preApprovedMeForm = this.formBuilder.group({
            name: '',
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(this.emailReg)]),
            contactNumber: '',
            message: '',
            propertyId: this.propertyId
          });
        }
      }, {
        key: "openLink",
        value: function openLink() {
          this._bottomSheetRef.dismiss();
        }
      }, {
        key: "preApprovedMe",
        value: function preApprovedMe(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
            var _this8 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee12$(_context12) {
              while (1) {
                switch (_context12.prev = _context12.next) {
                  case 0:
                    _context12.next = 2;
                    return this.loadingCtrl.create({
                      message: "Please wait..."
                    });

                  case 2:
                    loading = _context12.sent;
                    _context12.next = 5;
                    return loading.present();

                  case 5:
                    this.detailService.preApproved(value).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this8, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
                        return regeneratorRuntime.wrap(function _callee11$(_context11) {
                          while (1) {
                            switch (_context11.prev = _context11.next) {
                              case 0:
                                _context11.next = 2;
                                return loading.dismiss();

                              case 2:
                                if (res.code == 200) {
                                  this.presentToast('Request Sent Successfully');
                                } else {
                                  this.presentToast('Something went wrong');
                                }

                                this._bottomSheetRef.dismiss();

                              case 4:
                              case "end":
                                return _context11.stop();
                            }
                          }
                        }, _callee11, this);
                      }));
                    }, function (err) {
                      loading.dismiss();
                      console.log(err);
                    });

                  case 6:
                  case "end":
                    return _context12.stop();
                }
              }
            }, _callee12, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
            var toast;
            return regeneratorRuntime.wrap(function _callee13$(_context13) {
              while (1) {
                switch (_context13.prev = _context13.next) {
                  case 0:
                    _context13.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context13.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context13.stop();
                }
              }
            }, _callee13, this);
          }));
        }
      }]);

      return PreApprovedMe;
    }();

    PreApprovedMe.ctorParameters = function () {
      return [{
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
          args: [_angular_material__WEBPACK_IMPORTED_MODULE_7__["MAT_BOTTOM_SHEET_DATA"]]
        }]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatBottomSheetRef"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"]
      }];
    };

    PreApprovedMe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'pre-approved-me',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./pre-approved-me.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/pre-approved-me.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./pre-approved-me.scss */
      "./src/app/pages/property-detail/pre-approved-me.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_7__["MAT_BOTTOM_SHEET_DATA"])), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatBottomSheetRef"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"]])], PreApprovedMe);

    var ScheduleVisitBottom = /*#__PURE__*/function () {
      function ScheduleVisitBottom(data, _bottomSheetRef, formBuilder, toastCtrl, loadingCtrl, detailService) {
        _classCallCheck(this, ScheduleVisitBottom);

        this.data = data;
        this._bottomSheetRef = _bottomSheetRef;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.detailService = detailService;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        this._bottomSheetRef.disableClose = false;
        this.propertyId = data;
      }

      _createClass(ScheduleVisitBottom, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.scheduleVisitDetailForm();
        }
      }, {
        key: "scheduleVisitDetailForm",
        value: function scheduleVisitDetailForm() {
          this.scheduleVisitForm = this.formBuilder.group({
            name: '',
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern(this.emailReg)]),
            contactNumber: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
            message: '',
            propertyId: this.propertyId
          });
        }
      }, {
        key: "openLink",
        value: function openLink() {
          this._bottomSheetRef.dismiss();
        }
      }, {
        key: "scheduleVisit",
        value: function scheduleVisit(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
            var _this9 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee15$(_context15) {
              while (1) {
                switch (_context15.prev = _context15.next) {
                  case 0:
                    _context15.next = 2;
                    return this.loadingCtrl.create({
                      message: "Please wait..."
                    });

                  case 2:
                    loading = _context15.sent;
                    _context15.next = 5;
                    return loading.present();

                  case 5:
                    this.detailService.scheduleVisit(value).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this9, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
                        return regeneratorRuntime.wrap(function _callee14$(_context14) {
                          while (1) {
                            switch (_context14.prev = _context14.next) {
                              case 0:
                                _context14.next = 2;
                                return loading.dismiss();

                              case 2:
                                this.presentToast('Request Sent Successfully');

                                this._bottomSheetRef.dismiss();

                              case 4:
                              case "end":
                                return _context14.stop();
                            }
                          }
                        }, _callee14, this);
                      }));
                    }, function (err) {
                      loading.dismiss();
                      console.log(err);
                    });

                  case 6:
                  case "end":
                    return _context15.stop();
                }
              }
            }, _callee15, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
            var toast;
            return regeneratorRuntime.wrap(function _callee16$(_context16) {
              while (1) {
                switch (_context16.prev = _context16.next) {
                  case 0:
                    _context16.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context16.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context16.stop();
                }
              }
            }, _callee16, this);
          }));
        }
      }]);

      return ScheduleVisitBottom;
    }();

    ScheduleVisitBottom.ctorParameters = function () {
      return [{
        type: undefined,
        decorators: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
          args: [_angular_material__WEBPACK_IMPORTED_MODULE_7__["MAT_BOTTOM_SHEET_DATA"]]
        }]
      }, {
        type: _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatBottomSheetRef"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"]
      }];
    };

    ScheduleVisitBottom = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'schedule-visit-bottom',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./schedule-visit-bottom.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/schedule-visit-bottom.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./schedule-visit-bottom.scss */
      "./src/app/pages/property-detail/schedule-visit-bottom.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_7__["MAT_BOTTOM_SHEET_DATA"])), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatBottomSheetRef"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _property_detail_service__WEBPACK_IMPORTED_MODULE_11__["PropertyDetailService"]])], ScheduleVisitBottom);
    /***/
  },

  /***/
  "./src/app/pages/property-detail/property-detail.service.ts":
  /*!******************************************************************!*\
    !*** ./src/app/pages/property-detail/property-detail.service.ts ***!
    \******************************************************************/

  /*! exports provided: PropertyDetailService */

  /***/
  function srcAppPagesPropertyDetailPropertyDetailServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PropertyDetailService", function () {
      return PropertyDetailService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../api.service */
    "./src/app/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var PropertyDetailService = /*#__PURE__*/function () {
      function PropertyDetailService(http, apiService) {
        _classCallCheck(this, PropertyDetailService);

        this.http = http;
        this.apiService = apiService;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
          headers: headers
        });
      }

      _createClass(PropertyDetailService, [{
        key: "getPropertyDetails",
        value: function getPropertyDetails(propertyId) {
          return this.http.get(this.apiService.config + 'property/' + propertyId, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }, {
        key: "addToFavorite",
        value: function addToFavorite(data) {
          return this.http.post(this.apiService.config + 'favorite/', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }, {
        key: "subscribeDailyNewProperties",
        value: function subscribeDailyNewProperties(data) {
          return this.http.post(this.apiService.config + 'subscription/', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }, {
        key: "getRateOfInterest",
        value: function getRateOfInterest() {
          return this.http.get(this.apiService.config + 'tax/RateOfInterest', this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }, {
        key: "preApproved",
        value: function preApproved(data) {
          return this.http.post(this.apiService.config + 'mortgage/', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }, {
        key: "scheduleVisit",
        value: function scheduleVisit(data) {
          return this.http.post(this.apiService.config + 'scheduleVisit/', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }]);

      return PropertyDetailService;
    }();

    PropertyDetailService.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }, {
        type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]
      }];
    };

    PropertyDetailService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])], PropertyDetailService);
    /***/
  },

  /***/
  "./src/app/pages/property-detail/schedule-visit-bottom.scss":
  /*!******************************************************************!*\
    !*** ./src/app/pages/property-detail/schedule-visit-bottom.scss ***!
    \******************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesPropertyDetailScheduleVisitBottomScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "form {\n  width: 100%;\n  text-align: center;\n  font-size: 14px;\n}\n\n.mat-expansion-panel-header-title {\n  color: #101010 !important;\n}\n\n.expansion-header {\n  height: 50px !important;\n  padding: 0px 10px !important;\n}\n\n.panelTitle {\n  margin-top: 6px;\n  font-size: 16px;\n}\n\n.fieldFormat {\n  width: 90%;\n}\n\n.submit-btn {\n  width: 80%;\n  margin: 16px auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3NjaGVkdWxlLXZpc2l0LWJvdHRvbS5zY3NzIiwic3JjL2FwcC9wYWdlcy9wcm9wZXJ0eS1kZXRhaWwvc2NoZWR1bGUtdmlzaXQtYm90dG9tLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FDQ0o7O0FERUE7RUFDSSx5QkFBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7RUFDQSw0QkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3NjaGVkdWxlLXZpc2l0LWJvdHRvbS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZm9ybSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlIHtcbiAgICBjb2xvcjogIzEwMTAxMCAhaW1wb3J0YW50O1xufVxuXG4uZXhwYW5zaW9uLWhlYWRlciB7XG4gICAgaGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZzogMHB4IDEwcHggIWltcG9ydGFudDtcbn1cblxuLnBhbmVsVGl0bGUge1xuICAgIG1hcmdpbi10b3A6IDZweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG59XG5cbi5maWVsZEZvcm1hdCB7XG4gICAgd2lkdGg6IDkwJTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luOiAxNnB4IGF1dG87XG59IiwiZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlIHtcbiAgY29sb3I6ICMxMDEwMTAgIWltcG9ydGFudDtcbn1cblxuLmV4cGFuc2lvbi1oZWFkZXIge1xuICBoZWlnaHQ6IDUwcHggIWltcG9ydGFudDtcbiAgcGFkZGluZzogMHB4IDEwcHggIWltcG9ydGFudDtcbn1cblxuLnBhbmVsVGl0bGUge1xuICBtYXJnaW4tdG9wOiA2cHg7XG4gIGZvbnQtc2l6ZTogMTZweDtcbn1cblxuLmZpZWxkRm9ybWF0IHtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICB3aWR0aDogODAlO1xuICBtYXJnaW46IDE2cHggYXV0bztcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.scss":
  /*!*******************************************************************************************!*\
    !*** ./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.scss ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesPropertyDetailSubscribeNewsletterSubscribeNewsletterPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".row-div {\n  padding: 10px;\n  border-bottom: 1px solid #ccc;\n}\n\n.room-row {\n  font-size: 12px;\n  margin-top: 5px;\n}\n\n.activeFilter {\n  background: #e01a27;\n  border: 1px solid #888;\n  color: #fff;\n  text-align: center;\n}\n\n.room-col {\n  text-align: center;\n  border: 1px solid #888;\n}\n\n.fieldformat {\n  width: 50%;\n}\n\n.mat-expansion-panel-header-title {\n  color: #101010 !important;\n}\n\n.expansion-header {\n  height: 50px !important;\n  padding: 0px 10px !important;\n}\n\n.btn-margin {\n  text-align: center;\n  padding: 5px 0px;\n}\n\n.button-format {\n  text-transform: capitalize;\n  letter-spacing: 0px;\n  width: 40%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3N1YnNjcmliZS1uZXdzbGV0dGVyL3N1YnNjcmliZS1uZXdzbGV0dGVyLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3N1YnNjcmliZS1uZXdzbGV0dGVyL3N1YnNjcmliZS1uZXdzbGV0dGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7RUFDQSw2QkFBQTtBQ0NKOztBREVBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7QUNDSjs7QURFQTtFQUNJLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7QUNDSjs7QURFQTtFQUNJLFVBQUE7QUNDSjs7QURFQTtFQUNJLHlCQUFBO0FDQ0o7O0FERUE7RUFDSSx1QkFBQTtFQUNBLDRCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FDQ0o7O0FERUE7RUFDSSwwQkFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJvcGVydHktZGV0YWlsL3N1YnNjcmliZS1uZXdzbGV0dGVyL3N1YnNjcmliZS1uZXdzbGV0dGVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yb3ctZGl2IHtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xufVxuXG4ucm9vbS1yb3cge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG59XG5cbi5hY3RpdmVGaWx0ZXIge1xuICAgIGJhY2tncm91bmQ6ICNlMDFhMjc7XG4gICAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5yb29tLWNvbCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICM4ODg7XG59XG5cbi5maWVsZGZvcm1hdCB7XG4gICAgd2lkdGg6IDUwJTtcbn1cblxuLm1hdC1leHBhbnNpb24tcGFuZWwtaGVhZGVyLXRpdGxlIHtcbiAgICBjb2xvcjogIzEwMTAxMCAhaW1wb3J0YW50O1xufVxuXG4uZXhwYW5zaW9uLWhlYWRlciB7XG4gICAgaGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZzogMHB4IDEwcHggIWltcG9ydGFudDtcbn1cblxuLmJ0bi1tYXJnaW4ge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBwYWRkaW5nOiA1cHggMHB4O1xufVxuXG4uYnV0dG9uLWZvcm1hdCB7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgICB3aWR0aDogNDAlO1xufSIsIi5yb3ctZGl2IHtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XG59XG5cbi5yb29tLXJvdyB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG4uYWN0aXZlRmlsdGVyIHtcbiAgYmFja2dyb3VuZDogI2UwMWEyNztcbiAgYm9yZGVyOiAxcHggc29saWQgIzg4ODtcbiAgY29sb3I6ICNmZmY7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJvb20tY29sIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjODg4O1xufVxuXG4uZmllbGRmb3JtYXQge1xuICB3aWR0aDogNTAlO1xufVxuXG4ubWF0LWV4cGFuc2lvbi1wYW5lbC1oZWFkZXItdGl0bGUge1xuICBjb2xvcjogIzEwMTAxMCAhaW1wb3J0YW50O1xufVxuXG4uZXhwYW5zaW9uLWhlYWRlciB7XG4gIGhlaWdodDogNTBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAwcHggMTBweCAhaW1wb3J0YW50O1xufVxuXG4uYnRuLW1hcmdpbiB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZzogNXB4IDBweDtcbn1cblxuLmJ1dHRvbi1mb3JtYXQge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgbGV0dGVyLXNwYWNpbmc6IDBweDtcbiAgd2lkdGg6IDQwJTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.ts":
  /*!*****************************************************************************************!*\
    !*** ./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.ts ***!
    \*****************************************************************************************/

  /*! exports provided: SubscribeNewsletterPage */

  /***/
  function srcAppPagesPropertyDetailSubscribeNewsletterSubscribeNewsletterPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SubscribeNewsletterPage", function () {
      return SubscribeNewsletterPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _property_detail_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../property-detail.service */
    "./src/app/pages/property-detail/property-detail.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var src_app_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/data.service */
    "./src/app/data.service.ts");

    var SubscribeNewsletterPage = /*#__PURE__*/function () {
      function SubscribeNewsletterPage(router, loadingCtrl, formBuilder, toastCtrl, leadGenerationService, data) {
        _classCallCheck(this, SubscribeNewsletterPage);

        this.router = router;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.leadGenerationService = leadGenerationService;
        this.data = data;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        this.basementArray = [];
        this.propertyTypesArray = [];
        this.minPriceList = [];
        this.maxPriceList = [];
        this.minSquareFootageList = [];
        this.maxSquareFootageList = [];
        this.saleFlag = true;
        this.residentialFlag = true;
        this.saleOrLease = 'Sale';
        this.propertyType = 'residential';
        this.bedRooms = [];
        this.bathRoom = '';
        this.garageParking = '';
        this.bedroomAny = true;
        this.bedroom1 = false;
        this.bedroom2 = false;
        this.bedroom3 = false;
        this.bedroom4 = false;
        this.bedroom5 = false;
        this.bathroomAny = true;
        this.bathroom1 = false;
        this.bathroom2 = false;
        this.bathroom3 = false;
        this.bathroom4 = false;
        this.bathroom5 = false;
        this.gpAny = true;
        this.gp1 = false;
        this.gp2 = false;
        this.gp3 = false;
        this.gp4 = false;
        this.gp5 = false;
        this.detailFormFlag = true;
      }

      _createClass(SubscribeNewsletterPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.leadGenerationFormDetails();
          this.getBasementTypes();
          this.getMinPrices();
          this.getMaxPrices();
          this.getMinSquareFootage();
          this.getMaxSquareFootage();
          this.getPropertyTypes('res');
        }
      }, {
        key: "leadGenerationFormDetails",
        value: function leadGenerationFormDetails() {
          this.leadGenerationForm = this.formBuilder.group({
            name: '',
            phone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].pattern(this.emailReg)]),
            address: '',
            propertyOptions: '',
            basementStyle: '',
            listPriceMin: null,
            listPriceMax: null,
            approxSquareFootageMin: null,
            approxSquareFootageMax: null,
            message: '',
            areaRadius: '1'
          });
        }
      }, {
        key: "showMore",
        value: function showMore() {
          if (this.detailFormFlag) {
            this.detailFormFlag = false;
          } else {
            this.detailFormFlag = true;
          }
        }
      }, {
        key: "subscribeToDailyNews",
        value: function subscribeToDailyNews(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
            var _this10 = this;

            var loading, subscriptionData;
            return regeneratorRuntime.wrap(function _callee18$(_context18) {
              while (1) {
                switch (_context18.prev = _context18.next) {
                  case 0:
                    _context18.next = 2;
                    return this.loadingCtrl.create({
                      message: "Please wait..."
                    });

                  case 2:
                    loading = _context18.sent;
                    _context18.next = 5;
                    return loading.present();

                  case 5:
                    subscriptionData = {
                      name: value.name,
                      email: value.email,
                      phone: value.phone,
                      address: value.address,
                      basementStyle: value.basementStyle,
                      message: value.message,
                      propertyType: this.propertyType,
                      propertyOption: value.propertyOptions,
                      saleLease: this.saleOrLease,
                      bedRooms: this.bedRooms,
                      washrooms: this.bathRoom,
                      parkingSpaces: this.garageParking,
                      areaRadius: value.areaRadius,
                      areaMin: value.approxSquareFootageMin,
                      areaMax: value.approxSquareFootageMax,
                      priceMin: value.listPriceMin,
                      priceMax: value.listPriceMax,
                      isActive: true
                    };
                    this.leadGenerationService.subscribeDailyNewProperties(subscriptionData).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this10, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
                        return regeneratorRuntime.wrap(function _callee17$(_context17) {
                          while (1) {
                            switch (_context17.prev = _context17.next) {
                              case 0:
                                _context17.next = 2;
                                return loading.dismiss();

                              case 2:
                                if (res.code == 200) {
                                  this.presentToast('Subscription Added Successfully');
                                  this.router.navigateByUrl('/home-results');
                                } else {
                                  this.presentToast('Something went wrong');
                                }

                              case 3:
                              case "end":
                                return _context17.stop();
                            }
                          }
                        }, _callee17, this);
                      }));
                    }, function (err) {
                      loading.dismiss();

                      _this10.presentToast('Something went wrong');
                    });

                  case 7:
                  case "end":
                    return _context18.stop();
                }
              }
            }, _callee18, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
            var toast;
            return regeneratorRuntime.wrap(function _callee19$(_context19) {
              while (1) {
                switch (_context19.prev = _context19.next) {
                  case 0:
                    _context19.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context19.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context19.stop();
                }
              }
            }, _callee19, this);
          }));
        }
      }, {
        key: "saleLease",
        value: function saleLease(value) {
          if (value == 'lease') {
            this.saleFlag = false;
            this.saleOrLease = 'Lease';
          } else {
            this.saleFlag = true;
            this.saleOrLease = 'Sale';
          }
        }
      }, {
        key: "propertyTypeChange",
        value: function propertyTypeChange(value) {
          if (value == 'commercial') {
            this.getPropertyTypes('commer');
            this.residentialFlag = false;
            this.propertyType = 'commercial';
            this.bedRooms = [];
            this.bathRoom = "";
            this.garageParking = "";
          } else {
            this.getPropertyTypes('res');
            this.residentialFlag = true;
            this.propertyType = 'residential';
          }
        }
      }, {
        key: "bedroom",
        value: function bedroom(value) {
          this.bedroomAny = false;

          if (value === 'any') {
            this.bedroom1 = false;
            this.bedroom2 = false;
            this.bedroom3 = false;
            this.bedroom4 = false;
            this.bedroom5 = false;
            this.bedroomAny = true;
            this.bedRooms = [];
          } else if (value === '1') {
            if (this.bedroom1) {
              if (this.bedRooms.includes("1")) {
                var index = this.bedRooms.indexOf('1');

                if (index > -1) {
                  this.bedRooms.splice(index, 1);
                }
              }
            } else {
              this.bedRooms.push("1");
            }

            if (this.bedroom5 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
              this.bedroom1 = false;
              this.bedroom2 = false;
              this.bedroom3 = false;
              this.bedroom4 = false;
              this.bedroom5 = false;
              this.bedroomAny = true;
              this.bedRooms = [];
            } else {
              if (this.bedroom1) {
                if (!this.bedroom2 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
                  this.bedroom1 = false;
                  this.bedroomAny = true;
                } else {
                  this.bedroom1 = false;
                }
              } else {
                this.bedroom1 = true;
              }
            }
          } else if (value === '2') {
            if (this.bedroom2) {
              if (this.bedRooms.includes("2")) {
                var _index = this.bedRooms.indexOf('2');

                if (_index > -1) {
                  this.bedRooms.splice(_index, 1);
                }
              }
            } else {
              this.bedRooms.push("2");
            }

            if (this.bedroom1 && this.bedroom5 && this.bedroom3 && this.bedroom4) {
              this.bedroom1 = false;
              this.bedroom2 = false;
              this.bedroom3 = false;
              this.bedroom4 = false;
              this.bedroom5 = false;
              this.bedroomAny = true;
              this.bedRooms = [];
            } else {
              if (this.bedroom2) {
                if (!this.bedroom1 && !this.bedroom3 && !this.bedroom4 && !this.bedroom5) {
                  this.bedroom2 = false;
                  this.bedroomAny = true;
                } else {
                  this.bedroom2 = false;
                }
              } else {
                this.bedroom2 = true;
              }
            }
          } else if (value === '3') {
            if (this.bedroom3) {
              if (this.bedRooms.includes("3")) {
                var _index2 = this.bedRooms.indexOf('3');

                if (_index2 > -1) {
                  this.bedRooms.splice(_index2, 1);
                }
              }
            } else {
              this.bedRooms.push("3");
            }

            if (this.bedroom1 && this.bedroom2 && this.bedroom5 && this.bedroom4) {
              this.bedroom1 = false;
              this.bedroom2 = false;
              this.bedroom3 = false;
              this.bedroom4 = false;
              this.bedroom5 = false;
              this.bedroomAny = true;
              this.bedRooms = [];
            } else {
              if (this.bedroom3) {
                if (!this.bedroom1 && !this.bedroom2 && !this.bedroom4 && !this.bedroom5) {
                  this.bedroom3 = false;
                  this.bedroomAny = true;
                } else {
                  this.bedroom3 = false;
                }
              } else {
                this.bedroom3 = true;
              }
            }
          } else if (value === '4') {
            if (this.bedroom4) {
              if (this.bedRooms.includes("4")) {
                var _index3 = this.bedRooms.indexOf('4');

                if (_index3 > -1) {
                  this.bedRooms.splice(_index3, 1);
                }
              }
            } else {
              this.bedRooms.push("4");
            }

            if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom5) {
              this.bedroom1 = false;
              this.bedroom2 = false;
              this.bedroom3 = false;
              this.bedroom4 = false;
              this.bedroom5 = false;
              this.bedroomAny = true;
              this.bedRooms = [];
            } else {
              if (this.bedroom4) {
                if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom5) {
                  this.bedroom4 = false;
                  this.bedroomAny = true;
                } else {
                  this.bedroom4 = false;
                }
              } else {
                this.bedroom4 = true;
              }
            }
          } else if (value === '5') {
            if (this.bedroom5) {
              if (this.bedRooms.includes("5+")) {
                var _index4 = this.bedRooms.indexOf('5+');

                if (_index4 > -1) {
                  this.bedRooms.splice(_index4, 1);
                }
              }
            } else {
              this.bedRooms.push("5+");
            }

            if (this.bedroom1 && this.bedroom2 && this.bedroom3 && this.bedroom4) {
              this.bedroom1 = false;
              this.bedroom2 = false;
              this.bedroom3 = false;
              this.bedroom4 = false;
              this.bedroom5 = false;
              this.bedroomAny = true;
              this.bedRooms = [];
            } else {
              if (this.bedroom5) {
                if (!this.bedroom1 && !this.bedroom2 && !this.bedroom3 && !this.bedroom4) {
                  this.bedroom5 = false;
                  this.bedroomAny = true;
                } else {
                  this.bedroom5 = false;
                }
              } else {
                this.bedroom5 = true;
              }
            }
          }
        }
      }, {
        key: "bathroom",
        value: function bathroom(value) {
          this.bathroom1 = false;
          this.bathroom2 = false;
          this.bathroom3 = false;
          this.bathroom4 = false;
          this.bathroom5 = false;
          this.bathroomAny = false;

          if (value === 'any') {
            this.bathroomAny = true;
            this.bathRoom = '';
          } else if (value === '1') {
            this.bathroom1 = true;
            this.bathRoom = '1';
          } else if (value === '2') {
            this.bathroom2 = true;
            this.bathRoom = '2';
          } else if (value === '3') {
            this.bathroom3 = true;
            this.bathRoom = '3';
          } else if (value === '4') {
            this.bathroom4 = true;
            this.bathRoom = '4';
          } else if (value === '5') {
            this.bathroom5 = true;
            this.bathRoom = '5';
          }
        }
      }, {
        key: "gp",
        value: function gp(value) {
          this.gp1 = false;
          this.gp2 = false;
          this.gp3 = false;
          this.gp4 = false;
          this.gp5 = false;
          this.gpAny = false;

          if (value === 'any') {
            this.gpAny = true;
            this.garageParking = '';
          } else if (value === '1') {
            this.gp1 = true;
            this.garageParking = '1';
          } else if (value === '2') {
            this.gp2 = true;
            this.garageParking = '2';
          } else if (value === '3') {
            this.gp3 = true;
            this.garageParking = '3';
          } else if (value === '4') {
            this.gp4 = true;
            this.garageParking = '4';
          } else if (value === '5') {
            this.gp5 = true;
            this.garageParking = '5';
          }
        }
      }, {
        key: "getPropertyTypes",
        value: function getPropertyTypes(type) {
          this.propertyTypesArray = [];

          if (type == 'res') {
            this.propertyTypesArray = [{
              id: 1,
              name: 'Condo Apt'
            }, {
              id: 2,
              name: 'Condo Townhouse'
            }, {
              id: 3,
              name: 'Detached'
            }, {
              id: 4,
              name: 'Semi-Detached'
            }, {
              id: 5,
              name: 'Vacant Land'
            }];
          } else {
            this.propertyTypesArray = [{
              id: 1,
              name: 'Business'
            }, {
              id: 2,
              name: 'Office'
            }, {
              id: 3,
              name: 'All'
            }];
          }
        }
      }, {
        key: "getBasementTypes",
        value: function getBasementTypes() {
          this.basementArray = this.data.getBasements();
        }
      }, {
        key: "getMinPrices",
        value: function getMinPrices() {
          this.minPriceList = this.data.getMinPrices();
        }
      }, {
        key: "getMaxPrices",
        value: function getMaxPrices() {
          this.maxPriceList = this.data.getMaxPrices();
        }
      }, {
        key: "getMinSquareFootage",
        value: function getMinSquareFootage() {
          this.minSquareFootageList = this.data.getMinSquareFootage();
        }
      }, {
        key: "getMaxSquareFootage",
        value: function getMaxSquareFootage() {
          this.maxSquareFootageList = this.data.getMaxSquareFootage();
        }
      }]);

      return SubscribeNewsletterPage;
    }();

    SubscribeNewsletterPage.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _property_detail_service__WEBPACK_IMPORTED_MODULE_4__["PropertyDetailService"]
      }, {
        type: src_app_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"]
      }];
    };

    SubscribeNewsletterPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'subscribe-newsletter',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./subscribe-newsletter.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./subscribe-newsletter.page.scss */
      "./src/app/pages/property-detail/subscribe-newsletter/subscribe-newsletter.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _property_detail_service__WEBPACK_IMPORTED_MODULE_4__["PropertyDetailService"], src_app_data_service__WEBPACK_IMPORTED_MODULE_6__["DataService"]])], SubscribeNewsletterPage);
    /***/
  }
}]);
//# sourceMappingURL=pages-property-detail-property-detail-module-es5.js.map