(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-resetpwd-resetpwd-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/resetpwd/resetpwd.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/resetpwd/resetpwd.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding animated fadeIn login\">\n\n\t<div class=\"animated fadeInDown ion-text-center ion-padding-horizontal\">\n\t\t<div class=\"login-logo\"></div>\n\t</div>\n\n\t<!-- Reset form -->\n\t<div class=\"title\">{{ 'app.resetYourPassword' | translate }}</div>\n\n\t<form id=\"resetpwd\" [formGroup]=\"resetForm\" (ngSubmit)=\"resetPassword(resetForm.value)\">\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.email' | translate }}</mat-label>\n\t\t\t<input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n\t\t\t<mat-icon matSuffix>mail_outline</mat-icon>\n\t\t\t<mat-error *ngIf=\"resetForm.get('email').hasError('required')\">\n\t\t\t\t{{ 'register.emailRequired' | translate }}\n\t\t\t</mat-error>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.password' | translate }}</mat-label>\n\t\t\t<input matInput type=\"password\" autocomplete=\"off\" required formControlName=\"password\">\n\t\t\t<mat-icon matSuffix>vpn_key</mat-icon>\n\t\t\t<mat-error>{{ 'register.passwordRequired' | translate }}</mat-error>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'register.confirmPassword' | translate }}</mat-label>\n\t\t\t<input matInput type=\"password\" autocomplete=\"off\" required formControlName=\"confirmPassword\">\n\t\t\t<mat-icon matSuffix>vpn_key</mat-icon>\n\t\t\t<mat-error *ngIf=\"resetForm.get('confirmPassword').hasError('required')\">\n\t\t\t\t{{ 'register.confirmPasswordRequired' | translate }}\n\t\t\t</mat-error>\n\t\t\t<mat-error *ngIf=\"!resetForm.get('confirmPassword').hasError('required') &&\n\t\t\t\t\t\t\t   resetForm.get('confirmPassword').hasError('passwordsNotMatching')\">\n\t\t\t\t{{ 'register.passwordMatch' | translate }}\n\t\t\t</mat-error>\n\t\t</mat-form-field>\n\n\t\t<ion-button type=\"submit\" class=\"submit-btn\" color=\"danger\" [disabled]=\"resetForm.invalid\">\n\t\t\t{{ 'resetPassword.resetMyPassword' | translate }}\n\t\t</ion-button>\n\n\t\t<!-- <div class=\"already-account\">\n\t\t\t<a class=\"loginLink\" (click)=\"login()\">Go back to login</a>\n\t\t</div> -->\n\t</form>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/resetpwd/resetpwd.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/resetpwd/resetpwd.module.ts ***!
  \***************************************************/
/*! exports provided: ResetpwdPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpwdPageModule", function() { return ResetpwdPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _resetpwd_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./resetpwd.page */ "./src/app/pages/resetpwd/resetpwd.page.ts");
/* harmony import */ var _resetpwd_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./resetpwd.service */ "./src/app/pages/resetpwd/resetpwd.service.ts");












const routes = [
    {
        path: '',
        component: _resetpwd_page__WEBPACK_IMPORTED_MODULE_10__["ResetpwdPage"]
    }
];
let ResetpwdPageModule = class ResetpwdPageModule {
};
ResetpwdPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_9__["TranslateModule"].forChild()
        ],
        declarations: [
            _resetpwd_page__WEBPACK_IMPORTED_MODULE_10__["ResetpwdPage"]
        ],
        providers: [
            _resetpwd_service__WEBPACK_IMPORTED_MODULE_11__["ResetpwdService"]
        ]
    })
], ResetpwdPageModule);



/***/ }),

/***/ "./src/app/pages/resetpwd/resetpwd.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/resetpwd/resetpwd.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  text-align: center !important;\n  padding: 24px !important;\n}\n:host form {\n  width: 100%;\n  padding-top: 32px;\n  font-size: 14px;\n}\n.login-logo {\n  height: 120px;\n  width: 128px;\n  margin: 32px auto;\n  background: url(\"/assets/img/logo.png\") no-repeat;\n  background-size: 100%;\n}\n.title {\n  font-size: 21px;\n  text-align: center;\n  font-weight: 400 !important;\n}\n.fieldFormat {\n  width: 90%;\n}\n.loginLink {\n  font-size: 13px;\n  font-weight: 500;\n  margin-bottom: 5px;\n  color: #039be5;\n}\n.submit-btn {\n  width: 80%;\n  margin: 16px auto;\n}\n.already-account {\n  flex-direction: column;\n  box-sizing: border-box;\n  display: flex;\n  place-content: center;\n  align-items: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvcmVzZXRwd2QvcmVzZXRwd2QucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9yZXNldHB3ZC9yZXNldHB3ZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSw2QkFBQTtFQUNBLHdCQUFBO0FDQVI7QURHSTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNEUjtBREtBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGlEQUFBO0VBQ0EscUJBQUE7QUNGSjtBREtBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7QUNGSjtBREtBO0VBQ0ksVUFBQTtBQ0ZKO0FES0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUNGSjtBREtBO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0FDRko7QURLQTtFQUNJLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxhQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtBQ0ZKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVzZXRwd2QvcmVzZXRwd2QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGlvbi1jb250ZW50IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgICAgIHBhZGRpbmc6IDI0cHggIWltcG9ydGFudDtcbiAgICB9XG5cbiAgICBmb3JtIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmctdG9wOiAzMnB4O1xuICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgfVxufVxuXG4ubG9naW4tbG9nbyB7XG4gICAgaGVpZ2h0OiAxMjBweDtcbiAgICB3aWR0aDogMTI4cHg7XG4gICAgbWFyZ2luOiAzMnB4IGF1dG87XG4gICAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvbG9nby5wbmdcIikgbm8tcmVwZWF0O1xuICAgIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcbn1cblxuLnRpdGxlIHtcbiAgICBmb250LXNpemU6IDIxcHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbn1cblxuLmZpZWxkRm9ybWF0IHtcbiAgICB3aWR0aDogOTAlO1xufVxuXG4ubG9naW5MaW5rIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gICAgY29sb3I6ICMwMzliZTU7XG59XG5cbi5zdWJtaXQtYnRuIHtcbiAgICB3aWR0aDogODAlO1xuICAgIG1hcmdpbjogMTZweCBhdXRvO1xufVxuXG4uYWxyZWFkeS1hY2NvdW50IHtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn0iLCI6aG9zdCBpb24tY29udGVudCB7XG4gIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAyNHB4ICFpbXBvcnRhbnQ7XG59XG46aG9zdCBmb3JtIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAzMnB4O1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5sb2dpbi1sb2dvIHtcbiAgaGVpZ2h0OiAxMjBweDtcbiAgd2lkdGg6IDEyOHB4O1xuICBtYXJnaW46IDMycHggYXV0bztcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvbG9nby5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG59XG5cbi50aXRsZSB7XG4gIGZvbnQtc2l6ZTogMjFweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogNDAwICFpbXBvcnRhbnQ7XG59XG5cbi5maWVsZEZvcm1hdCB7XG4gIHdpZHRoOiA5MCU7XG59XG5cbi5sb2dpbkxpbmsge1xuICBmb250LXNpemU6IDEzcHg7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgY29sb3I6ICMwMzliZTU7XG59XG5cbi5zdWJtaXQtYnRuIHtcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luOiAxNnB4IGF1dG87XG59XG5cbi5hbHJlYWR5LWFjY291bnQge1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBkaXNwbGF5OiBmbGV4O1xuICBwbGFjZS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/resetpwd/resetpwd.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/resetpwd/resetpwd.page.ts ***!
  \*************************************************/
/*! exports provided: ResetpwdPage, confirmPasswordValidator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpwdPage", function() { return ResetpwdPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "confirmPasswordValidator", function() { return confirmPasswordValidator; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _resetpwd_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./resetpwd.service */ "./src/app/pages/resetpwd/resetpwd.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");







let ResetpwdPage = class ResetpwdPage {
    constructor(navCtrl, menuCtrl, loadingCtrl, toastCtrl, resetService, formBuilder) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.resetService = resetService;
        this.formBuilder = formBuilder;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        this._unsubscribeAll = new rxjs__WEBPACK_IMPORTED_MODULE_6__["Subject"]();
    }
    ngOnInit() {
        this.registerFormDetails();
    }
    registerFormDetails() {
        this.resetForm = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.emailReg)]),
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            confirmPassword: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, confirmPasswordValidator]]
        });
        this.resetForm.get('password').valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["takeUntil"])(this._unsubscribeAll)).subscribe(() => {
            this.resetForm.get('confirmPassword').updateValueAndValidity();
        });
    }
    resetPassword(value) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: "Loading...",
            });
            yield loading.present();
            this.resetService.resetPassword(value).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield loading.dismiss();
                if (data.code == 200) {
                    this.presentToast('Password Reset Successfully');
                    localStorage.removeItem('userDetails');
                    localStorage.removeItem('modalShown');
                    this.navCtrl.navigateRoot('/login');
                    setTimeout(() => {
                        window.location.reload();
                    }, 300);
                }
                else {
                    this.presentToast('Something went wrong');
                }
            }), err => {
                loading.dismiss();
                this.presentToast(err);
            });
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                showCloseButton: true,
                message: message,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    login() {
        this.navCtrl.navigateRoot('/login');
    }
};
ResetpwdPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _resetpwd_service__WEBPACK_IMPORTED_MODULE_5__["ResetpwdService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
ResetpwdPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-resetpwd',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./resetpwd.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/resetpwd/resetpwd.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./resetpwd.page.scss */ "./src/app/pages/resetpwd/resetpwd.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
        _resetpwd_service__WEBPACK_IMPORTED_MODULE_5__["ResetpwdService"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])
], ResetpwdPage);

const confirmPasswordValidator = (control) => {
    if (!control.parent || !control) {
        return null;
    }
    const password = control.parent.get('password');
    const confirmPassword = control.parent.get('confirmPassword');
    if (!password || !confirmPassword) {
        return null;
    }
    if (confirmPassword.value === '') {
        return null;
    }
    if (password.value === confirmPassword.value) {
        return null;
    }
    return { passwordsNotMatching: true };
};


/***/ }),

/***/ "./src/app/pages/resetpwd/resetpwd.service.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/resetpwd/resetpwd.service.ts ***!
  \****************************************************/
/*! exports provided: ResetpwdService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResetpwdService", function() { return ResetpwdService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../api.service */ "./src/app/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let ResetpwdService = class ResetpwdService {
    constructor(http, apiService) {
        this.http = http;
        this.apiService = apiService;
        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
        if (loginDetails != undefined) {
            this.token = loginDetails.data.token;
        }
        const headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
    }
    resetPassword(data) {
        return this.http.post(this.apiService.config + 'user/resetPassword', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
};
ResetpwdService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"] }
];
ResetpwdService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])
], ResetpwdService);



/***/ })

}]);
//# sourceMappingURL=pages-resetpwd-resetpwd-module-es2015.js.map