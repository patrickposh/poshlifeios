function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-forgotpwd-forgotpwd-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgotpwd/forgotpwd.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgotpwd/forgotpwd.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesForgotpwdForgotpwdPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-content class=\"ion-padding animated fadeIn login\">\n\t\n\t<!-- Logo -->\n\t<div class=\"animated fadeInDown ion-text-center ion-padding-horizontal\">\n\t\t<div class=\"login-logo\"></div>\n\t</div>\n\n\t<div class=\"title\">{{ 'app.recoverPassword' | translate }}</div>\n\n\t<form id=\"login\" [formGroup]=\"forgotPwdForm\" (ngSubmit)=\"forgotPassword(forgotPwdForm.value)\">\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'forgot.email' | translate }}</mat-label>\n\t\t\t<input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n\t\t\t<mat-icon matSuffix>mail_outline</mat-icon>\n\t\t</mat-form-field>\n\n\t\t<ion-button type=\"submit\" class=\"submit-btn\" color=\"danger\" [disabled]=\"forgotPwdForm.invalid\">\n\t\t\t{{ 'forgot.resetLink' | translate }}\n\t\t</ion-button>\n\n\t\t<div class=\"back-to-login\">\n\t\t\t<a class=\"loginLink\" (click)=\"login()\">{{ 'forgot.backToLogin' | translate }}</a>\n\t\t</div>\n\t</form>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/forgotpwd/forgotpwd.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/forgotpwd/forgotpwd.module.ts ***!
    \*****************************************************/

  /*! exports provided: ForgotpwdPageModule */

  /***/
  function srcAppPagesForgotpwdForgotpwdModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotpwdPageModule", function () {
      return ForgotpwdPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _forgotpwd_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./forgotpwd.page */
    "./src/app/pages/forgotpwd/forgotpwd.page.ts");
    /* harmony import */


    var _forgotpwd_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./forgotpwd.service */
    "./src/app/pages/forgotpwd/forgotpwd.service.ts");

    var routes = [{
      path: '',
      component: _forgotpwd_page__WEBPACK_IMPORTED_MODULE_10__["ForgotpwdPage"]
    }];

    var ForgotpwdPageModule = function ForgotpwdPageModule() {
      _classCallCheck(this, ForgotpwdPageModule);
    };

    ForgotpwdPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ReactiveFormsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateModule"].forChild(), _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(routes)],
      declarations: [_forgotpwd_page__WEBPACK_IMPORTED_MODULE_10__["ForgotpwdPage"]],
      providers: [_forgotpwd_service__WEBPACK_IMPORTED_MODULE_11__["ForgotpwdService"]]
    })], ForgotpwdPageModule);
    /***/
  },

  /***/
  "./src/app/pages/forgotpwd/forgotpwd.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/pages/forgotpwd/forgotpwd.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesForgotpwdForgotpwdPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  text-align: center !important;\n  padding: 24px !important;\n}\n:host form {\n  width: 100%;\n  padding-top: 32px;\n  font-size: 14px;\n}\n.login-logo {\n  height: 120px;\n  width: 128px;\n  margin: 32px auto;\n  background: url(\"/assets/img/logo.png\") no-repeat;\n  background-size: 100%;\n}\n.title {\n  font-size: 21px;\n  text-align: center;\n  font-weight: 400 !important;\n}\n.fieldFormat {\n  width: 90%;\n}\n.back-to-login {\n  flex-direction: row;\n  box-sizing: border-box;\n  display: flex;\n  place-content: center;\n  align-items: center;\n  margin: 10px auto 10px;\n  width: 250px;\n  font-weight: 400;\n}\n.loginLink {\n  font-size: 13px;\n  font-weight: 500;\n  margin-bottom: 16px;\n  color: #039be5;\n}\n.submit-btn {\n  width: 80%;\n  margin: 16px auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvZm9yZ290cHdkL2ZvcmdvdHB3ZC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2ZvcmdvdHB3ZC9mb3Jnb3Rwd2QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksNkJBQUE7RUFDQSx3QkFBQTtBQ0FSO0FER0k7RUFDSSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FDRFI7QURLQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpREFBQTtFQUNBLHFCQUFBO0FDRko7QURLQTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0FDRko7QURLQTtFQUNJLFVBQUE7QUNGSjtBREtBO0VBQ0ksbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUNGSjtBREtBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FDRko7QURLQTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtBQ0ZKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZm9yZ290cHdkL2ZvcmdvdHB3ZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgICAgICAgcGFkZGluZzogMjRweCAhaW1wb3J0YW50O1xuICAgIH1cblxuICAgIGZvcm0ge1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgcGFkZGluZy10b3A6IDMycHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICB9XG59XG5cbi5sb2dpbi1sb2dvIHtcbiAgICBoZWlnaHQ6IDEyMHB4O1xuICAgIHdpZHRoOiAxMjhweDtcbiAgICBtYXJnaW46IDMycHggYXV0bztcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9sb2dvLnBuZ1wiKSBuby1yZXBlYXQ7XG4gICAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xufVxuXG4udGl0bGUge1xuICAgIGZvbnQtc2l6ZTogMjFweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXIgO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbn1cblxuLmZpZWxkRm9ybWF0IHtcbiAgICB3aWR0aDogOTAlO1xufVxuXG4uYmFjay10by1sb2dpbiB7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcGxhY2UtY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luOiAxMHB4IGF1dG8gMTBweDtcbiAgICB3aWR0aDogMjUwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbn1cblxuLmxvZ2luTGluayB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgICBjb2xvcjogIzAzOWJlNTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICAgIHdpZHRoOiA4MCU7XG4gICAgbWFyZ2luOiAxNnB4IGF1dG87XG59IiwiOmhvc3QgaW9uLWNvbnRlbnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMjRweCAhaW1wb3J0YW50O1xufVxuOmhvc3QgZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogMzJweDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4ubG9naW4tbG9nbyB7XG4gIGhlaWdodDogMTIwcHg7XG4gIHdpZHRoOiAxMjhweDtcbiAgbWFyZ2luOiAzMnB4IGF1dG87XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2xvZ28ucG5nXCIpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xufVxuXG4udGl0bGUge1xuICBmb250LXNpemU6IDIxcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xufVxuXG4uZmllbGRGb3JtYXQge1xuICB3aWR0aDogOTAlO1xufVxuXG4uYmFjay10by1sb2dpbiB7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHBsYWNlLWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luOiAxMHB4IGF1dG8gMTBweDtcbiAgd2lkdGg6IDI1MHB4O1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4ubG9naW5MaW5rIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNTAwO1xuICBtYXJnaW4tYm90dG9tOiAxNnB4O1xuICBjb2xvcjogIzAzOWJlNTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICB3aWR0aDogODAlO1xuICBtYXJnaW46IDE2cHggYXV0bztcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/pages/forgotpwd/forgotpwd.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/forgotpwd/forgotpwd.page.ts ***!
    \***************************************************/

  /*! exports provided: ForgotpwdPage */

  /***/
  function srcAppPagesForgotpwdForgotpwdPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotpwdPage", function () {
      return ForgotpwdPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _forgotpwd_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./forgotpwd.service */
    "./src/app/pages/forgotpwd/forgotpwd.service.ts");

    var ForgotpwdPage = /*#__PURE__*/function () {
      function ForgotpwdPage(navCtrl, menuCtrl, toastCtrl, service, loadingCtrl, formBuilder) {
        _classCallCheck(this, ForgotpwdPage);

        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
      }

      _createClass(ForgotpwdPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.forgotFormDetails();
        }
      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          this.menuCtrl.enable(false);
          this.menuCtrl.swipeEnable(false);
        }
      }, {
        key: "forgotFormDetails",
        value: function forgotFormDetails() {
          this.forgotPwdForm = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.emailReg)])
          });
        }
      }, {
        key: "forgotPassword",
        value: function forgotPassword(value) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.loadingCtrl.create({
                      message: "Loading..."
                    });

                  case 2:
                    loading = _context2.sent;
                    _context2.next = 5;
                    return loading.present();

                  case 5:
                    this.service.forgotPassword(value).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                _context.next = 2;
                                return loading.dismiss();

                              case 2:
                                if (res.code == 200) {
                                  this.presentToast(res.data);
                                  this.navCtrl.navigateRoot('/login');
                                } else {
                                  this.presentToast(res.data);
                                }

                              case 3:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee, this);
                      }));
                    }, function (err) {
                      loading.dismiss();
                    });

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var toast;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context3.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "login",
        value: function login() {
          this.navCtrl.navigateRoot('/login');
        }
      }]);

      return ForgotpwdPage;
    }();

    ForgotpwdPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _forgotpwd_service__WEBPACK_IMPORTED_MODULE_4__["ForgotpwdService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }];
    };

    ForgotpwdPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-forgotpwd',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./forgotpwd.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/forgotpwd/forgotpwd.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./forgotpwd.page.scss */
      "./src/app/pages/forgotpwd/forgotpwd.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _forgotpwd_service__WEBPACK_IMPORTED_MODULE_4__["ForgotpwdService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]])], ForgotpwdPage);
    /***/
  },

  /***/
  "./src/app/pages/forgotpwd/forgotpwd.service.ts":
  /*!******************************************************!*\
    !*** ./src/app/pages/forgotpwd/forgotpwd.service.ts ***!
    \******************************************************/

  /*! exports provided: ForgotpwdService */

  /***/
  function srcAppPagesForgotpwdForgotpwdServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ForgotpwdService", function () {
      return ForgotpwdService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../api.service */
    "./src/app/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var ForgotpwdService = /*#__PURE__*/function () {
      function ForgotpwdService(http, apiService) {
        _classCallCheck(this, ForgotpwdService);

        this.http = http;
        this.apiService = apiService;
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
          headers: headers
        });
      }

      _createClass(ForgotpwdService, [{
        key: "forgotPassword",
        value: function forgotPassword(data) {
          return this.http.post(this.apiService.config + 'forgetPassword', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }]);

      return ForgotpwdService;
    }();

    ForgotpwdService.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }, {
        type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]
      }];
    };

    ForgotpwdService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])], ForgotpwdService);
    /***/
  }
}]);
//# sourceMappingURL=pages-forgotpwd-forgotpwd-module-es5.js.map