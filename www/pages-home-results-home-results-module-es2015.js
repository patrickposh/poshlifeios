(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-results-home-results-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\t<ion-toolbar class=\"toolbar-format\">\n\t\t<div class=\"header\">\n\t\t\t<div style=\"flex-grow: 1;\">\n\t\t\t\t<img class=\"logo-image\" src=\"assets/img/poshlife.png\">\n\t\t\t</div>\n\t\t\t<div class=\"language\" (click)=\"language()\">{{ 'app.language' | translate }}</div>\n\t\t</div>\n\t</ion-toolbar>\n\t<ion-toolbar id=\"home-result\" class=\"toolbar-border toolbar-format\">\n\t\t<form [formGroup]=\"searchForm\">\n\t\t\t<mat-form-field appearance=\"outline\" class=\"w100\" style=\"margin-left: 5px;\">\n\t\t\t\t<input matInput placeholder=\"{{'searchPlaceholder' | translate }}\" formControlName=\"myControl\"\n\t\t\t\t\t[matAutocomplete]=\"auto\" (keyup)=\"searchProperty()\" [(ngModel)]=\"searchText\">\n\t\t\t\t<mat-icon matPrefix>search</mat-icon>\n\t\t\t\t<mat-icon matSuffix *ngIf=\"searchText\" (click)=\"searchText = ''\">close</mat-icon>\n\t\t\t\t<mat-autocomplete #auto=\"matAutocomplete\">\n\t\t\t\t\t<mat-option style=\"font-size: 13px !important;\" *ngIf=\"isLoading\" class=\"is-loading\">\n\t\t\t\t\t\t{{ 'loading' | translate }}</mat-option>\n\t\t\t\t\t<ng-container *ngIf=\"!isLoading\">\n\t\t\t\t\t\t<mat-optgroup *ngFor=\"let group of searchValues\" [label]=\"group.name\">\n\t\t\t\t\t\t\t<mat-option class=\"matOption\" *ngFor=\"let option of group.data\" [value]=\"option.address\">\n\t\t\t\t\t\t\t\t<span *ngIf=\"group.name === 'Listing'\" routerLink=\"/property-detail/{{option.id}}\">\n\t\t\t\t\t\t\t\t\t<p class=\"p-address\">\n\t\t\t\t\t\t\t\t\t\t<span class=\"address-format\">\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.aptUnit\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{ option.aptUnit }}-\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t{{ option.address }}<br>\n\t\t\t\t\t\t\t\t\t\t\t{{ option.municipality }} - {{ option.community }}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"span-price\">\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.event === 'Sold' || option.event === 'Terminated'\"\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"text-decoration: line-through;\">\n\t\t\t\t\t\t\t\t\t\t\t\t$ {{ option.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.event !== 'Sold' && option.event !== 'Terminated'\">\n\t\t\t\t\t\t\t\t\t\t\t\t$ {{ option.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p class=\"p-secondary\">\n\t\t\t\t\t\t\t\t\t\t<span style=\"white-space: normal;\">\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"!option.bedroomsPlus\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{ option.bedrooms }} {{ 'property.bedrooms' | translate }},\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.bedroomsPlus\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{ option.bedrooms }}+{{ option.bedroomsPlus }} {{ 'property.bedrooms' | translate }},\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t{{ option.washrooms }} {{ 'property.bathroom' | translate }},\n\t\t\t\t\t\t\t\t\t\t\t{{ option.garageSpaces }} {{ 'property.garage' | translate }}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"span-type\">\n\t\t\t\t\t\t\t\t\t\t\t{{ option.type }}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p class=\"p-tertiary\">\n\t\t\t\t\t\t\t\t\t\t<span class=\"span-listing\">\n\t\t\t\t\t\t\t\t\t\t\tListing Date: {{option.listingEntryDate | date:'yyyy-MM-dd'}}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"event\">\n\t\t\t\t\t\t\t\t\t\t\t[{{ option.event }}]\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\n\t\t\t\t\t\t\t\t<span *ngIf=\"group.name === 'Locations'\"\n\t\t\t\t\t\t\t\t\tstyle=\"white-space: normal;\"\n\t\t\t\t\t\t\t\t\t(click)=\"redirectToMap(option.data)\">\n\t\t\t\t\t\t\t\t\t{{ option.locationSearch }}\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</mat-option>\n\t\t\t\t\t\t</mat-optgroup>\n\t\t\t\t\t</ng-container>\n\t\t\t\t</mat-autocomplete>\n\t\t\t</mat-form-field>\n\t\t</form>\n\t\t<div slot=\"end\" class=\"filter-btn\" (click)=\"searchFilter()\">\n\t\t\t<mat-icon class=\"text-white\">filter_alt</mat-icon><br>\n\t\t\t<span class=\"text-white\">{{ 'app.filter' | translate }}</span>\n\t\t</div>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<div class=\"nav\">\n\t\t<div class=\"option-format\" (click)=\"goToMap('residentialSale')\">\n\t\t\t<div class=\"icon-btn-format residential-border\">\n\t\t\t\t<ion-icon name=\"home\" class=\"residential-btn\"></ion-icon>\n\t\t\t</div>\n\t\t\t<span style=\"font-size: 13px;\" align=\"center\">{{ 'property.residentialSale&Sold' | translate }}</span>\n\t\t</div>\n\n\t\t<div class=\"option-format\" (click)=\"goToMap('residentialLease')\">\n\t\t\t<div class=\"icon-btn-format commercial-border\">\n\t\t\t\t<ion-icon name=\"map\" class=\"commercial-btn\"></ion-icon>\n\t\t\t</div>\n\t\t\t<span style=\"font-size: 13px;\" align=\"center\">{{ 'property.residentialLease' | translate }}</span>\n\t\t</div>\n\t\t<div class=\"option-format\" (click)=\"goToMap('commercialSale')\">\n\t\t\t<div class=\"icon-btn-format sale-border\">\n\t\t\t\t<ion-icon name=\"business\" class=\"sale-btn\"></ion-icon>\n\t\t\t</div>\n\t\t\t<span style=\"font-size: 13px;\" align=\"center\">{{ 'property.commercialSale&Sold' | translate }}</span>\n\t\t</div>\n\t\t<div class=\"option-format\" (click)=\"goToMap('commercialLease')\">\n\t\t\t<div class=\"icon-btn-format lease-border\">\n\t\t\t\t<ion-icon name=\"paper-plane\" class=\"lease-btn\"></ion-icon>\n\t\t\t</div>\n\t\t\t<span style=\"font-size: 13px;\" align=\"center\">{{ 'property.commercialLease' | translate }}</span>\n\t\t</div>\n\t</div>\n\n\t<ion-card *ngIf=\"properties?.length === 0\" color=\"danger\" class=\"ion-margin-top\">\n\t\t<ion-card-content>\n\t\t\t<p class=\"ion-text-center text-white\">{{ 'property.noPropertiesFound' | translate }}</p>\n\t\t</ion-card-content>\n\t</ion-card>\n\n\t<ion-grid class=\"ion-no-padding bg-gray\" style=\"border-top: 1px solid #ccc\" fixed>\n\t\t<div class=\"ion-no-margin\" [@staggerIn]=\"properties\">\n\t\t\t<!-- # -->\n\t\t\t<div class=\"bg-white mg-bt pos\" *ngFor=\"let property of properties\">\n\t\t\t\t<ion-button *ngIf=\"loginFlag\" color=\"danger\" routerLink=\"/login\" class=\"login-btn\">\n\t\t\t\t\t{{ 'app.loginRequired' | translate }}</ion-button>\n\t\t\t\t<div [ngClass]=\"{'login-required': loginFlag === true}\">\n\t\t\t\t\t<ion-item lines=\"none\" tappable routerLink=\"/property-detail/{{property.id}}\">\n\t\t\t\t\t\t<ion-thumbnail class=\"tumbnail-view\" slot=\"start\">\n\t\t\t\t\t\t\t<span *ngIf=\"property.propertyImage\">\n\t\t\t\t\t\t\t\t<img src=\"{{property.propertyImage}}\">\n\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t</ion-thumbnail>\n\t\t\t\t\t\t<ion-badge class=\"fl-rt leaseClass\" slot=\"end\"\n\t\t\t\t\t\t\t[ngClass]=\"{'direction-ltr' : !isArabic, 'direction-rtl' : isArabic}\"\n\t\t\t\t\t\t\t*ngIf=\"property.soldPrice === 0 && property.saleLease === 'Lease'\">\n\t\t\t\t\t\t\t<ion-text color=\"light\">{{ property.saleLease }}</ion-text>\n\t\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t\t<ion-badge class=\"fl-rt saleClass\" slot=\"end\"\n\t\t\t\t\t\t\t[ngClass]=\"{'direction-ltr' : !isArabic, 'direction-rtl' : isArabic}\"\n\t\t\t\t\t\t\t*ngIf=\"property.soldPrice === 0 && property.saleLease === 'Sale'\">\n\t\t\t\t\t\t\t<ion-text color=\"light\">{{ property.saleLease }}</ion-text>\n\t\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t\t<ion-badge class=\"fl-rt soldClass\" slot=\"end\" *ngIf=\"property.soldPrice !== 0\"\n\t\t\t\t\t\t\t[ngClass]=\"{'direction-ltr' : !isArabic, 'direction-rtl' : isArabic}\">\n\t\t\t\t\t\t\t<ion-text color=\"light\">{{ 'property.sold' | translate }}</ion-text>\n\t\t\t\t\t\t</ion-badge>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<ion-label>\n\n\t\t\t\t\t\t\t<ion-text color=\"dark1\">{{ property.address }}</ion-text>\n\t\t\t\t\t\t\t\n\n\t\t\t\t\t\t\t<p class=\"text-12x\">\n\t\t\t\t\t\t\t\t<ion-text color=\"dark\">{{ property.area }}</ion-text>\n\t\t\t\t\t\t\t</p>\n\n\t\t\t\t\t\t\t<p *ngIf=\"property.soldPrice === 0\" class=\"price-format\">\n\t\t\t\t\t\t\t\t{{ 'property.listed' | translate }}:\n\t\t\t\t\t\t\t\t<ion-text class=\"priceText\" *ngIf=\"property.saleLease == 'Sale'\">\n\t\t\t\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t\t\t\t<ion-text class=\"priceText\" *ngIf=\"property.saleLease == 'Lease'\">\n\t\t\t\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US' }} /month\n\t\t\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t\t\t</p>\n\n\t\t\t\t\t\t\t<p *ngIf=\"property.soldPrice !== 0\" class=\"price-format\">\n\t\t\t\t\t\t\t\t{{ 'property.soldFor' | translate }}:\n\t\t\t\t\t\t\t\t<ion-text class=\"priceText\">\n\t\t\t\t\t\t\t\t\t${{ property.soldPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t\t\t</p>\n\n\t\t\t\t\t\t\t<p class=\"p-type\">\n\t\t\t\t\t\t\t\t{{  property.type }}\n\t\t\t\t\t\t\t</p>\n\n\t\t\t\t\t\t\t<p *ngIf=\"property.soldPrice === 0\">\n\t\t\t\t\t\t\t\t{{ 'property.listedIn' | translate }}:\n\t\t\t\t\t\t\t\t<ion-text>\n\t\t\t\t\t\t\t\t\t{{ property.listingEntryDate | date }}\n\t\t\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t\t\t</p>\n\n\t\t\t\t\t\t\t<p *ngIf=\"property.soldPrice !== 0\">\n\t\t\t\t\t\t\t\t{{ 'property.soldDate' | translate }}:\n\t\t\t\t\t\t\t\t<ion-text>\n\t\t\t\t\t\t\t\t\t{{ property.soldDate | date }}\n\t\t\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t</ion-label>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-footer class=\"footer-format\">\n\t\t\t\t\t\t<ion-row>\n\t\t\t\t\t\t\t<ion-col class=\"pd-0\" size=\"10\">\n\t\t\t\t\t\t\t\t<ion-row class=\"font-s12\">\n\t\t\t\t\t\t\t\t\t<ion-col class=\"pd-0\" size=\"3\">{{ 'property.MLS' | translate }}</ion-col>\n\t\t\t\t\t\t\t\t\t<ion-col size=\"3\" class=\"col-left-border pd-0\">\n\t\t\t\t\t\t\t\t\t\t{{ 'property.bed' | translate }}\n\t\t\t\t\t\t\t\t\t</ion-col>\n\t\t\t\t\t\t\t\t\t<ion-col size=\"4\" class=\"col-left-border pd-0\">\n\t\t\t\t\t\t\t\t\t\t{{ 'property.bath' | translate }}\n\t\t\t\t\t\t\t\t\t</ion-col>\n\t\t\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t\t\t<ion-row class=\"font-s12\">\n\t\t\t\t\t\t\t\t\t<ion-col class=\"pd-0\" size=\"3\">{{ property.mls }}</ion-col>\n\t\t\t\t\t\t\t\t\t<ion-col size=\"3\" class=\"col-left-border pd-0\">{{ property.bedrooms }}</ion-col>\n\t\t\t\t\t\t\t\t\t<ion-col size=\"4\" class=\"col-left-border pd-0\">{{ property.washrooms }}</ion-col>\n\t\t\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t\t\t</ion-col>\n\t\t\t\t\t\t\t<ion-col size=\"2\" class=\"heart-format pd-0\">\n\t\t\t\t\t\t\t\t<ion-icon *ngIf=\"property.favorite\" name=\"heart\" color=\"danger\"></ion-icon>\n\t\t\t\t\t\t\t</ion-col>\n\t\t\t\t\t\t</ion-row>\n\t\t\t\t\t</ion-footer>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<!-- # -->\n\t\t</div>\n\t</ion-grid>\n\n\t<ion-button *ngIf=\"properties?.length !== 0 && moreResults\" class=\"btn-more ion-margin\" expand=\"full\" color=\"danger\"\n\t\t(click)=\"moreProperties()\">\n\t\t{{ 'app.moreResults' | translate }}\n\t</ion-button>\n</ion-content>\n\n<ion-footer class=\"animated fadeIn\">\n\t<ion-toolbar color=\"pmary\">\n\t\t<ion-grid class=\"ion-no-padding\">\n\t\t\t<ion-row>\n\t\t\t\t<ion-col size=\"3\" class=\"ion-no-padding\" align=\"center\" (click)=\"redirectToMap('address')\">\n\t\t\t\t\t<ion-button style=\"height: 20px;\" expand=\"full\" fill=\"clear\" color=\"dark\">\n\t\t\t\t\t\t<ion-icon name=\"map\"></ion-icon>\n\t\t\t\t\t</ion-button>\n\t\t\t\t\t<span style=\"font-size: 12px;\">{{ 'app.map' | translate }}</span>\n\t\t\t\t</ion-col>\n\n\t\t\t\t<ion-col size=\"3\" class=\"ion-no-padding\" align=\"center\" routerLink=\"/favorites\">\n\t\t\t\t\t<ion-button style=\"height: 20px;\" expand=\"full\" fill=\"clear\" color=\"dark\">\n\t\t\t\t\t\t<mat-icon>star_border</mat-icon>\n\t\t\t\t\t</ion-button>\n\t\t\t\t\t<span style=\"font-size: 12px;\">{{ 'app.watched' | translate }}</span>\n\t\t\t\t</ion-col>\n\n\t\t\t\t<ion-col *ngIf=\"!profileView\" size=\"3\" class=\"ion-no-padding\" align=\"center\" routerLink=\"/edit-profile\">\n\t\t\t\t\t<ion-button style=\"height: 20px;\" expand=\"full\" fill=\"clear\" color=\"dark\">\n\t\t\t\t\t\t<mat-icon>person_outline</mat-icon>\n\t\t\t\t\t</ion-button>\n\t\t\t\t\t<span style=\"font-size: 12px;\">{{ 'app.profile' | translate }}</span>\n\t\t\t\t</ion-col>\n\n\t\t\t\t<ion-col *ngIf=\"profileView\" size=\"3\" class=\"ion-no-padding\" align=\"center\" routerLink=\"/login\">\n\t\t\t\t\t<ion-button style=\"height: 20px;\" expand=\"full\" fill=\"clear\" color=\"dark\">\n\t\t\t\t\t\t<mat-icon>person_outline</mat-icon>\n\t\t\t\t\t</ion-button>\n\t\t\t\t\t<span style=\"font-size: 12px;\">{{ 'app.profile' | translate }}</span>\n\t\t\t\t</ion-col>\n\n\t\t\t\t<ion-col size=\"3\" class=\"ion-no-padding\" align=\"center\">\n\t\t\t\t\t<ion-button ion-button menuToggle=\"right\" style=\"height: 20px;\" expand=\"full\" fill=\"clear\"\n\t\t\t\t\t\tcolor=\"dark\">\n\t\t\t\t\t\t<ion-menu-button color=\"dark\">\n\t\t\t\t\t\t\t<mat-icon>reorder</mat-icon>\n\t\t\t\t\t\t</ion-menu-button>\n\t\t\t\t\t</ion-button>\n\t\t\t\t\t<span style=\"font-size: 12px;\">{{ 'app.menu' | translate }}</span>\n\t\t\t\t</ion-col>\n\t\t\t</ion-row>\n\t\t</ion-grid>\n\t</ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ "./src/app/pages/home-results/home-results.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.module.ts ***!
  \***********************************************************/
/*! exports provided: HttpLoaderFactory, HomeResultsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPageModule", function() { return HomeResultsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/fesm2015/ngx-translate-http-loader.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _home_results_page__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home-results.page */ "./src/app/pages/home-results/home-results.page.ts");
/* harmony import */ var _home_results_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./home-results.service */ "./src/app/pages/home-results/home-results.service.ts");














function HttpLoaderFactory(httpClient) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_8__["TranslateHttpLoader"](httpClient);
}
const routes = [
    {
        path: '',
        component: _home_results_page__WEBPACK_IMPORTED_MODULE_12__["HomeResultsPage"]
    }
];
let HomeResultsPageModule = class HomeResultsPageModule {
};
HomeResultsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_11__["PipesModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild({
                loader: {
                    provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateLoader"],
                    useFactory: HttpLoaderFactory,
                    deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]]
                }
            }),
            _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatOptionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatIconModule"]
        ],
        declarations: [
            _home_results_page__WEBPACK_IMPORTED_MODULE_12__["HomeResultsPage"]
        ],
        providers: [
            _home_results_service__WEBPACK_IMPORTED_MODULE_13__["HomeResultsService"]
        ]
    })
], HomeResultsPageModule);



/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host .header {\n  padding: 7px 14px 7px;\n  flex-shrink: 0;\n  display: flex;\n  flex-wrap: wrap;\n  align-items: center;\n}\n:host .language {\n  margin-top: 8px;\n  line-height: 25px;\n  border: 1px solid #fff;\n  padding: 0 5px;\n  color: #fefefe;\n  cursor: pointer;\n  font-size: 17px;\n}\n:host ion-content {\n  --background: linear-gradient(-135deg, #f6f6f6, var(--ion-color-pmary));\n}\n:host ion-item {\n  padding: 5px 0px;\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host .toolbar-format {\n  --background: #e01a27 !important;\n  zoom: 0.9 !important;\n  border: none;\n}\n:host .title-format {\n  font-size: 20px;\n  letter-spacing: 3px;\n  flex-basis: 0;\n  flex-grow: 1;\n  flex-shrink: 1;\n}\n:host ion-card {\n  border-radius: 0;\n}\n:host .direction-ltr {\n  position: absolute;\n  right: 10px;\n  bottom: 5px;\n}\n:host .direction-rtl {\n  position: absolute;\n  left: 10px;\n  bottom: 5px;\n}\n:host .saleClass {\n  background: #1e88e5;\n  color: #fff;\n  font-weight: 500;\n}\n:host .soldClass {\n  background: #ff9e26;\n  color: #fff;\n  font-weight: 500;\n}\n:host .leaseClass {\n  background: #558b2f;\n  color: #fff;\n  font-weight: 500;\n}\n:host .priceText {\n  color: #d32f2f;\n}\n:host .filter-btn {\n  width: 75px;\n  text-align: center;\n}\n:host .nav {\n  display: flex;\n  justify-content: space-between;\n  align-items: stretch;\n  width: 100%;\n  margin-bottom: 10px;\n  padding: 10px 0px;\n  background: #fff;\n  border-bottom: 1px solid #ccc;\n}\n:host .option-format {\n  padding: 5px;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-start;\n  align-items: center;\n  width: 25%;\n}\n:host .icon-btn-format {\n  padding: 10px;\n  width: 45px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  border-radius: 50%;\n  margin: 10px;\n}\n:host .residential-btn {\n  font-size: 24px;\n  color: #f7a321;\n}\n:host .commercial-btn {\n  font-size: 24px;\n  color: #000;\n}\n:host .sale-btn {\n  font-size: 24px;\n  color: #1e88e5;\n}\n:host .lease-btn {\n  font-size: 24px;\n  color: #558b2f;\n}\n:host .sale-border {\n  border: 1px solid #1e88e5;\n}\n:host .lease-border {\n  border: 1px solid #558b2f;\n}\n:host .residential-border {\n  border: 1px solid #f7a321;\n}\n:host .commercial-border {\n  border: 1px solid #000;\n}\n.mg-bt {\n  margin-bottom: 1%;\n  box-shadow: 0 3px 4px rgba(var(--ion-color-dark-rgb), 0.24);\n}\n.heart-format {\n  margin-top: 2%;\n  text-align: center;\n}\n.col-left-border {\n  border-left: 1px solid #d7d7d7;\n}\n.font-s12 {\n  font-size: 12px;\n  color: #424242;\n}\n.pd-0 {\n  padding: 2px 5px 2px 5px !important;\n}\n.toolbar-border {\n  border-top: 1px solid #ccc;\n}\n.fl-rt {\n  float: right;\n}\n.tumbnail-view {\n  height: 100px;\n  width: 100px;\n}\n.tumbnail-view img {\n  border-radius: 5px;\n  height: 100px;\n  width: 100px;\n}\n.price-format {\n  font-size: 16px;\n  margin: 5px 0px;\n}\n.p-type {\n  display: inline-block;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #009cff;\n  padding: 1px 4px;\n  font-size: 15px;\n  background-color: #e1f3ff;\n  color: #009cff;\n  font-weight: 400;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.mt-0 {\n  margin-top: 0px;\n}\n.btn-more {\n  color: #fff;\n}\n.bg-gray {\n  background: #e0e0e0;\n}\n.matOption {\n  position: relative;\n  display: flex;\n  justify-content: center;\n  padding: 8px 6px 8px 0px;\n  height: auto !important;\n  font-size: 14px !important;\n  line-height: 25px !important;\n  border-bottom: 1px solid #ccc !important;\n}\n.matOption .p-address {\n  display: flex;\n  margin: 0px;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n.matOption .p-address .address-format {\n  white-space: normal;\n  color: #333;\n}\n.matOption .p-address .span-price {\n  color: #ff5b29;\n  margin-left: 0.5em;\n}\n.matOption .p-secondary {\n  margin: 0px;\n  display: flex;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n.matOption .p-secondary .span-type {\n  margin-left: 0.5em;\n  color: #333;\n  font-weight: 600;\n}\n.matOption .p-tertiary {\n  margin: 0px;\n  display: flex;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n.matOption .p-tertiary .span-listing {\n  color: #999;\n  font-size: 13px;\n  white-space: normal;\n}\n.matOption .p-tertiary .event {\n  color: #333;\n  font-weight: 600;\n}\n.login-btn {\n  position: absolute;\n  z-index: 999;\n  text-transform: capitalize;\n  top: 45%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  --border-radius: 0px !important;\n}\n.pos {\n  position: relative;\n}\n.login-required {\n  -webkit-filter: blur(5px);\n          filter: blur(5px);\n  pointer-events: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL2hvbWUtcmVzdWx0cy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUtcmVzdWx0cy9ob21lLXJlc3VsdHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0kscUJBQUE7RUFDQSxjQUFBO0VBQ0EsYUFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtBQ0RSO0FESUk7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7QUNGUjtBREtJO0VBQ0ksdUVBQUE7QUNIUjtBRE1JO0VBQ0ksZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlEQUFBO0FDSlI7QURPSTtFQUNJLGdDQUFBO0VBQ0Esb0JBQUE7RUFDQSxZQUFBO0FDTFI7QURRSTtFQUNJLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ05SO0FEU0k7RUFDSSxnQkFBQTtBQ1BSO0FEVUk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FDUlI7QURXSTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUNUUjtBRFlJO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUNWUjtBRGFJO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUNYUjtBRGNJO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUNaUjtBRGVJO0VBQ0ksY0FBQTtBQ2JSO0FEZ0JJO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FDZFI7QURpQkk7RUFDSSxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSw2QkFBQTtBQ2ZSO0FEa0JJO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FDaEJSO0FEbUJJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FDakJSO0FEb0JJO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNsQlI7QURxQkk7RUFDSSxlQUFBO0VBQ0EsV0FBQTtBQ25CUjtBRHNCSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDcEJSO0FEdUJJO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNyQlI7QUR3Qkk7RUFDSSx5QkFBQTtBQ3RCUjtBRHlCSTtFQUNJLHlCQUFBO0FDdkJSO0FEMEJJO0VBQ0kseUJBQUE7QUN4QlI7QUQyQkk7RUFDSSxzQkFBQTtBQ3pCUjtBRDZCQTtFQUNJLGlCQUFBO0VBQ0EsMkRBQUE7QUMxQko7QUQ4QkE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QUMzQko7QUQ4QkE7RUFDSSw4QkFBQTtBQzNCSjtBRDhCQTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDM0JKO0FEOEJBO0VBQ0ksbUNBQUE7QUMzQko7QUQ4QkE7RUFDSSwwQkFBQTtBQzNCSjtBRDhCQTtFQUNJLFlBQUE7QUMzQko7QUQ4QkE7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQzNCSjtBRDZCSTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7QUMzQlI7QUQrQkE7RUFDSSxlQUFBO0VBQ0EsZUFBQTtBQzVCSjtBRCtCQTtFQUNJLHFCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUM1Qko7QUQrQkE7RUFDSSxlQUFBO0FDNUJKO0FEK0JBO0VBQ0ksV0FBQTtBQzVCSjtBRCtCQTtFQUNJLG1CQUFBO0FDNUJKO0FEK0JBO0VBQ0ksa0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSx3QkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7RUFDQSw0QkFBQTtFQUNBLHdDQUFBO0FDNUJKO0FEOEJJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0VBQ0EsdUJBQUE7QUM1QlI7QUQ4QlE7RUFDSSxtQkFBQTtFQUNBLFdBQUE7QUM1Qlo7QUQrQlE7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QUM3Qlo7QURpQ0k7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7RUFDQSx1QkFBQTtBQy9CUjtBRGlDUTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0FDL0JaO0FEbUNJO0VBQ0ksV0FBQTtFQUNBLGFBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0VBQ0EsdUJBQUE7QUNqQ1I7QURtQ1E7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDakNaO0FEb0NRO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FDbENaO0FEdUNBO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsMEJBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0EsK0JBQUE7QUNwQ0o7QUR1Q0E7RUFDSSxrQkFBQTtBQ3BDSjtBRHVDQTtFQUNJLHlCQUFBO1VBQUEsaUJBQUE7RUFDQSxvQkFBQTtBQ3BDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUtcmVzdWx0cy9ob21lLXJlc3VsdHMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXG4gICAgLmhlYWRlciB7XG4gICAgICAgIHBhZGRpbmc6IDdweCAxNHB4IDdweDtcbiAgICAgICAgZmxleC1zaHJpbms6IDA7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG5cbiAgICAubGFuZ3VhZ2Uge1xuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNXB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZmO1xuICAgICAgICBwYWRkaW5nOiAwIDVweDtcbiAgICAgICAgY29sb3I6ICNmZWZlZmU7XG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAxN3B4O1xuICAgIH1cblxuICAgIGlvbi1jb250ZW50IHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgI2Y2ZjZmNiwgdmFyKC0taW9uLWNvbG9yLXBtYXJ5KSk7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICBwYWRkaW5nOiA1cHggMHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICAgIH1cblxuICAgIC50b29sYmFyLWZvcm1hdCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI2UwMWEyNyAhaW1wb3J0YW50O1xuICAgICAgICB6b29tOiAwLjkgIWltcG9ydGFudDtcbiAgICAgICAgYm9yZGVyOiBub25lO1xuICAgIH1cblxuICAgIC50aXRsZS1mb3JtYXQge1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGxldHRlci1zcGFjaW5nOiAzcHg7XG4gICAgICAgIGZsZXgtYmFzaXM6IDA7XG4gICAgICAgIGZsZXgtZ3JvdzogMTtcbiAgICAgICAgZmxleC1zaHJpbms6IDE7XG4gICAgfVxuXG4gICAgaW9uLWNhcmQge1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgIH1cblxuICAgIC5kaXJlY3Rpb24tbHRyIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICByaWdodDogMTBweDtcbiAgICAgICAgYm90dG9tOiA1cHg7XG4gICAgfVxuXG4gICAgLmRpcmVjdGlvbi1ydGwge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGxlZnQ6IDEwcHg7XG4gICAgICAgIGJvdHRvbTogNXB4O1xuICAgIH1cblxuICAgIC5zYWxlQ2xhc3Mge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjMWU4OGU1O1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG5cbiAgICAuc29sZENsYXNzIHtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmOWUyNjtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgLmxlYXNlQ2xhc3Mge1xuICAgICAgICBiYWNrZ3JvdW5kOiAjNTU4YjJmO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG5cbiAgICAucHJpY2VUZXh0IHtcbiAgICAgICAgY29sb3I6ICNkMzJmMmY7XG4gICAgfVxuXG4gICAgLmZpbHRlci1idG4ge1xuICAgICAgICB3aWR0aDogNzVweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5uYXYge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBzdHJldGNoO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgcGFkZGluZzogMTBweCAwcHg7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xuICAgIH1cblxuICAgIC5vcHRpb24tZm9ybWF0IHtcbiAgICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAyNSU7XG4gICAgfVxuICAgIFxuICAgIC5pY29uLWJ0bi1mb3JtYXQge1xuICAgICAgICBwYWRkaW5nOiAxMHB4O1xuICAgICAgICB3aWR0aDogNDVweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgICAgbWFyZ2luOiAxMHB4O1xuICAgIH1cblxuICAgIC5yZXNpZGVudGlhbC1idG4ge1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIGNvbG9yOiAjZjdhMzIxO1xuICAgIH1cblxuICAgIC5jb21tZXJjaWFsLWJ0biB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgICAgY29sb3I6ICMwMDA7XG4gICAgfVxuXG4gICAgLnNhbGUtYnRuIHtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgICAgICBjb2xvcjogIzFlODhlNTtcbiAgICB9XG5cbiAgICAubGVhc2UtYnRuIHtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgICAgICBjb2xvcjogIzU1OGIyZjtcbiAgICB9XG5cbiAgICAuc2FsZS1ib3JkZXIge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMWU4OGU1O1xuICAgIH1cblxuICAgIC5sZWFzZS1ib3JkZXIge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjNTU4YjJmO1xuICAgIH1cblxuICAgIC5yZXNpZGVudGlhbC1ib3JkZXIge1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjZjdhMzIxO1xuICAgIH1cblxuICAgIC5jb21tZXJjaWFsLWJvcmRlciB7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDA7XG4gICAgfVxufVxuXG4ubWctYnQge1xuICAgIG1hcmdpbi1ib3R0b206IDElO1xuICAgIGJveC1zaGFkb3c6IDAgM3B4IDRweCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuMjQpO1xufVxuXG5cbi5oZWFydC1mb3JtYXQge1xuICAgIG1hcmdpbi10b3A6IDIlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNvbC1sZWZ0LWJvcmRlciB7XG4gICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjZDdkN2Q3O1xufVxuXG4uZm9udC1zMTIge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBjb2xvcjogIzQyNDI0Mjtcbn1cblxuLnBkLTAge1xuICAgIHBhZGRpbmc6IDJweCA1cHggMnB4IDVweCAhaW1wb3J0YW50O1xufVxuXG4udG9vbGJhci1ib3JkZXIge1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xufVxuXG4uZmwtcnQge1xuICAgIGZsb2F0OiByaWdodDtcbn1cblxuLnR1bWJuYWlsLXZpZXcge1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgd2lkdGg6IDEwMHB4O1xuXG4gICAgaW1nIHtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xuICAgICAgICB3aWR0aDogMTAwcHg7XG4gICAgfVxufVxuXG4ucHJpY2UtZm9ybWF0IHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbWFyZ2luOiA1cHggMHB4O1xufVxuXG4ucC10eXBlIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDA5Y2ZmO1xuICAgIHBhZGRpbmc6IDFweCA0cHg7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlMWYzZmY7XG4gICAgY29sb3I6ICMwMDljZmY7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG4ubXQtMCB7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xufVxuXG4uYnRuLW1vcmUge1xuICAgIGNvbG9yOiAjZmZmO1xufVxuXG4uYmctZ3JheSB7XG4gICAgYmFja2dyb3VuZDogI2UwZTBlMDtcbn1cblxuLm1hdE9wdGlvbiB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgcGFkZGluZzogOHB4IDZweCA4cHggMHB4O1xuICAgIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICAgIGxpbmUtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2MgIWltcG9ydGFudDtcblxuICAgIC5wLWFkZHJlc3Mge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgZmxleC13cmFwOiBub3dyYXA7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG5cbiAgICAgICAgLmFkZHJlc3MtZm9ybWF0IHtcbiAgICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgICAgICAgICBjb2xvcjogIzMzMztcbiAgICAgICAgfVxuXG4gICAgICAgIC5zcGFuLXByaWNlIHtcbiAgICAgICAgICAgIGNvbG9yOiAjZmY1YjI5O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC41ZW07XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAucC1zZWNvbmRhcnkge1xuICAgICAgICBtYXJnaW46IDBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC13cmFwOiBub3dyYXA7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG5cbiAgICAgICAgLnNwYW4tdHlwZSB7XG4gICAgICAgICAgICBtYXJnaW4tbGVmdDogLjVlbTtcbiAgICAgICAgICAgIGNvbG9yOiAjMzMzO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5wLXRlcnRpYXJ5IHtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuXG4gICAgICAgIC5zcGFuLWxpc3Rpbmcge1xuICAgICAgICAgICAgY29sb3I6ICM5OTk7XG4gICAgICAgICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgICAgICB9XG5cbiAgICAgICAgLmV2ZW50IHtcbiAgICAgICAgICAgIGNvbG9yOiAjMzMzO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuLmxvZ2luLWJ0biB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDk5OTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICB0b3A6IDQ1JTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gICAgLS1ib3JkZXItcmFkaXVzOiAwcHggIWltcG9ydGFudDtcbn1cblxuLnBvcyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ubG9naW4tcmVxdWlyZWQge1xuICAgIGZpbHRlcjogYmx1cig1cHgpO1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuIiwiOmhvc3QgLmhlYWRlciB7XG4gIHBhZGRpbmc6IDdweCAxNHB4IDdweDtcbiAgZmxleC1zaHJpbms6IDA7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbjpob3N0IC5sYW5ndWFnZSB7XG4gIG1hcmdpbi10b3A6IDhweDtcbiAgbGluZS1oZWlnaHQ6IDI1cHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmY7XG4gIHBhZGRpbmc6IDAgNXB4O1xuICBjb2xvcjogI2ZlZmVmZTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBmb250LXNpemU6IDE3cHg7XG59XG46aG9zdCBpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsICNmNmY2ZjYsIHZhcigtLWlvbi1jb2xvci1wbWFyeSkpO1xufVxuOmhvc3QgaW9uLWl0ZW0ge1xuICBwYWRkaW5nOiA1cHggMHB4O1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xufVxuOmhvc3QgLnRvb2xiYXItZm9ybWF0IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZTAxYTI3ICFpbXBvcnRhbnQ7XG4gIHpvb206IDAuOSAhaW1wb3J0YW50O1xuICBib3JkZXI6IG5vbmU7XG59XG46aG9zdCAudGl0bGUtZm9ybWF0IHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBsZXR0ZXItc3BhY2luZzogM3B4O1xuICBmbGV4LWJhc2lzOiAwO1xuICBmbGV4LWdyb3c6IDE7XG4gIGZsZXgtc2hyaW5rOiAxO1xufVxuOmhvc3QgaW9uLWNhcmQge1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuOmhvc3QgLmRpcmVjdGlvbi1sdHIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxMHB4O1xuICBib3R0b206IDVweDtcbn1cbjpob3N0IC5kaXJlY3Rpb24tcnRsIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxMHB4O1xuICBib3R0b206IDVweDtcbn1cbjpob3N0IC5zYWxlQ2xhc3Mge1xuICBiYWNrZ3JvdW5kOiAjMWU4OGU1O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbjpob3N0IC5zb2xkQ2xhc3Mge1xuICBiYWNrZ3JvdW5kOiAjZmY5ZTI2O1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cbjpob3N0IC5sZWFzZUNsYXNzIHtcbiAgYmFja2dyb3VuZDogIzU1OGIyZjtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG46aG9zdCAucHJpY2VUZXh0IHtcbiAgY29sb3I6ICNkMzJmMmY7XG59XG46aG9zdCAuZmlsdGVyLWJ0biB7XG4gIHdpZHRoOiA3NXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG46aG9zdCAubmF2IHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICBhbGlnbi1pdGVtczogc3RyZXRjaDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIHBhZGRpbmc6IDEwcHggMHB4O1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYztcbn1cbjpob3N0IC5vcHRpb24tZm9ybWF0IHtcbiAgcGFkZGluZzogNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyNSU7XG59XG46aG9zdCAuaWNvbi1idG4tZm9ybWF0IHtcbiAgcGFkZGluZzogMTBweDtcbiAgd2lkdGg6IDQ1cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIG1hcmdpbjogMTBweDtcbn1cbjpob3N0IC5yZXNpZGVudGlhbC1idG4ge1xuICBmb250LXNpemU6IDI0cHg7XG4gIGNvbG9yOiAjZjdhMzIxO1xufVxuOmhvc3QgLmNvbW1lcmNpYWwtYnRuIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjb2xvcjogIzAwMDtcbn1cbjpob3N0IC5zYWxlLWJ0biB7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgY29sb3I6ICMxZTg4ZTU7XG59XG46aG9zdCAubGVhc2UtYnRuIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjb2xvcjogIzU1OGIyZjtcbn1cbjpob3N0IC5zYWxlLWJvcmRlciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMxZTg4ZTU7XG59XG46aG9zdCAubGVhc2UtYm9yZGVyIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzU1OGIyZjtcbn1cbjpob3N0IC5yZXNpZGVudGlhbC1ib3JkZXIge1xuICBib3JkZXI6IDFweCBzb2xpZCAjZjdhMzIxO1xufVxuOmhvc3QgLmNvbW1lcmNpYWwtYm9yZGVyIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwMDtcbn1cblxuLm1nLWJ0IHtcbiAgbWFyZ2luLWJvdHRvbTogMSU7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDRweCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuMjQpO1xufVxuXG4uaGVhcnQtZm9ybWF0IHtcbiAgbWFyZ2luLXRvcDogMiU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNvbC1sZWZ0LWJvcmRlciB7XG4gIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2Q3ZDdkNztcbn1cblxuLmZvbnQtczEyIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzQyNDI0Mjtcbn1cblxuLnBkLTAge1xuICBwYWRkaW5nOiAycHggNXB4IDJweCA1cHggIWltcG9ydGFudDtcbn1cblxuLnRvb2xiYXItYm9yZGVyIHtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNjY2M7XG59XG5cbi5mbC1ydCB7XG4gIGZsb2F0OiByaWdodDtcbn1cblxuLnR1bWJuYWlsLXZpZXcge1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG59XG4udHVtYm5haWwtdmlldyBpbWcge1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbn1cblxuLnByaWNlLWZvcm1hdCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luOiA1cHggMHB4O1xufVxuXG4ucC10eXBlIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwOWNmZjtcbiAgcGFkZGluZzogMXB4IDRweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTFmM2ZmO1xuICBjb2xvcjogIzAwOWNmZjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbi5tdC0wIHtcbiAgbWFyZ2luLXRvcDogMHB4O1xufVxuXG4uYnRuLW1vcmUge1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLmJnLWdyYXkge1xuICBiYWNrZ3JvdW5kOiAjZTBlMGUwO1xufVxuXG4ubWF0T3B0aW9uIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcGFkZGluZzogOHB4IDZweCA4cHggMHB4O1xuICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNHB4ICFpbXBvcnRhbnQ7XG4gIGxpbmUtaGVpZ2h0OiAyNXB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjICFpbXBvcnRhbnQ7XG59XG4ubWF0T3B0aW9uIC5wLWFkZHJlc3Mge1xuICBkaXNwbGF5OiBmbGV4O1xuICBtYXJnaW46IDBweDtcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4ubWF0T3B0aW9uIC5wLWFkZHJlc3MgLmFkZHJlc3MtZm9ybWF0IHtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgY29sb3I6ICMzMzM7XG59XG4ubWF0T3B0aW9uIC5wLWFkZHJlc3MgLnNwYW4tcHJpY2Uge1xuICBjb2xvcjogI2ZmNWIyOTtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xufVxuLm1hdE9wdGlvbiAucC1zZWNvbmRhcnkge1xuICBtYXJnaW46IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4ubWF0T3B0aW9uIC5wLXNlY29uZGFyeSAuc3Bhbi10eXBlIHtcbiAgbWFyZ2luLWxlZnQ6IDAuNWVtO1xuICBjb2xvcjogIzMzMztcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi5tYXRPcHRpb24gLnAtdGVydGlhcnkge1xuICBtYXJnaW46IDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC13cmFwOiBub3dyYXA7XG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG4ubWF0T3B0aW9uIC5wLXRlcnRpYXJ5IC5zcGFuLWxpc3Rpbmcge1xuICBjb2xvcjogIzk5OTtcbiAgZm9udC1zaXplOiAxM3B4O1xuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xufVxuLm1hdE9wdGlvbiAucC10ZXJ0aWFyeSAuZXZlbnQge1xuICBjb2xvcjogIzMzMztcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLmxvZ2luLWJ0biB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTk5O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgdG9wOiA0NSU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIC0tYm9yZGVyLXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5wb3Mge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5sb2dpbi1yZXF1aXJlZCB7XG4gIGZpbHRlcjogYmx1cig1cHgpO1xuICBwb2ludGVyLWV2ZW50czogbm9uZTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.ts ***!
  \*********************************************************/
/*! exports provided: HomeResultsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPage", function() { return HomeResultsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../pages/modal/search-filter/search-filter.page */ "./src/app/pages/modal/search-filter/search-filter.page.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm2015/animations.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _home_results_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home-results.service */ "./src/app/pages/home-results/home-results.service.ts");










let HomeResultsPage = class HomeResultsPage {
    constructor(navCtrl, menuCtrl, modalCtrl, loadingCtrl, toastCtrl, translate, homeResultService, fb, geolocation, asCtrl, platform) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.translate = translate;
        this.homeResultService = homeResultService;
        this.fb = fb;
        this.geolocation = geolocation;
        this.asCtrl = asCtrl;
        this.platform = platform;
        this.properties = [];
        this.currentLat = "";
        this.currentLng = "";
        this.userId = "";
        this.loginFlag = true;
        this.profileView = true;
        this.saleLease = 'Sale';
        this.propertyType = 'residential';
        this.cnt = 1;
        this.searchValues = [];
        this.moreResults = true;
        this.isArabic = false;
        this.loginDetails = JSON.parse(localStorage.getItem('userDetails'));
        var language = localStorage.getItem('language');
        if (language != null && language != undefined) {
            if (language == 'ar' || language == 'per') {
                this.isArabic = !this.isArabic;
            }
        }
        if (this.loginDetails != undefined) {
            this.loginFlag = false;
            this.profileView = false;
            this.userId = JSON.stringify(this.loginDetails.data.userVM.id);
        }
        this.translate.setDefaultLang(_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].language);
    }
    advancedFilterObject() {
        return {
            address: "",
            approxSquareFootageMax: null,
            approxSquareFootageMin: null,
            basement1: "",
            bedRooms: [],
            currentPage: this.cnt,
            daysOnMarket: "",
            latitude: this.currentLat,
            longitude: this.currentLng,
            listPriceMax: null,
            listPriceMin: null,
            openHouse: "-1",
            parkingSpaces: "",
            propertyOption: "",
            propertyType: this.propertyType,
            soldDays: "",
            saleLease: this.saleLease,
            userId: this.userId,
            washrooms: "",
            withSold: false
        };
    }
    ngOnInit() {
        this.searchForm = this.fb.group({
            myControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]()
        });
    }
    searchProperty() {
        var key = this.searchForm.get("myControl").value;
        if (key && key.length > 2) {
            this.isLoading = true;
            var searchData = {
                currentPage: 1,
                searchText: key
            };
            this.homeResultService.searchProperties(searchData).subscribe(data => {
                this.isLoading = false;
                if (data.data != undefined) {
                    if (data.data && data.data.length > 0) {
                        var add = { locationSearch: data.data[0].locationSearch, data: data.data[0] };
                        this.searchValues = [
                            {
                                name: 'Locations',
                                data: [add]
                            },
                            {
                                name: 'Listing',
                                data: data.data
                            }
                        ];
                    }
                    else {
                        this.searchValues = [];
                    }
                }
            }, err => {
                this.isLoading = false;
            });
        }
        else {
            this.searchValues = [];
        }
    }
    ionViewWillEnter() {
        this.cnt = 1;
        this.filterObject = this.advancedFilterObject();
        this.geolocation.getCurrentPosition().then((resp) => {
            this.currentLat = (resp.coords.latitude).toString();
            this.currentLng = (resp.coords.longitude).toString();
            this.filterObject.latitude = this.currentLat;
            this.filterObject.longitude = this.currentLng;
            this.findAll();
        }, err => {
            this.currentLat = "43.63943345";
            this.currentLng = "-79.39972759226308";
            this.filterObject.latitude = this.currentLat;
            this.filterObject.longitude = this.currentLng;
            this.findAll();
        });
        this.menuCtrl.enable(true);
        this.cnt = 1;
        this.moreResults = true;
    }
    ionViewDidEnter() {
        // this.platform.backButton.subscribeWithPriority(2, () => {
        // 	navigator['app'].exitApp();
        // });
    }
    ionViewDidLeave() {
        this.menuCtrl.enable(true);
    }
    settings() {
        this.navCtrl.navigateForward('settings');
    }
    findAll() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var loading;
            this.translate.get('app.pleaseWait').subscribe((value) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                loading = yield this.loadingCtrl.create({
                    message: value,
                });
                yield loading.present();
            }));
            this.homeResultService.filterProperties(this.filterObject).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield loading.dismiss();
                this.properties = data.data;
                /* ****************CONDITION TO CHECK LOG IN USER ***************/
                if (this.saleLease === 'Lease') {
                    this.loginFlag = false;
                }
                else {
                    if (this.loginDetails != undefined && this.loginDetails != null) {
                        this.loginFlag = false;
                    }
                    else {
                        this.loginFlag = true;
                    }
                }
            }), err => {
                loading.dismiss();
                this.translate.get('property.failedToLoadProperties').subscribe((value) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    this.presentToast(value);
                }));
            });
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                showCloseButton: true,
                message: message,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    saleLeaseChange(value) {
        this.cnt = 0;
        if (value) {
            this.saleLease = 'Lease';
            this.filterObject.saleLease = 'Lease';
            this.filterObject.soldDays = "";
        }
        else {
            this.saleLease = 'Sale';
            this.filterObject.saleLease = 'Sale';
        }
        this.findAll();
    }
    propertyTypeChange(value) {
        this.cnt = 0;
        if (value) {
            this.propertyType = 'commercial';
            this.filterObject.propertyType = 'commercial';
        }
        else {
            this.propertyType = 'residential';
            this.filterObject.propertyType = 'residential';
        }
        this.findAll();
    }
    searchFilter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_4__["SearchFilterPage"],
                componentProps: {
                    setFilterObject: this.filterObject
                }
            });
            modal.onDidDismiss().then((dataReturned) => {
                if (dataReturned !== null) {
                    if (dataReturned.data != null) {
                        this.filterObject = dataReturned.data;
                        if (dataReturned.data.saleLease === 'Lease') {
                            this.saleLease = 'Lease';
                            this.loginFlag = false;
                        }
                        else if (dataReturned.data.saleLease === 'Sale') {
                            this.saleLease = 'Sale';
                            if (this.loginDetails != undefined && this.loginDetails != null) {
                                this.loginFlag = false;
                            }
                            else {
                                this.loginFlag = true;
                            }
                        }
                        this.findAll();
                    }
                }
            });
            return yield modal.present();
        });
    }
    language() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.translate.get(['app.selectLanguage', 'app.cancel']).subscribe((value) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                const actionSheet = yield this.asCtrl.create({
                    header: value['app.selectLanguage'],
                    buttons: [{
                            text: 'English',
                            handler: () => {
                                this.translate.use('en');
                                localStorage.setItem("language", 'en');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: 'Española',
                            handler: () => {
                                this.translate.use('sp');
                                localStorage.setItem("language", 'sp');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: 'हिंदी',
                            handler: () => {
                                this.translate.use('hn');
                                localStorage.setItem("language", 'hn');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: 'Française',
                            handler: () => {
                                this.translate.use('fr');
                                localStorage.setItem("language", 'fr');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: 'عربى',
                            handler: () => {
                                this.translate.use('ar');
                                localStorage.setItem("language", 'ar');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: '中文',
                            handler: () => {
                                this.translate.use('ch');
                                localStorage.setItem("language", 'ch');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: 'فارسی',
                            handler: () => {
                                this.translate.use('per');
                                localStorage.setItem("language", 'per');
                                setTimeout(() => {
                                    window.location.reload();
                                }, 150);
                            }
                        }, {
                            text: value['app.cancel'],
                            role: 'cancel',
                        }]
                });
                yield actionSheet.present();
            }));
        });
    }
    moreProperties() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: "Please wait...",
            });
            yield loading.present();
            this.cnt = this.cnt + 1;
            this.filterObject.currentPage = this.cnt;
            this.homeResultService.filterProperties(this.filterObject).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield loading.dismiss();
                if (res.data.length === 0) {
                    this.moreResults = false;
                }
                res.data.forEach(element => {
                    this.properties.push(element);
                });
            }), err => {
                loading.dismiss();
                console.log(err);
            });
        });
    }
    redirectToMap(data) {
        if (data === 'address') {
            this.cnt = -1;
            this.saleLease = "Sale";
            this.propertyType = "residential";
            this.filterObject = this.advancedFilterObject();
        }
        else {
            this.filterObject.saleLease = data.saleLease;
            this.filterObject.propertyType = data.propertyType;
            this.filterObject.latitude = data.latitude;
            this.filterObject.longitude = data.longitude;
        }
        setTimeout(() => {
            var filter = JSON.stringify(this.filterObject);
            this.navCtrl.navigateForward('/map-view', { state: { filterData: filter } });
        }, 150);
    }
    goToMap(type) {
        var filterObj = this.filterObject;
        if (type === 'commercialSale') {
            filterObj.propertyType = 'commercial';
            filterObj.saleLease = 'Sale';
        }
        else if (type === 'commercialLease') {
            filterObj.propertyType = 'commercial';
            filterObj.saleLease = 'Lease';
        }
        else if (type === 'residentialSale') {
            filterObj.propertyType = 'residential';
            filterObj.saleLease = 'Sale';
        }
        else if (type === 'residentialLease') {
            filterObj.propertyType = 'residential';
            filterObj.saleLease = 'Lease';
        }
        setTimeout(() => {
            var filter = JSON.stringify(filterObj);
            this.navCtrl.navigateForward('/map-view', { state: { filterData: filter } });
        }, 150);
    }
};
HomeResultsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"] },
    { type: _home_results_service__WEBPACK_IMPORTED_MODULE_9__["HomeResultsService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] }
];
HomeResultsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-results',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home-results.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-results/home-results.page.html")).default,
        animations: [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["trigger"])('staggerIn', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["transition"])('* => *', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({ opacity: 0, transform: `translate3d(100px,0,0)` }), { optional: true }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["stagger"])('200ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["animate"])('400ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_5__["style"])({ opacity: 1, transform: `translate3d(0,0,0)` }))]), { optional: true })
                ])
            ])
        ],
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home-results.page.scss */ "./src/app/pages/home-results/home-results.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_6__["TranslateService"],
        _home_results_service__WEBPACK_IMPORTED_MODULE_9__["HomeResultsService"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]])
], HomeResultsPage);



/***/ }),

/***/ "./src/app/pages/home-results/home-results.service.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/home-results/home-results.service.ts ***!
  \************************************************************/
/*! exports provided: HomeResultsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsService", function() { return HomeResultsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../api.service */ "./src/app/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let HomeResultsService = class HomeResultsService {
    constructor(http, apiService) {
        this.http = http;
        this.apiService = apiService;
        const headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
    }
    filterProperties(data) {
        return this.http.post(this.apiService.config + 'property/searchAttributes', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
    searchProperties(data) {
        return this.http.post(this.apiService.config + 'property/search', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
};
HomeResultsService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"] }
];
HomeResultsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])
], HomeResultsService);



/***/ })

}]);
//# sourceMappingURL=pages-home-results-home-results-module-es2015.js.map