(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content class=\"ion-padding animated fadeIn login\">\n\t<!-- Logo -->\n\t<div class=\"animated fadeInDown ion-text-center ion-padding-horizontal\">\n\t\t<div class=\"login-logo\"></div>\n\t</div>\n\n\t<!-- Login form -->\n\t<div class=\"title\">{{ 'app.login' | translate }}</div>\n\n\t<form id=\"login\" [formGroup]=\"onLoginForm\" (ngSubmit)=\"login(onLoginForm.value)\">\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'login.email' | translate }}</mat-label>\n\t\t\t<input matInput type=\"email\" autocomplete=\"off\" required formControlName=\"email\">\n\t\t\t<mat-icon matSuffix>mail_outline</mat-icon>\n\t\t</mat-form-field>\n\n\t\t<mat-form-field appearance=\"outline\" class=\"fieldFormat\">\n\t\t\t<mat-label>{{ 'login.password' | translate }}</mat-label>\n\t\t\t<input matInput type=\"password\" autocomplete=\"off\" required formControlName=\"password\">\n\t\t\t<mat-icon matSuffix>vpn_key</mat-icon>\n\t\t</mat-form-field>\n\n\t\t<div class=\"remember-forgot-password\">\n\t\t\t<a class=\"forgot-password\" (click)=\"forgotPassword()\">\n\t\t\t\t{{ 'login.forgotPassword' | translate }}\n\t\t\t</a>\n\t\t</div>\n\n\t\t<ion-button type=\"submit\" class=\"submit-btn\" color=\"danger\" [disabled]=\"onLoginForm.invalid\">\n\t\t\t{{ 'login.login' | translate }}\n\t\t</ion-button>\n\n\t\t<div class=\"or-format\">-- {{ 'register.or' | translate }} --</div>\n\n\t\t<ion-button type=\"button\" class=\"google-btn\">\n\t\t\t{{ 'login.loginWithGoogle' | translate }}\n\t\t</ion-button>\n\n\t\t<ion-button type=\"button\" class=\"fb-btn\" (click)=\"loginWithFacebok()\">\n\t\t\t{{ 'login.loginWithFacebook' | translate }}\n\t\t</ion-button>\n\n\t\t<div class=\"remember-forgot-password\">\n\t\t\t<a class=\"forgot-password\" (click)=\"signUpPage()\">\n\t\t\t\t{{ 'login.newUser' | translate }}\n\t\t\t</a>\n\t\t</div>\n\n\t</form>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./login.service */ "./src/app/pages/login/login.service.ts");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");













const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_10__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatInputModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_9__["IonicModule"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateModule"].forChild(),
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forChild(routes),
        ],
        declarations: [
            _login_page__WEBPACK_IMPORTED_MODULE_10__["LoginPage"]
        ],
        providers: [
            _login_service__WEBPACK_IMPORTED_MODULE_11__["LoginService"],
            _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_12__["Facebook"]
        ]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  text-align: center !important;\n  padding: 24px !important;\n}\n:host form {\n  width: 100%;\n  padding-top: 32px;\n  font-size: 14px;\n}\n.login-logo {\n  height: 120px;\n  width: 128px;\n  margin: 32px auto;\n  background: url(\"/assets/img/logo.png\") no-repeat;\n  background-size: 100%;\n}\n.title {\n  font-size: 21px;\n  text-align: center;\n  font-weight: 400 !important;\n}\n.remember-forgot-password {\n  font-size: 13px;\n  margin-top: 8px;\n  flex-direction: column;\n  box-sizing: border-box;\n  display: flex;\n  place-content: center space-between;\n  align-items: center;\n}\n.fieldFormat {\n  width: 90%;\n}\n.forgot-password {\n  font-size: 13px;\n  font-weight: 500;\n  margin-bottom: 16px;\n  color: #039be5;\n}\n.submit-btn {\n  width: 80%;\n}\n.fb-btn {\n  text-transform: inherit;\n  --background: #3f5c9a;\n  font-size: 13px;\n  margin: auto;\n  margin-bottom: 15px;\n  display: flex;\n  width: 70%;\n  letter-spacing: 0px;\n}\n.google-btn {\n  text-transform: inherit;\n  --background: #d73d32;\n  font-size: 13px;\n  margin: auto;\n  margin-bottom: 7px;\n  display: flex;\n  width: 70%;\n  letter-spacing: 0px;\n}\n.or-format {\n  color: #888;\n  font-weight: 500;\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSw2QkFBQTtFQUNBLHdCQUFBO0FDQVI7QURHSTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUNEUjtBREtBO0VBQ0ksYUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGlEQUFBO0VBQ0EscUJBQUE7QUNGSjtBREtBO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsMkJBQUE7QUNGSjtBREtBO0VBQ0ksZUFBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLG1DQUFBO0VBQ0EsbUJBQUE7QUNGSjtBREtBO0VBQ0ksVUFBQTtBQ0ZKO0FES0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUNGSjtBREtBO0VBQ0ksVUFBQTtBQ0ZKO0FES0E7RUFDSSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7QUNGSjtBREtBO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0FDRko7QURLQTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNGSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xuICAgICAgICBwYWRkaW5nOiAyNHB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgZm9ybSB7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBwYWRkaW5nLXRvcDogMzJweDtcbiAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgIH1cbn1cblxuLmxvZ2luLWxvZ28ge1xuICAgIGhlaWdodDogMTIwcHg7XG4gICAgd2lkdGg6IDEyOHB4O1xuICAgIG1hcmdpbjogMzJweCBhdXRvO1xuICAgIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2xvZ28ucG5nXCIpIG5vLXJlcGVhdDtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7XG59XG5cbi50aXRsZSB7XG4gICAgZm9udC1zaXplOiAyMXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlciA7XG4gICAgZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xufVxuXG4ucmVtZW1iZXItZm9yZ290LXBhc3N3b3JkIHtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbWFyZ2luLXRvcDogOHB4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHBsYWNlLWNvbnRlbnQ6IGNlbnRlciBzcGFjZS1iZXR3ZWVuO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5maWVsZEZvcm1hdCB7XG4gICAgd2lkdGg6IDkwJTtcbn1cblxuLmZvcmdvdC1wYXNzd29yZCB7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgICBjb2xvcjogIzAzOWJlNTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICAgIHdpZHRoOiA4MCU7XG59XG5cbi5mYi1idG4ge1xuICAgIHRleHQtdHJhbnNmb3JtOiBpbmhlcml0O1xuICAgIC0tYmFja2dyb3VuZDogIzNmNWM5YTtcbiAgICBmb250LXNpemU6IDEzcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogNzAlO1xuICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG59XG5cbi5nb29nbGUtYnRuIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogaW5oZXJpdDtcbiAgICAtLWJhY2tncm91bmQ6ICNkNzNkMzI7XG4gICAgZm9udC1zaXplOiAxM3B4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiA3cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogNzAlO1xuICAgIGxldHRlci1zcGFjaW5nOiAwcHg7XG59XG5cbi5vci1mb3JtYXQge1xuICAgIGNvbG9yOiAjODg4O1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgbWFyZ2luOiAxMHB4O1xufSIsIjpob3N0IGlvbi1jb250ZW50IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDI0cHggIWltcG9ydGFudDtcbn1cbjpob3N0IGZvcm0ge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy10b3A6IDMycHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmxvZ2luLWxvZ28ge1xuICBoZWlnaHQ6IDEyMHB4O1xuICB3aWR0aDogMTI4cHg7XG4gIG1hcmdpbjogMzJweCBhdXRvO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltZy9sb2dvLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogMTAwJTtcbn1cblxuLnRpdGxlIHtcbiAgZm9udC1zaXplOiAyMXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiA0MDAgIWltcG9ydGFudDtcbn1cblxuLnJlbWVtYmVyLWZvcmdvdC1wYXNzd29yZCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luLXRvcDogOHB4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBkaXNwbGF5OiBmbGV4O1xuICBwbGFjZS1jb250ZW50OiBjZW50ZXIgc3BhY2UtYmV0d2VlbjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLmZpZWxkRm9ybWF0IHtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLmZvcmdvdC1wYXNzd29yZCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luLWJvdHRvbTogMTZweDtcbiAgY29sb3I6ICMwMzliZTU7XG59XG5cbi5zdWJtaXQtYnRuIHtcbiAgd2lkdGg6IDgwJTtcbn1cblxuLmZiLWJ0biB7XG4gIHRleHQtdHJhbnNmb3JtOiBpbmhlcml0O1xuICAtLWJhY2tncm91bmQ6ICMzZjVjOWE7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICB3aWR0aDogNzAlO1xuICBsZXR0ZXItc3BhY2luZzogMHB4O1xufVxuXG4uZ29vZ2xlLWJ0biB7XG4gIHRleHQtdHJhbnNmb3JtOiBpbmhlcml0O1xuICAtLWJhY2tncm91bmQ6ICNkNzNkMzI7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgbWFyZ2luOiBhdXRvO1xuICBtYXJnaW4tYm90dG9tOiA3cHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIHdpZHRoOiA3MCU7XG4gIGxldHRlci1zcGFjaW5nOiAwcHg7XG59XG5cbi5vci1mb3JtYXQge1xuICBjb2xvcjogIzg4ODtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgbWFyZ2luOiAxMHB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login.service */ "./src/app/pages/login/login.service.ts");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");







let LoginPage = class LoginPage {
    constructor(navCtrl, menuCtrl, toastCtrl, loginService, loadingCtrl, formBuilder, router, facebook) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.loginService = loginService;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.router = router;
        this.facebook = facebook;
        this.emailReg = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(false);
        this.menuCtrl.swipeEnable(false);
    }
    ngOnInit() {
        this.loginFormDetails();
    }
    loginFormDetails() {
        this.onLoginForm = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(this.emailReg)]),
            password: ''
        });
    }
    login(inputData) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: "Loading...",
            });
            yield loading.present();
            let loginData = {};
            loginData['email'] = inputData.email;
            loginData['password'] = inputData.password;
            this.loginService.login(loginData).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                if (res.code === 200 && res.data.token) {
                    localStorage.setItem("userDetails", JSON.stringify(res));
                    this.menuCtrl.enable(true);
                    yield loading.dismiss();
                    setTimeout(() => {
                        window.location.reload();
                        // this.router.navigateByUrl('/tabs/(home:home)');
                    }, 500);
                }
                else {
                    yield loading.dismiss();
                    if (res.errorKey === "email400") {
                        this.presentToast("This Email Id is Invalid");
                    }
                    else if (res.errorKey === "password400") {
                        this.presentToast("This Password is Invalid");
                    }
                    else if (res.errorKey === "active400") {
                        this.presentToast("Please Activate Your Account");
                    }
                }
            }), err => {
                this.presentToast("Something Went Wrong");
                loading.dismiss();
            });
        });
    }
    loginWithFacebok() {
        var userData = null;
        ;
        this.facebook.login(['email', 'public_profile']).then((response) => {
            this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
                userData = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'] };
                console.log("d", userData);
            });
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                showCloseButton: true,
                message: message,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    signUpPage() {
        this.navCtrl.navigateRoot('/register');
    }
    forgotPassword() {
        this.navCtrl.navigateRoot('/forgotpwd-view');
    }
};
LoginPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _login_service__WEBPACK_IMPORTED_MODULE_5__["LoginService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"]])
], LoginPage);



/***/ }),

/***/ "./src/app/pages/login/login.service.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/login/login.service.ts ***!
  \**********************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../api.service */ "./src/app/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let LoginService = class LoginService {
    constructor(http, apiService) {
        this.http = http;
        this.apiService = apiService;
        const headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
    }
    login(data) {
        return this.http.post(this.apiService.config + 'login', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
};
LoginService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"] }
];
LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])
], LoginService);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map