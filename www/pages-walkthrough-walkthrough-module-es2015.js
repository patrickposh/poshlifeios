(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-walkthrough-walkthrough-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n\t<ion-slides pager=\"true\" dir=\"ltr\" options=\"slideOpts\">\n\t\t<ion-slide class=\"bg-profile\">\n\t\t\t<div class=\"w100 ion-padding\">\n\t\t\t\t<img src=\"assets/img/PoshLife-Logo.jpg\" alt=\"POSH LIFE\" class=\"slide-image animated fadeInDown slow\">\n\t\t\t\t<h2 class=\"slide-title fw100 ion-margin-bottom\">{{ 'app.tagLine' | translate }}</h2>\n\t\t\t\t<hr>\n\t\t\t\t<ion-button expand=\"full\" shape=\"round\" color=\"danger\" class=\"btn-next ion-margin-top\"\n\t\t\t\t\t(click)=\"openLoginPage()\">\n\t\t\t\t\t{{ 'app.signInOrSignUp' | translate }}\n\t\t\t\t</ion-button>\n\t\t\t\t<ion-button expand=\"full\" shape=\"round\" color=\"dark\" class=\"btn-next ion-margin-top\"\n\t\t\t\t\t(click)=\"openHomeLocation()\">\n\t\t\t\t\t{{ 'app.getStarted' | translate }}\n\t\t\t\t</ion-button>\n\t\t\t</div>\n\t\t</ion-slide>\n\t</ion-slides>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/walkthrough/walkthrough.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/walkthrough/walkthrough.module.ts ***!
  \*********************************************************/
/*! exports provided: WalkthroughPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPageModule", function() { return WalkthroughPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _walkthrough_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./walkthrough.page */ "./src/app/pages/walkthrough/walkthrough.page.ts");








const routes = [
    {
        path: '',
        component: _walkthrough_page__WEBPACK_IMPORTED_MODULE_7__["WalkthroughPage"]
    }
];
let WalkthroughPageModule = class WalkthroughPageModule {
};
WalkthroughPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"].forChild()
        ],
        declarations: [_walkthrough_page__WEBPACK_IMPORTED_MODULE_7__["WalkthroughPage"]]
    })
], WalkthroughPageModule);



/***/ }),

/***/ "./src/app/pages/walkthrough/walkthrough.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/walkthrough/walkthrough.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-slides {\n  height: 100%;\n}\n\n.slide-title {\n  margin-top: 0;\n}\n\n.btn-next {\n  color: #fff !important;\n}\n\n.slide-image {\n  max-width: 128px !important;\n  margin: 0 0 18px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvd2Fsa3Rocm91Z2gucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy93YWxrdGhyb3VnaC93YWxrdGhyb3VnaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxZQUFBO0FDQ0Q7O0FERUE7RUFDQyxhQUFBO0FDQ0Q7O0FERUE7RUFDQyxzQkFBQTtBQ0NEOztBREVBO0VBQ0MsMkJBQUE7RUFDQSxnQkFBQTtBQ0NEIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvd2Fsa3Rocm91Z2gvd2Fsa3Rocm91Z2gucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXNsaWRlcyB7XG5cdGhlaWdodDogMTAwJTtcbn1cblxuLnNsaWRlLXRpdGxlIHtcblx0bWFyZ2luLXRvcDogMDtcbn1cblxuLmJ0bi1uZXh0IHtcblx0Y29sb3I6ICNmZmYgIWltcG9ydGFudDtcbn1cblxuLnNsaWRlLWltYWdlIHtcblx0bWF4LXdpZHRoOiAxMjhweCAhaW1wb3J0YW50O1xuXHRtYXJnaW46IDAgMCAxOHB4O1xufSIsImlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5zbGlkZS10aXRsZSB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5idG4tbmV4dCB7XG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG59XG5cbi5zbGlkZS1pbWFnZSB7XG4gIG1heC13aWR0aDogMTI4cHggIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwIDAgMThweDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/walkthrough/walkthrough.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/walkthrough/walkthrough.page.ts ***!
  \*******************************************************/
/*! exports provided: WalkthroughPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPage", function() { return WalkthroughPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




let WalkthroughPage = class WalkthroughPage {
    constructor(navCtrl, menuCtrl, router) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.router = router;
        this.showSkip = true;
        this.slideOpts = {
            effect: 'flip',
            speed: 1000
        };
        this.dir = 'ltr';
        this.menuCtrl.enable(false);
    }
    openHomeLocation() {
        this.router.navigateByUrl('/tabs/(home:home)');
    }
    openLoginPage() {
        this.navCtrl.navigateForward('/login');
    }
};
WalkthroughPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"], { static: true }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonSlides"])
], WalkthroughPage.prototype, "slides", void 0);
WalkthroughPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-walkthrough',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./walkthrough.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/walkthrough/walkthrough.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./walkthrough.page.scss */ "./src/app/pages/walkthrough/walkthrough.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], WalkthroughPage);



/***/ })

}]);
//# sourceMappingURL=pages-walkthrough-walkthrough-module-es2015.js.map