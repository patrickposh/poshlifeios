(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-map-view-map-view-module"],{

/***/ "./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js":
/*!******************************************************************************!*\
  !*** ./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*
 * Leaflet.markercluster 1.4.1+master.94f9815,
 * Provides Beautiful Animated Marker Clustering functionality for Leaflet, a JS library for interactive maps.
 * https://github.com/Leaflet/Leaflet.markercluster
 * (c) 2012-2017, Dave Leaver, smartrak
 */
(function (global, factory) {
	 true ? factory(exports) :
	undefined;
}(this, (function (exports) { 'use strict';

/*
 * L.MarkerClusterGroup extends L.FeatureGroup by clustering the markers contained within
 */

var MarkerClusterGroup = L.MarkerClusterGroup = L.FeatureGroup.extend({

	options: {
		maxClusterRadius: 80, //A cluster will cover at most this many pixels from its center
		iconCreateFunction: null,
		clusterPane: L.Marker.prototype.options.pane,

		spiderfyOnMaxZoom: true,
		showCoverageOnHover: true,
		zoomToBoundsOnClick: true,
		singleMarkerMode: false,

		disableClusteringAtZoom: null,

		// Setting this to false prevents the removal of any clusters outside of the viewpoint, which
		// is the default behaviour for performance reasons.
		removeOutsideVisibleBounds: true,

		// Set to false to disable all animations (zoom and spiderfy).
		// If false, option animateAddingMarkers below has no effect.
		// If L.DomUtil.TRANSITION is falsy, this option has no effect.
		animate: true,

		//Whether to animate adding markers after adding the MarkerClusterGroup to the map
		// If you are adding individual markers set to true, if adding bulk markers leave false for massive performance gains.
		animateAddingMarkers: false,

		//Increase to increase the distance away that spiderfied markers appear from the center
		spiderfyDistanceMultiplier: 1,

		// Make it possible to specify a polyline options on a spider leg
		spiderLegPolylineOptions: { weight: 1.5, color: '#222', opacity: 0.5 },

		// When bulk adding layers, adds markers in chunks. Means addLayers may not add all the layers in the call, others will be loaded during setTimeouts
		chunkedLoading: false,
		chunkInterval: 200, // process markers for a maximum of ~ n milliseconds (then trigger the chunkProgress callback)
		chunkDelay: 50, // at the end of each interval, give n milliseconds back to system/browser
		chunkProgress: null, // progress callback: function(processed, total, elapsed) (e.g. for a progress indicator)

		//Options to pass to the L.Polygon constructor
		polygonOptions: {}
	},

	initialize: function (options) {
		L.Util.setOptions(this, options);
		if (!this.options.iconCreateFunction) {
			this.options.iconCreateFunction = this._defaultIconCreateFunction;
		}

		this._featureGroup = L.featureGroup();
		this._featureGroup.addEventParent(this);

		this._nonPointGroup = L.featureGroup();
		this._nonPointGroup.addEventParent(this);

		this._inZoomAnimation = 0;
		this._needsClustering = [];
		this._needsRemoving = []; //Markers removed while we aren't on the map need to be kept track of
		//The bounds of the currently shown area (from _getExpandedVisibleBounds) Updated on zoom/move
		this._currentShownBounds = null;

		this._queue = [];

		this._childMarkerEventHandlers = {
			'dragstart': this._childMarkerDragStart,
			'move': this._childMarkerMoved,
			'dragend': this._childMarkerDragEnd,
		};

		// Hook the appropriate animation methods.
		var animate = L.DomUtil.TRANSITION && this.options.animate;
		L.extend(this, animate ? this._withAnimation : this._noAnimation);
		// Remember which MarkerCluster class to instantiate (animated or not).
		this._markerCluster = animate ? L.MarkerCluster : L.MarkerClusterNonAnimated;
	},

	addLayer: function (layer) {

		if (layer instanceof L.LayerGroup) {
			return this.addLayers([layer]);
		}

		//Don't cluster non point data
		if (!layer.getLatLng) {
			this._nonPointGroup.addLayer(layer);
			this.fire('layeradd', { layer: layer });
			return this;
		}

		if (!this._map) {
			this._needsClustering.push(layer);
			this.fire('layeradd', { layer: layer });
			return this;
		}

		if (this.hasLayer(layer)) {
			return this;
		}


		//If we have already clustered we'll need to add this one to a cluster

		if (this._unspiderfy) {
			this._unspiderfy();
		}

		this._addLayer(layer, this._maxZoom);
		this.fire('layeradd', { layer: layer });

		// Refresh bounds and weighted positions.
		this._topClusterLevel._recalculateBounds();

		this._refreshClustersIcons();

		//Work out what is visible
		var visibleLayer = layer,
		    currentZoom = this._zoom;
		if (layer.__parent) {
			while (visibleLayer.__parent._zoom >= currentZoom) {
				visibleLayer = visibleLayer.__parent;
			}
		}

		if (this._currentShownBounds.contains(visibleLayer.getLatLng())) {
			if (this.options.animateAddingMarkers) {
				this._animationAddLayer(layer, visibleLayer);
			} else {
				this._animationAddLayerNonAnimated(layer, visibleLayer);
			}
		}
		return this;
	},

	removeLayer: function (layer) {

		if (layer instanceof L.LayerGroup) {
			return this.removeLayers([layer]);
		}

		//Non point layers
		if (!layer.getLatLng) {
			this._nonPointGroup.removeLayer(layer);
			this.fire('layerremove', { layer: layer });
			return this;
		}

		if (!this._map) {
			if (!this._arraySplice(this._needsClustering, layer) && this.hasLayer(layer)) {
				this._needsRemoving.push({ layer: layer, latlng: layer._latlng });
			}
			this.fire('layerremove', { layer: layer });
			return this;
		}

		if (!layer.__parent) {
			return this;
		}

		if (this._unspiderfy) {
			this._unspiderfy();
			this._unspiderfyLayer(layer);
		}

		//Remove the marker from clusters
		this._removeLayer(layer, true);
		this.fire('layerremove', { layer: layer });

		// Refresh bounds and weighted positions.
		this._topClusterLevel._recalculateBounds();

		this._refreshClustersIcons();

		layer.off(this._childMarkerEventHandlers, this);

		if (this._featureGroup.hasLayer(layer)) {
			this._featureGroup.removeLayer(layer);
			if (layer.clusterShow) {
				layer.clusterShow();
			}
		}

		return this;
	},

	//Takes an array of markers and adds them in bulk
	addLayers: function (layersArray, skipLayerAddEvent) {
		if (!L.Util.isArray(layersArray)) {
			return this.addLayer(layersArray);
		}

		var fg = this._featureGroup,
		    npg = this._nonPointGroup,
		    chunked = this.options.chunkedLoading,
		    chunkInterval = this.options.chunkInterval,
		    chunkProgress = this.options.chunkProgress,
		    l = layersArray.length,
		    offset = 0,
		    originalArray = true,
		    m;

		if (this._map) {
			var started = (new Date()).getTime();
			var process = L.bind(function () {
				var start = (new Date()).getTime();
				for (; offset < l; offset++) {
					if (chunked && offset % 200 === 0) {
						// every couple hundred markers, instrument the time elapsed since processing started:
						var elapsed = (new Date()).getTime() - start;
						if (elapsed > chunkInterval) {
							break; // been working too hard, time to take a break :-)
						}
					}

					m = layersArray[offset];

					// Group of layers, append children to layersArray and skip.
					// Side effects:
					// - Total increases, so chunkProgress ratio jumps backward.
					// - Groups are not included in this group, only their non-group child layers (hasLayer).
					// Changing array length while looping does not affect performance in current browsers:
					// http://jsperf.com/for-loop-changing-length/6
					if (m instanceof L.LayerGroup) {
						if (originalArray) {
							layersArray = layersArray.slice();
							originalArray = false;
						}
						this._extractNonGroupLayers(m, layersArray);
						l = layersArray.length;
						continue;
					}

					//Not point data, can't be clustered
					if (!m.getLatLng) {
						npg.addLayer(m);
						if (!skipLayerAddEvent) {
							this.fire('layeradd', { layer: m });
						}
						continue;
					}

					if (this.hasLayer(m)) {
						continue;
					}

					this._addLayer(m, this._maxZoom);
					if (!skipLayerAddEvent) {
						this.fire('layeradd', { layer: m });
					}

					//If we just made a cluster of size 2 then we need to remove the other marker from the map (if it is) or we never will
					if (m.__parent) {
						if (m.__parent.getChildCount() === 2) {
							var markers = m.__parent.getAllChildMarkers(),
							    otherMarker = markers[0] === m ? markers[1] : markers[0];
							fg.removeLayer(otherMarker);
						}
					}
				}

				if (chunkProgress) {
					// report progress and time elapsed:
					chunkProgress(offset, l, (new Date()).getTime() - started);
				}

				// Completed processing all markers.
				if (offset === l) {

					// Refresh bounds and weighted positions.
					this._topClusterLevel._recalculateBounds();

					this._refreshClustersIcons();

					this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds);
				} else {
					setTimeout(process, this.options.chunkDelay);
				}
			}, this);

			process();
		} else {
			var needsClustering = this._needsClustering;

			for (; offset < l; offset++) {
				m = layersArray[offset];

				// Group of layers, append children to layersArray and skip.
				if (m instanceof L.LayerGroup) {
					if (originalArray) {
						layersArray = layersArray.slice();
						originalArray = false;
					}
					this._extractNonGroupLayers(m, layersArray);
					l = layersArray.length;
					continue;
				}

				//Not point data, can't be clustered
				if (!m.getLatLng) {
					npg.addLayer(m);
					continue;
				}

				if (this.hasLayer(m)) {
					continue;
				}

				needsClustering.push(m);
			}
		}
		return this;
	},

	//Takes an array of markers and removes them in bulk
	removeLayers: function (layersArray) {
		var i, m,
		    l = layersArray.length,
		    fg = this._featureGroup,
		    npg = this._nonPointGroup,
		    originalArray = true;

		if (!this._map) {
			for (i = 0; i < l; i++) {
				m = layersArray[i];

				// Group of layers, append children to layersArray and skip.
				if (m instanceof L.LayerGroup) {
					if (originalArray) {
						layersArray = layersArray.slice();
						originalArray = false;
					}
					this._extractNonGroupLayers(m, layersArray);
					l = layersArray.length;
					continue;
				}

				this._arraySplice(this._needsClustering, m);
				npg.removeLayer(m);
				if (this.hasLayer(m)) {
					this._needsRemoving.push({ layer: m, latlng: m._latlng });
				}
				this.fire('layerremove', { layer: m });
			}
			return this;
		}

		if (this._unspiderfy) {
			this._unspiderfy();

			// Work on a copy of the array, so that next loop is not affected.
			var layersArray2 = layersArray.slice(),
			    l2 = l;
			for (i = 0; i < l2; i++) {
				m = layersArray2[i];

				// Group of layers, append children to layersArray and skip.
				if (m instanceof L.LayerGroup) {
					this._extractNonGroupLayers(m, layersArray2);
					l2 = layersArray2.length;
					continue;
				}

				this._unspiderfyLayer(m);
			}
		}

		for (i = 0; i < l; i++) {
			m = layersArray[i];

			// Group of layers, append children to layersArray and skip.
			if (m instanceof L.LayerGroup) {
				if (originalArray) {
					layersArray = layersArray.slice();
					originalArray = false;
				}
				this._extractNonGroupLayers(m, layersArray);
				l = layersArray.length;
				continue;
			}

			if (!m.__parent) {
				npg.removeLayer(m);
				this.fire('layerremove', { layer: m });
				continue;
			}

			this._removeLayer(m, true, true);
			this.fire('layerremove', { layer: m });

			if (fg.hasLayer(m)) {
				fg.removeLayer(m);
				if (m.clusterShow) {
					m.clusterShow();
				}
			}
		}

		// Refresh bounds and weighted positions.
		this._topClusterLevel._recalculateBounds();

		this._refreshClustersIcons();

		//Fix up the clusters and markers on the map
		this._topClusterLevel._recursivelyAddChildrenToMap(null, this._zoom, this._currentShownBounds);

		return this;
	},

	//Removes all layers from the MarkerClusterGroup
	clearLayers: function () {
		//Need our own special implementation as the LayerGroup one doesn't work for us

		//If we aren't on the map (yet), blow away the markers we know of
		if (!this._map) {
			this._needsClustering = [];
			this._needsRemoving = [];
			delete this._gridClusters;
			delete this._gridUnclustered;
		}

		if (this._noanimationUnspiderfy) {
			this._noanimationUnspiderfy();
		}

		//Remove all the visible layers
		this._featureGroup.clearLayers();
		this._nonPointGroup.clearLayers();

		this.eachLayer(function (marker) {
			marker.off(this._childMarkerEventHandlers, this);
			delete marker.__parent;
		}, this);

		if (this._map) {
			//Reset _topClusterLevel and the DistanceGrids
			this._generateInitialClusters();
		}

		return this;
	},

	//Override FeatureGroup.getBounds as it doesn't work
	getBounds: function () {
		var bounds = new L.LatLngBounds();

		if (this._topClusterLevel) {
			bounds.extend(this._topClusterLevel._bounds);
		}

		for (var i = this._needsClustering.length - 1; i >= 0; i--) {
			bounds.extend(this._needsClustering[i].getLatLng());
		}

		bounds.extend(this._nonPointGroup.getBounds());

		return bounds;
	},

	//Overrides LayerGroup.eachLayer
	eachLayer: function (method, context) {
		var markers = this._needsClustering.slice(),
			needsRemoving = this._needsRemoving,
			thisNeedsRemoving, i, j;

		if (this._topClusterLevel) {
			this._topClusterLevel.getAllChildMarkers(markers);
		}

		for (i = markers.length - 1; i >= 0; i--) {
			thisNeedsRemoving = true;

			for (j = needsRemoving.length - 1; j >= 0; j--) {
				if (needsRemoving[j].layer === markers[i]) {
					thisNeedsRemoving = false;
					break;
				}
			}

			if (thisNeedsRemoving) {
				method.call(context, markers[i]);
			}
		}

		this._nonPointGroup.eachLayer(method, context);
	},

	//Overrides LayerGroup.getLayers
	getLayers: function () {
		var layers = [];
		this.eachLayer(function (l) {
			layers.push(l);
		});
		return layers;
	},

	//Overrides LayerGroup.getLayer, WARNING: Really bad performance
	getLayer: function (id) {
		var result = null;

		id = parseInt(id, 10);

		this.eachLayer(function (l) {
			if (L.stamp(l) === id) {
				result = l;
			}
		});

		return result;
	},

	//Returns true if the given layer is in this MarkerClusterGroup
	hasLayer: function (layer) {
		if (!layer) {
			return false;
		}

		var i, anArray = this._needsClustering;

		for (i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === layer) {
				return true;
			}
		}

		anArray = this._needsRemoving;
		for (i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i].layer === layer) {
				return false;
			}
		}

		return !!(layer.__parent && layer.__parent._group === this) || this._nonPointGroup.hasLayer(layer);
	},

	//Zoom down to show the given layer (spiderfying if necessary) then calls the callback
	zoomToShowLayer: function (layer, callback) {

		if (typeof callback !== 'function') {
			callback = function () {};
		}

		var showMarker = function () {
			if ((layer._icon || layer.__parent._icon) && !this._inZoomAnimation) {
				this._map.off('moveend', showMarker, this);
				this.off('animationend', showMarker, this);

				if (layer._icon) {
					callback();
				} else if (layer.__parent._icon) {
					this.once('spiderfied', callback, this);
					layer.__parent.spiderfy();
				}
			}
		};

		if (layer._icon && this._map.getBounds().contains(layer.getLatLng())) {
			//Layer is visible ond on screen, immediate return
			callback();
		} else if (layer.__parent._zoom < Math.round(this._map._zoom)) {
			//Layer should be visible at this zoom level. It must not be on screen so just pan over to it
			this._map.on('moveend', showMarker, this);
			this._map.panTo(layer.getLatLng());
		} else {
			this._map.on('moveend', showMarker, this);
			this.on('animationend', showMarker, this);
			layer.__parent.zoomToBounds();
		}
	},

	//Overrides FeatureGroup.onAdd
	onAdd: function (map) {
		this._map = map;
		var i, l, layer;

		if (!isFinite(this._map.getMaxZoom())) {
			throw "Map has no maxZoom specified";
		}

		this._featureGroup.addTo(map);
		this._nonPointGroup.addTo(map);

		if (!this._gridClusters) {
			this._generateInitialClusters();
		}

		this._maxLat = map.options.crs.projection.MAX_LATITUDE;

		//Restore all the positions as they are in the MCG before removing them
		for (i = 0, l = this._needsRemoving.length; i < l; i++) {
			layer = this._needsRemoving[i];
			layer.newlatlng = layer.layer._latlng;
			layer.layer._latlng = layer.latlng;
		}
		//Remove them, then restore their new positions
		for (i = 0, l = this._needsRemoving.length; i < l; i++) {
			layer = this._needsRemoving[i];
			this._removeLayer(layer.layer, true);
			layer.layer._latlng = layer.newlatlng;
		}
		this._needsRemoving = [];

		//Remember the current zoom level and bounds
		this._zoom = Math.round(this._map._zoom);
		this._currentShownBounds = this._getExpandedVisibleBounds();

		this._map.on('zoomend', this._zoomEnd, this);
		this._map.on('moveend', this._moveEnd, this);

		if (this._spiderfierOnAdd) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
			this._spiderfierOnAdd();
		}

		this._bindEvents();

		//Actually add our markers to the map:
		l = this._needsClustering;
		this._needsClustering = [];
		this.addLayers(l, true);
	},

	//Overrides FeatureGroup.onRemove
	onRemove: function (map) {
		map.off('zoomend', this._zoomEnd, this);
		map.off('moveend', this._moveEnd, this);

		this._unbindEvents();

		//In case we are in a cluster animation
		this._map._mapPane.className = this._map._mapPane.className.replace(' leaflet-cluster-anim', '');

		if (this._spiderfierOnRemove) { //TODO FIXME: Not sure how to have spiderfier add something on here nicely
			this._spiderfierOnRemove();
		}

		delete this._maxLat;

		//Clean up all the layers we added to the map
		this._hideCoverage();
		this._featureGroup.remove();
		this._nonPointGroup.remove();

		this._featureGroup.clearLayers();

		this._map = null;
	},

	getVisibleParent: function (marker) {
		var vMarker = marker;
		while (vMarker && !vMarker._icon) {
			vMarker = vMarker.__parent;
		}
		return vMarker || null;
	},

	//Remove the given object from the given array
	_arraySplice: function (anArray, obj) {
		for (var i = anArray.length - 1; i >= 0; i--) {
			if (anArray[i] === obj) {
				anArray.splice(i, 1);
				return true;
			}
		}
	},

	/**
	 * Removes a marker from all _gridUnclustered zoom levels, starting at the supplied zoom.
	 * @param marker to be removed from _gridUnclustered.
	 * @param z integer bottom start zoom level (included)
	 * @private
	 */
	_removeFromGridUnclustered: function (marker, z) {
		var map = this._map,
		    gridUnclustered = this._gridUnclustered,
			minZoom = Math.floor(this._map.getMinZoom());

		for (; z >= minZoom; z--) {
			if (!gridUnclustered[z].removeObject(marker, map.project(marker.getLatLng(), z))) {
				break;
			}
		}
	},

	_childMarkerDragStart: function (e) {
		e.target.__dragStart = e.target._latlng;
	},

	_childMarkerMoved: function (e) {
		if (!this._ignoreMove && !e.target.__dragStart) {
			var isPopupOpen = e.target._popup && e.target._popup.isOpen();

			this._moveChild(e.target, e.oldLatLng, e.latlng);

			if (isPopupOpen) {
				e.target.openPopup();
			}
		}
	},

	_moveChild: function (layer, from, to) {
		layer._latlng = from;
		this.removeLayer(layer);

		layer._latlng = to;
		this.addLayer(layer);
	},

	_childMarkerDragEnd: function (e) {
		var dragStart = e.target.__dragStart;
		delete e.target.__dragStart;
		if (dragStart) {
			this._moveChild(e.target, dragStart, e.target._latlng);
		}		
	},


	//Internal function for removing a marker from everything.
	//dontUpdateMap: set to true if you will handle updating the map manually (for bulk functions)
	_removeLayer: function (marker, removeFromDistanceGrid, dontUpdateMap) {
		var gridClusters = this._gridClusters,
			gridUnclustered = this._gridUnclustered,
			fg = this._featureGroup,
			map = this._map,
			minZoom = Math.floor(this._map.getMinZoom());

		//Remove the marker from distance clusters it might be in
		if (removeFromDistanceGrid) {
			this._removeFromGridUnclustered(marker, this._maxZoom);
		}

		//Work our way up the clusters removing them as we go if required
		var cluster = marker.__parent,
			markers = cluster._markers,
			otherMarker;

		//Remove the marker from the immediate parents marker list
		this._arraySplice(markers, marker);

		while (cluster) {
			cluster._childCount--;
			cluster._boundsNeedUpdate = true;

			if (cluster._zoom < minZoom) {
				//Top level, do nothing
				break;
			} else if (removeFromDistanceGrid && cluster._childCount <= 1) { //Cluster no longer required
				//We need to push the other marker up to the parent
				otherMarker = cluster._markers[0] === marker ? cluster._markers[1] : cluster._markers[0];

				//Update distance grid
				gridClusters[cluster._zoom].removeObject(cluster, map.project(cluster._cLatLng, cluster._zoom));
				gridUnclustered[cluster._zoom].addObject(otherMarker, map.project(otherMarker.getLatLng(), cluster._zoom));

				//Move otherMarker up to parent
				this._arraySplice(cluster.__parent._childClusters, cluster);
				cluster.__parent._markers.push(otherMarker);
				otherMarker.__parent = cluster.__parent;

				if (cluster._icon) {
					//Cluster is currently on the map, need to put the marker on the map instead
					fg.removeLayer(cluster);
					if (!dontUpdateMap) {
						fg.addLayer(otherMarker);
					}
				}
			} else {
				cluster._iconNeedsUpdate = true;
			}

			cluster = cluster.__parent;
		}

		delete marker.__parent;
	},

	_isOrIsParent: function (el, oel) {
		while (oel) {
			if (el === oel) {
				return true;
			}
			oel = oel.parentNode;
		}
		return false;
	},

	//Override L.Evented.fire
	fire: function (type, data, propagate) {
		if (data && data.layer instanceof L.MarkerCluster) {
			//Prevent multiple clustermouseover/off events if the icon is made up of stacked divs (Doesn't work in ie <= 8, no relatedTarget)
			if (data.originalEvent && this._isOrIsParent(data.layer._icon, data.originalEvent.relatedTarget)) {
				return;
			}
			type = 'cluster' + type;
		}

		L.FeatureGroup.prototype.fire.call(this, type, data, propagate);
	},

	//Override L.Evented.listens
	listens: function (type, propagate) {
		return L.FeatureGroup.prototype.listens.call(this, type, propagate) || L.FeatureGroup.prototype.listens.call(this, 'cluster' + type, propagate);
	},

	//Default functionality
	_defaultIconCreateFunction: function (cluster) {
		var childCount = cluster.getChildCount();

		var c = ' marker-cluster-';
		if (childCount < 10) {
			c += 'small';
		} else if (childCount < 100) {
			c += 'medium';
		} else {
			c += 'large';
		}

		return new L.DivIcon({ html: '<div><span>' + childCount + '</span></div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
	},

	_bindEvents: function () {
		var map = this._map,
		    spiderfyOnMaxZoom = this.options.spiderfyOnMaxZoom,
		    showCoverageOnHover = this.options.showCoverageOnHover,
		    zoomToBoundsOnClick = this.options.zoomToBoundsOnClick;

		//Zoom on cluster click or spiderfy if we are at the lowest level
		if (spiderfyOnMaxZoom || zoomToBoundsOnClick) {
			this.on('clusterclick', this._zoomOrSpiderfy, this);
		}

		//Show convex hull (boundary) polygon on mouse over
		if (showCoverageOnHover) {
			this.on('clustermouseover', this._showCoverage, this);
			this.on('clustermouseout', this._hideCoverage, this);
			map.on('zoomend', this._hideCoverage, this);
		}
	},

	_zoomOrSpiderfy: function (e) {
		var cluster = e.layer,
		    bottomCluster = cluster;

		while (bottomCluster._childClusters.length === 1) {
			bottomCluster = bottomCluster._childClusters[0];
		}

		if (bottomCluster._zoom === this._maxZoom &&
			bottomCluster._childCount === cluster._childCount &&
			this.options.spiderfyOnMaxZoom) {

			// All child markers are contained in a single cluster from this._maxZoom to this cluster.
			cluster.spiderfy();
		} else if (this.options.zoomToBoundsOnClick) {
			cluster.zoomToBounds();
		}

		// Focus the map again for keyboard users.
		if (e.originalEvent && e.originalEvent.keyCode === 13) {
			this._map._container.focus();
		}
	},

	_showCoverage: function (e) {
		var map = this._map;
		if (this._inZoomAnimation) {
			return;
		}
		if (this._shownPolygon) {
			map.removeLayer(this._shownPolygon);
		}
		if (e.layer.getChildCount() > 2 && e.layer !== this._spiderfied) {
			this._shownPolygon = new L.Polygon(e.layer.getConvexHull(), this.options.polygonOptions);
			map.addLayer(this._shownPolygon);
		}
	},

	_hideCoverage: function () {
		if (this._shownPolygon) {
			this._map.removeLayer(this._shownPolygon);
			this._shownPolygon = null;
		}
	},

	_unbindEvents: function () {
		var spiderfyOnMaxZoom = this.options.spiderfyOnMaxZoom,
			showCoverageOnHover = this.options.showCoverageOnHover,
			zoomToBoundsOnClick = this.options.zoomToBoundsOnClick,
			map = this._map;

		if (spiderfyOnMaxZoom || zoomToBoundsOnClick) {
			this.off('clusterclick', this._zoomOrSpiderfy, this);
		}
		if (showCoverageOnHover) {
			this.off('clustermouseover', this._showCoverage, this);
			this.off('clustermouseout', this._hideCoverage, this);
			map.off('zoomend', this._hideCoverage, this);
		}
	},

	_zoomEnd: function () {
		if (!this._map) { //May have been removed from the map by a zoomEnd handler
			return;
		}
		this._mergeSplitClusters();

		this._zoom = Math.round(this._map._zoom);
		this._currentShownBounds = this._getExpandedVisibleBounds();
	},

	_moveEnd: function () {
		if (this._inZoomAnimation) {
			return;
		}

		var newBounds = this._getExpandedVisibleBounds();

		this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), this._zoom, newBounds);
		this._topClusterLevel._recursivelyAddChildrenToMap(null, Math.round(this._map._zoom), newBounds);

		this._currentShownBounds = newBounds;
		return;
	},

	_generateInitialClusters: function () {
		var maxZoom = Math.ceil(this._map.getMaxZoom()),
			minZoom = Math.floor(this._map.getMinZoom()),
			radius = this.options.maxClusterRadius,
			radiusFn = radius;

		//If we just set maxClusterRadius to a single number, we need to create
		//a simple function to return that number. Otherwise, we just have to
		//use the function we've passed in.
		if (typeof radius !== "function") {
			radiusFn = function () { return radius; };
		}

		if (this.options.disableClusteringAtZoom !== null) {
			maxZoom = this.options.disableClusteringAtZoom - 1;
		}
		this._maxZoom = maxZoom;
		this._gridClusters = {};
		this._gridUnclustered = {};

		//Set up DistanceGrids for each zoom
		for (var zoom = maxZoom; zoom >= minZoom; zoom--) {
			this._gridClusters[zoom] = new L.DistanceGrid(radiusFn(zoom));
			this._gridUnclustered[zoom] = new L.DistanceGrid(radiusFn(zoom));
		}

		// Instantiate the appropriate L.MarkerCluster class (animated or not).
		this._topClusterLevel = new this._markerCluster(this, minZoom - 1);
	},

	//Zoom: Zoom to start adding at (Pass this._maxZoom to start at the bottom)
	_addLayer: function (layer, zoom) {
		var gridClusters = this._gridClusters,
		    gridUnclustered = this._gridUnclustered,
			minZoom = Math.floor(this._map.getMinZoom()),
		    markerPoint, z;

		if (this.options.singleMarkerMode) {
			this._overrideMarkerIcon(layer);
		}

		layer.on(this._childMarkerEventHandlers, this);

		//Find the lowest zoom level to slot this one in
		for (; zoom >= minZoom; zoom--) {
			markerPoint = this._map.project(layer.getLatLng(), zoom); // calculate pixel position

			//Try find a cluster close by
			var closest = gridClusters[zoom].getNearObject(markerPoint);
			if (closest) {
				closest._addChild(layer);
				layer.__parent = closest;
				return;
			}

			//Try find a marker close by to form a new cluster with
			closest = gridUnclustered[zoom].getNearObject(markerPoint);
			if (closest) {
				var parent = closest.__parent;
				if (parent) {
					this._removeLayer(closest, false);
				}

				//Create new cluster with these 2 in it

				var newCluster = new this._markerCluster(this, zoom, closest, layer);
				gridClusters[zoom].addObject(newCluster, this._map.project(newCluster._cLatLng, zoom));
				closest.__parent = newCluster;
				layer.__parent = newCluster;

				//First create any new intermediate parent clusters that don't exist
				var lastParent = newCluster;
				for (z = zoom - 1; z > parent._zoom; z--) {
					lastParent = new this._markerCluster(this, z, lastParent);
					gridClusters[z].addObject(lastParent, this._map.project(closest.getLatLng(), z));
				}
				parent._addChild(lastParent);

				//Remove closest from this zoom level and any above that it is in, replace with newCluster
				this._removeFromGridUnclustered(closest, zoom);

				return;
			}

			//Didn't manage to cluster in at this zoom, record us as a marker here and continue upwards
			gridUnclustered[zoom].addObject(layer, markerPoint);
		}

		//Didn't get in anything, add us to the top
		this._topClusterLevel._addChild(layer);
		layer.__parent = this._topClusterLevel;
		return;
	},

	/**
	 * Refreshes the icon of all "dirty" visible clusters.
	 * Non-visible "dirty" clusters will be updated when they are added to the map.
	 * @private
	 */
	_refreshClustersIcons: function () {
		this._featureGroup.eachLayer(function (c) {
			if (c instanceof L.MarkerCluster && c._iconNeedsUpdate) {
				c._updateIcon();
			}
		});
	},

	//Enqueue code to fire after the marker expand/contract has happened
	_enqueue: function (fn) {
		this._queue.push(fn);
		if (!this._queueTimeout) {
			this._queueTimeout = setTimeout(L.bind(this._processQueue, this), 300);
		}
	},
	_processQueue: function () {
		for (var i = 0; i < this._queue.length; i++) {
			this._queue[i].call(this);
		}
		this._queue.length = 0;
		clearTimeout(this._queueTimeout);
		this._queueTimeout = null;
	},

	//Merge and split any existing clusters that are too big or small
	_mergeSplitClusters: function () {
		var mapZoom = Math.round(this._map._zoom);

		//In case we are starting to split before the animation finished
		this._processQueue();

		if (this._zoom < mapZoom && this._currentShownBounds.intersects(this._getExpandedVisibleBounds())) { //Zoom in, split
			this._animationStart();
			//Remove clusters now off screen
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), this._zoom, this._getExpandedVisibleBounds());

			this._animationZoomIn(this._zoom, mapZoom);

		} else if (this._zoom > mapZoom) { //Zoom out, merge
			this._animationStart();

			this._animationZoomOut(this._zoom, mapZoom);
		} else {
			this._moveEnd();
		}
	},

	//Gets the maps visible bounds expanded in each direction by the size of the screen (so the user cannot see an area we do not cover in one pan)
	_getExpandedVisibleBounds: function () {
		if (!this.options.removeOutsideVisibleBounds) {
			return this._mapBoundsInfinite;
		} else if (L.Browser.mobile) {
			return this._checkBoundsMaxLat(this._map.getBounds());
		}

		return this._checkBoundsMaxLat(this._map.getBounds().pad(1)); // Padding expands the bounds by its own dimensions but scaled with the given factor.
	},

	/**
	 * Expands the latitude to Infinity (or -Infinity) if the input bounds reach the map projection maximum defined latitude
	 * (in the case of Web/Spherical Mercator, it is 85.0511287798 / see https://en.wikipedia.org/wiki/Web_Mercator#Formulas).
	 * Otherwise, the removeOutsideVisibleBounds option will remove markers beyond that limit, whereas the same markers without
	 * this option (or outside MCG) will have their position floored (ceiled) by the projection and rendered at that limit,
	 * making the user think that MCG "eats" them and never displays them again.
	 * @param bounds L.LatLngBounds
	 * @returns {L.LatLngBounds}
	 * @private
	 */
	_checkBoundsMaxLat: function (bounds) {
		var maxLat = this._maxLat;

		if (maxLat !== undefined) {
			if (bounds.getNorth() >= maxLat) {
				bounds._northEast.lat = Infinity;
			}
			if (bounds.getSouth() <= -maxLat) {
				bounds._southWest.lat = -Infinity;
			}
		}

		return bounds;
	},

	//Shared animation code
	_animationAddLayerNonAnimated: function (layer, newCluster) {
		if (newCluster === layer) {
			this._featureGroup.addLayer(layer);
		} else if (newCluster._childCount === 2) {
			newCluster._addToMap();

			var markers = newCluster.getAllChildMarkers();
			this._featureGroup.removeLayer(markers[0]);
			this._featureGroup.removeLayer(markers[1]);
		} else {
			newCluster._updateIcon();
		}
	},

	/**
	 * Extracts individual (i.e. non-group) layers from a Layer Group.
	 * @param group to extract layers from.
	 * @param output {Array} in which to store the extracted layers.
	 * @returns {*|Array}
	 * @private
	 */
	_extractNonGroupLayers: function (group, output) {
		var layers = group.getLayers(),
		    i = 0,
		    layer;

		output = output || [];

		for (; i < layers.length; i++) {
			layer = layers[i];

			if (layer instanceof L.LayerGroup) {
				this._extractNonGroupLayers(layer, output);
				continue;
			}

			output.push(layer);
		}

		return output;
	},

	/**
	 * Implements the singleMarkerMode option.
	 * @param layer Marker to re-style using the Clusters iconCreateFunction.
	 * @returns {L.Icon} The newly created icon.
	 * @private
	 */
	_overrideMarkerIcon: function (layer) {
		var icon = layer.options.icon = this.options.iconCreateFunction({
			getChildCount: function () {
				return 1;
			},
			getAllChildMarkers: function () {
				return [layer];
			}
		});

		return icon;
	}
});

// Constant bounds used in case option "removeOutsideVisibleBounds" is set to false.
L.MarkerClusterGroup.include({
	_mapBoundsInfinite: new L.LatLngBounds(new L.LatLng(-Infinity, -Infinity), new L.LatLng(Infinity, Infinity))
});

L.MarkerClusterGroup.include({
	_noAnimation: {
		//Non Animated versions of everything
		_animationStart: function () {
			//Do nothing...
		},
		_animationZoomIn: function (previousZoomLevel, newZoomLevel) {
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), previousZoomLevel);
			this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());

			//We didn't actually animate, but we use this event to mean "clustering animations have finished"
			this.fire('animationend');
		},
		_animationZoomOut: function (previousZoomLevel, newZoomLevel) {
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), previousZoomLevel);
			this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());

			//We didn't actually animate, but we use this event to mean "clustering animations have finished"
			this.fire('animationend');
		},
		_animationAddLayer: function (layer, newCluster) {
			this._animationAddLayerNonAnimated(layer, newCluster);
		}
	},

	_withAnimation: {
		//Animated versions here
		_animationStart: function () {
			this._map._mapPane.className += ' leaflet-cluster-anim';
			this._inZoomAnimation++;
		},

		_animationZoomIn: function (previousZoomLevel, newZoomLevel) {
			var bounds = this._getExpandedVisibleBounds(),
			    fg = this._featureGroup,
				minZoom = Math.floor(this._map.getMinZoom()),
			    i;

			this._ignoreMove = true;

			//Add all children of current clusters to map and remove those clusters from map
			this._topClusterLevel._recursively(bounds, previousZoomLevel, minZoom, function (c) {
				var startPos = c._latlng,
				    markers  = c._markers,
				    m;

				if (!bounds.contains(startPos)) {
					startPos = null;
				}

				if (c._isSingleParent() && previousZoomLevel + 1 === newZoomLevel) { //Immediately add the new child and remove us
					fg.removeLayer(c);
					c._recursivelyAddChildrenToMap(null, newZoomLevel, bounds);
				} else {
					//Fade out old cluster
					c.clusterHide();
					c._recursivelyAddChildrenToMap(startPos, newZoomLevel, bounds);
				}

				//Remove all markers that aren't visible any more
				//TODO: Do we actually need to do this on the higher levels too?
				for (i = markers.length - 1; i >= 0; i--) {
					m = markers[i];
					if (!bounds.contains(m._latlng)) {
						fg.removeLayer(m);
					}
				}

			});

			this._forceLayout();

			//Update opacities
			this._topClusterLevel._recursivelyBecomeVisible(bounds, newZoomLevel);
			//TODO Maybe? Update markers in _recursivelyBecomeVisible
			fg.eachLayer(function (n) {
				if (!(n instanceof L.MarkerCluster) && n._icon) {
					n.clusterShow();
				}
			});

			//update the positions of the just added clusters/markers
			this._topClusterLevel._recursively(bounds, previousZoomLevel, newZoomLevel, function (c) {
				c._recursivelyRestoreChildPositions(newZoomLevel);
			});

			this._ignoreMove = false;

			//Remove the old clusters and close the zoom animation
			this._enqueue(function () {
				//update the positions of the just added clusters/markers
				this._topClusterLevel._recursively(bounds, previousZoomLevel, minZoom, function (c) {
					fg.removeLayer(c);
					c.clusterShow();
				});

				this._animationEnd();
			});
		},

		_animationZoomOut: function (previousZoomLevel, newZoomLevel) {
			this._animationZoomOutSingle(this._topClusterLevel, previousZoomLevel - 1, newZoomLevel);

			//Need to add markers for those that weren't on the map before but are now
			this._topClusterLevel._recursivelyAddChildrenToMap(null, newZoomLevel, this._getExpandedVisibleBounds());
			//Remove markers that were on the map before but won't be now
			this._topClusterLevel._recursivelyRemoveChildrenFromMap(this._currentShownBounds, Math.floor(this._map.getMinZoom()), previousZoomLevel, this._getExpandedVisibleBounds());
		},

		_animationAddLayer: function (layer, newCluster) {
			var me = this,
			    fg = this._featureGroup;

			fg.addLayer(layer);
			if (newCluster !== layer) {
				if (newCluster._childCount > 2) { //Was already a cluster

					newCluster._updateIcon();
					this._forceLayout();
					this._animationStart();

					layer._setPos(this._map.latLngToLayerPoint(newCluster.getLatLng()));
					layer.clusterHide();

					this._enqueue(function () {
						fg.removeLayer(layer);
						layer.clusterShow();

						me._animationEnd();
					});

				} else { //Just became a cluster
					this._forceLayout();

					me._animationStart();
					me._animationZoomOutSingle(newCluster, this._map.getMaxZoom(), this._zoom);
				}
			}
		}
	},

	// Private methods for animated versions.
	_animationZoomOutSingle: function (cluster, previousZoomLevel, newZoomLevel) {
		var bounds = this._getExpandedVisibleBounds(),
			minZoom = Math.floor(this._map.getMinZoom());

		//Animate all of the markers in the clusters to move to their cluster center point
		cluster._recursivelyAnimateChildrenInAndAddSelfToMap(bounds, minZoom, previousZoomLevel + 1, newZoomLevel);

		var me = this;

		//Update the opacity (If we immediately set it they won't animate)
		this._forceLayout();
		cluster._recursivelyBecomeVisible(bounds, newZoomLevel);

		//TODO: Maybe use the transition timing stuff to make this more reliable
		//When the animations are done, tidy up
		this._enqueue(function () {

			//This cluster stopped being a cluster before the timeout fired
			if (cluster._childCount === 1) {
				var m = cluster._markers[0];
				//If we were in a cluster animation at the time then the opacity and position of our child could be wrong now, so fix it
				this._ignoreMove = true;
				m.setLatLng(m.getLatLng());
				this._ignoreMove = false;
				if (m.clusterShow) {
					m.clusterShow();
				}
			} else {
				cluster._recursively(bounds, newZoomLevel, minZoom, function (c) {
					c._recursivelyRemoveChildrenFromMap(bounds, minZoom, previousZoomLevel + 1);
				});
			}
			me._animationEnd();
		});
	},

	_animationEnd: function () {
		if (this._map) {
			this._map._mapPane.className = this._map._mapPane.className.replace(' leaflet-cluster-anim', '');
		}
		this._inZoomAnimation--;
		this.fire('animationend');
	},

	//Force a browser layout of stuff in the map
	// Should apply the current opacity and location to all elements so we can update them again for an animation
	_forceLayout: function () {
		//In my testing this works, infact offsetWidth of any element seems to work.
		//Could loop all this._layers and do this for each _icon if it stops working

		L.Util.falseFn(document.body.offsetWidth);
	}
});

L.markerClusterGroup = function (options) {
	return new L.MarkerClusterGroup(options);
};

var MarkerCluster = L.MarkerCluster = L.Marker.extend({
	options: L.Icon.prototype.options,

	initialize: function (group, zoom, a, b) {

		L.Marker.prototype.initialize.call(this, a ? (a._cLatLng || a.getLatLng()) : new L.LatLng(0, 0),
            { icon: this, pane: group.options.clusterPane });

		this._group = group;
		this._zoom = zoom;

		this._markers = [];
		this._childClusters = [];
		this._childCount = 0;
		this._iconNeedsUpdate = true;
		this._boundsNeedUpdate = true;

		this._bounds = new L.LatLngBounds();

		if (a) {
			this._addChild(a);
		}
		if (b) {
			this._addChild(b);
		}
	},

	//Recursively retrieve all child markers of this cluster
	getAllChildMarkers: function (storageArray, ignoreDraggedMarker) {
		storageArray = storageArray || [];

		for (var i = this._childClusters.length - 1; i >= 0; i--) {
			this._childClusters[i].getAllChildMarkers(storageArray);
		}

		for (var j = this._markers.length - 1; j >= 0; j--) {
			if (ignoreDraggedMarker && this._markers[j].__dragStart) {
				continue;
			}
			storageArray.push(this._markers[j]);
		}

		return storageArray;
	},

	//Returns the count of how many child markers we have
	getChildCount: function () {
		return this._childCount;
	},

	//Zoom to the minimum of showing all of the child markers, or the extents of this cluster
	zoomToBounds: function (fitBoundsOptions) {
		var childClusters = this._childClusters.slice(),
			map = this._group._map,
			boundsZoom = map.getBoundsZoom(this._bounds),
			zoom = this._zoom + 1,
			mapZoom = map.getZoom(),
			i;

		//calculate how far we need to zoom down to see all of the markers
		while (childClusters.length > 0 && boundsZoom > zoom) {
			zoom++;
			var newClusters = [];
			for (i = 0; i < childClusters.length; i++) {
				newClusters = newClusters.concat(childClusters[i]._childClusters);
			}
			childClusters = newClusters;
		}

		if (boundsZoom > zoom) {
			this._group._map.setView(this._latlng, zoom);
		} else if (boundsZoom <= mapZoom) { //If fitBounds wouldn't zoom us down, zoom us down instead
			this._group._map.setView(this._latlng, mapZoom + 1);
		} else {
			this._group._map.fitBounds(this._bounds, fitBoundsOptions);
		}
	},

	getBounds: function () {
		var bounds = new L.LatLngBounds();
		bounds.extend(this._bounds);
		return bounds;
	},

	_updateIcon: function () {
		this._iconNeedsUpdate = true;
		if (this._icon) {
			this.setIcon(this);
		}
	},

	//Cludge for Icon, we pretend to be an icon for performance
	createIcon: function () {
		if (this._iconNeedsUpdate) {
			this._iconObj = this._group.options.iconCreateFunction(this);
			this._iconNeedsUpdate = false;
		}
		return this._iconObj.createIcon();
	},
	createShadow: function () {
		return this._iconObj.createShadow();
	},


	_addChild: function (new1, isNotificationFromChild) {

		this._iconNeedsUpdate = true;

		this._boundsNeedUpdate = true;
		this._setClusterCenter(new1);

		if (new1 instanceof L.MarkerCluster) {
			if (!isNotificationFromChild) {
				this._childClusters.push(new1);
				new1.__parent = this;
			}
			this._childCount += new1._childCount;
		} else {
			if (!isNotificationFromChild) {
				this._markers.push(new1);
			}
			this._childCount++;
		}

		if (this.__parent) {
			this.__parent._addChild(new1, true);
		}
	},

	/**
	 * Makes sure the cluster center is set. If not, uses the child center if it is a cluster, or the marker position.
	 * @param child L.MarkerCluster|L.Marker that will be used as cluster center if not defined yet.
	 * @private
	 */
	_setClusterCenter: function (child) {
		if (!this._cLatLng) {
			// when clustering, take position of the first point as the cluster center
			this._cLatLng = child._cLatLng || child._latlng;
		}
	},

	/**
	 * Assigns impossible bounding values so that the next extend entirely determines the new bounds.
	 * This method avoids having to trash the previous L.LatLngBounds object and to create a new one, which is much slower for this class.
	 * As long as the bounds are not extended, most other methods would probably fail, as they would with bounds initialized but not extended.
	 * @private
	 */
	_resetBounds: function () {
		var bounds = this._bounds;

		if (bounds._southWest) {
			bounds._southWest.lat = Infinity;
			bounds._southWest.lng = Infinity;
		}
		if (bounds._northEast) {
			bounds._northEast.lat = -Infinity;
			bounds._northEast.lng = -Infinity;
		}
	},

	_recalculateBounds: function () {
		var markers = this._markers,
		    childClusters = this._childClusters,
		    latSum = 0,
		    lngSum = 0,
		    totalCount = this._childCount,
		    i, child, childLatLng, childCount;

		// Case where all markers are removed from the map and we are left with just an empty _topClusterLevel.
		if (totalCount === 0) {
			return;
		}

		// Reset rather than creating a new object, for performance.
		this._resetBounds();

		// Child markers.
		for (i = 0; i < markers.length; i++) {
			childLatLng = markers[i]._latlng;

			this._bounds.extend(childLatLng);

			latSum += childLatLng.lat;
			lngSum += childLatLng.lng;
		}

		// Child clusters.
		for (i = 0; i < childClusters.length; i++) {
			child = childClusters[i];

			// Re-compute child bounds and weighted position first if necessary.
			if (child._boundsNeedUpdate) {
				child._recalculateBounds();
			}

			this._bounds.extend(child._bounds);

			childLatLng = child._wLatLng;
			childCount = child._childCount;

			latSum += childLatLng.lat * childCount;
			lngSum += childLatLng.lng * childCount;
		}

		this._latlng = this._wLatLng = new L.LatLng(latSum / totalCount, lngSum / totalCount);

		// Reset dirty flag.
		this._boundsNeedUpdate = false;
	},

	//Set our markers position as given and add it to the map
	_addToMap: function (startPos) {
		if (startPos) {
			this._backupLatlng = this._latlng;
			this.setLatLng(startPos);
		}
		this._group._featureGroup.addLayer(this);
	},

	_recursivelyAnimateChildrenIn: function (bounds, center, maxZoom) {
		this._recursively(bounds, this._group._map.getMinZoom(), maxZoom - 1,
			function (c) {
				var markers = c._markers,
					i, m;
				for (i = markers.length - 1; i >= 0; i--) {
					m = markers[i];

					//Only do it if the icon is still on the map
					if (m._icon) {
						m._setPos(center);
						m.clusterHide();
					}
				}
			},
			function (c) {
				var childClusters = c._childClusters,
					j, cm;
				for (j = childClusters.length - 1; j >= 0; j--) {
					cm = childClusters[j];
					if (cm._icon) {
						cm._setPos(center);
						cm.clusterHide();
					}
				}
			}
		);
	},

	_recursivelyAnimateChildrenInAndAddSelfToMap: function (bounds, mapMinZoom, previousZoomLevel, newZoomLevel) {
		this._recursively(bounds, newZoomLevel, mapMinZoom,
			function (c) {
				c._recursivelyAnimateChildrenIn(bounds, c._group._map.latLngToLayerPoint(c.getLatLng()).round(), previousZoomLevel);

				//TODO: depthToAnimateIn affects _isSingleParent, if there is a multizoom we may/may not be.
				//As a hack we only do a animation free zoom on a single level zoom, if someone does multiple levels then we always animate
				if (c._isSingleParent() && previousZoomLevel - 1 === newZoomLevel) {
					c.clusterShow();
					c._recursivelyRemoveChildrenFromMap(bounds, mapMinZoom, previousZoomLevel); //Immediately remove our children as we are replacing them. TODO previousBounds not bounds
				} else {
					c.clusterHide();
				}

				c._addToMap();
			}
		);
	},

	_recursivelyBecomeVisible: function (bounds, zoomLevel) {
		this._recursively(bounds, this._group._map.getMinZoom(), zoomLevel, null, function (c) {
			c.clusterShow();
		});
	},

	_recursivelyAddChildrenToMap: function (startPos, zoomLevel, bounds) {
		this._recursively(bounds, this._group._map.getMinZoom() - 1, zoomLevel,
			function (c) {
				if (zoomLevel === c._zoom) {
					return;
				}

				//Add our child markers at startPos (so they can be animated out)
				for (var i = c._markers.length - 1; i >= 0; i--) {
					var nm = c._markers[i];

					if (!bounds.contains(nm._latlng)) {
						continue;
					}

					if (startPos) {
						nm._backupLatlng = nm.getLatLng();

						nm.setLatLng(startPos);
						if (nm.clusterHide) {
							nm.clusterHide();
						}
					}

					c._group._featureGroup.addLayer(nm);
				}
			},
			function (c) {
				c._addToMap(startPos);
			}
		);
	},

	_recursivelyRestoreChildPositions: function (zoomLevel) {
		//Fix positions of child markers
		for (var i = this._markers.length - 1; i >= 0; i--) {
			var nm = this._markers[i];
			if (nm._backupLatlng) {
				nm.setLatLng(nm._backupLatlng);
				delete nm._backupLatlng;
			}
		}

		if (zoomLevel - 1 === this._zoom) {
			//Reposition child clusters
			for (var j = this._childClusters.length - 1; j >= 0; j--) {
				this._childClusters[j]._restorePosition();
			}
		} else {
			for (var k = this._childClusters.length - 1; k >= 0; k--) {
				this._childClusters[k]._recursivelyRestoreChildPositions(zoomLevel);
			}
		}
	},

	_restorePosition: function () {
		if (this._backupLatlng) {
			this.setLatLng(this._backupLatlng);
			delete this._backupLatlng;
		}
	},

	//exceptBounds: If set, don't remove any markers/clusters in it
	_recursivelyRemoveChildrenFromMap: function (previousBounds, mapMinZoom, zoomLevel, exceptBounds) {
		var m, i;
		this._recursively(previousBounds, mapMinZoom - 1, zoomLevel - 1,
			function (c) {
				//Remove markers at every level
				for (i = c._markers.length - 1; i >= 0; i--) {
					m = c._markers[i];
					if (!exceptBounds || !exceptBounds.contains(m._latlng)) {
						c._group._featureGroup.removeLayer(m);
						if (m.clusterShow) {
							m.clusterShow();
						}
					}
				}
			},
			function (c) {
				//Remove child clusters at just the bottom level
				for (i = c._childClusters.length - 1; i >= 0; i--) {
					m = c._childClusters[i];
					if (!exceptBounds || !exceptBounds.contains(m._latlng)) {
						c._group._featureGroup.removeLayer(m);
						if (m.clusterShow) {
							m.clusterShow();
						}
					}
				}
			}
		);
	},

	//Run the given functions recursively to this and child clusters
	// boundsToApplyTo: a L.LatLngBounds representing the bounds of what clusters to recurse in to
	// zoomLevelToStart: zoom level to start running functions (inclusive)
	// zoomLevelToStop: zoom level to stop running functions (inclusive)
	// runAtEveryLevel: function that takes an L.MarkerCluster as an argument that should be applied on every level
	// runAtBottomLevel: function that takes an L.MarkerCluster as an argument that should be applied at only the bottom level
	_recursively: function (boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel) {
		var childClusters = this._childClusters,
		    zoom = this._zoom,
		    i, c;

		if (zoomLevelToStart <= zoom) {
			if (runAtEveryLevel) {
				runAtEveryLevel(this);
			}
			if (runAtBottomLevel && zoom === zoomLevelToStop) {
				runAtBottomLevel(this);
			}
		}

		if (zoom < zoomLevelToStart || zoom < zoomLevelToStop) {
			for (i = childClusters.length - 1; i >= 0; i--) {
				c = childClusters[i];
				if (c._boundsNeedUpdate) {
					c._recalculateBounds();
				}
				if (boundsToApplyTo.intersects(c._bounds)) {
					c._recursively(boundsToApplyTo, zoomLevelToStart, zoomLevelToStop, runAtEveryLevel, runAtBottomLevel);
				}
			}
		}
	},

	//Returns true if we are the parent of only one cluster and that cluster is the same as us
	_isSingleParent: function () {
		//Don't need to check this._markers as the rest won't work if there are any
		return this._childClusters.length > 0 && this._childClusters[0]._childCount === this._childCount;
	}
});

/*
* Extends L.Marker to include two extra methods: clusterHide and clusterShow.
* 
* They work as setOpacity(0) and setOpacity(1) respectively, but
* don't overwrite the options.opacity
* 
*/

L.Marker.include({
	clusterHide: function () {
		var backup = this.options.opacity;
		this.setOpacity(0);
		this.options.opacity = backup;
		return this;
	},
	
	clusterShow: function () {
		return this.setOpacity(this.options.opacity);
	}
});

L.DistanceGrid = function (cellSize) {
	this._cellSize = cellSize;
	this._sqCellSize = cellSize * cellSize;
	this._grid = {};
	this._objectPoint = { };
};

L.DistanceGrid.prototype = {

	addObject: function (obj, point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    grid = this._grid,
		    row = grid[y] = grid[y] || {},
		    cell = row[x] = row[x] || [],
		    stamp = L.Util.stamp(obj);

		this._objectPoint[stamp] = point;

		cell.push(obj);
	},

	updateObject: function (obj, point) {
		this.removeObject(obj);
		this.addObject(obj, point);
	},

	//Returns true if the object was found
	removeObject: function (obj, point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    grid = this._grid,
		    row = grid[y] = grid[y] || {},
		    cell = row[x] = row[x] || [],
		    i, len;

		delete this._objectPoint[L.Util.stamp(obj)];

		for (i = 0, len = cell.length; i < len; i++) {
			if (cell[i] === obj) {

				cell.splice(i, 1);

				if (len === 1) {
					delete row[x];
				}

				return true;
			}
		}

	},

	eachObject: function (fn, context) {
		var i, j, k, len, row, cell, removed,
		    grid = this._grid;

		for (i in grid) {
			row = grid[i];

			for (j in row) {
				cell = row[j];

				for (k = 0, len = cell.length; k < len; k++) {
					removed = fn.call(context, cell[k]);
					if (removed) {
						k--;
						len--;
					}
				}
			}
		}
	},

	getNearObject: function (point) {
		var x = this._getCoord(point.x),
		    y = this._getCoord(point.y),
		    i, j, k, row, cell, len, obj, dist,
		    objectPoint = this._objectPoint,
		    closestDistSq = this._sqCellSize,
		    closest = null;

		for (i = y - 1; i <= y + 1; i++) {
			row = this._grid[i];
			if (row) {

				for (j = x - 1; j <= x + 1; j++) {
					cell = row[j];
					if (cell) {

						for (k = 0, len = cell.length; k < len; k++) {
							obj = cell[k];
							dist = this._sqDist(objectPoint[L.Util.stamp(obj)], point);
							if (dist < closestDistSq ||
								dist <= closestDistSq && closest === null) {
								closestDistSq = dist;
								closest = obj;
							}
						}
					}
				}
			}
		}
		return closest;
	},

	_getCoord: function (x) {
		var coord = Math.floor(x / this._cellSize);
		return isFinite(coord) ? coord : x;
	},

	_sqDist: function (p, p2) {
		var dx = p2.x - p.x,
		    dy = p2.y - p.y;
		return dx * dx + dy * dy;
	}
};

/* Copyright (c) 2012 the authors listed at the following URL, and/or
the authors of referenced articles or incorporated external code:
http://en.literateprograms.org/Quickhull_(Javascript)?action=history&offset=20120410175256

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Retrieved from: http://en.literateprograms.org/Quickhull_(Javascript)?oldid=18434
*/

(function () {
	L.QuickHull = {

		/*
		 * @param {Object} cpt a point to be measured from the baseline
		 * @param {Array} bl the baseline, as represented by a two-element
		 *   array of latlng objects.
		 * @returns {Number} an approximate distance measure
		 */
		getDistant: function (cpt, bl) {
			var vY = bl[1].lat - bl[0].lat,
				vX = bl[0].lng - bl[1].lng;
			return (vX * (cpt.lat - bl[0].lat) + vY * (cpt.lng - bl[0].lng));
		},

		/*
		 * @param {Array} baseLine a two-element array of latlng objects
		 *   representing the baseline to project from
		 * @param {Array} latLngs an array of latlng objects
		 * @returns {Object} the maximum point and all new points to stay
		 *   in consideration for the hull.
		 */
		findMostDistantPointFromBaseLine: function (baseLine, latLngs) {
			var maxD = 0,
				maxPt = null,
				newPoints = [],
				i, pt, d;

			for (i = latLngs.length - 1; i >= 0; i--) {
				pt = latLngs[i];
				d = this.getDistant(pt, baseLine);

				if (d > 0) {
					newPoints.push(pt);
				} else {
					continue;
				}

				if (d > maxD) {
					maxD = d;
					maxPt = pt;
				}
			}

			return { maxPoint: maxPt, newPoints: newPoints };
		},


		/*
		 * Given a baseline, compute the convex hull of latLngs as an array
		 * of latLngs.
		 *
		 * @param {Array} latLngs
		 * @returns {Array}
		 */
		buildConvexHull: function (baseLine, latLngs) {
			var convexHullBaseLines = [],
				t = this.findMostDistantPointFromBaseLine(baseLine, latLngs);

			if (t.maxPoint) { // if there is still a point "outside" the base line
				convexHullBaseLines =
					convexHullBaseLines.concat(
						this.buildConvexHull([baseLine[0], t.maxPoint], t.newPoints)
					);
				convexHullBaseLines =
					convexHullBaseLines.concat(
						this.buildConvexHull([t.maxPoint, baseLine[1]], t.newPoints)
					);
				return convexHullBaseLines;
			} else {  // if there is no more point "outside" the base line, the current base line is part of the convex hull
				return [baseLine[0]];
			}
		},

		/*
		 * Given an array of latlngs, compute a convex hull as an array
		 * of latlngs
		 *
		 * @param {Array} latLngs
		 * @returns {Array}
		 */
		getConvexHull: function (latLngs) {
			// find first baseline
			var maxLat = false, minLat = false,
				maxLng = false, minLng = false,
				maxLatPt = null, minLatPt = null,
				maxLngPt = null, minLngPt = null,
				maxPt = null, minPt = null,
				i;

			for (i = latLngs.length - 1; i >= 0; i--) {
				var pt = latLngs[i];
				if (maxLat === false || pt.lat > maxLat) {
					maxLatPt = pt;
					maxLat = pt.lat;
				}
				if (minLat === false || pt.lat < minLat) {
					minLatPt = pt;
					minLat = pt.lat;
				}
				if (maxLng === false || pt.lng > maxLng) {
					maxLngPt = pt;
					maxLng = pt.lng;
				}
				if (minLng === false || pt.lng < minLng) {
					minLngPt = pt;
					minLng = pt.lng;
				}
			}
			
			if (minLat !== maxLat) {
				minPt = minLatPt;
				maxPt = maxLatPt;
			} else {
				minPt = minLngPt;
				maxPt = maxLngPt;
			}

			var ch = [].concat(this.buildConvexHull([minPt, maxPt], latLngs),
								this.buildConvexHull([maxPt, minPt], latLngs));
			return ch;
		}
	};
}());

L.MarkerCluster.include({
	getConvexHull: function () {
		var childMarkers = this.getAllChildMarkers(),
			points = [],
			p, i;

		for (i = childMarkers.length - 1; i >= 0; i--) {
			p = childMarkers[i].getLatLng();
			points.push(p);
		}

		return L.QuickHull.getConvexHull(points);
	}
});

//This code is 100% based on https://github.com/jawj/OverlappingMarkerSpiderfier-Leaflet
//Huge thanks to jawj for implementing it first to make my job easy :-)

L.MarkerCluster.include({

	_2PI: Math.PI * 2,
	_circleFootSeparation: 25, //related to circumference of circle
	_circleStartAngle: 0,

	_spiralFootSeparation:  28, //related to size of spiral (experiment!)
	_spiralLengthStart: 11,
	_spiralLengthFactor: 5,

	_circleSpiralSwitchover: 9, //show spiral instead of circle from this marker count upwards.
								// 0 -> always spiral; Infinity -> always circle

	spiderfy: function () {
		if (this._group._spiderfied === this || this._group._inZoomAnimation) {
			return;
		}

		var childMarkers = this.getAllChildMarkers(null, true),
			group = this._group,
			map = group._map,
			center = map.latLngToLayerPoint(this._latlng),
			positions;

		this._group._unspiderfy();
		this._group._spiderfied = this;

		//TODO Maybe: childMarkers order by distance to center

		if (childMarkers.length >= this._circleSpiralSwitchover) {
			positions = this._generatePointsSpiral(childMarkers.length, center);
		} else {
			center.y += 10; // Otherwise circles look wrong => hack for standard blue icon, renders differently for other icons.
			positions = this._generatePointsCircle(childMarkers.length, center);
		}

		this._animationSpiderfy(childMarkers, positions);
	},

	unspiderfy: function (zoomDetails) {
		/// <param Name="zoomDetails">Argument from zoomanim if being called in a zoom animation or null otherwise</param>
		if (this._group._inZoomAnimation) {
			return;
		}
		this._animationUnspiderfy(zoomDetails);

		this._group._spiderfied = null;
	},

	_generatePointsCircle: function (count, centerPt) {
		var circumference = this._group.options.spiderfyDistanceMultiplier * this._circleFootSeparation * (2 + count),
			legLength = circumference / this._2PI,  //radius from circumference
			angleStep = this._2PI / count,
			res = [],
			i, angle;

		legLength = Math.max(legLength, 35); // Minimum distance to get outside the cluster icon.

		res.length = count;

		for (i = 0; i < count; i++) { // Clockwise, like spiral.
			angle = this._circleStartAngle + i * angleStep;
			res[i] = new L.Point(centerPt.x + legLength * Math.cos(angle), centerPt.y + legLength * Math.sin(angle))._round();
		}

		return res;
	},

	_generatePointsSpiral: function (count, centerPt) {
		var spiderfyDistanceMultiplier = this._group.options.spiderfyDistanceMultiplier,
			legLength = spiderfyDistanceMultiplier * this._spiralLengthStart,
			separation = spiderfyDistanceMultiplier * this._spiralFootSeparation,
			lengthFactor = spiderfyDistanceMultiplier * this._spiralLengthFactor * this._2PI,
			angle = 0,
			res = [],
			i;

		res.length = count;

		// Higher index, closer position to cluster center.
		for (i = count; i >= 0; i--) {
			// Skip the first position, so that we are already farther from center and we avoid
			// being under the default cluster icon (especially important for Circle Markers).
			if (i < count) {
				res[i] = new L.Point(centerPt.x + legLength * Math.cos(angle), centerPt.y + legLength * Math.sin(angle))._round();
			}
			angle += separation / legLength + i * 0.0005;
			legLength += lengthFactor / angle;
		}
		return res;
	},

	_noanimationUnspiderfy: function () {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			childMarkers = this.getAllChildMarkers(null, true),
			m, i;

		group._ignoreMove = true;

		this.setOpacity(1);
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			fg.removeLayer(m);

			if (m._preSpiderfyLatlng) {
				m.setLatLng(m._preSpiderfyLatlng);
				delete m._preSpiderfyLatlng;
			}
			if (m.setZIndexOffset) {
				m.setZIndexOffset(0);
			}

			if (m._spiderLeg) {
				map.removeLayer(m._spiderLeg);
				delete m._spiderLeg;
			}
		}

		group.fire('unspiderfied', {
			cluster: this,
			markers: childMarkers
		});
		group._ignoreMove = false;
		group._spiderfied = null;
	}
});

//Non Animated versions of everything
L.MarkerClusterNonAnimated = L.MarkerCluster.extend({
	_animationSpiderfy: function (childMarkers, positions) {
		var group = this._group,
			map = group._map,
			fg = group._featureGroup,
			legOptions = this._group.options.spiderLegPolylineOptions,
			i, m, leg, newPos;

		group._ignoreMove = true;

		// Traverse in ascending order to make sure that inner circleMarkers are on top of further legs. Normal markers are re-ordered by newPosition.
		// The reverse order trick no longer improves performance on modern browsers.
		for (i = 0; i < childMarkers.length; i++) {
			newPos = map.layerPointToLatLng(positions[i]);
			m = childMarkers[i];

			// Add the leg before the marker, so that in case the latter is a circleMarker, the leg is behind it.
			leg = new L.Polyline([this._latlng, newPos], legOptions);
			map.addLayer(leg);
			m._spiderLeg = leg;

			// Now add the marker.
			m._preSpiderfyLatlng = m._latlng;
			m.setLatLng(newPos);
			if (m.setZIndexOffset) {
				m.setZIndexOffset(1000000); //Make these appear on top of EVERYTHING
			}

			fg.addLayer(m);
		}
		this.setOpacity(0.3);

		group._ignoreMove = false;
		group.fire('spiderfied', {
			cluster: this,
			markers: childMarkers
		});
	},

	_animationUnspiderfy: function () {
		this._noanimationUnspiderfy();
	}
});

//Animated versions here
L.MarkerCluster.include({

	_animationSpiderfy: function (childMarkers, positions) {
		var me = this,
			group = this._group,
			map = group._map,
			fg = group._featureGroup,
			thisLayerLatLng = this._latlng,
			thisLayerPos = map.latLngToLayerPoint(thisLayerLatLng),
			svg = L.Path.SVG,
			legOptions = L.extend({}, this._group.options.spiderLegPolylineOptions), // Copy the options so that we can modify them for animation.
			finalLegOpacity = legOptions.opacity,
			i, m, leg, legPath, legLength, newPos;

		if (finalLegOpacity === undefined) {
			finalLegOpacity = L.MarkerClusterGroup.prototype.options.spiderLegPolylineOptions.opacity;
		}

		if (svg) {
			// If the initial opacity of the spider leg is not 0 then it appears before the animation starts.
			legOptions.opacity = 0;

			// Add the class for CSS transitions.
			legOptions.className = (legOptions.className || '') + ' leaflet-cluster-spider-leg';
		} else {
			// Make sure we have a defined opacity.
			legOptions.opacity = finalLegOpacity;
		}

		group._ignoreMove = true;

		// Add markers and spider legs to map, hidden at our center point.
		// Traverse in ascending order to make sure that inner circleMarkers are on top of further legs. Normal markers are re-ordered by newPosition.
		// The reverse order trick no longer improves performance on modern browsers.
		for (i = 0; i < childMarkers.length; i++) {
			m = childMarkers[i];

			newPos = map.layerPointToLatLng(positions[i]);

			// Add the leg before the marker, so that in case the latter is a circleMarker, the leg is behind it.
			leg = new L.Polyline([thisLayerLatLng, newPos], legOptions);
			map.addLayer(leg);
			m._spiderLeg = leg;

			// Explanations: https://jakearchibald.com/2013/animated-line-drawing-svg/
			// In our case the transition property is declared in the CSS file.
			if (svg) {
				legPath = leg._path;
				legLength = legPath.getTotalLength() + 0.1; // Need a small extra length to avoid remaining dot in Firefox.
				legPath.style.strokeDasharray = legLength; // Just 1 length is enough, it will be duplicated.
				legPath.style.strokeDashoffset = legLength;
			}

			// If it is a marker, add it now and we'll animate it out
			if (m.setZIndexOffset) {
				m.setZIndexOffset(1000000); // Make normal markers appear on top of EVERYTHING
			}
			if (m.clusterHide) {
				m.clusterHide();
			}
			
			// Vectors just get immediately added
			fg.addLayer(m);

			if (m._setPos) {
				m._setPos(thisLayerPos);
			}
		}

		group._forceLayout();
		group._animationStart();

		// Reveal markers and spider legs.
		for (i = childMarkers.length - 1; i >= 0; i--) {
			newPos = map.layerPointToLatLng(positions[i]);
			m = childMarkers[i];

			//Move marker to new position
			m._preSpiderfyLatlng = m._latlng;
			m.setLatLng(newPos);
			
			if (m.clusterShow) {
				m.clusterShow();
			}

			// Animate leg (animation is actually delegated to CSS transition).
			if (svg) {
				leg = m._spiderLeg;
				legPath = leg._path;
				legPath.style.strokeDashoffset = 0;
				//legPath.style.strokeOpacity = finalLegOpacity;
				leg.setStyle({opacity: finalLegOpacity});
			}
		}
		this.setOpacity(0.3);

		group._ignoreMove = false;

		setTimeout(function () {
			group._animationEnd();
			group.fire('spiderfied', {
				cluster: me,
				markers: childMarkers
			});
		}, 200);
	},

	_animationUnspiderfy: function (zoomDetails) {
		var me = this,
			group = this._group,
			map = group._map,
			fg = group._featureGroup,
			thisLayerPos = zoomDetails ? map._latLngToNewLayerPoint(this._latlng, zoomDetails.zoom, zoomDetails.center) : map.latLngToLayerPoint(this._latlng),
			childMarkers = this.getAllChildMarkers(null, true),
			svg = L.Path.SVG,
			m, i, leg, legPath, legLength, nonAnimatable;

		group._ignoreMove = true;
		group._animationStart();

		//Make us visible and bring the child markers back in
		this.setOpacity(1);
		for (i = childMarkers.length - 1; i >= 0; i--) {
			m = childMarkers[i];

			//Marker was added to us after we were spiderfied
			if (!m._preSpiderfyLatlng) {
				continue;
			}

			//Close any popup on the marker first, otherwise setting the location of the marker will make the map scroll
			m.closePopup();

			//Fix up the location to the real one
			m.setLatLng(m._preSpiderfyLatlng);
			delete m._preSpiderfyLatlng;

			//Hack override the location to be our center
			nonAnimatable = true;
			if (m._setPos) {
				m._setPos(thisLayerPos);
				nonAnimatable = false;
			}
			if (m.clusterHide) {
				m.clusterHide();
				nonAnimatable = false;
			}
			if (nonAnimatable) {
				fg.removeLayer(m);
			}

			// Animate the spider leg back in (animation is actually delegated to CSS transition).
			if (svg) {
				leg = m._spiderLeg;
				legPath = leg._path;
				legLength = legPath.getTotalLength() + 0.1;
				legPath.style.strokeDashoffset = legLength;
				leg.setStyle({opacity: 0});
			}
		}

		group._ignoreMove = false;

		setTimeout(function () {
			//If we have only <= one child left then that marker will be shown on the map so don't remove it!
			var stillThereChildCount = 0;
			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i];
				if (m._spiderLeg) {
					stillThereChildCount++;
				}
			}


			for (i = childMarkers.length - 1; i >= 0; i--) {
				m = childMarkers[i];

				if (!m._spiderLeg) { //Has already been unspiderfied
					continue;
				}

				if (m.clusterShow) {
					m.clusterShow();
				}
				if (m.setZIndexOffset) {
					m.setZIndexOffset(0);
				}

				if (stillThereChildCount > 1) {
					fg.removeLayer(m);
				}

				map.removeLayer(m._spiderLeg);
				delete m._spiderLeg;
			}
			group._animationEnd();
			group.fire('unspiderfied', {
				cluster: me,
				markers: childMarkers
			});
		}, 200);
	}
});


L.MarkerClusterGroup.include({
	//The MarkerCluster currently spiderfied (if any)
	_spiderfied: null,

	unspiderfy: function () {
		this._unspiderfy.apply(this, arguments);
	},

	_spiderfierOnAdd: function () {
		this._map.on('click', this._unspiderfyWrapper, this);

		if (this._map.options.zoomAnimation) {
			this._map.on('zoomstart', this._unspiderfyZoomStart, this);
		}
		//Browsers without zoomAnimation or a big zoom don't fire zoomstart
		this._map.on('zoomend', this._noanimationUnspiderfy, this);

		if (!L.Browser.touch) {
			this._map.getRenderer(this);
			//Needs to happen in the pageload, not after, or animations don't work in webkit
			//  http://stackoverflow.com/questions/8455200/svg-animate-with-dynamically-added-elements
			//Disable on touch browsers as the animation messes up on a touch zoom and isn't very noticable
		}
	},

	_spiderfierOnRemove: function () {
		this._map.off('click', this._unspiderfyWrapper, this);
		this._map.off('zoomstart', this._unspiderfyZoomStart, this);
		this._map.off('zoomanim', this._unspiderfyZoomAnim, this);
		this._map.off('zoomend', this._noanimationUnspiderfy, this);

		//Ensure that markers are back where they should be
		// Use no animation to avoid a sticky leaflet-cluster-anim class on mapPane
		this._noanimationUnspiderfy();
	},

	//On zoom start we add a zoomanim handler so that we are guaranteed to be last (after markers are animated)
	//This means we can define the animation they do rather than Markers doing an animation to their actual location
	_unspiderfyZoomStart: function () {
		if (!this._map) { //May have been removed from the map by a zoomEnd handler
			return;
		}

		this._map.on('zoomanim', this._unspiderfyZoomAnim, this);
	},

	_unspiderfyZoomAnim: function (zoomDetails) {
		//Wait until the first zoomanim after the user has finished touch-zooming before running the animation
		if (L.DomUtil.hasClass(this._map._mapPane, 'leaflet-touching')) {
			return;
		}

		this._map.off('zoomanim', this._unspiderfyZoomAnim, this);
		this._unspiderfy(zoomDetails);
	},

	_unspiderfyWrapper: function () {
		/// <summary>_unspiderfy but passes no arguments</summary>
		this._unspiderfy();
	},

	_unspiderfy: function (zoomDetails) {
		if (this._spiderfied) {
			this._spiderfied.unspiderfy(zoomDetails);
		}
	},

	_noanimationUnspiderfy: function () {
		if (this._spiderfied) {
			this._spiderfied._noanimationUnspiderfy();
		}
	},

	//If the given layer is currently being spiderfied then we unspiderfy it so it isn't on the map anymore etc
	_unspiderfyLayer: function (layer) {
		if (layer._spiderLeg) {
			this._featureGroup.removeLayer(layer);

			if (layer.clusterShow) {
				layer.clusterShow();
			}
				//Position will be fixed up immediately in _animationUnspiderfy
			if (layer.setZIndexOffset) {
				layer.setZIndexOffset(0);
			}

			this._map.removeLayer(layer._spiderLeg);
			delete layer._spiderLeg;
		}
	}
});

/**
 * Adds 1 public method to MCG and 1 to L.Marker to facilitate changing
 * markers' icon options and refreshing their icon and their parent clusters
 * accordingly (case where their iconCreateFunction uses data of childMarkers
 * to make up the cluster icon).
 */


L.MarkerClusterGroup.include({
	/**
	 * Updates the icon of all clusters which are parents of the given marker(s).
	 * In singleMarkerMode, also updates the given marker(s) icon.
	 * @param layers L.MarkerClusterGroup|L.LayerGroup|Array(L.Marker)|Map(L.Marker)|
	 * L.MarkerCluster|L.Marker (optional) list of markers (or single marker) whose parent
	 * clusters need to be updated. If not provided, retrieves all child markers of this.
	 * @returns {L.MarkerClusterGroup}
	 */
	refreshClusters: function (layers) {
		if (!layers) {
			layers = this._topClusterLevel.getAllChildMarkers();
		} else if (layers instanceof L.MarkerClusterGroup) {
			layers = layers._topClusterLevel.getAllChildMarkers();
		} else if (layers instanceof L.LayerGroup) {
			layers = layers._layers;
		} else if (layers instanceof L.MarkerCluster) {
			layers = layers.getAllChildMarkers();
		} else if (layers instanceof L.Marker) {
			layers = [layers];
		} // else: must be an Array(L.Marker)|Map(L.Marker)
		this._flagParentsIconsNeedUpdate(layers);
		this._refreshClustersIcons();

		// In case of singleMarkerMode, also re-draw the markers.
		if (this.options.singleMarkerMode) {
			this._refreshSingleMarkerModeMarkers(layers);
		}

		return this;
	},

	/**
	 * Simply flags all parent clusters of the given markers as having a "dirty" icon.
	 * @param layers Array(L.Marker)|Map(L.Marker) list of markers.
	 * @private
	 */
	_flagParentsIconsNeedUpdate: function (layers) {
		var id, parent;

		// Assumes layers is an Array or an Object whose prototype is non-enumerable.
		for (id in layers) {
			// Flag parent clusters' icon as "dirty", all the way up.
			// Dumb process that flags multiple times upper parents, but still
			// much more efficient than trying to be smart and make short lists,
			// at least in the case of a hierarchy following a power law:
			// http://jsperf.com/flag-nodes-in-power-hierarchy/2
			parent = layers[id].__parent;
			while (parent) {
				parent._iconNeedsUpdate = true;
				parent = parent.__parent;
			}
		}
	},

	/**
	 * Re-draws the icon of the supplied markers.
	 * To be used in singleMarkerMode only.
	 * @param layers Array(L.Marker)|Map(L.Marker) list of markers.
	 * @private
	 */
	_refreshSingleMarkerModeMarkers: function (layers) {
		var id, layer;

		for (id in layers) {
			layer = layers[id];

			// Make sure we do not override markers that do not belong to THIS group.
			if (this.hasLayer(layer)) {
				// Need to re-create the icon first, then re-draw the marker.
				layer.setIcon(this._overrideMarkerIcon(layer));
			}
		}
	}
});

L.Marker.include({
	/**
	 * Updates the given options in the marker's icon and refreshes the marker.
	 * @param options map object of icon options.
	 * @param directlyRefreshClusters boolean (optional) true to trigger
	 * MCG.refreshClustersOf() right away with this single marker.
	 * @returns {L.Marker}
	 */
	refreshIconOptions: function (options, directlyRefreshClusters) {
		var icon = this.options.icon;

		L.setOptions(icon, options);

		this.setIcon(icon);

		// Shortcut to refresh the associated MCG clusters right away.
		// To be used when refreshing a single marker.
		// Otherwise, better use MCG.refreshClusters() once at the end with
		// the list of modified markers.
		if (directlyRefreshClusters && this.__parent) {
			this.__parent._group.refreshClusters(this);
		}

		return this;
	}
});

exports.MarkerClusterGroup = MarkerClusterGroup;
exports.MarkerCluster = MarkerCluster;

})));
//# sourceMappingURL=leaflet.markercluster-src.js.map


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/map-view/map-view.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/map-view/map-view.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n\t<ion-toolbar class=\"toolbar-format\">\n    \t<ion-buttons slot=\"start\" style=\"margin-top: 5px;\">\n      \t\t<ion-back-button text=\"\" style=\"color: #fff;\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<div style=\"padding: 0px 10px;\">\n\t\t\t<img class=\"logo-image\" src=\"assets/img/poshlife.png\">\n\t\t</div>\n  \t</ion-toolbar>\n  \n\t<ion-toolbar id=\"home-result\" class=\"toolbar-border toolbar-format\">\n    \t<form [formGroup]=\"searchForm\">\n\t\t\t<mat-form-field appearance=\"outline\" class=\"w100\" style=\"margin-left: 5px;\">\n\t\t\t\t<input matInput placeholder=\"{{'searchPlaceholder' | translate }}\" formControlName=\"myControl\"\n\t\t\t\t\t[matAutocomplete]=\"auto\" (keyup)=\"searchProperty()\" [(ngModel)]=\"searchText\">\n\t\t\t\t<mat-icon matPrefix>search</mat-icon>\n\t\t\t\t<mat-icon matSuffix *ngIf=\"searchText\" (click)=\"searchText = ''\">close</mat-icon>\n\t\t\t\t<mat-autocomplete #auto=\"matAutocomplete\">\n\t\t\t\t\t<mat-option style=\"font-size: 13px !important;\" *ngIf=\"isLoading\" class=\"is-loading\">{{ 'loading' | translate }}</mat-option>\n\t\t\t\t\t<ng-container *ngIf=\"!isLoading\">\n\t\t\t\t\t\t<mat-optgroup *ngFor=\"let group of searchValues\" [label]=\"group.name\">\n\t\t\t\t\t\t\t<mat-option class=\"matOption\" *ngFor=\"let option of group.data\" [value]=\"option.address\">\n\t\t\t\t\t\t\t\t<span *ngIf=\"group.name === 'Listing'\" (click)=\"goToDetailPage(option.id)\">\n\t\t\t\t\t\t\t\t\t<p class=\"p-address\">\n\t\t\t\t\t\t\t\t\t\t<span class=\"address-format\">\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.aptUnit\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{ option.aptUnit }}-\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t{{ option.address }}<br>\n\t\t\t\t\t\t\t\t\t\t\t{{ option.municipality }} - {{ option.community }}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"span-price\">\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.event === 'Sold' || option.event === 'Terminated'\"\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"text-decoration: line-through;\">\n\t\t\t\t\t\t\t\t\t\t\t\t$ {{ option.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.event !== 'Sold' && option.event !== 'Terminated'\">\n\t\t\t\t\t\t\t\t\t\t\t\t$ {{ option.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p class=\"p-secondary\">\n\t\t\t\t\t\t\t\t\t\t<span style=\"white-space: normal;\">\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"!option.bedroomsPlus\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{ option.bedrooms }} {{ 'property.bedrooms' | translate }},\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"option.bedroomsPlus\">\n\t\t\t\t\t\t\t\t\t\t\t\t{{ option.bedrooms }}+{{ option.bedroomsPlus }} {{ 'property.bedrooms' | translate }},\n\t\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t\t{{ option.washrooms }} {{ 'property.bathroom' | translate }},\n\t\t\t\t\t\t\t\t\t\t\t{{ option.garageSpaces }} {{ 'property.garage' | translate }}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"span-type\">\n\t\t\t\t\t\t\t\t\t\t\t{{ option.type }}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t\t<p class=\"p-tertiary\">\n\t\t\t\t\t\t\t\t\t\t<span class=\"span-listing\">\n\t\t\t\t\t\t\t\t\t\t\tListing Date: {{option.listingEntryDate | date:'yyyy-MM-dd'}}\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t\t<span class=\"event\">\n\t\t\t\t\t\t\t\t\t\t\t[{{ option.event }}]\n\t\t\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t\t</p>\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t\t<span *ngIf=\"group.name === 'Locations'\"\n\t\t\t\t\t\t\t\t\tstyle=\"white-space: normal;\"\n\t\t\t\t\t\t\t\t\t(click)=\"selectLocation(option.data)\">\n\t\t\t\t\t\t\t\t\t{{ option.locationSearch }}\n\t\t\t\t\t\t\t\t</span>\n\t\t\t\t\t\t\t</mat-option>\n\t\t\t\t\t\t</mat-optgroup>\n\t\t\t\t\t</ng-container>\n\t\t\t\t</mat-autocomplete>\n\t\t\t</mat-form-field>\n\t\t</form>\n\n\t\t<div slot=\"end\" class=\"filter-btn\" (click)=\"searchFilter()\">\n\t\t\t<mat-icon class=\"text-white\">filter_alt</mat-icon><br>\n\t\t\t<span class=\"text-white\">{{ 'app.filter' | translate }}</span>\n\t\t</div>\n  \t</ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<div id=\"myMap\" style=\"width: 100%; height: 100%;\">\n\t\t<div class=\"leaflet-top leaflet-left\">\n\t\t\t<ion-button *ngIf=\"!satellite\" class=\"satelliteFormat\" (click)=\"satelliteView()\">\n\t\t\t\t<ion-icon name=\"map\" style=\"margin-right: 5px;\"></ion-icon>\n\t\t\t\t{{ 'property.satellite' | translate }}\n\t\t\t</ion-button>\n\t\t\t<ion-button *ngIf=\"satellite\" class=\"satelliteFormat\" (click)=\"satelliteView()\">\n\t\t\t\t<ion-icon name=\"map\" style=\"margin-right: 5px;\"></ion-icon>\n\t\t\t\t{{ 'property.street' | translate }}\n\t\t\t</ion-button>\n\t\t\t<ion-button color=\"danger\" class=\"schoolFormat\" (click)=\"schoolPopup()\">\n\t\t\t\t<ion-icon name=\"school\" style=\"margin-right: 5px;\"></ion-icon>\n\t\t\t\t{{ 'property.school' | translate }}\n\t\t\t</ion-button>\n\t\t\t<div *ngIf=\"showSchoolPopup\" class=\"school-popover\">\n\t\t\t\t<h6 class=\"fw400\">Preferences</h6>\n\t\t\t\t<div class=\"show-school-div\">\n\t\t\t\t\t<span class=\"school-title\">Show School</span>\n\t\t\t\t\t<ion-toggle class=\"school-toggle\"\n\t\t\t\t\t\t[(ngModel)]=\"showSchoolFlag\"\n\t\t\t\t\t\t(ionChange)=\"showSchool(showSchoolFlag)\"\n\t\t\t\t\t\tmode=\"ios\" color=\"danger\">\n\t\t\t\t\t</ion-toggle>\n\t\t\t\t</div>\n\t\t\t\t<!-- <div>\n\t\t\t\t\t<h6 class=\"fw400\">School Setting</h6>\n\t\t\t\t\t<div style=\"padding: 5px 0px;\">\n\t\t\t\t\t\t<ion-checkbox color=\"danger\" class=\"chk\" slot=\"start\"></ion-checkbox>\n\t\t\t\t\t\t<span class=\"chk-title\">Elementary</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div style=\"padding: 5px 0px;\">\n\t\t\t\t\t\t<ion-checkbox color=\"danger\" class=\"chk\" slot=\"start\"></ion-checkbox>\n\t\t\t\t\t\t<span class=\"chk-title\">Secondary</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<hr style=\"background: #ccc;\">\n\t\t\t\t\t<div style=\"padding: 5px 0px;\">\n\t\t\t\t\t\t<ion-checkbox color=\"danger\" class=\"chk\" slot=\"start\"></ion-checkbox>\n\t\t\t\t\t\t<span class=\"chk-title\">Public</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div style=\"padding: 5px 0px;\">\n\t\t\t\t\t\t<ion-checkbox color=\"danger\" class=\"chk\" slot=\"start\"></ion-checkbox>\n\t\t\t\t\t\t<span class=\"chk-title\">Catholic</span>\n\t\t\t\t\t</div>\n\t\t\t\t</div> -->\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<div class=\"leaflet-top leaflet-right saleLeaseFormat\">\n\t\t\t<input type=\"button\" [ngClass]=\"{'saleActive' : saleFlag, 'inactive-btn' : !saleFlag}\" \n\t\t\t\tvalue=\"{{'property.saleAndSold' | translate }}\" (click)=\"saleLeaseChange('Sale')\" \n\t\t\t\tclass=\"saleLeaseButton\"/>\n\t\t\t<input type=\"button\" [ngClass]=\"{'leaseActive' : !saleFlag, 'inactive-btn' : saleFlag}\" \n\t\t\t\tvalue=\"{{'property.lease' | translate }}\" (click)=\"saleLeaseChange('Lease')\"\n\t\t\t\tclass=\"saleLeaseButton\"/>\n\t\t</div>\n\n\t\t<div class=\"leaflet-bottom leaflet-left bottom-div\">\n\t\t\t<input type=\"button\" [ngClass]=\"{'residentialActive' : propertyTypeFlag, 'inactive-btn' : !propertyTypeFlag}\" \n\t\t\t\tvalue=\"{{'property.residential' | translate }}\" (click)=\"propertyTypeChange('residential')\"\n\t\t\t\tclass=\"resiCommer\"/>\n\t\t\t<input type=\"button\" [ngClass]=\"{'commercialActive' : !propertyTypeFlag, 'inactive-btn' : propertyTypeFlag}\" \n\t\t\t\tvalue=\"{{'property.commercial' | translate }}\" (click)=\"propertyTypeChange('commercial')\" \n\t\t\t\tclass=\"resiCommer\"/>\n\t\t</div>\n\t</div>\n</ion-content>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/map-view/property-info-sheet.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/map-view/property-info-sheet.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"header-info\">\n\t<span class=\"total-listing\">{{properties.length}} listing(s) in total</span>\n\t<ion-icon class=\"close-icon\" name=\"close\" color=\"dark\" (click)=\"closePopup($event)\"></ion-icon>\n</div>\n<div class=\"bg-white mg-bt pos\">\n\t<div *ngFor=\"let property of properties\">\n\t\t<ion-button *ngIf=\"loginFlag\" color=\"danger\" (click)=\"login()\" class=\"login-btn\">Login Required</ion-button>\n\t\t<div [ngClass]=\"{'login-required': loginFlag === true}\" class=\"popup-listing\" (click)=\"openLink($event, property.id)\">\n\t\t\t<div class=\"header\">\n\t\t\t\t<p class=\"p-type\">{{ property.type }}</p>\n\n\t\t\t\t<img *ngIf=\"property.propertyType !== 'commercial'\" style=\"width: 18px; margin-left: 12px;\"\n\t\t\t\t\tsrc=\"assets/img/bedrooms.svg\">\n\t\t\t\t<p *ngIf=\"!property.bedroomsPlus && property.propertyType !== 'commercial'\" style=\"margin: 4px 4px\">\n\t\t\t\t\t{{ property.bedrooms }}\n\t\t\t\t</p>\n\t\t\t\t<p *ngIf=\"property.bedroomsPlus && property.propertyType !== 'commercial'\" style=\"margin: 4px 4px\">\n\t\t\t\t\t{{ property.bedrooms }} + {{ property.bedroomsPlus }}\n\t\t\t\t</p>\n\t\t\t\t\n\t\t\t\t<img *ngIf=\"property.propertyType !== 'commercial'\"\n\t\t\t\t\tstyle=\"width: 18px; margin-left: 12px;\"\n\t\t\t\t\tsrc=\"assets/img/bathrooms.svg\">\n\t\t\t\t<p *ngIf=\"property.propertyType !== 'commercial'\" style=\"margin: 4px 4px\">\n\t\t\t\t\t{{ property.washrooms }}\n\t\t\t\t</p>\n\n\t\t\t\t<img *ngIf=\"property.propertyType !== 'commercial'\" style=\"width: 18px; margin-left: 12px;\"\n\t\t\t\t\tsrc=\"assets/img/parking.svg\">\n\t\t\t\t<p *ngIf=\"property.propertyType !== 'commercial'\" style=\"margin: 4px 4px\">\n\t\t\t\t\t{{ property.garageSpaces }}\n\t\t\t\t</p>\n\t\t\t\t<p style=\"margin: 4px 12px;\">\n\t\t\t\t\t<ion-badge class=\"fl-rt leaseClass\" slot=\"end\"\n\t\t\t\t\t\t*ngIf=\"property.soldPrice === 0 && property.saleLease === 'Lease'\">\n\t\t\t\t\t\t<ion-text color=\"light\">{{ property.saleLease }}</ion-text>\n\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t<ion-badge class=\"fl-rt saleClass\" slot=\"end\"\n\t\t\t\t\t\t*ngIf=\"property.soldPrice === 0 && property.saleLease === 'Sale'\">\n\t\t\t\t\t\t<ion-text color=\"light\">{{ property.saleLease }}</ion-text>\n\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t<ion-badge class=\"fl-rt soldClass\" slot=\"end\" *ngIf=\"property.soldPrice !== 0\">\n\t\t\t\t\t\t<ion-text color=\"light\">{{ 'property.sold' | translate }}</ion-text>\n\t\t\t\t\t</ion-badge>\n\t\t\t\t</p>\n\t\t\t</div>\n\t\t\t<div class=\"photo\">\n\t\t\t\t<div class=\"photo-container\" [style.background-image]=\"'url('+imageUrl+property.propertyImage+')'\">\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"main\">\n\t\t\t\t<p *ngIf=\"property.soldPrice === 0\" class=\"price-format\">\n\t\t\t\t\t{{ 'property.listed' | translate }}:\n\t\t\t\t\t<span class=\"priceText\" *ngIf=\"property.saleLease == 'Sale'\">\n\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t</span>\n\t\t\t\t\t<span class=\"priceText\" *ngIf=\"property.saleLease == 'Lease'\">\n\t\t\t\t\t\t${{ property.listPrice | number:'1.0':'en-US' }} /month\n\t\t\t\t\t</span>\n\t\t\t\t</p>\n\n\t\t\t\t<p *ngIf=\"property.soldPrice !== 0\" class=\"price-format\">\n\t\t\t\t\t{{ 'property.soldFor' | translate }}:\n\t\t\t\t\t<span class=\"priceText\">\n\t\t\t\t\t\t${{ property.soldPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t</span>\n\t\t\t\t</p>\n\t\t\t\t<p style=\"margin: 5px 0px 0px;\" *ngIf=\"property.soldPrice === 0\">\n\t\t\t\t\tList {{ property.listingEntryDate | date:'yyyy-MM-dd' }}, {{ property.difference_In_ListingDays }} days ago\n\t\t\t\t</p>\n\t\t\t\t<p style=\"margin: 5px 0px 0px;\" *ngIf=\"property.soldPrice !== 0\">\n\t\t\t\t\tSold {{ property.soldDate | date:'yyyy-MM-dd' }}, {{ property.difference_In_SoldDays }} days ago\n\t\t\t\t</p>\n\t\t\t\t<p style=\"margin: 5px 0px 0px; color: #666;\">\n\t\t\t\t\t<span *ngIf=\"property.aptUnit\">{{ property.aptUnit }} - </span> \n\t\t\t\t\t{{ property.address }}, {{ property.area }}</p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n</div>");

/***/ }),

/***/ "./src/app/pages/map-view/map-view.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/map-view/map-view.module.ts ***!
  \***************************************************/
/*! exports provided: MapViewPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapViewPageModule", function() { return MapViewPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../pipes/pipes.module */ "./src/app/pipes/pipes.module.ts");
/* harmony import */ var _map_view_page__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./map-view.page */ "./src/app/pages/map-view/map-view.page.ts");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/esm2015/bottom-sheet.js");
/* harmony import */ var _map_view_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./map-view.service */ "./src/app/pages/map-view/map-view.service.ts");















const routes = [
    {
        path: '',
        component: _map_view_page__WEBPACK_IMPORTED_MODULE_11__["MapViewPage"]
    }
];
let MapViewPageModule = class MapViewPageModule {
};
MapViewPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatListModule"],
            _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_10__["PipesModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatFormFieldModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatOptionModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatInputModule"],
            _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"],
            _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_12__["MatBottomSheetModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes),
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_8__["TranslateModule"].forChild()
        ],
        declarations: [
            _map_view_page__WEBPACK_IMPORTED_MODULE_11__["MapViewPage"],
            _map_view_page__WEBPACK_IMPORTED_MODULE_11__["PropertyInfoSheet"]
        ],
        providers: [
            _map_view_service__WEBPACK_IMPORTED_MODULE_13__["MapViewService"]
        ],
        entryComponents: [
            _map_view_page__WEBPACK_IMPORTED_MODULE_11__["PropertyInfoSheet"]
        ]
    })
], MapViewPageModule);



/***/ }),

/***/ "./src/app/pages/map-view/map-view.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/map-view/map-view.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".matOption {\n  position: relative;\n  display: flex;\n  justify-content: center;\n  padding: 8px 6px 8px 0px;\n  height: auto !important;\n  font-size: 14px !important;\n  line-height: 25px !important;\n  border-bottom: 1px solid #ccc !important;\n}\n.matOption .p-address {\n  display: flex;\n  margin: 0px;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n.matOption .p-address .address-format {\n  white-space: normal;\n  color: #333;\n}\n.matOption .p-address .span-price {\n  color: #ff5b29;\n  margin-left: 0.5em;\n}\n.matOption .p-secondary {\n  margin: 0px;\n  display: flex;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n.matOption .p-secondary .span-type {\n  margin-left: 0.5em;\n  color: #333;\n  font-weight: 600;\n}\n.matOption .p-tertiary {\n  margin: 0px;\n  display: flex;\n  flex-wrap: nowrap;\n  justify-content: space-between;\n  align-items: flex-start;\n}\n.matOption .p-tertiary .span-listing {\n  color: #999;\n  font-size: 13px;\n  white-space: normal;\n}\n.matOption .p-tertiary .event {\n  color: #333;\n  font-weight: 600;\n}\n.toolbar-format {\n  --background: #e01a27 !important;\n  zoom: 0.9 !important;\n  border: none;\n}\n.title-format {\n  font-size: 18px;\n  letter-spacing: 3px;\n}\n.filter-btn {\n  width: 75px;\n  text-align: center;\n}\nion-content {\n  border-bottom: 6px solid;\n  border-top: 3px solid;\n}\n.saleLeaseFormat {\n  display: inline-flex;\n  padding: 5px 10px;\n}\n.saleActive {\n  background: #1e88e5;\n  color: #fff;\n  border: none;\n}\n.leaseActive {\n  background: #558b2f;\n  color: #fff;\n  border: none;\n}\n.inactive-btn {\n  background: #fff;\n  color: #333;\n  border: 1px solid #ccc;\n}\n.saleLeaseButton {\n  height: 25px;\n  padding: 0px 10px;\n  box-shadow: none;\n  border-radius: 5px;\n  pointer-events: auto;\n  font-weight: 600;\n  font-family: \"Montserrat\";\n  margin-right: 4%;\n}\n.satelliteFormat {\n  --background: #fff;\n  --color: #000;\n  height: 25px;\n  box-shadow: none;\n  border-radius: 5px;\n  pointer-events: auto;\n  font-weight: 600;\n  font-family: \"Montserrat\";\n  font-size: 12px;\n  text-transform: capitalize;\n}\n.schoolFormat {\n  height: 25px;\n  box-shadow: none;\n  border-radius: 5px;\n  pointer-events: auto;\n  font-weight: 600;\n  font-family: \"Montserrat\";\n  font-size: 12px;\n  text-transform: capitalize;\n}\n.school-popover {\n  left: 100px;\n  top: 42px;\n  position: absolute;\n  pointer-events: auto;\n  width: 210px;\n  padding: 0 12px 12px;\n  border: 1px solid #ebeef5;\n  box-shadow: 0 2px 12px 0 rgba(0, 0, 0, 0.1);\n  background-color: #fff;\n  font-family: \"Montserrat\";\n}\n.school-popover::before {\n  content: \"\";\n  z-index: 10;\n  top: -5px;\n  width: 20px;\n  height: 20px;\n  background-color: inherit;\n  transform: rotate(45deg);\n  position: absolute;\n}\n.show-school-div {\n  display: inline-flex;\n  align-items: center;\n}\n.school-title {\n  font-size: 14px;\n}\n.school-toggle {\n  position: absolute;\n  right: 5px;\n  zoom: 0.7;\n}\n.chk {\n  position: absolute;\n}\n.chk-title {\n  margin-left: 30px;\n  font-size: 14px;\n}\n.bottom-div {\n  margin: 20px 15px;\n  display: inline-flex;\n}\n.residentialActive {\n  background: #000;\n  color: #fff;\n  border: none;\n}\n.commercialActive {\n  background: #3f51b5;\n  color: #fff;\n  border: none;\n}\n.resiCommer {\n  font-weight: 600;\n  font-family: \"Montserrat\";\n  height: 25px;\n  padding: 0px 10px;\n  pointer-events: auto;\n  margin-right: 2%;\n  box-shadow: none;\n  border-radius: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvbWFwLXZpZXcvbWFwLXZpZXcucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9tYXAtdmlldy9tYXAtdmlldy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHdCQUFBO0VBQ0EsdUJBQUE7RUFDQSwwQkFBQTtFQUNBLDRCQUFBO0VBQ0Esd0NBQUE7QUNDSjtBRENJO0VBQ0ksYUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLDhCQUFBO0VBQ0EsdUJBQUE7QUNDUjtBRENRO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0FDQ1o7QURFUTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtBQ0FaO0FESUk7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7RUFDQSx1QkFBQTtBQ0ZSO0FESVE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0ZaO0FETUk7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsOEJBQUE7RUFDQSx1QkFBQTtBQ0pSO0FETVE7RUFDSSxXQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FDSlo7QURPUTtFQUNJLFdBQUE7RUFDQSxnQkFBQTtBQ0xaO0FEVUE7RUFDSSxnQ0FBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtBQ1BKO0FEVUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNQSjtBRFVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FDUEo7QURVQTtFQUNJLHdCQUFBO0VBQ0EscUJBQUE7QUNQSjtBRFVBO0VBQ0ksb0JBQUE7RUFDQSxpQkFBQTtBQ1BKO0FEVUE7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDUEo7QURVQTtFQUNJLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNQSjtBRFVBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QUNQSjtBRFVBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGdCQUFBO0FDUEo7QURVQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0FDUEo7QURVQTtFQUNJLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0FDUEo7QURVQTtFQUNJLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtFQUNBLHlCQUFBO0VBQ0EsMkNBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0FDUEo7QURVQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSx3QkFBQTtFQUNBLGtCQUFBO0FDUEo7QURVQTtFQUNJLG9CQUFBO0VBQ0EsbUJBQUE7QUNQSjtBRFVBO0VBQ0ksZUFBQTtBQ1BKO0FEVUE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FDUEo7QURVQTtFQUNJLGtCQUFBO0FDUEo7QURVQTtFQUNJLGlCQUFBO0VBQ0EsZUFBQTtBQ1BKO0FEVUE7RUFDSSxpQkFBQTtFQUNBLG9CQUFBO0FDUEo7QURVQTtFQUNJLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUNQSjtBRFVBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ1BKO0FEVUE7RUFDSSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNQSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcC12aWV3L21hcC12aWV3LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXRPcHRpb24ge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHBhZGRpbmc6IDhweCA2cHggOHB4IDBweDtcbiAgICBoZWlnaHQ6IGF1dG8gIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgICBsaW5lLWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjICFpbXBvcnRhbnQ7XG5cbiAgICAucC1hZGRyZXNzIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuXG4gICAgICAgIC5hZGRyZXNzLWZvcm1hdCB7XG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgICAgICAgICAgY29sb3I6ICMzMzM7XG4gICAgICAgIH1cblxuICAgICAgICAuc3Bhbi1wcmljZSB7XG4gICAgICAgICAgICBjb2xvcjogI2ZmNWIyOTtcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiAuNWVtO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLnAtc2Vjb25kYXJ5IHtcbiAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtd3JhcDogbm93cmFwO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuXG4gICAgICAgIC5zcGFuLXR5cGUge1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IC41ZW07XG4gICAgICAgICAgICBjb2xvcjogIzMzMztcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAucC10ZXJ0aWFyeSB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LXdyYXA6IG5vd3JhcDtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcblxuICAgICAgICAuc3Bhbi1saXN0aW5nIHtcbiAgICAgICAgICAgIGNvbG9yOiAjOTk5O1xuICAgICAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICAgICAgfVxuXG4gICAgICAgIC5ldmVudCB7XG4gICAgICAgICAgICBjb2xvcjogIzMzMztcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbi50b29sYmFyLWZvcm1hdCB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjZTAxYTI3ICFpbXBvcnRhbnQ7XG4gICAgem9vbTogMC45ICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4udGl0bGUtZm9ybWF0IHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDNweDtcbn1cblxuLmZpbHRlci1idG4ge1xuICAgIHdpZHRoOiA3NXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAgIGJvcmRlci1ib3R0b206IDZweCBzb2xpZDtcbiAgICBib3JkZXItdG9wOiAzcHggc29saWQ7XG59XG5cbi5zYWxlTGVhc2VGb3JtYXQge1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgIHBhZGRpbmc6IDVweCAxMHB4O1xufVxuXG4uc2FsZUFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogIzFlODhlNTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IG5vbmU7XG59XG5cbi5sZWFzZUFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogIzU1OGIyZjtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IG5vbmU7XG59XG5cbi5pbmFjdGl2ZS1idG4ge1xuICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcbn1cblxuLnNhbGVMZWFzZUJ1dHRvbiB7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIHBhZGRpbmc6IDBweCAxMHB4O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBvaW50ZXItZXZlbnRzOiBhdXRvO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0JztcbiAgICBtYXJnaW4tcmlnaHQ6IDQlO1xufVxuXG4uc2F0ZWxsaXRlRm9ybWF0IHtcbiAgICAtLWJhY2tncm91bmQ6ICNmZmY7XG4gICAgLS1jb2xvcjogIzAwMDtcbiAgICBoZWlnaHQ6IDI1cHg7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgcG9pbnRlci1ldmVudHM6IGF1dG87XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnNjaG9vbEZvcm1hdCB7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIHBvaW50ZXItZXZlbnRzOiBhdXRvO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1mYW1pbHk6ICdNb250c2VycmF0JztcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG5cbi5zY2hvb2wtcG9wb3ZlciB7XG4gICAgbGVmdDogMTAwcHg7XG4gICAgdG9wOiA0MnB4O1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBwb2ludGVyLWV2ZW50czogYXV0bztcbiAgICB3aWR0aDogMjEwcHg7XG4gICAgcGFkZGluZzogMCAxMnB4IDEycHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ViZWVmNTtcbiAgICBib3gtc2hhZG93OiAwIDJweCAxMnB4IDAgcmdiYSgwLDAsMCwuMSk7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBmb250LWZhbWlseTogJ01vbnRzZXJyYXQnO1xufVxuXG4uc2Nob29sLXBvcG92ZXI6OmJlZm9yZSB7XG4gICAgY29udGVudDogXCJcIjtcbiAgICB6LWluZGV4OiAxMDtcbiAgICB0b3A6IC01cHg7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgaGVpZ2h0OiAyMHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IGluaGVyaXQ7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLnNob3ctc2Nob29sLWRpdiB7XG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnNjaG9vbC10aXRsZSB7XG4gICAgZm9udC1zaXplOiAxNHB4O1xufVxuXG4uc2Nob29sLXRvZ2dsZSB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiA1cHg7XG4gICAgem9vbTogMC43O1xufVxuXG4uY2hrIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5jaGstdGl0bGUge1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmJvdHRvbS1kaXYge1xuICAgIG1hcmdpbjogMjBweCAxNXB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG4ucmVzaWRlbnRpYWxBY3RpdmUge1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYm9yZGVyOiBub25lO1xufVxuXG4uY29tbWVyY2lhbEFjdGl2ZSB7XG4gICAgYmFja2dyb3VuZDogIzNmNTFiNTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBib3JkZXI6IG5vbmU7XG59XG5cbi5yZXNpQ29tbWVyIHtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGZvbnQtZmFtaWx5OiAnTW9udHNlcnJhdCc7XG4gICAgaGVpZ2h0OiAyNXB4O1xuICAgIHBhZGRpbmc6IDBweCAxMHB4O1xuICAgIHBvaW50ZXItZXZlbnRzOiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogMiU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59IiwiLm1hdE9wdGlvbiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDhweCA2cHggOHB4IDBweDtcbiAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xuICBsaW5lLWhlaWdodDogMjVweCAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjYyAhaW1wb3J0YW50O1xufVxuLm1hdE9wdGlvbiAucC1hZGRyZXNzIHtcbiAgZGlzcGxheTogZmxleDtcbiAgbWFyZ2luOiAwcHg7XG4gIGZsZXgtd3JhcDogbm93cmFwO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuLm1hdE9wdGlvbiAucC1hZGRyZXNzIC5hZGRyZXNzLWZvcm1hdCB7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIGNvbG9yOiAjMzMzO1xufVxuLm1hdE9wdGlvbiAucC1hZGRyZXNzIC5zcGFuLXByaWNlIHtcbiAgY29sb3I6ICNmZjViMjk7XG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcbn1cbi5tYXRPcHRpb24gLnAtc2Vjb25kYXJ5IHtcbiAgbWFyZ2luOiAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogbm93cmFwO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuLm1hdE9wdGlvbiAucC1zZWNvbmRhcnkgLnNwYW4tdHlwZSB7XG4gIG1hcmdpbi1sZWZ0OiAwLjVlbTtcbiAgY29sb3I6ICMzMzM7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG4ubWF0T3B0aW9uIC5wLXRlcnRpYXJ5IHtcbiAgbWFyZ2luOiAwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogbm93cmFwO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuLm1hdE9wdGlvbiAucC10ZXJ0aWFyeSAuc3Bhbi1saXN0aW5nIHtcbiAgY29sb3I6ICM5OTk7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cbi5tYXRPcHRpb24gLnAtdGVydGlhcnkgLmV2ZW50IHtcbiAgY29sb3I6ICMzMzM7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi50b29sYmFyLWZvcm1hdCB7XG4gIC0tYmFja2dyb3VuZDogI2UwMWEyNyAhaW1wb3J0YW50O1xuICB6b29tOiAwLjkgIWltcG9ydGFudDtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4udGl0bGUtZm9ybWF0IHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBsZXR0ZXItc3BhY2luZzogM3B4O1xufVxuXG4uZmlsdGVyLWJ0biB7XG4gIHdpZHRoOiA3NXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlvbi1jb250ZW50IHtcbiAgYm9yZGVyLWJvdHRvbTogNnB4IHNvbGlkO1xuICBib3JkZXItdG9wOiAzcHggc29saWQ7XG59XG5cbi5zYWxlTGVhc2VGb3JtYXQge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgcGFkZGluZzogNXB4IDEwcHg7XG59XG5cbi5zYWxlQWN0aXZlIHtcbiAgYmFja2dyb3VuZDogIzFlODhlNTtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuLmxlYXNlQWN0aXZlIHtcbiAgYmFja2dyb3VuZDogIzU1OGIyZjtcbiAgY29sb3I6ICNmZmY7XG4gIGJvcmRlcjogbm9uZTtcbn1cblxuLmluYWN0aXZlLWJ0biB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGNvbG9yOiAjMzMzO1xuICBib3JkZXI6IDFweCBzb2xpZCAjY2NjO1xufVxuXG4uc2FsZUxlYXNlQnV0dG9uIHtcbiAgaGVpZ2h0OiAyNXB4O1xuICBwYWRkaW5nOiAwcHggMTBweDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwb2ludGVyLWV2ZW50czogYXV0bztcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xuICBtYXJnaW4tcmlnaHQ6IDQlO1xufVxuXG4uc2F0ZWxsaXRlRm9ybWF0IHtcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xuICAtLWNvbG9yOiAjMDAwO1xuICBoZWlnaHQ6IDI1cHg7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgcG9pbnRlci1ldmVudHM6IGF1dG87XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtZmFtaWx5OiBcIk1vbnRzZXJyYXRcIjtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuLnNjaG9vbEZvcm1hdCB7XG4gIGhlaWdodDogMjVweDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBwb2ludGVyLWV2ZW50czogYXV0bztcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uc2Nob29sLXBvcG92ZXIge1xuICBsZWZ0OiAxMDBweDtcbiAgdG9wOiA0MnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHBvaW50ZXItZXZlbnRzOiBhdXRvO1xuICB3aWR0aDogMjEwcHg7XG4gIHBhZGRpbmc6IDAgMTJweCAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZWJlZWY1O1xuICBib3gtc2hhZG93OiAwIDJweCAxMnB4IDAgcmdiYSgwLCAwLCAwLCAwLjEpO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCI7XG59XG5cbi5zY2hvb2wtcG9wb3Zlcjo6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgei1pbmRleDogMTA7XG4gIHRvcDogLTVweDtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogaW5oZXJpdDtcbiAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5zaG93LXNjaG9vbC1kaXYge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuLnNjaG9vbC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLnNjaG9vbC10b2dnbGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA1cHg7XG4gIHpvb206IDAuNztcbn1cblxuLmNoayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLmNoay10aXRsZSB7XG4gIG1hcmdpbi1sZWZ0OiAzMHB4O1xuICBmb250LXNpemU6IDE0cHg7XG59XG5cbi5ib3R0b20tZGl2IHtcbiAgbWFyZ2luOiAyMHB4IDE1cHg7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xufVxuXG4ucmVzaWRlbnRpYWxBY3RpdmUge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBjb2xvcjogI2ZmZjtcbiAgYm9yZGVyOiBub25lO1xufVxuXG4uY29tbWVyY2lhbEFjdGl2ZSB7XG4gIGJhY2tncm91bmQ6ICMzZjUxYjU7XG4gIGNvbG9yOiAjZmZmO1xuICBib3JkZXI6IG5vbmU7XG59XG5cbi5yZXNpQ29tbWVyIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xuICBoZWlnaHQ6IDI1cHg7XG4gIHBhZGRpbmc6IDBweCAxMHB4O1xuICBwb2ludGVyLWV2ZW50czogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiAyJTtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/map-view/map-view.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/map-view/map-view.page.ts ***!
  \*************************************************/
/*! exports provided: MapViewPage, PropertyInfoSheet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapViewPage", function() { return MapViewPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropertyInfoSheet", function() { return PropertyInfoSheet; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/esm2015/bottom-sheet.js");
/* harmony import */ var _modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modal/search-filter/search-filter.page */ "./src/app/pages/modal/search-filter/search-filter.page.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var src_app_api_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/api.service */ "./src/app/api.service.ts");
/* harmony import */ var _map_view_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./map-view.service */ "./src/app/pages/map-view/map-view.service.ts");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! leaflet */ "./node_modules/leaflet/dist/leaflet-src.js");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var leaflet_markercluster__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! leaflet.markercluster */ "./node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js");
/* harmony import */ var leaflet_markercluster__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(leaflet_markercluster__WEBPACK_IMPORTED_MODULE_13__);














let MapViewPage = class MapViewPage {
    constructor(navCtrl, modalCtrl, mapViewService, toastCtrl, route, bottomSheet, router, fb, loadingCtrl, geolocation, translateService) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.mapViewService = mapViewService;
        this.toastCtrl = toastCtrl;
        this.route = route;
        this.bottomSheet = bottomSheet;
        this.router = router;
        this.fb = fb;
        this.loadingCtrl = loadingCtrl;
        this.geolocation = geolocation;
        this.translateService = translateService;
        this.properties = [];
        this.schools = [];
        this.currentLat = "";
        this.currentLng = "";
        this.markerCluster = leaflet__WEBPACK_IMPORTED_MODULE_12__["markerClusterGroup"]({
            spiderfyOnMaxZoom: false
        });
        this.searchValues = [];
        this.propertyMarker = leaflet__WEBPACK_IMPORTED_MODULE_12__["icon"]({
            iconUrl: '../../assets/icon/marker-icon.png',
            iconSize: [23, 40],
            iconAnchor: [9, 21],
            popupAnchor: [0, -14]
        });
        this.schoolMarker = leaflet__WEBPACK_IMPORTED_MODULE_12__["icon"]({
            iconUrl: '../../assets/icon/school (1).png',
            iconSize: [25, 30],
            iconAnchor: [9, 21],
            popupAnchor: [0, -14]
        });
        this.saleFlag = true;
        this.propertyTypeFlag = true;
        this.showSchoolPopup = false;
        this.showSchoolFlag = false;
        this.satellite = false;
        this.currentZoomLevel = 15;
        this.radius = "1";
        this.saleLease = 'Sale';
        this.propertyType = 'residential';
    }
    advancedFilterObject() {
        return {
            address: "",
            approxSquareFootageMax: null,
            approxSquareFootageMin: null,
            basement1: "",
            bedRooms: [],
            currentPage: -1,
            daysOnMarket: "",
            listPriceMax: null,
            listPriceMin: null,
            openHouse: "-1",
            parkingSpaces: "",
            propertyOption: "",
            soldDays: "",
            radius: this.radius,
            saleLease: this.saleLease,
            propertyType: this.propertyType,
            washrooms: "",
            latitude: this.currentLat,
            longitude: this.currentLng,
            withSold: false
        };
    }
    ngOnInit() {
        if (window.history.state.filterData) {
            this.filterObject = JSON.parse(window.history.state.filterData);
            this.currentLat = this.filterObject.latitude;
            this.currentLng = this.filterObject.longitude;
            this.filterObject.radius = this.radius;
            this.saleLease = this.filterObject.saleLease;
            this.propertyType = this.filterObject.propertyType;
            if (this.propertyType == 'commercial') {
                this.propertyTypeFlag = false;
            }
            if (this.saleLease == 'Lease') {
                this.saleFlag = false;
            }
            this.findAll();
        }
        else {
            this.geolocation.getCurrentPosition().then((resp) => {
                this.currentLat = (resp.coords.latitude).toString();
                this.currentLng = (resp.coords.longitude).toString();
                this.filterObject.latitude = this.currentLat;
                this.filterObject.longitude = this.currentLng;
                setTimeout(() => {
                    this.findAll();
                }, 500);
            }, err => {
                this.currentLat = "43.63943345";
                this.currentLng = "-79.39972759226308";
                this.filterObject.latitude = this.currentLat;
                this.filterObject.longitude = this.currentLng;
                setTimeout(() => {
                    this.findAll();
                }, 500);
            });
        }
        this.filterObject = this.advancedFilterObject();
        this.searchForm = this.fb.group({
            myControl: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]()
        });
        this.map = leaflet__WEBPACK_IMPORTED_MODULE_12__["map"]('myMap', {
            zoomControl: true
        });
        this.map.zoomControl.setPosition('bottomright');
        leaflet__WEBPACK_IMPORTED_MODULE_12__["DomUtil"].setOpacity(this.map.zoomControl.getContainer(), 0.8);
        this.prevZoom = this.map.getZoom();
        this.map.on('dragend', e => {
            var self = this;
            var latLng = e.target.getCenter();
            self.currentLat = (latLng.lat).toString();
            self.currentLng = (latLng.lng).toString();
            this.filterObject.latitude = this.currentLat;
            this.filterObject.longitude = this.currentLng;
            this.findAll();
        });
        this.map.on('zoomend', e => {
            var self = this;
            var latLng = e.target.getCenter();
            self.currentLat = (latLng.lat).toString();
            self.currentLng = (latLng.lng).toString();
            this.filterObject.latitude = this.currentLat;
            this.filterObject.longitude = this.currentLng;
            self.moreProperties();
        });
        this.map.setMinZoom(2);
    }
    searchProperty() {
        var key = this.searchForm.get("myControl").value;
        if (key && key.length > 2) {
            this.isLoading = true;
            var searchData = {
                currentPage: 1,
                searchText: key
            };
            this.mapViewService.searchProperties(searchData).subscribe(data => {
                this.isLoading = false;
                if (data.data != undefined) {
                    if (data.data && data.data.length > 0) {
                        var add = { locationSearch: data.data[0].locationSearch, data: data.data[0] };
                        this.searchValues = [
                            {
                                name: 'Locations',
                                data: [add]
                            },
                            {
                                name: 'Listing',
                                data: data.data
                            }
                        ];
                    }
                    else {
                        this.searchValues = [];
                    }
                }
            }, err => {
                this.isLoading = false;
            });
        }
        else {
            this.searchValues = [];
        }
    }
    clearAddress() {
        this.filterObject = this.advancedFilterObject();
        this.findAll();
    }
    findAll() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.properties = [];
            var loading;
            this.translateService.get('loading').subscribe((value) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                loading = yield this.loadingCtrl.create({
                    message: value,
                });
                yield loading.present();
            }));
            this.mapViewService.filterProperties(this.filterObject).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield loading.dismiss();
                data.data.forEach(element => {
                    if (element.soldPrice === 0) {
                        var listing = moment__WEBPACK_IMPORTED_MODULE_11__(element.listingEntryDate).format("DD/MM/YYYY");
                        var startDate = moment__WEBPACK_IMPORTED_MODULE_11__(listing, "DD/MM/YYYY");
                        var currenDate = moment__WEBPACK_IMPORTED_MODULE_11__(new Date()).format("DD/MM/YYYY");
                        var endDate = moment__WEBPACK_IMPORTED_MODULE_11__(currenDate, "DD/MM/YYYY");
                        element['difference_In_ListingDays'] = endDate.diff(startDate, 'days');
                    }
                    else {
                        var listing = moment__WEBPACK_IMPORTED_MODULE_11__(element.soldDate).format("DD/MM/YYYY");
                        var startDate = moment__WEBPACK_IMPORTED_MODULE_11__(listing, "DD/MM/YYYY");
                        var currenDate = moment__WEBPACK_IMPORTED_MODULE_11__(new Date()).format("DD/MM/YYYY");
                        var endDate = moment__WEBPACK_IMPORTED_MODULE_11__(currenDate, "DD/MM/YYYY");
                        element['difference_In_SoldDays'] = endDate.diff(startDate, 'days');
                    }
                });
                this.properties = data.data;
                if (data.data.length === 0) {
                    this.presentToast(data.message);
                }
                this.markerCluster.clearLayers();
                this.map.setView([this.currentLat, this.currentLng], this.currentZoomLevel);
                this.showMap();
            }), err => {
                loading.dismiss();
            });
        });
    }
    presentToast(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                showCloseButton: true,
                message: message,
                duration: 2000,
                position: 'bottom'
            });
            toast.present();
        });
    }
    showMap() {
        if (this.satellite) {
            leaflet__WEBPACK_IMPORTED_MODULE_12__["tileLayer"]('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}?', {
                maxZoom: 18,
                subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
            }).addTo(this.map);
        }
        else {
            leaflet__WEBPACK_IMPORTED_MODULE_12__["tileLayer"]('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoicG9zaGxpZmUiLCJhIjoiY2tkczdwNThsMDV0NzJzcGNmZmtkcGNqdyJ9.YnjXTw7u8tB3TVz-OiWniA'
            }).addTo(this.map);
        }
        this.addClusters();
    }
    addClusters() {
        for (var i = 0; i < this.properties.length; ++i) {
            var m = leaflet__WEBPACK_IMPORTED_MODULE_12__["marker"]([this.properties[i].latitude, this.properties[i].longitude], { icon: this.propertyMarker }).on('click', this.openBottomSheet.bind(this, [this.properties[i]]));
            m.myData = { property: this.properties[i] };
            this.markerCluster.addLayer(m);
        }
        this.markerCluster.on('clusterclick', a => {
            if (a.layer._zoom == 18) {
                var data;
                var tempArray = [];
                var markers = a.layer._markers;
                for (var i = 0; i < markers.length; i++) {
                    data = markers[i].myData.property;
                    tempArray.push(data);
                }
                this.openBottomSheet(tempArray, 'e');
            }
        });
        this.map.addLayer(this.markerCluster);
    }
    selectLocation(value) {
        this.currentLat = value.latitude;
        this.currentLng = value.longitude;
        this.filterObject.latitude = this.currentLat;
        this.filterObject.longitude = this.currentLng;
        this.getSearchAddressProperties();
    }
    getSearchAddressProperties() {
        this.properties = [];
        var loading;
        this.translateService.get('loading').subscribe((value) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            loading = yield this.loadingCtrl.create({
                message: value
            });
            yield loading.present();
        }));
        this.mapViewService.filterProperties(this.filterObject).subscribe((data) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            yield loading.dismiss();
            data.data.forEach(element => {
                if (element.soldPrice === 0) {
                    var listing = moment__WEBPACK_IMPORTED_MODULE_11__(element.listingEntryDate).format("DD/MM/YYYY");
                    var startDate = moment__WEBPACK_IMPORTED_MODULE_11__(listing, "DD/MM/YYYY");
                    var currenDate = moment__WEBPACK_IMPORTED_MODULE_11__(new Date()).format("DD/MM/YYYY");
                    var endDate = moment__WEBPACK_IMPORTED_MODULE_11__(currenDate, "DD/MM/YYYY");
                    element['difference_In_ListingDays'] = endDate.diff(startDate, 'days');
                }
                else {
                    var listing = moment__WEBPACK_IMPORTED_MODULE_11__(element.soldDate).format("DD/MM/YYYY");
                    var startDate = moment__WEBPACK_IMPORTED_MODULE_11__(listing, "DD/MM/YYYY");
                    var currenDate = moment__WEBPACK_IMPORTED_MODULE_11__(new Date()).format("DD/MM/YYYY");
                    var endDate = moment__WEBPACK_IMPORTED_MODULE_11__(currenDate, "DD/MM/YYYY");
                    element['difference_In_SoldDays'] = endDate.diff(startDate, 'days');
                }
            });
            this.properties = data.data;
            this.markerCluster.clearLayers();
            if (this.properties.length > 0) {
                this.map.setView([this.properties[0].latitude, this.properties[0].longitude], this.currentZoomLevel);
                this.currentLat = this.properties[0].latitude;
                this.currentLng = this.properties[0].longitude;
                this.filterObject.latitude = this.currentLat;
                this.filterObject.longitude = this.currentLng;
            }
            else {
                this.map.setView([this.currentLat, this.currentLng], this.currentZoomLevel);
            }
            this.showMap();
        }), err => {
            loading.dismiss();
        });
    }
    satelliteView() {
        this.satellite = !this.satellite;
        this.findAll();
    }
    moreProperties() {
        var currZoom = this.map.getZoom();
        this.currentZoomLevel = this.map.getZoom();
        switch (this.currentZoomLevel) {
            case 15:
                this.radius = "1";
                break;
            case 14:
                this.radius = "1";
                break;
            case 13:
                this.radius = "3";
                break;
            case 12:
                this.radius = "5";
                break;
            case 11:
                this.radius = "5";
                break;
            case 10:
                this.radius = "8";
                break;
            case 9:
                this.radius = "8";
                break;
            case 8:
                this.radius = "10";
                break;
            case 7:
                this.radius = "10";
                break;
            case 6:
                this.radius = "20";
                break;
            case 5:
                this.radius = "30";
                break;
            case 4:
                this.radius = "40";
                break;
            case 3:
                this.radius = "50";
                break;
            case 2:
                this.radius = "60";
                break;
            default:
                this.radius = "1";
                break;
        }
        this.filterObject.radius = this.radius;
        if (this.showSchoolFlag) {
            if (currZoom > 13) {
                this.showSchoolsOnMap();
            }
            else {
                this.map.eachLayer((layer) => {
                    layer.remove();
                });
                this.findAll();
            }
        }
        else {
            this.findAll();
        }
    }
    saleLeaseChange(value) {
        if (value == 'Lease') {
            this.saleFlag = false;
            this.saleLease = 'Lease';
            this.filterObject.saleLease = 'Lease';
        }
        else {
            this.saleFlag = true;
            this.saleLease = 'Sale';
            this.filterObject.saleLease = 'Sale';
        }
        this.findAll();
    }
    propertyTypeChange(value) {
        if (value == 'commercial') {
            this.propertyTypeFlag = false;
            this.propertyType = 'commercial';
            this.filterObject.propertyType = 'commercial';
        }
        else {
            this.propertyTypeFlag = true;
            this.propertyType = 'residential';
            this.filterObject.propertyType = 'residential';
        }
        this.findAll();
    }
    searchFilter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalCtrl.create({
                component: _modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_7__["SearchFilterPage"],
                componentProps: {
                    setFilterObject: this.filterObject
                }
            });
            modal.onDidDismiss().then((dataReturned) => {
                if (dataReturned !== null) {
                    if (dataReturned.data != null) {
                        this.filterObject = dataReturned.data;
                        if (dataReturned.data.saleLease === 'Sale') {
                            this.saleFlag = true;
                        }
                        else {
                            this.saleFlag = false;
                        }
                        if (dataReturned.data.propertyType === 'residential') {
                            this.propertyTypeFlag = true;
                        }
                        else {
                            this.propertyTypeFlag = false;
                        }
                        this.filterObject.currentPage = -1;
                        this.filterObject.radius = this.radius;
                        this.findAll();
                    }
                }
            });
            return yield modal.present();
        });
    }
    openBottomSheet(e, data) {
        this.bottomSheet.open(PropertyInfoSheet, { data: e });
    }
    schoolPopup() {
        this.showSchoolPopup = !this.showSchoolPopup;
    }
    showSchool(value) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadingCtrl.create({
                message: "Please wait...",
            });
            if (value) {
                let data = {
                    latitude: this.currentLat,
                    longitude: this.currentLng,
                    radious: 1500
                };
                this.mapViewService.showSchools(data).subscribe((res) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                    yield loading.dismiss();
                    if (res.code == 200) {
                        this.schools = res.data.results;
                        this.showSchoolsOnMap();
                    }
                }), err => {
                    loading.dismiss();
                });
            }
            else {
                loading.dismiss();
                this.map.eachLayer((layer) => {
                    layer.remove();
                });
                this.findAll();
            }
        });
    }
    showSchoolsOnMap() {
        var marker;
        if (this.schools.length > 0) {
            for (var i = 0; i < this.schools.length; ++i) {
                marker = leaflet__WEBPACK_IMPORTED_MODULE_12__["marker"]([this.schools[i].geometry.location.lat, this.schools[i].geometry.location.lng], { icon: this.schoolMarker });
                marker.bindPopup("<div class='row popup-width'>" +
                    "<b style='font-size:14px'>" + this.schools[i].name + "</b><br>" +
                    "<div style='margin-top:5px'>" + this.schools[i].formatted_address + "</div>" +
                    "</div>").addTo(this.map);
            }
        }
    }
    goToDetailPage(id) {
        setTimeout(() => {
            this.router.navigate(['property-detail/' + id]);
        }, 500);
    }
};
MapViewPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _map_view_service__WEBPACK_IMPORTED_MODULE_10__["MapViewService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__["MatBottomSheet"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"] },
    { type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"] }
];
MapViewPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-map-view',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./map-view.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/map-view/map-view.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./map-view.page.scss */ "./src/app/pages/map-view/map-view.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _map_view_service__WEBPACK_IMPORTED_MODULE_10__["MapViewService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__["MatBottomSheet"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"],
        _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateService"]])
], MapViewPage);

let PropertyInfoSheet = class PropertyInfoSheet {
    constructor(data, router, platform, apiService, _bottomSheetRef) {
        this.data = data;
        this.router = router;
        this.platform = platform;
        this.apiService = apiService;
        this._bottomSheetRef = _bottomSheetRef;
        this.properties = [];
        this.loginFlag = true;
        this.properties = data;
        this._bottomSheetRef.disableClose = false;
        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
        if (loginDetails != undefined) {
            this.loginFlag = false;
        }
        this.properties.forEach(element => {
            if (element.saleLease == 'Lease') {
                this.loginFlag = false;
            }
        });
        this.platform.backButton.subscribeWithPriority(10, () => {
            this._bottomSheetRef.dismiss();
        });
        this.imageUrl = this.apiService.config + 'property/map-image/';
    }
    openLink(event, id) {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
        this.router.navigateByUrl('/property-detail/' + id);
    }
    closePopup(event) {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
    }
    login() {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
        this.router.navigateByUrl('/login');
    }
};
PropertyInfoSheet.ctorParameters = () => [
    { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__["MAT_BOTTOM_SHEET_DATA"],] }] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: src_app_api_service__WEBPACK_IMPORTED_MODULE_9__["ApiConfigService"] },
    { type: _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__["MatBottomSheetRef"] }
];
PropertyInfoSheet = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'property-info-sheet',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./property-info-sheet.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/map-view/property-info-sheet.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./property-info-sheet.scss */ "./src/app/pages/map-view/property-info-sheet.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__["MAT_BOTTOM_SHEET_DATA"])),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], src_app_api_service__WEBPACK_IMPORTED_MODULE_9__["ApiConfigService"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_6__["MatBottomSheetRef"]])
], PropertyInfoSheet);



/***/ }),

/***/ "./src/app/pages/map-view/map-view.service.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/map-view/map-view.service.ts ***!
  \****************************************************/
/*! exports provided: MapViewService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapViewService", function() { return MapViewService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm2015/http.js");
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../api.service */ "./src/app/api.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");





let MapViewService = class MapViewService {
    constructor(http, apiService) {
        this.http = http;
        this.apiService = apiService;
        const headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
    }
    filterProperties(data) {
        return this.http.post(this.apiService.config + 'property/mapSearch', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
    searchProperties(data) {
        return this.http.post(this.apiService.config + 'property/search', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
    showSchools(data) {
        return this.http.post(this.apiService.config + 'homepage/schools', data, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(response => response.json()));
    }
};
MapViewService.ctorParameters = () => [
    { type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"] },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"] }
];
MapViewService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])
], MapViewService);



/***/ }),

/***/ "./src/app/pages/map-view/property-info-sheet.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/map-view/property-info-sheet.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: linear-gradient(-135deg, #f6f6f6, var(--ion-color-pmary));\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host ion-card {\n  border-radius: 0;\n}\n.header-info {\n  position: relative;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  min-height: 35px;\n  border-bottom: 1px solid #d8d8d8;\n  padding: 5px 40px;\n  line-height: 18px;\n  text-align: center;\n  background-color: #f6f6f6;\n}\n.total-listing {\n  font-size: 16px;\n  color: #333;\n}\n.close-icon {\n  position: absolute;\n  right: 5px;\n  top: 0;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: 24px;\n  height: 100%;\n  font-size: 20px;\n  cursor: pointer;\n}\n.mg-bt {\n  box-shadow: 0 3px 4px rgba(var(--ion-color-dark-rgb), 0.24);\n}\n.saleClass {\n  background: #1e88e5;\n  color: #fff;\n  font-weight: 500;\n}\n.soldClass {\n  background: #ff9e26;\n  color: #fff;\n  font-weight: 500;\n}\n.leaseClass {\n  background: #558b2f;\n  color: #fff;\n  font-weight: 500;\n}\n.footer-format {\n  margin: 0px 10px;\n}\n.heart-format {\n  margin-top: 2%;\n  text-align: center;\n}\n.col-left-border {\n  border-left: 1px solid #d7d7d7;\n}\n.font-s12 {\n  font-size: 12px;\n  color: #424242;\n}\n.pd-0 {\n  padding: 2px 5px 2px 5px !important;\n}\n.toolbar-border {\n  border-top: 1px solid #ccc;\n}\n.fl-rt {\n  float: right;\n}\n.tumbnail-view {\n  height: 80px;\n  width: 80px;\n}\n.tumbnail-view img {\n  border-radius: 5px;\n}\n.price-format {\n  font-size: 16px;\n  margin: 5px 0px 0px;\n}\n.priceText {\n  color: #d32f2f;\n}\n.mt-0 {\n  margin-top: 0px;\n}\n.bg-gray {\n  background: #e0e0e0;\n}\n.login-btn {\n  position: absolute;\n  z-index: 999;\n  text-transform: capitalize;\n  top: 45%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  --border-radius: 0px !important;\n}\n.pos {\n  position: relative;\n  max-height: 47vh;\n  overflow-y: auto;\n}\n.login-required {\n  -webkit-filter: blur(5px);\n          filter: blur(5px);\n  pointer-events: none;\n}\n.popup-listing {\n  width: 100%;\n  cursor: pointer;\n  display: flex;\n  flex-wrap: wrap;\n  align-items: flex-start;\n  padding: 10px;\n  border-bottom: 1px solid #ccc;\n  background-color: #fff;\n}\n.header {\n  display: flex;\n  align-items: center;\n  padding: 0 4px;\n  width: 100%;\n}\n.photo {\n  flex-shrink: 0;\n  display: flex;\n  flex-direction: column;\n  justify-content: space-between;\n  width: 112px;\n  min-height: 112px;\n  margin: 4px 0 0 4px;\n}\n.photo-container {\n  width: 112px;\n  height: 84px;\n  background-size: cover;\n  background-position: 50%;\n  background-repeat: no-repeat;\n}\n.main {\n  display: flex;\n  flex-direction: column;\n  margin: 4px 4px 0 8px;\n  width: calc(100% - 128px);\n  min-height: 112px;\n  font-size: 15px;\n}\n.p-type {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #009cff;\n  padding: 1px 4px;\n  font-size: 15px;\n  background-color: #e1f3ff;\n  color: #009cff;\n  font-weight: 400;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n.prop-image {\n  width: 112px;\n  height: 100px;\n  background-size: cover;\n  background-position: 50%;\n  background-repeat: no-repeat;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvbWFwLXZpZXcvcHJvcGVydHktaW5mby1zaGVldC5zY3NzIiwic3JjL2FwcC9wYWdlcy9tYXAtdmlldy9wcm9wZXJ0eS1pbmZvLXNoZWV0LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0k7RUFDSSx1RUFBQTtBQ0FSO0FESUk7RUFDSSxnQkFBQTtFQUNBLGlEQUFBO0FDRlI7QURLSTtFQUNJLGdCQUFBO0FDSFI7QURPQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdDQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUNKSjtBRE9BO0VBQ0ksZUFBQTtFQUNBLFdBQUE7QUNKSjtBRE9BO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsTUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQ0pKO0FET0E7RUFDSSwyREFBQTtBQ0pKO0FET0E7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0pKO0FET0E7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0pKO0FET0E7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ0pKO0FET0E7RUFDSSxnQkFBQTtBQ0pKO0FET0E7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QUNKSjtBRE9BO0VBQ0ksOEJBQUE7QUNKSjtBRE9BO0VBQ0ksZUFBQTtFQUNBLGNBQUE7QUNKSjtBRE9BO0VBQ0ksbUNBQUE7QUNKSjtBRE9BO0VBQ0ksMEJBQUE7QUNKSjtBRE9BO0VBQ0ksWUFBQTtBQ0pKO0FET0E7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQ0pKO0FETUk7RUFDSSxrQkFBQTtBQ0pSO0FEUUE7RUFDSSxlQUFBO0VBQ0EsbUJBQUE7QUNMSjtBRFFBO0VBQ0ksY0FBQTtBQ0xKO0FEUUE7RUFDSSxlQUFBO0FDTEo7QURRQTtFQUNJLG1CQUFBO0FDTEo7QURRQTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLDBCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLCtCQUFBO0FDTEo7QURRQTtFQUNJLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQ0xKO0FEUUE7RUFDSSx5QkFBQTtVQUFBLGlCQUFBO0VBQ0Esb0JBQUE7QUNMSjtBRFFBO0VBQ0ksV0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0EsYUFBQTtFQUNBLDZCQUFBO0VBQ0Esc0JBQUE7QUNMSjtBRFFBO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUNMSjtBRFFBO0VBQ0ksY0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNMSjtBRFFBO0VBQ0ksWUFBQTtFQUNBLFlBQUE7RUFDQSxzQkFBQTtFQUNBLHdCQUFBO0VBQ0EsNEJBQUE7QUNMSjtBRFFBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0xKO0FEUUE7RUFDSSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNMSjtBRFFBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHdCQUFBO0VBQ0EsNEJBQUE7QUNMSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21hcC12aWV3L3Byb3BlcnR5LWluZm8tc2hlZXQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsICNmNmY2ZjYsIHZhcigtLWlvbi1jb2xvci1wbWFyeSkpO1xuICAgICAgICAvLyAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICAgIH1cblxuICAgIGlvbi1jYXJkIHtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcbiAgICB9XG59XG5cbi5oZWFkZXItaW5mbyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBtaW4taGVpZ2h0OiAzNXB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZDhkOGQ4O1xuICAgIHBhZGRpbmc6IDVweCA0MHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjZmNmY2O1xufVxuXG4udG90YWwtbGlzdGluZyB7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGNvbG9yOiAjMzMzO1xufVxuXG4uY2xvc2UtaWNvbiB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiA1cHg7XG4gICAgdG9wOiAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB3aWR0aDogMjRweDtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLm1nLWJ0IHtcbiAgICBib3gtc2hhZG93OiAwIDNweCA0cHggcmdiYSh2YXIoLS1pb24tY29sb3ItZGFyay1yZ2IpLCAwLjI0KTtcbn1cblxuLnNhbGVDbGFzcyB7XG4gICAgYmFja2dyb3VuZDogIzFlODhlNTtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uc29sZENsYXNzIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmY5ZTI2O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5sZWFzZUNsYXNzIHtcbiAgICBiYWNrZ3JvdW5kOiAjNTU4YjJmO1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5mb290ZXItZm9ybWF0IHtcbiAgICBtYXJnaW46IDBweCAxMHB4O1xufVxuXG4uaGVhcnQtZm9ybWF0IHtcbiAgICBtYXJnaW4tdG9wOiAyJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jb2wtbGVmdC1ib3JkZXIge1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2Q3ZDdkNztcbn1cblxuLmZvbnQtczEyIHtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY29sb3I6ICM0MjQyNDI7XG59XG5cbi5wZC0wIHtcbiAgICBwYWRkaW5nOiAycHggNXB4IDJweCA1cHggIWltcG9ydGFudDtcbn1cblxuLnRvb2xiYXItYm9yZGVyIHtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2NjYztcbn1cblxuLmZsLXJ0IHtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbi50dW1ibmFpbC12aWV3IHtcbiAgICBoZWlnaHQ6IDgwcHg7XG4gICAgd2lkdGg6IDgwcHg7XG5cbiAgICBpbWcge1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgfVxufVxuXG4ucHJpY2UtZm9ybWF0IHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbWFyZ2luOiA1cHggMHB4IDBweDtcbn1cblxuLnByaWNlVGV4dCB7XG4gICAgY29sb3I6ICNkMzJmMmY7XG59XG5cbi5tdC0wIHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG59XG5cbi5iZy1ncmF5IHtcbiAgICBiYWNrZ3JvdW5kOiAjZTBlMGUwO1xufVxuXG4ubG9naW4tYnRuIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogOTk5O1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIHRvcDogNDUlO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xufVxuXG4ucG9zIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbWF4LWhlaWdodDogNDd2aDtcbiAgICBvdmVyZmxvdy15OiBhdXRvO1xufVxuXG4ubG9naW4tcmVxdWlyZWQge1xuICAgIGZpbHRlcjogYmx1cig1cHgpO1xuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG4ucG9wdXAtbGlzdGluZyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC13cmFwOiB3cmFwO1xuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLmhlYWRlciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIHBhZGRpbmc6IDAgNHB4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4ucGhvdG8ge1xuICAgIGZsZXgtc2hyaW5rOiAwO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgd2lkdGg6IDExMnB4O1xuICAgIG1pbi1oZWlnaHQ6IDExMnB4O1xuICAgIG1hcmdpbjogNHB4IDAgMCA0cHg7XG59XG5cbi5waG90by1jb250YWluZXIge1xuICAgIHdpZHRoOiAxMTJweDtcbiAgICBoZWlnaHQ6IDg0cHg7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCU7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuLm1haW4ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBtYXJnaW46IDRweCA0cHggMCA4cHg7XG4gICAgd2lkdGg6IGNhbGMoMTAwJSAtIDEyOHB4KTtcbiAgICBtaW4taGVpZ2h0OiAxMTJweDtcbiAgICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5wLXR5cGUge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDA5Y2ZmO1xuICAgIHBhZGRpbmc6IDFweCA0cHg7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlMWYzZmY7XG4gICAgY29sb3I6ICMwMDljZmY7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMHB4O1xufVxuXG4ucHJvcC1pbWFnZSB7XG4gICAgd2lkdGg6IDExMnB4O1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCU7XG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn0iLCI6aG9zdCBpb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsICNmNmY2ZjYsIHZhcigtLWlvbi1jb2xvci1wbWFyeSkpO1xufVxuOmhvc3QgaW9uLWl0ZW0ge1xuICBib3JkZXItcmFkaXVzOiAwO1xuICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xufVxuOmhvc3QgaW9uLWNhcmQge1xuICBib3JkZXItcmFkaXVzOiAwO1xufVxuXG4uaGVhZGVyLWluZm8ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtaW4taGVpZ2h0OiAzNXB4O1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2Q4ZDhkODtcbiAgcGFkZGluZzogNXB4IDQwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNmY2ZjY7XG59XG5cbi50b3RhbC1saXN0aW5nIHtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzMzMztcbn1cblxuLmNsb3NlLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiA1cHg7XG4gIHRvcDogMDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIHdpZHRoOiAyNHB4O1xuICBoZWlnaHQ6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubWctYnQge1xuICBib3gtc2hhZG93OiAwIDNweCA0cHggcmdiYSh2YXIoLS1pb24tY29sb3ItZGFyay1yZ2IpLCAwLjI0KTtcbn1cblxuLnNhbGVDbGFzcyB7XG4gIGJhY2tncm91bmQ6ICMxZTg4ZTU7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuXG4uc29sZENsYXNzIHtcbiAgYmFja2dyb3VuZDogI2ZmOWUyNjtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5sZWFzZUNsYXNzIHtcbiAgYmFja2dyb3VuZDogIzU1OGIyZjtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG5cbi5mb290ZXItZm9ybWF0IHtcbiAgbWFyZ2luOiAwcHggMTBweDtcbn1cblxuLmhlYXJ0LWZvcm1hdCB7XG4gIG1hcmdpbi10b3A6IDIlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jb2wtbGVmdC1ib3JkZXIge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNkN2Q3ZDc7XG59XG5cbi5mb250LXMxMiB7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgY29sb3I6ICM0MjQyNDI7XG59XG5cbi5wZC0wIHtcbiAgcGFkZGluZzogMnB4IDVweCAycHggNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi50b29sYmFyLWJvcmRlciB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xufVxuXG4uZmwtcnQge1xuICBmbG9hdDogcmlnaHQ7XG59XG5cbi50dW1ibmFpbC12aWV3IHtcbiAgaGVpZ2h0OiA4MHB4O1xuICB3aWR0aDogODBweDtcbn1cbi50dW1ibmFpbC12aWV3IGltZyB7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cblxuLnByaWNlLWZvcm1hdCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luOiA1cHggMHB4IDBweDtcbn1cblxuLnByaWNlVGV4dCB7XG4gIGNvbG9yOiAjZDMyZjJmO1xufVxuXG4ubXQtMCB7XG4gIG1hcmdpbi10b3A6IDBweDtcbn1cblxuLmJnLWdyYXkge1xuICBiYWNrZ3JvdW5kOiAjZTBlMGUwO1xufVxuXG4ubG9naW4tYnRuIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTk7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICB0b3A6IDQ1JTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgLS1ib3JkZXItcmFkaXVzOiAwcHggIWltcG9ydGFudDtcbn1cblxuLnBvcyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWF4LWhlaWdodDogNDd2aDtcbiAgb3ZlcmZsb3cteTogYXV0bztcbn1cblxuLmxvZ2luLXJlcXVpcmVkIHtcbiAgZmlsdGVyOiBibHVyKDVweCk7XG4gIHBvaW50ZXItZXZlbnRzOiBub25lO1xufVxuXG4ucG9wdXAtbGlzdGluZyB7XG4gIHdpZHRoOiAxMDAlO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtd3JhcDogd3JhcDtcbiAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjY2NjO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xufVxuXG4uaGVhZGVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgcGFkZGluZzogMCA0cHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ucGhvdG8ge1xuICBmbGV4LXNocmluazogMDtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xuICB3aWR0aDogMTEycHg7XG4gIG1pbi1oZWlnaHQ6IDExMnB4O1xuICBtYXJnaW46IDRweCAwIDAgNHB4O1xufVxuXG4ucGhvdG8tY29udGFpbmVyIHtcbiAgd2lkdGg6IDExMnB4O1xuICBoZWlnaHQ6IDg0cHg7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDUwJTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuLm1haW4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICBtYXJnaW46IDRweCA0cHggMCA4cHg7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAxMjhweCk7XG4gIG1pbi1oZWlnaHQ6IDExMnB4O1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5wLXR5cGUge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAwOWNmZjtcbiAgcGFkZGluZzogMXB4IDRweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTFmM2ZmO1xuICBjb2xvcjogIzAwOWNmZjtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG59XG5cbi5wcm9wLWltYWdlIHtcbiAgd2lkdGg6IDExMnB4O1xuICBoZWlnaHQ6IDEwMHB4O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA1MCU7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59Il19 */");

/***/ })

}]);
//# sourceMappingURL=pages-map-view-map-view-module-es2015.js.map