function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-favorites-favorites-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/favorites/favorites.page.html":
  /*!*******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/favorites/favorites.page.html ***!
    \*******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesFavoritesFavoritesPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n\t<ion-toolbar color=\"pmary\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button text=\"\" color=\"dark\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title class=\"fw400\">\n\t\t\t{{ 'app.watchedProperties' | translate }}\n\t\t</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content>\n\t<ion-card *ngIf=\"favorites?.length === 0\" color=\"danger\" class=\"ion-margin-top\">\n\t\t<ion-card-content>\n\t\t\t<p class=\"ion-text-center text-white\">{{ 'watched.noWatchedProperties' | translate }}.</p>\n\t\t</ion-card-content>\n\t</ion-card>\n\n\t<ion-card *ngIf=\"loginFlag\" color=\"danger\" class=\"ion-margin-top\">\n\t\t<ion-card-content>\n\t\t\t<p class=\"ion-text-center text-white\">{{ 'watched.loginRequiredForWatched' | translate }}.</p>\n\t\t</ion-card-content>\n\t</ion-card>\n\n\t<ion-list class=\"ion-no-padding\" *ngIf=\"favorites?.length > 0\" [@staggerIn]=\"favorites\">\n\t\t<ion-item-sliding *ngFor=\"let favorite of favorites\" #slidingList>\n\n\t\t\t<ion-item class=\"bg-white\" lines=\"none\" tappable (click)=\"itemTapped(favorite)\">\n\t\t\t\t<ion-thumbnail class=\"tumbnail-view\" slot=\"start\">\n\t\t\t\t\t<span *ngIf=\"favorite.propertyImage\">\n\t\t\t\t\t\t<img src=\"{{favorite.propertyImage}}\">\n\t\t\t\t\t</span>\n\t\t\t\t</ion-thumbnail>\n\n\t\t\t\t<ion-label>\n\n\t\t\t\t\t<ion-badge class=\"fl-rt leaseClass\" slot=\"end\"\n\t\t\t\t\t\t[ngClass]=\"{'direction-ltr' : !isArabic, 'direction-rtl' : isArabic}\"\n\t\t\t\t\t\t*ngIf=\"favorite.soldPrice === 0 && favorite.saleLease === 'Lease'\">\n\t\t\t\t\t\t<ion-text color=\"light\">{{ favorite.saleLease }}</ion-text>\n\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t<ion-badge class=\"fl-rt saleClass\" slot=\"end\"\n\t\t\t\t\t\t[ngClass]=\"{'direction-ltr' : !isArabic, 'direction-rtl' : isArabic}\"\n\t\t\t\t\t\t*ngIf=\"favorite.soldPrice === 0 && favorite.saleLease === 'Sale'\">\n\t\t\t\t\t\t<ion-text color=\"light\">{{ favorite.saleLease }}</ion-text>\n\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t<ion-badge class=\"fl-rt soldClass\" slot=\"end\" *ngIf=\"favorite.soldPrice !== 0\"\n\t\t\t\t\t\t[ngClass]=\"{'direction-ltr' : !isArabic, 'direction-rtl' : isArabic}\">\n\t\t\t\t\t\t<ion-text color=\"light\">{{ 'property.sold' | translate }}</ion-text>\n\t\t\t\t\t</ion-badge>\n\n\t\t\t\t\t<h2>\n\t\t\t\t\t\t<ion-text color=\"dark1\">{{ favorite.address }}</ion-text>\n\t\t\t\t\t</h2>\n\n\t\t\t\t\t<p class=\"text-12x\">\n\t\t\t\t\t\t<ion-text color=\"dark\">{{ favorite.area }}</ion-text>\n\t\t\t\t\t</p>\n\n\t\t\t\t\t<p *ngIf=\"favorite.soldPrice === 0\" class=\"price-format\">\n\t\t\t\t\t\t{{ 'property.listed' | translate }}:\n\t\t\t\t\t\t<ion-text class=\"priceText\" *ngIf=\"favorite.saleLease == 'Sale'\">\n\t\t\t\t\t\t\t${{ favorite.listPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t\t<ion-text class=\"priceText\" *ngIf=\"favorite.saleLease == 'Lease'\">\n\t\t\t\t\t\t\t${{ favorite.listPrice | number:'1.0':'en-US' }} /month\n\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t</p>\n\n\t\t\t\t\t<p *ngIf=\"favorite.soldPrice !== 0\" class=\"price-format\">\n\t\t\t\t\t\t{{ 'property.soldFor' | translate }}:\n\t\t\t\t\t\t<ion-text class=\"priceText\">\n\t\t\t\t\t\t\t${{ favorite.soldPrice | number:'1.0':'en-US' }}\n\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t</p>\n\n\t\t\t\t\t<p class=\"p-type\">\n\t\t\t\t\t\t{{  favorite.type }}\n\t\t\t\t\t</p>\n\n\t\t\t\t\t<p *ngIf=\"favorite.soldPrice === 0\">\n\t\t\t\t\t\t{{ 'property.listedIn' | translate }}:\n\t\t\t\t\t\t<ion-text>\n\t\t\t\t\t\t\t{{ favorite.listingEntryDate | date }}\n\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t</p>\n\n\t\t\t\t\t<p *ngIf=\"favorite.soldPrice !== 0\">\n\t\t\t\t\t\t{{ 'property.soldDate' | translate }}:\n\t\t\t\t\t\t<ion-text>\n\t\t\t\t\t\t\t{{ favorite.soldDate | date }}\n\t\t\t\t\t\t</ion-text>\n\t\t\t\t\t</p>\n\n\t\t\t\t</ion-label>\n\t\t\t</ion-item>\n\n\t\t\t<ion-footer class=\"footer-format\">\n\t\t\t\t<ion-row class=\"font-s12\">\n\t\t\t\t\t<ion-col class=\"pd-0\" size=\"3\">{{ 'property.MLS' | translate }}</ion-col>\n\t\t\t\t\t<ion-col size=\"3\" class=\"col-left-border pd-0\">{{ 'property.bed' | translate }}</ion-col>\n\t\t\t\t\t<ion-col size=\"4\" class=\"col-left-border pd-0\">{{ 'property.bath' | translate }}</ion-col>\n\t\t\t\t</ion-row>\n\t\t\t\t<ion-row class=\"font-s12\">\n\t\t\t\t\t<ion-col class=\"pd-0\" size=\"3\">{{ favorite.mls }}</ion-col>\n\t\t\t\t\t<ion-col size=\"3\" class=\"col-left-border pd-0\">{{ favorite.bedrooms }}</ion-col>\n\t\t\t\t\t<ion-col size=\"4\" class=\"col-left-border pd-0\">{{ favorite.washrooms }}</ion-col>\n\t\t\t\t</ion-row>\n\t\t\t</ion-footer>\n\t\t\t\n\t\t\t<ion-item-options>\n\t\t\t\t<ion-item-option color=\"danger\" (click)=\"removeFavorite(favorite.id)\">\n\t\t\t\t\t{{ 'watched.delete' | translate }}\n\t\t\t\t</ion-item-option>\n\t\t\t</ion-item-options>\n\t\t</ion-item-sliding>\n\t</ion-list>\n</ion-content>\n";
    /***/
  },

  /***/
  "./src/app/pages/favorites/favorites.module.ts":
  /*!*****************************************************!*\
    !*** ./src/app/pages/favorites/favorites.module.ts ***!
    \*****************************************************/

  /*! exports provided: FavoritesPageModule */

  /***/
  function srcAppPagesFavoritesFavoritesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FavoritesPageModule", function () {
      return FavoritesPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _favorites_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./favorites.page */
    "./src/app/pages/favorites/favorites.page.ts");
    /* harmony import */


    var _favorites_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./favorites.service */
    "./src/app/pages/favorites/favorites.service.ts");

    var routes = [{
      path: '',
      component: _favorites_page__WEBPACK_IMPORTED_MODULE_9__["FavoritesPage"]
    }];

    var FavoritesPageModule = function FavoritesPageModule() {
      _classCallCheck(this, FavoritesPageModule);
    };

    FavoritesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes), _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild()],
      declarations: [_favorites_page__WEBPACK_IMPORTED_MODULE_9__["FavoritesPage"]],
      providers: [_favorites_service__WEBPACK_IMPORTED_MODULE_10__["FavoritesService"]]
    })], FavoritesPageModule);
    /***/
  },

  /***/
  "./src/app/pages/favorites/favorites.page.scss":
  /*!*****************************************************!*\
    !*** ./src/app/pages/favorites/favorites.page.scss ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesFavoritesFavoritesPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host ion-content {\n  --background: linear-gradient(-135deg, #f6f6f6, var(--ion-color-pmary));\n}\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium);\n}\n:host ion-list {\n  background: #ccc;\n}\n:host ion-item-sliding {\n  border-bottom: 1px solid #ccc;\n  margin-bottom: 1%;\n  background: #fff;\n}\n:host .fl-rt {\n  float: right;\n}\n:host .tumbnail-view {\n  height: 100px;\n  width: 100px;\n}\n:host .tumbnail-view img {\n  border-radius: 5px;\n  height: 100px;\n  width: 100px;\n}\n:host .direction-ltr {\n  position: absolute;\n  right: 10px;\n  bottom: 5px;\n}\n:host .direction-rtl {\n  position: absolute;\n  left: 10px;\n  bottom: 5px;\n}\n:host .saleClass {\n  background: #1e88e5;\n  color: #fff;\n  font-weight: 500;\n}\n:host .soldClass {\n  background: #ff9e26;\n  color: #fff;\n  font-weight: 500;\n}\n:host .leaseClass {\n  background: #558b2f;\n  color: #fff;\n  font-weight: 500;\n}\n:host .priceText {\n  color: #d32f2f;\n}\n:host .bg-gray {\n  background: #e0e0e0;\n}\n:host .price-format {\n  font-size: 16px;\n  margin: 5px 0px;\n}\n:host .mg-bt {\n  margin-bottom: 1%;\n  box-shadow: 0 3px 4px rgba(var(--ion-color-dark-rgb), 0.24);\n}\n:host .footer-format {\n  margin: 0px 10px;\n}\n:host .font-s12 {\n  font-size: 12px;\n  color: #424242;\n}\n:host .pd-0 {\n  padding: 2px 5px 2px 5px !important;\n}\n:host .mt-0 {\n  margin-top: 0px;\n}\n:host .p-type {\n  display: inline-block;\n  justify-content: center;\n  align-items: center;\n  border: 1px solid #009cff;\n  padding: 1px 4px;\n  font-size: 15px;\n  background-color: #e1f3ff;\n  color: #009cff;\n  font-weight: 400;\n  margin-top: 0px;\n  margin-bottom: 0px;\n}\n:host .col-left-border {\n  border-left: 1px solid #d7d7d7;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvZmF2b3JpdGVzL2Zhdm9yaXRlcy5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2Zhdm9yaXRlcy9mYXZvcml0ZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksdUVBQUE7QUNBUjtBREdJO0VBQ0ksZ0JBQUE7RUFDQSxpREFBQTtBQ0RSO0FESUk7RUFDSSxnQkFBQTtBQ0ZSO0FES0k7RUFDSSw2QkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUNIUjtBRE1JO0VBQ0ksWUFBQTtBQ0pSO0FET0k7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQ0xSO0FET1E7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0FDTFo7QURTSTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUNQUjtBRFVJO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtBQ1JSO0FEV0k7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ1RSO0FEWUk7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ1ZSO0FEYUk7RUFDSSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtBQ1hSO0FEY0k7RUFDSSxjQUFBO0FDWlI7QURlSTtFQUNJLG1CQUFBO0FDYlI7QURnQkk7RUFDSSxlQUFBO0VBQ0EsZUFBQTtBQ2RSO0FEaUJJO0VBQ0ksaUJBQUE7RUFDQSwyREFBQTtBQ2ZSO0FEa0JJO0VBQ0ksZ0JBQUE7QUNoQlI7QURtQkk7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQ2pCUjtBRG9CSTtFQUNJLG1DQUFBO0FDbEJSO0FEcUJJO0VBQ0ksZUFBQTtBQ25CUjtBRHNCSTtFQUNJLHFCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUNwQlI7QUR1Qkk7RUFDSSw4QkFBQTtBQ3JCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Zhdm9yaXRlcy9mYXZvcml0ZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuICAgIGlvbi1jb250ZW50IHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgI2Y2ZjZmNiwgdmFyKC0taW9uLWNvbG9yLXBtYXJ5KSk7XG4gICAgfVxuXG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICBib3JkZXItYm90dG9tOiAxcHggZG90dGVkIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pO1xuICAgIH1cblxuICAgIGlvbi1saXN0IHtcbiAgICAgICAgYmFja2dyb3VuZDogI2NjYztcbiAgICB9XG5cbiAgICBpb24taXRlbS1zbGlkaW5nIHtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDElO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIH1cblxuICAgIC5mbC1ydCB7XG4gICAgICAgIGZsb2F0OiByaWdodDtcbiAgICB9XG5cbiAgICAudHVtYm5haWwtdmlldyB7XG4gICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgIHdpZHRoOiAxMDBweDtcbiAgICBcbiAgICAgICAgaW1nIHtcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICAgICAgICAgIGhlaWdodDogMTAwcHg7XG4gICAgICAgICAgICB3aWR0aDogMTAwcHg7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAuZGlyZWN0aW9uLWx0ciB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgcmlnaHQ6IDEwcHg7XG4gICAgICAgIGJvdHRvbTogNXB4O1xuICAgIH1cblxuICAgIC5kaXJlY3Rpb24tcnRsIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBsZWZ0OiAxMHB4O1xuICAgICAgICBib3R0b206IDVweDtcbiAgICB9XG5cbiAgICAuc2FsZUNsYXNzIHtcbiAgICAgICAgYmFja2dyb3VuZDogIzFlODhlNTtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgLnNvbGRDbGFzcyB7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZjllMjY7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgIH1cblxuICAgIC5sZWFzZUNsYXNzIHtcbiAgICAgICAgYmFja2dyb3VuZDogIzU1OGIyZjtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgfVxuXG4gICAgLnByaWNlVGV4dCB7XG4gICAgICAgIGNvbG9yOiAjZDMyZjJmO1xuICAgIH1cblxuICAgIC5iZy1ncmF5IHtcbiAgICAgICAgYmFja2dyb3VuZDogI2UwZTBlMDtcbiAgICB9XG4gICAgXG4gICAgLnByaWNlLWZvcm1hdCB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgbWFyZ2luOiA1cHggMHB4O1xuICAgIH1cblxuICAgIC5tZy1idCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDElO1xuICAgICAgICBib3gtc2hhZG93OiAwIDNweCA0cHggcmdiYSh2YXIoLS1pb24tY29sb3ItZGFyay1yZ2IpLCAwLjI0KTtcbiAgICB9XG4gICAgXG4gICAgLmZvb3Rlci1mb3JtYXQge1xuICAgICAgICBtYXJnaW46IDBweCAxMHB4O1xuICAgIH1cblxuICAgIC5mb250LXMxMiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICAgICAgY29sb3I6ICM0MjQyNDI7XG4gICAgfVxuICAgIFxuICAgIC5wZC0wIHtcbiAgICAgICAgcGFkZGluZzogMnB4IDVweCAycHggNXB4ICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgLm10LTAge1xuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgfVxuXG4gICAgLnAtdHlwZSB7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMDljZmY7XG4gICAgICAgIHBhZGRpbmc6IDFweCA0cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2UxZjNmZjtcbiAgICAgICAgY29sb3I6ICMwMDljZmY7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICAgIH1cblxuICAgIC5jb2wtbGVmdC1ib3JkZXIge1xuICAgICAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNkN2Q3ZDc7XG4gICAgfVxufSIsIjpob3N0IGlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgI2Y2ZjZmNiwgdmFyKC0taW9uLWNvbG9yLXBtYXJ5KSk7XG59XG46aG9zdCBpb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XG59XG46aG9zdCBpb24tbGlzdCB7XG4gIGJhY2tncm91bmQ6ICNjY2M7XG59XG46aG9zdCBpb24taXRlbS1zbGlkaW5nIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNjY2M7XG4gIG1hcmdpbi1ib3R0b206IDElO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuOmhvc3QgLmZsLXJ0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuOmhvc3QgLnR1bWJuYWlsLXZpZXcge1xuICBoZWlnaHQ6IDEwMHB4O1xuICB3aWR0aDogMTAwcHg7XG59XG46aG9zdCAudHVtYm5haWwtdmlldyBpbWcge1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGhlaWdodDogMTAwcHg7XG4gIHdpZHRoOiAxMDBweDtcbn1cbjpob3N0IC5kaXJlY3Rpb24tbHRyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTBweDtcbiAgYm90dG9tOiA1cHg7XG59XG46aG9zdCAuZGlyZWN0aW9uLXJ0bCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMTBweDtcbiAgYm90dG9tOiA1cHg7XG59XG46aG9zdCAuc2FsZUNsYXNzIHtcbiAgYmFja2dyb3VuZDogIzFlODhlNTtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG46aG9zdCAuc29sZENsYXNzIHtcbiAgYmFja2dyb3VuZDogI2ZmOWUyNjtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA1MDA7XG59XG46aG9zdCAubGVhc2VDbGFzcyB7XG4gIGJhY2tncm91bmQ6ICM1NThiMmY7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXdlaWdodDogNTAwO1xufVxuOmhvc3QgLnByaWNlVGV4dCB7XG4gIGNvbG9yOiAjZDMyZjJmO1xufVxuOmhvc3QgLmJnLWdyYXkge1xuICBiYWNrZ3JvdW5kOiAjZTBlMGUwO1xufVxuOmhvc3QgLnByaWNlLWZvcm1hdCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luOiA1cHggMHB4O1xufVxuOmhvc3QgLm1nLWJ0IHtcbiAgbWFyZ2luLWJvdHRvbTogMSU7XG4gIGJveC1zaGFkb3c6IDAgM3B4IDRweCByZ2JhKHZhcigtLWlvbi1jb2xvci1kYXJrLXJnYiksIDAuMjQpO1xufVxuOmhvc3QgLmZvb3Rlci1mb3JtYXQge1xuICBtYXJnaW46IDBweCAxMHB4O1xufVxuOmhvc3QgLmZvbnQtczEyIHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogIzQyNDI0Mjtcbn1cbjpob3N0IC5wZC0wIHtcbiAgcGFkZGluZzogMnB4IDVweCAycHggNXB4ICFpbXBvcnRhbnQ7XG59XG46aG9zdCAubXQtMCB7XG4gIG1hcmdpbi10b3A6IDBweDtcbn1cbjpob3N0IC5wLXR5cGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDA5Y2ZmO1xuICBwYWRkaW5nOiAxcHggNHB4O1xuICBmb250LXNpemU6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNlMWYzZmY7XG4gIGNvbG9yOiAjMDA5Y2ZmO1xuICBmb250LXdlaWdodDogNDAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbn1cbjpob3N0IC5jb2wtbGVmdC1ib3JkZXIge1xuICBib3JkZXItbGVmdDogMXB4IHNvbGlkICNkN2Q3ZDc7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/favorites/favorites.page.ts":
  /*!***************************************************!*\
    !*** ./src/app/pages/favorites/favorites.page.ts ***!
    \***************************************************/

  /*! exports provided: FavoritesPage */

  /***/
  function srcAppPagesFavoritesFavoritesPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FavoritesPage", function () {
      return FavoritesPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var _favorites_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./favorites.service */
    "./src/app/pages/favorites/favorites.service.ts");

    var FavoritesPage = /*#__PURE__*/function () {
      function FavoritesPage(navCtrl, loadingCtrl, toastCtrl, favoriteSerivce, translate) {
        _classCallCheck(this, FavoritesPage);

        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.favoriteSerivce = favoriteSerivce;
        this.translate = translate;
        this.userId = null;
        this.loginFlag = false;
        this.isArabic = false;
        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));
        var language = localStorage.getItem('language');

        if (language != null && language != undefined) {
          if (language == 'ar') {
            this.isArabic = !this.isArabic;
          }
        }

        if (loginDetails != undefined) {
          this.userId = loginDetails.data.userVM.id;
        } else {
          this.loginFlag = true;
        }
      }

      _createClass(FavoritesPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          if (this.userId != null) {
            this.getFavoriteProperties(this.userId);
          }
        }
      }, {
        key: "getFavoriteProperties",
        value: function getFavoriteProperties(userId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this.translate.get('app.pleaseWait').subscribe(function (value) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                _context.next = 2;
                                return this.loadingCtrl.create({
                                  message: value
                                });

                              case 2:
                                loading = _context.sent;
                                _context.next = 5;
                                return loading.present();

                              case 5:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee, this);
                      }));
                    });
                    this.favoriteSerivce.getAllFavoriteProperties(userId).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                          while (1) {
                            switch (_context2.prev = _context2.next) {
                              case 0:
                                _context2.next = 2;
                                return loading.dismiss();

                              case 2:
                                this.favorites = res.data;

                              case 3:
                              case "end":
                                return _context2.stop();
                            }
                          }
                        }, _callee2, this);
                      }));
                    }, function (err) {
                      loading.dismiss();

                      if (err.status == 401) {
                        _this.presentToast("Session Timeout");

                        localStorage.removeItem('userDetails');
                        localStorage.removeItem('modalShown');

                        _this.navCtrl.navigateRoot('/login');

                        setTimeout(function () {
                          window.location.reload();
                        }, 300);
                      }
                    });

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "itemTapped",
        value: function itemTapped(favorite) {
          this.navCtrl.navigateForward('property-detail/' + favorite.id);
        }
      }, {
        key: "removeFavorite",
        value: function removeFavorite(favoriteId) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
            var _this2 = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee6$(_context6) {
              while (1) {
                switch (_context6.prev = _context6.next) {
                  case 0:
                    this.translate.get('app.pleaseWait').subscribe(function (value) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                          while (1) {
                            switch (_context4.prev = _context4.next) {
                              case 0:
                                _context4.next = 2;
                                return this.loadingCtrl.create({
                                  message: value
                                });

                              case 2:
                                loading = _context4.sent;
                                _context4.next = 5;
                                return loading.present();

                              case 5:
                              case "end":
                                return _context4.stop();
                            }
                          }
                        }, _callee4, this);
                      }));
                    });
                    this.favoriteSerivce.removeFavoruteProperties(this.userId, favoriteId).subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                          while (1) {
                            switch (_context5.prev = _context5.next) {
                              case 0:
                                if (!(res.code == 200)) {
                                  _context5.next = 5;
                                  break;
                                }

                                _context5.next = 3;
                                return loading.dismiss();

                              case 3:
                                this.presentToast('Property Removed from Favourites');
                                this.getFavoriteProperties(this.userId);

                              case 5:
                              case "end":
                                return _context5.stop();
                            }
                          }
                        }, _callee5, this);
                      }));
                    }, function (err) {
                      loading.dismiss();
                      console.log(err);
                    });

                  case 2:
                  case "end":
                    return _context6.stop();
                }
              }
            }, _callee6, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
            var toast;
            return regeneratorRuntime.wrap(function _callee7$(_context7) {
              while (1) {
                switch (_context7.prev = _context7.next) {
                  case 0:
                    _context7.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context7.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context7.stop();
                }
              }
            }, _callee7, this);
          }));
        }
      }]);

      return FavoritesPage;
    }();

    FavoritesPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
      }, {
        type: _favorites_service__WEBPACK_IMPORTED_MODULE_5__["FavoritesService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('slidingList', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonItemSliding"])], FavoritesPage.prototype, "slidingList", void 0);
    FavoritesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-favorites',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./favorites.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/favorites/favorites.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({
        opacity: 0,
        transform: "translate3d(0,10px,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('600ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./favorites.page.scss */
      "./src/app/pages/favorites/favorites.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _favorites_service__WEBPACK_IMPORTED_MODULE_5__["FavoritesService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"]])], FavoritesPage);
    /***/
  },

  /***/
  "./src/app/pages/favorites/favorites.service.ts":
  /*!******************************************************!*\
    !*** ./src/app/pages/favorites/favorites.service.ts ***!
    \******************************************************/

  /*! exports provided: FavoritesService */

  /***/
  function srcAppPagesFavoritesFavoritesServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FavoritesService", function () {
      return FavoritesService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../api.service */
    "./src/app/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var FavoritesService = /*#__PURE__*/function () {
      function FavoritesService(http, apiService) {
        _classCallCheck(this, FavoritesService);

        this.http = http;
        this.apiService = apiService;
        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));

        if (loginDetails != undefined) {
          this.token = loginDetails.data.token;
        }

        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
          headers: headers
        });
      }

      _createClass(FavoritesService, [{
        key: "getAllFavoriteProperties",
        value: function getAllFavoriteProperties(userId) {
          return this.http.get(this.apiService.config + 'favorite/user/' + userId, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }, {
        key: "removeFavoruteProperties",
        value: function removeFavoruteProperties(userId, favoriteId) {
          return this.http["delete"](this.apiService.config + 'favorite/' + userId + '/' + favoriteId, this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }]);

      return FavoritesService;
    }();

    FavoritesService.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }, {
        type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]
      }];
    };

    FavoritesService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])], FavoritesService);
    /***/
  }
}]);
//# sourceMappingURL=pages-favorites-favorites-module-es5.js.map