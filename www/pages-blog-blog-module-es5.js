function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-blog-blog-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/blog/blog.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/blog/blog.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesBlogBlogPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n\t<ion-toolbar color=\"pmary\">\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button text=\"\" color=\"dark\"></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>\n\t\t\t<ion-text color=\"dark\" class=\"fw400\">{{ 'app.blogs' | translate }}</ion-text>\n\t\t</ion-title>\n\t</ion-toolbar>\n</ion-header>\n<ion-content>\n    <div class=\"ion-no-margin\" [@staggerIn]=\"blogs\">\n        <div  *ngFor=\"let blog of blogs\">\n            <ion-card class=\"bg-white\" (click)=\"blogInfo(blog)\">\n                <div *ngIf=\"blog.blogImage\">\n                    <img src=\"{{imageUrl}}{{blog.blogImage}}\">\n                </div>\n                <div class=\"ion-padding\">\n                    <h4 class=\"blog-title\">{{ blog.title }}</h4>\n                    <span color=\"dark\" style=\"padding-bottom: 10px; display: flex;\">By: {{ blog.createdBy }}</span>\n                    <span color=\"dark\">Date: {{ blog.createdDate | date }}</span>\n                </div>\n            </ion-card>\n        </div>\n    </div>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/pages/blog/blog.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/pages/blog/blog.module.ts ***!
    \*******************************************/

  /*! exports provided: BlogPageModule */

  /***/
  function srcAppPagesBlogBlogModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlogPageModule", function () {
      return BlogPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _blog_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./blog.page */
    "./src/app/pages/blog/blog.page.ts");
    /* harmony import */


    var _blog_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./blog.service */
    "./src/app/pages/blog/blog.service.ts");

    var routes = [{
      path: '',
      component: _blog_page__WEBPACK_IMPORTED_MODULE_10__["BlogPage"]
    }];

    var BlogPageModule = function BlogPageModule() {
      _classCallCheck(this, BlogPageModule);
    };

    BlogPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatExpansionModule"], _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatIconModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_8__["IonicModule"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"].forChild(), _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)],
      declarations: [_blog_page__WEBPACK_IMPORTED_MODULE_10__["BlogPage"]],
      providers: [_blog_service__WEBPACK_IMPORTED_MODULE_11__["BlogService"]]
    })], BlogPageModule);
    /***/
  },

  /***/
  "./src/app/pages/blog/blog.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/pages/blog/blog.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesBlogBlogPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".blog-title {\n  white-space: initial;\n  color: #000;\n  font-weight: 100;\n  margin-top: 0px;\n  margin-bottom: 5px;\n}\n\n.p-blog {\n  margin: 0 0 10px;\n  font-weight: 100 !important;\n  line-height: 1.7em;\n  font-size: 15px;\n  color: #3a3a3a;\n}\n\nion-card {\n  margin: 0px 2px 5px 2px;\n}\n\nimg::before {\n  content: \"\";\n  width: 100%;\n  position: absolute;\n  height: 100%;\n  top: 0;\n  left: 0;\n  background: #222;\n  opacity: 0.6;\n}\n\n.centered {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haml0cGF3YXIvRG9jdW1lbnRzL3dvcmtzcGFjZS9wcm9qZWN0L1Byb3BlcnR5QXBwL3NyYy9hcHAvcGFnZXMvYmxvZy9ibG9nLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYmxvZy9ibG9nLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxnQkFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQ0NKOztBREVBO0VBQ0ksdUJBQUE7QUNDSjs7QURFQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYmxvZy9ibG9nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibG9nLXRpdGxlIHtcbiAgICB3aGl0ZS1zcGFjZTogaW5pdGlhbDtcbiAgICBjb2xvcjogIzAwMDtcbiAgICBmb250LXdlaWdodDogMTAwO1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5wLWJsb2cge1xuICAgIG1hcmdpbjogMCAwIDEwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDEwMCAhaW1wb3J0YW50O1xuICAgIGxpbmUtaGVpZ2h0OiAxLjdlbTtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgY29sb3I6ICMzYTNhM2E7XG59XG5cbmlvbi1jYXJkIHtcbiAgICBtYXJnaW46IDBweCAycHggNXB4IDJweDtcbn1cblxuaW1nOjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgYmFja2dyb3VuZDogIzIyMjtcbiAgICBvcGFjaXR5OiAuNjtcbn1cblxuLmNlbnRlcmVkIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiA1MCU7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xufSIsIi5ibG9nLXRpdGxlIHtcbiAgd2hpdGUtc3BhY2U6IGluaXRpYWw7XG4gIGNvbG9yOiAjMDAwO1xuICBmb250LXdlaWdodDogMTAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuLnAtYmxvZyB7XG4gIG1hcmdpbjogMCAwIDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiAxMDAgIWltcG9ydGFudDtcbiAgbGluZS1oZWlnaHQ6IDEuN2VtO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGNvbG9yOiAjM2EzYTNhO1xufVxuXG5pb24tY2FyZCB7XG4gIG1hcmdpbjogMHB4IDJweCA1cHggMnB4O1xufVxuXG5pbWc6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMTAwJTtcbiAgdG9wOiAwO1xuICBsZWZ0OiAwO1xuICBiYWNrZ3JvdW5kOiAjMjIyO1xuICBvcGFjaXR5OiAwLjY7XG59XG5cbi5jZW50ZXJlZCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/blog/blog.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/pages/blog/blog.page.ts ***!
    \*****************************************/

  /*! exports provided: BlogPage */

  /***/
  function srcAppPagesBlogBlogPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlogPage", function () {
      return BlogPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/animations */
    "./node_modules/@angular/animations/fesm2015/animations.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/dist/fesm5.js");
    /* harmony import */


    var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ngx-translate/core */
    "./node_modules/@ngx-translate/core/fesm2015/ngx-translate-core.js");
    /* harmony import */


    var _modal_blog_info_blog_info_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../modal/blog-info/blog-info.page */
    "./src/app/pages/modal/blog-info/blog-info.page.ts");
    /* harmony import */


    var _blog_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./blog.service */
    "./src/app/pages/blog/blog.service.ts");
    /* harmony import */


    var src_app_api_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/api.service */
    "./src/app/api.service.ts");

    var BlogPage = /*#__PURE__*/function () {
      function BlogPage(modalCtrl, navCtrl, toastCtrl, loadingCtrl, blogService, apiService, translateService) {
        _classCallCheck(this, BlogPage);

        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.blogService = blogService;
        this.apiService = apiService;
        this.translateService = translateService;
        this.blogs = [];
        this.loginRequired = false;
        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));

        if (loginDetails == undefined) {
          this.loginRequired = true;
        }

        this.imageUrl = this.apiService.config + 'blog/download-blogImage/';
      }

      _createClass(BlogPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.getAllBlogs();
        }
      }, {
        key: "getAllBlogs",
        value: function getAllBlogs() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this = this;

            var loading;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    this.translateService.get('loading').subscribe(function (value) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                        return regeneratorRuntime.wrap(function _callee$(_context) {
                          while (1) {
                            switch (_context.prev = _context.next) {
                              case 0:
                                _context.next = 2;
                                return this.loadingCtrl.create({
                                  message: value
                                });

                              case 2:
                                loading = _context.sent;
                                _context.next = 5;
                                return loading.present();

                              case 5:
                              case "end":
                                return _context.stop();
                            }
                          }
                        }, _callee, this);
                      }));
                    });
                    this.blogService.getAllBlogs().subscribe(function (res) {
                      return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                          while (1) {
                            switch (_context2.prev = _context2.next) {
                              case 0:
                                _context2.next = 2;
                                return loading.dismiss();

                              case 2:
                                this.blogs = res.data;

                              case 3:
                              case "end":
                                return _context2.stop();
                            }
                          }
                        }, _callee2, this);
                      }));
                    }, function (err) {
                      loading.dismiss();

                      if (err.status == 401) {
                        _this.presentToast("Session Timeout");

                        localStorage.removeItem('userDetails');
                        localStorage.removeItem('modalShown');

                        _this.navCtrl.navigateRoot('/login');

                        setTimeout(function () {
                          window.location.reload();
                        }, 300);
                      }
                    });

                  case 2:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "presentToast",
        value: function presentToast(message) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
            var toast;
            return regeneratorRuntime.wrap(function _callee4$(_context4) {
              while (1) {
                switch (_context4.prev = _context4.next) {
                  case 0:
                    _context4.next = 2;
                    return this.toastCtrl.create({
                      showCloseButton: true,
                      message: message,
                      duration: 2000,
                      position: 'bottom'
                    });

                  case 2:
                    toast = _context4.sent;
                    toast.present();

                  case 4:
                  case "end":
                    return _context4.stop();
                }
              }
            }, _callee4, this);
          }));
        }
      }, {
        key: "blogInfo",
        value: function blogInfo(blog) {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
            var modal;
            return regeneratorRuntime.wrap(function _callee5$(_context5) {
              while (1) {
                switch (_context5.prev = _context5.next) {
                  case 0:
                    _context5.next = 2;
                    return this.modalCtrl.create({
                      component: _modal_blog_info_blog_info_page__WEBPACK_IMPORTED_MODULE_5__["BlogInfoPage"],
                      componentProps: {
                        blogDetail: blog
                      }
                    });

                  case 2:
                    modal = _context5.sent;
                    _context5.next = 5;
                    return modal.present();

                  case 5:
                    return _context5.abrupt("return", _context5.sent);

                  case 6:
                  case "end":
                    return _context5.stop();
                }
              }
            }, _callee5, this);
          }));
        }
      }]);

      return BlogPage;
    }();

    BlogPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"]
      }, {
        type: _blog_service__WEBPACK_IMPORTED_MODULE_6__["BlogService"]
      }, {
        type: src_app_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiConfigService"]
      }, {
        type: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]
      }];
    };

    BlogPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-blog',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./blog.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/blog/blog.page.html"))["default"],
      animations: [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('staggerIn', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({
        opacity: 0,
        transform: "translate3d(0,10px,0)"
      }), {
        optional: true
      }), Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':enter', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["stagger"])('300ms', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('600ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({
        opacity: 1,
        transform: "translate3d(0,0,0)"
      }))]), {
        optional: true
      })])])],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./blog.page.scss */
      "./src/app/pages/blog/blog.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"], _blog_service__WEBPACK_IMPORTED_MODULE_6__["BlogService"], src_app_api_service__WEBPACK_IMPORTED_MODULE_7__["ApiConfigService"], _ngx_translate_core__WEBPACK_IMPORTED_MODULE_4__["TranslateService"]])], BlogPage);
    /***/
  },

  /***/
  "./src/app/pages/blog/blog.service.ts":
  /*!********************************************!*\
    !*** ./src/app/pages/blog/blog.service.ts ***!
    \********************************************/

  /*! exports provided: BlogService */

  /***/
  function srcAppPagesBlogBlogServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "BlogService", function () {
      return BlogService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/http */
    "./node_modules/@angular/http/fesm2015/http.js");
    /* harmony import */


    var _api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../api.service */
    "./src/app/api.service.ts");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var BlogService = /*#__PURE__*/function () {
      function BlogService(http, apiService) {
        _classCallCheck(this, BlogService);

        this.http = http;
        this.apiService = apiService;
        var loginDetails = JSON.parse(localStorage.getItem('userDetails'));

        if (loginDetails != undefined) {
          this.token = loginDetails.data.token;
        }

        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]();
        headers.append('Content-Type', 'application/json');
        headers.append('Access-Control-Allow-Origin', '*');
        headers.append('Authorization', 'Bearer ' + this.token);
        this.options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({
          headers: headers
        });
      }

      _createClass(BlogService, [{
        key: "getAllBlogs",
        value: function getAllBlogs() {
          return this.http.get(this.apiService.config + 'blog/', this.options).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function (response) {
            return response.json();
          }));
        }
      }]);

      return BlogService;
    }();

    BlogService.ctorParameters = function () {
      return [{
        type: _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]
      }, {
        type: _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]
      }];
    };

    BlogService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"], _api_service__WEBPACK_IMPORTED_MODULE_3__["ApiConfigService"]])], BlogService);
    /***/
  }
}]);
//# sourceMappingURL=pages-blog-blog-module-es5.js.map